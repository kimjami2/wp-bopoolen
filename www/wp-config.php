<?php
/**
 * Baskonfiguration för WordPress.
 *
 * Denna fil används av wp-config.php-genereringsskript under installationen.
 * Du behöver inte använda webbplatsen, du kan kopiera denna fil direkt till
 * "wp-config.php" och fylla i värdena.
 *
 * Denna fil innehåller följande konfigurationer:
 *
 * * Inställningar för MySQL
 * * Säkerhetsnycklar
 * * Tabellprefix för databas
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
define( 'WP_DEBUG', false );
 // define('WP_DEBUG_LOG', true);
 // define('WP_DEBUG_DISPLAY', false);
 // @ini_set('display_errors',0);
define('WP_MEMORY_LIMIT', '256M');
set_time_limit(180);

// change post auto delete days
define( 'EMPTY_TRASH_DAYS', 9999999 );


define('WP_ENV', 'development');

// ** MySQL-inställningar - MySQL-uppgifter får du från ditt webbhotell ** //
/** Namnet på databasen du vill använda för WordPress */
define('DB_NAME', 'bopoolen.se');

/** MySQL-databasens användarnamn */
define('DB_USER', 'root');

/** MySQL-databasens lösenord */
define('DB_PASSWORD', 'root');

/** MySQL-server */
define('DB_HOST', 'localhost');

/** Teckenkodning för tabellerna i databasen. */
define('DB_CHARSET', 'utf8mb4');

/** Kollationeringstyp för databasen. Ändra inte om du är osäker. */
define('DB_COLLATE', '');

/**#@+
 * Unika autentiseringsnycklar och salter.
 *
 * Ändra dessa till unika fraser!
 * Du kan generera nycklar med {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Du kan när som helst ändra dessa nycklar för att göra aktiva cookies obrukbara, vilket tvingar alla användare att logga in på nytt.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/iacAj+GrxMs{_M`<07fY4#~>@QJ&B6,ez0199oCsMT81F3%[6S~v]Qg|P_Iuxon');
define('SECURE_AUTH_KEY',  '!+8N-=My,mTq}0WfRmW^}Pbl9p~2$2t9+}.2*G5*;s=G:P0i>2Oag|m1BH^U+S[l');
define('LOGGED_IN_KEY',    'q^h&eD,ip|(pn)_~lcCOLiap6,eJ]c0-YL3>YVm8ypmF28bez|rjf5cbIvn?!;xq');
define('NONCE_KEY',        'nmn=w^a`NT,BtxdGeWoOv>8K2{xo)]StB||YF}0c|mze$>8B,M$0_]VTPi L|O.s');
define('AUTH_SALT',        '#3|WToI}@MQDi:q{mLKGRP-V)&#A:j<q8OR Xa]-[qG7-(qzFu1V#n0hlkl@mcqX');
define('SECURE_AUTH_SALT', 'R.-F[E~.Upp6m+<B+m+iSVXPohZMnvn}|#Ghsl;u^~&z_,|VFF(FNTb]YQH:O7<,');
define('LOGGED_IN_SALT',   'P6ZdK2cYl<IZ8*|+0F~91}m+3vWJ-lbtRs)Oye4h[M/Z7}ZPHMfV%JK.BXR&dVSV');
define('NONCE_SALT',       '|0&6TD]7nO!][exk5MicPSP vMSaIc/;JL*Ebe9vF3%%SM4c0EzHGX(qLd wP8og');

/**#@-*/

/**
 * Tabellprefix för WordPress Databasen.
 *
 * Du kan ha flera installationer i samma databas om du ger varje installation ett unikt
 * prefix. Endast siffror, bokstäver och understreck!
 */
$table_prefix  = 'wp_';

/**
 * För utvecklare: WordPress felsökningsläge.
 *
 * Ändra detta till true för att aktivera meddelanden under utveckling.
 * Det är rekommderat att man som tilläggsskapare och temaskapare använder WP_DEBUG
 * i sin utvecklingsmiljö.
 *
 * För information om andra konstanter som kan användas för felsökning,
 * se dokumentationen.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Det var allt, sluta redigera här! Blogga på. */

/** Absoluta sökväg till WordPress-katalogen. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Anger WordPress-värden och inkluderade filer. */
require_once(ABSPATH . 'wp-settings.php');

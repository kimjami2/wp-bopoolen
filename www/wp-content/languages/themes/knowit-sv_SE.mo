��    �      �      �	      �	     �	     �	     �	     �	  `   �	  
   Q
     \
     d
     m
     |
  	   �
     �
     �
  	   �
  	   �
     �
     �
     �
     �
     �
  	   �
                    (     ;  	   J     T  
   f     q     �     �     �  #   �     �     �     
               )  !   @     b     s     �     �     �     �     �     �     �  
   	  
             8     A     F  	   L  =   V     �  E   �  _   �     @     Z     _     h  	   t     ~     �     �     �     �     �     �     �     �     �  
   �     �  
          
        #     (     6     F     Y     b     g     t     �     �  =   �     �     �     �                    '     8     I     X     g     p     w  �                       +     3     @     P     b     w     |     �     �     �     �     �  
   �     �     �     �     �  &        <     P     d  G   j  :   �     �     �     �               (  	   :  *   D     o     {  $   �     �  L   �  J     
   g  '   r     �  
   �  �  �     G     Y     u     �  c   �     �            	     
   )  	   4     >  	   F     P     ]     f     s     w  %   |     �     �     �     �  
   �     �     �             
   !     ,     F     a     v     �     �     �     �     �     �     �  #   
     .     E     Z     p     �     �     �     �     �     �     �     �                 	   &  5   0     f  P   j  ^   �          /     4     =  	   D     N     c     p  	   x     �     �     �     �     �  
   �     �     �  
   �     �  
   �            	        !  	   8     B     J     X     g     p  8        �  
   �     �  
   �     �     �                    +  
   7     B     K  �   T     �     �     �  	   �     �           !      9      P      T   	   d      n      �      �      �   	   �      �      �      �      �   %   �      �      
!     !  >   $!  D   c!     �!     �!  
   �!     �!     �!     �!     �!  <   �!     <"     J"  &   W"  #   ~"  R   �"  L   �"     B#  (   S#     |#     �#   - Choose agreement type - Choose leasing period - Choose number of rooms - Choose types of housing A reasonable rent for students is therefore 2476 kr (10 month rents) with a max rent of 3500 kr. Accessible Address Address: Agreement type Agreement type: Apartment Area Area in Kvm Available Biography Blacklisted: City City: Click here to upload images Comments Comments: Commmon area: Common area Complaints: Contact the lessor Create account Create ad Create seeking ad Dishwasher Display email in ads Display my name in ads: Display name in ads Display phone number in ads Don’t have an account? Create one Edit profil Electricity Email English Enter a username Enter your description Enter your description in english Enter your email Enter your first name Enter your last name Enter your password Enter your title Enter your title in english Exchange FIND STUDENT HOUSING Find tenant First name Fixed term Forgotten your password? Freetext From From: Furnished Hereby I attest that the rent, according to me, is reasonable House I approve <a href="/anvandarvillkor" target="_blank">terms of use</a> If you choose not to display your name in the ad, your user name will then be displayed instead Included in the apartment Info Internet Kitchenette Last name Lease contract Leasing period Living Living area Login Lund Lund nearby Member since Member since  Messages Month rent Month rent in Kr My Profile My ads My profile Name Not signed in Number of rooms Numeric value only Password Pets Phone number Phone number: Phone: Phonenumber: Please attest that according to you this is a reasonable rent Please fill in all fields! Post number Post number: Postal code Price Private area Private bathroom Private entrance Private toilet Profile image: Property Rating Rating: Reminder: The student financial aid is at 9904 kr and according to Boverkets recommendation (?) the housing cost should be 25% of ones income. Rent Rent: Rental agreement: Renter? Repeat email Repeat password Repeat your email Repeat your password Room Room in apartment Room in house Room in student housing Room: Save Search See profil Send Smoking allowed Social security number Social security number: Something went wrong, please try again Sublease Agreements Sublease agreements Terms The advertiser has confirmed to charge reasonable rent for the dwelling The username or password is not correct, please try again! To To: Types of housing Until further notice Upload images User Information: User name Username already exists, please try again! Verified ID Washing machine You have to approve the terms of use You haven’t created any ads. Your account has been blocked. Please contact bopoolen for more information. Your account has been created, please login! <a href="/logga-in">Login</a> Your email ads made by students that seeks housing here yyyy-mm-dd Project-Id-Version: Bopoolen Wordpress Theme
Report-Msgid-Bugs-To: 
POT-Creation-Date: Fri May 27 2016 17:01:00 GMT+0200 (CEST)
PO-Revision-Date: Sat May 28 2016 11:20:14 GMT+0200 (CEST)
Last-Translator: bopoolen <admin@bopoolen.se>
Language-Team: 
Language: Swedish
Plural-Forms: nplurals=2; plural=n != 1
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: ../../themes/bopoolen
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Loco-Target-Locale: sv_SE
X-Generator: Loco - https://localise.biz/ - Välj avtalstyp - Välj tillgänglighetstyp - Välj antal rum - Välj boendeform En rimlig hyresnivå för studenter är därför 2476 kr (10 månaders hyra)med maxtak på 3500 kr. Handikappvänligt Adress Adress: Avtalstyp Avtalstyp: Lägenhet Område Yta i Kvm Tillgänglig Biografi Svartlistad: Ort Ort: Klicka här för att ladda upp bilder Kommentarer Kommentarer: Gemensam yta: Gemensam yta Klagomål: Kontakta uthyraren Skapa konto Skapa annons Skapa sökes annons Diskmaskin Visa epostadress i annons Visa mitt namn i annonser: Visa namn i annonser Visa telefonnummer i annonser Har du inget konto? Skapa ett Redigera profil El Epostadress Engelska Fyll i ett användarnamn Fyll i din beskrivning Fyll i din beskrivning på engelska Fyll i din epostadress Fyll i ditt förnamn Fyll i ditt efternamn Fyll i ditt lösenord Skriv din titel Skriv din titel på engelska Byte HITTA STUDENTBOSTAD Hitta hyresgäst Förnamn Tidsbestämt Har du glömt ditt lösenord? Fritext Från Från: Möblerat Här med intygar jag att hyran enligt mig är skälig Hus Jag godkänner <a href="/anvandarvillkor" target="_blank">användarvillkoren</a> Om du väljer att inte visa ditt namn i annonsen kommer ditt användarnamn att visas istället Ingår i lägenheten Info Internet Pentry Efternamn Förstahandskontrakt Kontraktstid Boendet Boendeyta Logga in Lund Lund med omnejd Medlem sedan Medlem sedan Meddelande Månadshyra Månadshyra i Kr Min Profil Mina annonser Min profil Namn Ej inloggad Antal rum Använd endast siffror Lösenord Husdjur Telefonnummer Telefonnummer: Telefon: Telefonnummer: Var god intynga att detta enligt dig är en skälig hyra Vänligen fyll i alla fält! Postnummer Postnummer: Postnummer Pris Egen yta Eget badrum Egen ingång Egen toalett Profilbild: Bostadstyp Omdömen Omdömen Påminnelse/tänk på: studiemedlet är idag på 9904 kr, och Boverkets rekommendation (?) att boendekostnad ska vara 25% av inkomst. Hyra Hyra: Hyresavtal: Uthyrare? Upprepa epostadress Upprepa lösenord Upprepa din epostadress Upprepa ditt lösenord Rum Rum i lägenhet Rum i hus Rum i studentboende Rum: Spara Sök Se profil Skicka Rökning tillåten Personnummer Personnummer: Något gick fel, vänligen prova igen Andrahandskontrakt Andrahandskontrakt Vilkor Annonsören har godkännt att ta ut skälig hyra för bostaden Användarnamn eller lösenordet stämmer inte, vänligen prova igen! Till Till: Boendeform Tillsvidare Ladda upp bilder Användarinformation: Användarnamn Användarnamn finns redan registrerat, vänligen prova igen! Verifierad ID Tvättmaskin Du måste godkänna användarvillkoren Du har inte skapat några annonser. Ditt konto har blivit blockerat. Vänligen kontakta bopoolen för mer information. Ditt konto har skapats, vänligen logga in! <a href="/logga-in">Logga in</a> Din e-postadress annonser från bostadssökande studenter Här åååå-mm-dd 
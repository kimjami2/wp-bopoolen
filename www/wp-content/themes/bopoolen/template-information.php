<?php
/**
 * Template Name: Information landing
 */
?>

<?php
  $_SESSION['site-section'] = 'information';
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/information/information', 'start'); ?>
<?php endwhile; ?>

<?php
/**
 * Template Name: Om oss landing
 */
?>

<?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/information/information', 'about'); ?>
<?php endwhile; ?>
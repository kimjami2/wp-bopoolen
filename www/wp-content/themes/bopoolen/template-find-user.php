<?php
/**
 * Template Name: Find users
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/profile/find', 'user'); ?>
<?php endwhile; ?>

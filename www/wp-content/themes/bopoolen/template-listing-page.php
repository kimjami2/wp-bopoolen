<?php
/**
* Template Name: Listing page - Map style
*/
?>

<?php while (have_posts()) : the_post(); ?>

  <div class="container-fluid listing-page no-padding">
    <!-- <div class="row"> -->
      <div class="col-md-12">
        <?php get_template_part('templates/listing/search', 'map'); ?>
      </div>
      <!-- <div class="row"> -->
        <div class="container filters">
          <div class="col-md-12">
            <?php get_template_part('templates/listing/search', 'form'); ?>
          </div>
        </div>
      <!-- </div> -->
      <!-- <div class="row listings"> -->
        <div class="container-fluid listings">
          <div class="col-md-10 col-md-push-1">
            <?php get_template_part('templates/listing/search', 'listing'); ?>
          </div>
        </div>
      <!-- </div> -->
    <!-- </div> -->
  </div>

<?php endwhile; ?>

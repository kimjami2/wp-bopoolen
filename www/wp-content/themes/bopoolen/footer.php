	
	<a href="http://www.hyresgastforeningen.se/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/hsb-annons.jpg" id="banner" /></a>
	
	<div id="lus-banner">
		<a href="http://www.lus.lu.se/">
			<img src="<?php bloginfo('template_url'); ?>/img/lus.png" />
			<p><?php _e('BoPoolen.nu is a service from Lund University Student Unions');?></p>
		</a>
	</div>
	
	<div class="clear"></div>

	<?php wp_nav_menu(array(
		'theme_location'  => 'footer_menu',
		'container'       => '',
		'items_wrap'      => '<ul id="footmenu">%3$s</ul>',
		'fallback_cb'     => '',
		'depth'           => 1
	)); ?>

	<div class="clear"></div>	
		<div class="end-bar"></div>
		</div> <!-- slut inner-wrapper-->
		<div class="clear">&nbsp;</div>
		<div class="end-bar"></div>
		</div> <!-- slut outer-wrapper-->

		<footer id="sidfot">
			<div id="foot-wrapper">
				<p class="left"><?php _e('Contact');?>: 070 - 456 11 87 – <a href="mailto:bopoolen@lus.lu.se">bopoolen@lus.lu.se</a></p>
				<p class="right"><?php _e('With support from');?>: <a href="http://www.lu.se/"><img src="<?php bloginfo('template_url') ?>/img/lu.png" /></a> <a href="http://www.lund.se/"><img src="<?php bloginfo('template_url') ?>/img/lund.png" /></a></p>
				<p class="cred"><?php _e('Proudly powered by <a href="http://sv.wordpress.org/">Wordpress</a> <!--with theme design by <a href="http://salmiakmedia.se/">Salmiak Media</a> and ad system developed by <a href="http://lindblad.info/">Lindblad IT</a>.');?>--></p>
			</div>
		</footer><!-- slut sidfot-->

	<script type="text/javascript">
		var msg_pending = '<?php _e('Sending message');?>…';
		var msg_sent = '<?php _e('Your message is sent');?>';
	</script>
	<script src="<?php bloginfo('template_url'); ?>/js/jquery.prettyPhoto.js"  type="text/javascript"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/bp.standard.js"  type="text/javascript"></script>
	
	
	<?php wp_footer(); ?> 	

</body>
</html>
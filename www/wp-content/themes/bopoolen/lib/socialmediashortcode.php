<?php

function social_media( $atts, $content = null) {
  extract(shortcode_atts(array(
      'instagram' => '',
      'facebook' => '',
      'twitter' => '',
    ), $atts));

  return '<a href="' .$facebook. '" target="_blank" class="social"><i class="fa fa-facebook"></i></a>';
}
add_shortcode( 'social_media', 'social_media' );

/*
 * [social_media instagram="http://instagram.com" facebook="http://facebook.com" twitter="http://twitter.com"]
 * http://www.facebook.com/sharer/sharer.php?u={URL}
 * http://twitter.com/share
 *
 */

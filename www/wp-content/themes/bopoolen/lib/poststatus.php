<?php
class Toscho_Retrans {

  protected $params;

  public function __construct(array $options) {
      $defaults = array (
        'domain'       => 'default',
        'context'      => 'backend',
        'replacements' => array (),
        'post_type'    => array ('rentad', 'searchad'),
      );

      $this->params = array_merge( $defaults, $options );

      // When to add the filter
      $hook = 'backend' == $this->params['context'] ? 'admin_head' : 'template_redirect';
      add_action( $hook, array ( $this, 'register_filter' ) );
  }

  public function register_filter() {
      add_filter( 'gettext', array ( $this, 'translate' ), 10, 3 );
  }

  public function translate( $translated, $original, $domain ) {
      // exit early
      if ( 'backend' == $this->params['context'] ) {
          global $post_type;

          if ( ! empty ( $post_type ) && ! in_array( $post_type, $this->params['post_type'] ) ) {
              return $translated;
          }
      }

      if ( $this->params['domain'] !== $domain ) {
          return $translated;
      }
      return strtr( $original, $this->params['replacements'] );
  }
}

$Toscho_Retrans = new Toscho_Retrans(
    array (
        'replacements' => array (
            'Published' => 'Publicerad',
            'Pending review' => 'Väntar på granskning',
            'Save' => 'Spara',
            'Preview' => 'Visa',
            'as' => 'som',
            'Pending' => 'väntande',
            'Edit' => 'Ändra',
            'Publish' => 'Publicera',
            'Changes' => 'Ändringar',
            'Update' => 'Uppdatera',
            'Pending Review' => 'Väntar på granskning',
            'Quick' => 'Snabb',
            'Trash' => 'Radera',
            'View' => 'Visa',
            'Draft' => 'Inaktiv',
            'password' => 'lösenord',
            'Password' => 'Lösenord',
            'psomsword' => 'lösenord',
            'Delete' => 'Radera',
            'delete' => 'radera',
            'Lsomt' => 'Senast',
            'lsomt' => 'Senast',
            'Modified' => 'ändrad',
            'modified' => 'ändrad',
            'Last' => 'Senast',
        )
    ,   'post_type'    => array ('rentad', 'searchad')
    )
);


function rejected_post_status(){
	register_post_status( 'rejected', array(
		'label'                     => _x( 'Rejected', 'post' ),
		'public'                    => true,
		'exclude_from_search'       => false,
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
		'label_count'               => _n_noop( 'Rejected <span class="count">(%s)</span>', 'Rejected <span class="count">(%s)</span>' ),
	) );
}
add_action( 'init', 'rejected_post_status' );

add_action('admin_footer-post.php', 'jc_append_post_status_list');
function jc_append_post_status_list(){
     global $post;
     $complete = '';
     $label = '';
     if($post->post_type == 'rentad' || $post->post_type == 'searchad'){
          if($post->post_status == 'rejected'){
               $complete = ' selected=\"selected\"';
               $label = '<span id=\"post-status-display\"> Rejected</span>';
          }
          echo '
          <script>
          jQuery(document).ready(function($){
               $("select#post_status").append("<option value=\"rejected\" '.$complete.'>Rejected</option>");
               $(".misc-pub-section label").append("'.$label.'");
          });
          </script>
          ';
     }
}

//Registers new post status "Modified" for rentad and searchad custom post types
function modified_post_status(){
    register_post_status( 'modified', array(
        'label'                     => _x( 'Modified', 'post' ),
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Modified <span class="count">(%s)</span>', 'Modified <span class="count">(%s)</span>' ),
    ) );
}
add_action( 'init', 'modified_post_status' );

//Registers new post status "Raderad" for rentad and searchad custom post types
function raderad_post_status(){
    register_post_status( 'raderad', array(
        'label'                     => _x( 'Raderad', 'post' ),
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Raderad <span class="count">(%s)</span>', 'Raderad <span class="count">(%s)</span>' ),
    ) );
}
add_action( 'init', 'raderad_post_status' );

function modified_append_post_status_list(){
     global $post;
     $complete = '';
     $label = '';
     if($post->post_type == 'rentad' || $post->post_type == 'searchad'){
          if($post->post_status == 'modified'){
               $complete = ' selected=\"selected\"';
               $label = '<span id=\"post-status-display\"> Modified</span>';
          } else if($post->post_status == 'raderad') {
              $complete = ' selected=\"selected\"';
              $label = '<span id=\"post-status-display\"> Raderad</span>';
          }
          echo '
          <script>
          jQuery(document).ready(function($){
               $("select#post_status").append("<option value=\"modified\" '.$complete.'>Modified</option>");
               $("select#post_status").append("<option value=\"raderad\" '.$complete.'>Raderad</option>");
               $(".misc-pub-section label").append("'.$label.'");
          });
          </script>
          ';
     }
}

add_action('admin_footer-post.php', 'modified_append_post_status_list');

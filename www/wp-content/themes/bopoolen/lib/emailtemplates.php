<?php

/*
 * Adds the email template custom post type. Also the functionality to send email from 'searchad' and 'rentad' post types using
 * "Email Template" meta box. The metabox is populated with a list of published email templates. Upon selecting a specific email template the
 * fields to be filled before sending the e-mail will be shown. These fields are automatically generated from the email template.
 *
 * The fields can be left empty. An email template with no fields can be created as well. The email SUBJECT can be specified in the Other Info
 * meta box while creating/editing an email template.
 *
 * The pattern for adding replaceable fields in the email template is {{}}. So anything within the two currly braces will be replaced
 * by a text input field in the Email Template meta box.
 */

// Registers new custom post type for email templates
add_action( 'init', 'custom_email_template', 0 );

function custom_email_template() {
  register_post_type( 'email_template',
    array(
    'labels'             => array(
      'name'               => __( 'Email Templates', 'bopoolen' ),
      'singular_name'      => __( 'Email Template', 'bopoolen' ),
      'add_new'            => __( 'Add New Email Template', 'bopoolen' ),
      'add_new_item'       => __( 'Add New Email Template', 'bopoolen' ),
      'edit_item'          => __( 'Edit Email Template', 'bopoolen' ),
      'new_item'           => __( 'New Email Template', 'bopoolen' ),
      'view_item'          => __( 'View Email Template', 'bopoolen' ),
      'search_items'       => __( 'Search Email Templates', 'bopoolen' ),
      'not_found'          => __( 'No Email Templates Found', 'bopoolen' ),
      'not_found_in_trash' => __( 'No Email Templates Found in Trash', 'bopoolen' )
    ),
    'menu_icon'          => 'dashicons-clipboard',
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array('slug' => 'emailtemplates'),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'menu_position'      => null,
    'supports'           => array( 'title', 'editor', 'thumbnail', 'revisions' )
  ));
}

//Adding the meta boxes
function email_template_custom_fields() {

  global $pagenow;

  if ($pagenow == "post.php") {
    add_meta_box('send_email_metabox', __( 'Email Template', 'bopoolen' ), 'send_email_metabox_fields', array('searchad', 'rentad'), 'side', 'default');
  }

  add_meta_box('email_template_other_info', __( 'Other Info', 'bopoolen' ), 'email_template_fields_other_info', 'email_template', 'normal', 'high');

}

add_action( 'add_meta_boxes', 'email_template_custom_fields' );

//Dispaly the meta box components
function email_template_fields_other_info() {

  global $post;

  echo '<input type="hidden" name="email_subject_nonce" id="email_subject_nonce" value="' .
  wp_create_nonce( 'email_subject_nonce' ) . '" />';
  echo '<h4>' . __('E-mail Subject', 'bopoolen') . ':</h4>';
  echo '<input type="text" name="_email_subject" value="' . get_post_meta( $post->ID, '_email_subject', true )  . '" placeholder="" class="widefat" />';

}

//Save the meta box input data
function save_email_template_custom_fields($post_id, $post) {

  if (strcasecmp(get_post_type($post_id), "email_template") == 0) {

    if ( !wp_verify_nonce( $_POST['email_subject_nonce'], 'email_subject_nonce' )) {
        return $post->ID;
    }

    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ))
      return $post->ID;

    $events_meta['_email_subject'] = $_POST['_email_subject'];

    // Add values of $events_meta as custom fields
    foreach ($events_meta as $key => $value) { // Cycle through the $events_meta array!

      if( !$value ) {
        delete_post_meta($post->ID, $key);
      } else {
        update_post_meta($post->ID, $key, $value);
      }

    }

    $content = get_post_field('post_content', $post_id);
    $pattern = '!\{\{(\w+)\}\}!';

    preg_match_all($pattern, $content, $matches);
    update_post_meta($post_id, "email_template_variables", array_unique($matches[1]));
    update_post_meta($post_id, "email_template_slug", $post->post_name);

  }

}

add_action('save_post', 'save_email_template_custom_fields', 1, 2);

//Rendering the meta box fields in 'searchad' and 'rentad' for sending emails
function send_email_metabox_fields() {

  global $post;

  $post_ids = get_posts(array(
        'posts_per_page'	=> -1,
        'post_type'   => 'email_template',
        'post_status' => 'publish',
        'fields'      => 'ids',
  ));

  if (!empty ($post_ids)) {
    echo "<select id='email-template-dropdown' style='width: 100%'>";
    echo "<option value='none'>" . __('Select a template', 'bopoolen') . "</option>";

    foreach($post_ids as $post_id) {
      $slug = get_post_meta( $post_id, "email_template_slug", true);
      $title = get_the_title($post_id);
      echo "<option value='$slug'>" . $title . "</option>";
    }

    echo "</select>";
    echo "<br />";
  }

  foreach ($post_ids as $post_id) {
    $variables = get_post_meta( $post_id, "email_template_variables", true );
    $slug = get_post_meta( $post_id, "email_template_slug", true);
    render_email_template_fields($variables, $slug, $post->ID, $post_id);
  }

  add_action( 'admin_footer', 'render_email_template_javascript' );

}

function render_email_template_fields ($variables, $slug, $current_post_id, $template_post_id) {

  echo "<div id='$slug' class='email_template_form' style='display: none;'>";

  echo '<input type="hidden" name="email_form_nonce" id="email_form_nonce" value="' .
  wp_create_nonce( 'process_email_template_form_action' ) . '" />';

  echo '<input type="hidden" name="current_post_id" id="current_post_id" value="' . $current_post_id . '" />';

  echo '<input type="hidden" name="template_post_id" id="template_post_id" value="' . $template_post_id . '" />';

  foreach ($variables as $variable) {
    echo '<h4>'.__($variable, 'bopoolen').':</h4>';
    echo "<input type='text' name='$variable' value='' placeholder='' class='widefat' />";
  }

  echo "<button type='button' class='button' style='margin-top: 15px;'>" . __('Send E-mail', 'bopoolen') . "</button>";
  echo "<span class='spinner' style='margin-top: 20px; float: none;'></span>";

  echo "<span class='message' style='display: none; margin-top: 15px;'></span>";
  echo "</div>";

}

//The JavaScript for sending the email with AJAX
function render_email_template_javascript() { ?>

    <script type="text/javascript">
    jQuery(document).ready(function($) {
      var dropdown = document.getElementById("email-template-dropdown");
      var formDivs = document.querySelectorAll("#send_email_metabox .inside div");

      $(dropdown).change(function() {
        var selected = dropdown.options[dropdown.selectedIndex].value;
        $(formDivs).css("display", "none");
        $('#' + selected).toggle();
      });

      $('.email_template_form').each(function() {

        var form = $(this);
        var message = form.children("span.message");
        var spinner = form.children("span.spinner");
        var inputs = form.children('input');
        var inputsToClear = form.children('input').not('#email_form_nonce, #current_post_id, #template_post_id');

        form.find("button").on("click", function(e) {
          e.preventDefault();

          var data = {
            action: 'process_email_template_form_action',
            data: form.children('input').serialize()
          };

          $.ajax({

            url: ajaxurl,
            type: 'POST',
            data: data,

            beforeSend: function(){
              spinner.css("visibility", "visible");
            },

            success: function(response) {
              spinner.css("visibility", "hidden");
              message.text(response).css("display", "block");
              window.setTimeout(function () {
                message.text('').css("display", "none");
              }, 5000);
              inputsToClear.val('');
            },

            error: function(response) {
              spinner.css("visibility", "hidden");
              message.text(response).css("display", "block");
              window.setTimeout(function () {
                message.text('').css("display", "none");
              }, 5000);
            }

          });

        });

      });

    });
    </script> <?php

}

function process_email_template_form() {

  parse_str($_POST['data'], $data);
  $nonce = $data['email_form_nonce'];

  if ( !wp_verify_nonce( $nonce, 'process_email_template_form_action' ) )
        wp_die();

  $template_post_id = intval($data['template_post_id']);
  $current_post_id = intval($data['current_post_id']);

  array_splice($data, 0, 3);

  $response = send_email_from_template($data, $template_post_id, $current_post_id);
  echo $response;
  wp_die();

}

add_action( 'wp_ajax_process_email_template_form_action', 'process_email_template_form');

// Sends email with wp_mail() using wppmail
function send_email_from_template( $replace_values, $template_post_id, $current_post_id) {

  $author_id = get_post_field('post_author', $current_post_id);
  $receiver = get_the_author_meta('user_email', $author_id);
  $subject = get_post_meta($template_post_id, '_email_subject', true);

  $template = get_post_field('post_content', $template_post_id);
  $pattern = '!\{\{(\w+)\}\}!';
  $content = preg_replace_callback($pattern, function ($matches) use ($replace_values) {
      return $replace_values[$matches[1]];
  }, $template);

  $mail_send = wp_mail( $receiver, $subject, $content);

  if ( $mail_send ) {
    return __('Mail Successfully Sent!', 'bopoolen');
  }
  else {
    return __('Error Sending E-mail', 'bopoolen');
  }

}

<?php

add_action('wp_dashboard_setup', 'ads_dashboard_widget');

function ads_dashboard_widget() {
  global $wp_meta_boxes;

  wp_add_dashboard_widget('custom_help_widget', 'Annons översikt', 'custom_dashboard_ad_help');
}

function custom_dashboard_ad_help() {

  // get rentads post counts
  $rent_count_posts = wp_count_posts('rentad');
  $rent_published_posts = $rent_count_posts->publish;
  $rent_pending_posts = $rent_count_posts->pending;
  $rent_draft_posts = $rent_count_posts->draft;
  $rent_auto_unpublished_posts = $rent_count_posts->auto_unpublished;
  $rent_modified_posts = $rent_count_posts->modified;

  // get searchads post counts
  $search_count_posts = wp_count_posts('searchad');
  $search_published_posts = $search_count_posts->publish;
  $search_pending_posts = $search_count_posts->pending;
  $search_draft_posts = $search_count_posts->draft;
  $search_auto_unpublished_posts = $search_count_posts->auto_unpublished;
  $search_modified_posts = $search_count_posts->modified;

  echo '<h4 style="font-weight: bold;">'.__('Uthyrningsannonser', 'bopoolen').'</h4>';
  echo '<p style="color:#27ae60;">'.__('Aktiva:', 'bopoolen').' '.$rent_published_posts.'</p>';
  echo '<p style="color:#f39c12;">'.__('Väntar granskning:', 'bopoolen').' '.$rent_pending_posts.'</p>';
  echo '<p style="color:#f39c12;">'.__('Väntar granskning (modifierade):', 'bopoolen').' '.$rent_modified_posts.'</p>';
  echo '<p style="color:#dece00;">'.__('Automatiskt Avpublicerade:', 'bopoolen').' '.$rent_auto_unpublished_posts.'</p>';
  echo '<p style="color:#e74c3c;">'.__('Inatkiva:', 'bopoolen').' '.$rent_draft_posts.'</p>';

  echo '<hr>';

  echo '<h4 style="font-weight: bold;">'.__('Sökesannonser', 'bopoolen').'</h4>';
  echo '<p style="color:#27ae60;">'.__('Aktiva:', 'bopoolen').' '.$search_published_posts.'</p>';
  echo '<p style="color:#f39c12;">'.__('Väntar granskning:', 'bopoolen').' '.$search_pending_posts.'</p>';
  echo '<p style="color:#f39c12;">'.__('Väntar granskning (modifierade):', 'bopoolen').' '.$search_modified_posts.'</p>';
  echo '<p style="color:#dece00;">'.__('Automatiskt Avpublicerade:', 'bopoolen').' '.$search_auto_unpublished_posts.'</p>';
  echo '<p style="color:#e74c3c;">'.__('Inatkiva:', 'bopoolen').' '.$search_draft_posts.'</p>';
}

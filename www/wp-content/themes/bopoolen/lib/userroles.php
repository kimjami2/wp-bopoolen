<?php
// hide amdmin bar for all user except admin
// show_admin_bar(false);

// Hook on 'admin_init' to disable all users except admin to view wp-admin
// Redirect non-admin users to home page
add_action( 'admin_init', 'redirect_non_admin_users' );
function redirect_non_admin_users() {
	if ( ! current_user_can( 'manage_options' ) && '/wp-admin/admin-ajax.php' != $_SERVER['PHP_SELF'] ) {
		wp_redirect( '/profiles/?user='.get_current_user_id() );
		exit;
	}
}

// check if user is blacklisted, logout user > redirect to login with message!
function blacklisted_user() {
	if (get_the_author_meta( '_blacklist', get_current_user_id()) == 'checked') {
		wp_logout();
		echo '<script>window.location.href = "'.home_url().'/logga-in?mode=blocked";</script>';
	}
}
add_action('get_header', 'blacklisted_user');

// Add two custom user role, search user for "hitta annonser" & rentuser for "uthyrnings annonser"
$result = add_role( 'searchuser', __('Search User' ),array(
    'read' => true,
    'edit_posts' => true,
    'create_posts' => true,
    'publish_posts' => false,
    'edit_pages' => false,
    'edit_others_posts' => false,
    'manage_categories' => false,
  )
);

$result = add_role( 'rentuser', __('Rent User' ),array(
    'read' => true,
    'edit_posts' => true,
    'create_posts' => true,
    'publish_posts' => false,
    'edit_pages' => false,
    'edit_others_posts' => false,
    'manage_categories' => false,
  )
);

function new_user_list_fields( $contactmethods ) {
    $contactmethods['svartlistad'] = 'Svartlistad';
		$contactmethods['klagomål'] = 'Klagomål';
		$contactmethods['interncomment'] = 'Intern kommentar';
    return $contactmethods;
}
add_filter( 'user_contactmethods', 'new_user_list_fields', 10, 1 );


function new_modify_user_table( $column ) {
    $column['svartlistad'] = 'Svartlistad';
		$column['klagomål'] = 'Klagomål';
		$column['interncomment'] = 'Intern kommentar';
    return $column;
}
add_filter( 'manage_users_columns', 'new_modify_user_table' );

function new_modify_user_table_row( $val, $column_name, $user_id ) {
    $user = get_userdata( $user_id );
    switch ($column_name) {
        case 'svartlistad' :
						if (get_the_author_meta( '_blacklist', $user_id ) == 'checked') {
							return __('Ja');
						}
            break;
				case 'klagomål' :
						if (get_the_author_meta( '_complaint', $user_id ) == 'checked') {
							return __('Ja');
						}
            break;
				case 'interncomment' :
						return get_the_author_meta( '_usercomment', $user_id );
            break;
        default:
    }
    return $return;
}
add_filter( 'manage_users_custom_column', 'new_modify_user_table_row', 10, 3 );

// add_action('manage_users_columns','remove_posts_from_list');
// function remove_posts_from_list($column_headers) {
//   unset($column_headers['posts']);
//   return $column_headers;
// }

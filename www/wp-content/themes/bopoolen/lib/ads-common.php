<?php

add_action('admin_enqueue_scripts', function () {
    wp_enqueue_style('ads-admin', get_stylesheet_directory_uri() . '/assets/styles/layouts/a_ds-admin.css' );
});

function modifiable_post_types($post_type = null) {
    static $modifiable_post_types = array();
    if ($post_type !== null) {
        $modifiable_post_types[] = $post_type;
    }
    return $modifiable_post_types;
}

add_action("json_api-posts-update_post", function() {
    global $post;
    $modifiable_post_types = modifiable_post_types();
    if (isset($post) && in_array($post->post_type, $modifiable_post_types) && $post->post_status === "publish") {
        $all_post_meta = get_post_meta($post->ID);
        if (isset($all_post_meta['_bop_oldvalues'])) {
            unset($all_post_meta['_bop_oldvalues']);
        }
        $old_data = array('meta_values' => $all_post_meta);
        $old_data['post_title'] = $post->post_title;
        $old_data['post_content'] = $post->post_content;
        $success = update_post_meta($post->ID, '_bop_oldvalues', serialize($old_data));
    }
});

$bop_old_values = null;
global $bop_old_values;

function populate_old_values() {
    global $post;
    if ($old_values === null) {
        if (isset($post)) {
            $modified_fields_string = get_post_meta($post->ID, '_bop_oldvalues', true);
            $modified_fields = array();
            if (!empty($modified_fields_string)) {
                return unserialize($modified_fields_string);
            }
        }
    }
    return array();
}

function old_values() {
    static $old_values = null;
    if ($old_values === null) {
        $old_values = populate_old_values();
    }
    return $old_values;
}

function get_changed_meta_values($current_values) {
    $old_values = old_values();

    $modified_info = array();
    $modified_fields = $old_values['meta_values'];

    foreach ($current_values as $data) {
        if (isset($modified_fields[$data['key']])) {
            $value = $modified_fields[$data['key']];
            if (is_array($value) && count($value) === 1) {
                $value = $value[0];
            }

            if ($data['value'] !== $value) {
                $modified_info[] = array(
                    "header" => $data['header'],
                    "old_value" => $value,
                );
            }
        }
    }
    return $modified_info;
}

function get_changed_base_info($title, $content) {
    $old_values = old_values();
    $changed_title = $old_values['post_title'] !== $title;
    $changed_content = $old_values['post_content'] !== $content;
    return array($changed_title ? $old_values['post_title'] : false, $changed_content ? $old_values['post_content'] : false);
}

function add_modified_base_info_box_if_required($post, $post_type) {
    list($old_title, $old_content) = get_changed_base_info($post->post_title, $post->post_content);
    if (!!$old_title || !!$old_content) {
        add_meta_box('search_ad_modified_info', 'Ändrad grundinformation', 'search_ad_modified_info_box', $post_type, 'normal', 'high');
    }
}

function render_changed_values_list($modified_info) {
    $has_modified_field = !empty($modified_info);
    if ($has_modified_field) {
        echo '<div class="bop_modified_fields_container">';
        echo '<h3>' . __('Värde har ändrats', 'bopoolen') . '</h3>';
        echo '<p>' . __('Nedan följer en lista med de fält som ändrats. För varje fält visas det gamla värdet.', 'bopoolen') . '</p>';
        foreach ($modified_info as $item) {
            echo '<div class="bop_modified_field_container">';
            echo "<h4>{$item['header']}</h4>";
            echo "<div class=\"bop_modified_field--value\">{$item['old_value']}</div>";
            echo '</div>';
        }
        echo '</div>';
    }
}

function search_ad_modified_info_box() {
    global $post;
    list($old_title, $old_content) = get_changed_base_info($post->post_title, $post->post_content);
    $modified_fields = array();
    if (!!$old_title) {
        $modified_fields[] = array(
            'old_value' => $old_title,
            'header' => __('Titel', 'bopoolen'),
        );
    }
    if (!!$old_content) {
        $modified_fields[] = array(
            'old_value' => $old_content,
            'header' => __('Beskrivning', 'bopoolen'),
        );
    }
    render_changed_values_list($modified_fields);
}

?>

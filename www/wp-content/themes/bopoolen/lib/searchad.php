<?php

require_once 'ads-common.php';
modifiable_post_types("searchad");

// Registers new real estate custom post type
add_action( 'init', 'custom_search_ad', 1 );
function custom_search_ad() {
  register_post_type( 'searchad',
    array(
      'labels' => array(
        'name' => __( 'Sökes Ads' ),
        'singular_name' => __( 'sokes' ),
        'add_new' => __( 'Add New Ad' ),
        'add_new_item' => __( 'Add New Ad' ),
        'edit_item' => __( 'Edit Ad' ),
        'new_item' => __( 'Add New Ad' ),
        'view_item' => __( 'View Ad' ),
        'search_items' => __( 'Search Ad' ),
        'not_found' => __( 'No ads found' ),
        'not_found_in_trash' => __( 'No ads found in trash' )
      ),
      'menu_icon'   => 'dashicons-format-gallery',
      'public' => true,
      'supports' => array( 'title', 'editor', 'revisions', 'thumbnail', ),
      'capability_type' => 'post',
      'rewrite' => array("slug" => "searchad"), // Permalinks format
      'register_meta_box_cb' => 'add_search_ad_custom_fields',
    )
  );
}

// Add the Real Estate Meta Boxes
add_action( 'add_meta_boxes', 'add_search_ad_custom_fields' );
function add_search_ad_custom_fields() {
  add_meta_box('search_ad_custom_fields_creator', 'Skapare', 'search_ad_custom_fields_creator', 'searchad', 'normal', 'default');
  add_meta_box('search_ad_custom_fields_eng', 'Engelskinformation', 'search_ad_custom_fields_eng', 'searchad', 'normal', 'default');
  add_meta_box('search_ad_custom_fields', 'Sökes information', 'search_ad_custom_fields', 'searchad', 'normal', 'default');
  add_meta_box('search_ad_custom_inactive_ad', 'Inaktiverad annons', 'rent_ad_custom_inactive_ad', 'searchad', 'normal', 'default');
  add_meta_box('search_ad_custom_deleting_ad', 'Raderad annons', 'rent_ad_custom_deleting_ad', 'searchad', 'normal', 'default');

  global $post;
  add_modified_base_info_box_if_required($post, 'searchad');
}

// The Real Estate Location Metabox
function search_ad_custom_fields_creator() {
  global $post;

  // Noncename needed to verify where the data originated
  echo '<style>input[type="checkbox"] { margin-left: 10px; }</style>';
  echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
  wp_create_nonce( plugin_basename(__FILE__) ) . '" />';


  echo '<h4>'.__('Personnummer', 'bopoolen').':</h4>';
  echo '<input type="text" disabled name="" value="' . get_the_author_meta( '_securitynumber', $post->post_author )  . '" placeholder="Personnummer" class="widefat" />';

}


function search_ad_custom_fields_eng() {
  global $post;

  // Noncename needed to verify where the data originated
  echo '<style>input[type="checkbox"] { margin-left: 10px; }</style>';
  echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
  wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

  $titleEng         = get_post_meta($post->ID, '_titleeng', true);
  $descriptionEng   = get_post_meta($post->ID, '_descriptioneng', true);



  echo '<h4>'.__('Titel på engelska', 'bopoolen').':</h4>';
  echo '<input type="text" name="_titleeng" value="' . $titleEng  . '" placeholder="Titel på engelska" class="widefat" />';

  echo '<h4>'.__('Beskrivning på engelska', 'bopoolen').':</h4>';
  $settings = array( 'media_buttons' => false );
  wp_editor( $descriptionEng, '_descriptioneng', $settings );

  $current_values = array(
      array(
          'key' => '_titleeng',
          'value' => $titleEng,
          'header' => __('Titel på engelska', 'bopoolen'),
      ),
      array(
          'key' => '_descriptioneng',
          'value' => $descriptionEng,
          'header' => __('Beskrivning på engelska', 'bopoolen'),
      ),
  );

  $modified_info = get_changed_meta_values($current_values);
  render_changed_values_list($modified_info);
}

function search_ad_custom_fields() {
  global $post;

  // Noncename needed to verify where the data originated
  echo '<style>input[type="checkbox"] { margin-left: 10px; }</style>';
  echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
  wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

  // Get the data if its already been entered
  $area                 = get_post_meta($post->ID, '_area', true);
  $contractType         = get_post_meta($post->ID, '_contractType', true);
  $accommodationType    = get_post_meta($post->ID, '_accommodationType', true);
  $availablefrom         = get_post_meta($post->ID, '_availablefrom', true);
  $availableto         = get_post_meta($post->ID, '_availableto', true);



  // Echo out the field
  echo '<h4>'.__('Område:', 'bopoolen').'</h4>';
  echo '<input type="text" name="_area" value="' . $area  . '" placeholder="Malmö, lund etc" class="widefat" />';

  echo '<h4>'.__('Avtalstyp:', 'bopoolen').'</h4>';
  echo '<input type="text" name="_contractType" value="' . $contractType  . '" placeholder="2015-01-01" class="widefat" />';

  echo '<h4>'.__('Gatuadress:', 'bopoolen').'</h4>';
  echo '<input type="text" name="_accommodationType" value="' . $accommodationType  . '" placeholder="Stortorget 9" class="widefat" />';

  echo '<h4>'.__('Från:', 'bopoolen').'</h4>';
  echo '<input type="text" name="_availablefrom" value="' . $availablefrom  . '" placeholder="yyyy-mm-dd" class="widefat" />';

  echo '<h4>'.__('Till:', 'bopoolen').'</h4>';
  echo '<input type="text" name="_availableto" value="' . $availableto  . '" placeholder="yyyy-mm-dd,tillsvidare" class="widefat" />';

  $current_values = array(
      array(
          'key' => '_area',
          'value' => $area,
          'header' => __('Område:', 'bopoolen'),
      ),
      array(
          'key' => '_contractType',
          'value' => $contractType,
          'header' => __('Avtalstyp:', 'bopoolen'),
      ),
      array(
          'key' => '_accommodationType',
          'value' => $accommodationType,
          'header' => __('Gatuadress:', 'bopoolen'),
      ),
      array(
          'key' => '_availablefrom',
          'value' => $availablefrom,
          'header' => __('Från:', 'bopoolen'),
      ),
      array(
          'key' => '_availableto',
          'value' => $availableto,
          'header' => __('Till:', 'bopoolen'),
      ),
  );

  $modified_info = get_changed_meta_values($current_values);
  render_changed_values_list($modified_info);
}

// The field for inactive ad
function search_ad_custom_inactive_ad() {
  global $post;

  // Noncename needed to verify where the data originated
  echo '<style>input[type="checkbox"] { margin-left: 10px; }</style>';
  echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

  // Get the data if its already been entered
  $inactivecomment    = get_post_meta($post->ID, '_inactivecomment', true);

  // Get which radiobutton is clicked
  $inactivereason     = get_post_meta($post->ID, '_inactivereason', true);

  // Get other reason comment if inactivereason = other
  if($inactivereason == 'other') {
    $inactivereasoncomment = get_post_meta($post->ID, '_inactivereasoncomment', true);
  }

  // Echo out the field
  echo '<h4>'.__('Kommentar vid inaktivering utav annons', 'bopoolen').':</h4>';
  echo '<textarea rows="5" name="_inactivecomment" class="widefat" style="width:100%;resize:vertical;">' . $inactivecomment  . '</textarea>';
  echo '<h4>'.__('Anledning till inaktivering utav annons', 'bopoolen').':</h4>';

  // Echo out the radio buttons
  echo '<input type="radio" class="radiobutton" name="_inactivereason" value="found_tenant"'.checkValue('found_tenant', $inactivereason).'>'.__('Hittat hyresgäst', 'bopoolen').'<br>';
  echo '<input type="radio" class="radiobutton" name="_inactivereason" value="unavailable"'.checkValue('unavailable', $inactivereason).'>'.__('Vill inte längra hyra ut', 'bopoolen').'<br>';
  echo '<input type="radio" class="radiobutton" name="_inactivereason" value="other"'.checkValue('other', $inactivereason).'>'.__('Annan anledning' ,'bopoolen').'<br>';

  // Echo out the inactive reason other text field
  echo '<h4>'.__('Kommentar vid "Annan anledning" ovan.', 'bopoolen').':</h4>';
  echo '<textarea rows="5" name="_inactivereasoncomment" class="widefat" style="width:100%;resize:vertical;">' . $inactivereasoncomment  . '</textarea>';
}

// The field for deleting ad
function search_ad_custom_deleting_ad() {
  global $post;

  // Noncename needed to verify where the data originated
  echo '<style>input[type="checkbox"] { margin-left: 10px; }</style>';
  echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

  // Get the data if its already been entered
  $deletecomment    = get_post_meta($post->ID, '_deletecomment', true);

  // Get which radiobutton is clicked
  $deletereason     = get_post_meta($post->ID, '_deletereason', true);

  // Get other reason comment if inactivereason = other
  if($deletereason == 'other') {
    $deletereasoncomment = get_post_meta($post->ID, '_deletereasoncomment', true);
  }

  // Echo out the field
  echo '<h4>'.__('Kommentar vid raderad annons', 'bopoolen').':</h4>';
  echo '<textarea rows="5" name="_deletecomment" class="widefat" style="width:100%;resize:vertical;">' . $deletecomment  . '</textarea>';
  echo '<h4>'.__('Anledning till radering utav annons', 'bopoolen').':</h4>';

  // Echo out the radio buttons
  echo '<input type="radio" class="radiobutton" name="_deletereason" value="found_tenant"'.checkValue('found_tenant', $deletereason).'>'.__('Hittat hyresgäst', 'bopoolen').'<br>';
  echo '<input type="radio" class="radiobutton" name="_deletereason" value="unavailable"'.checkValue('unavailable', $deletereason).'>'.__('Vill inte längra hyra ut', 'bopoolen').'<br>';
  echo '<input type="radio" class="radiobutton" name="_deletereason" value="other"'.checkValue('other', $deletereason).'>'.__('Annan anledning' ,'bopoolen').'<br>';

  // Echo out the inactive reason other text field
  echo '<h4>'.__('Kommentar vid "Annan anledning" ovan.', 'bopoolen').':</h4>';
  echo '<textarea rows="5" name="_deletereasoncomment" class="widefat" style="width:100%;resize:vertical;">' . $deletereasoncomment  . '</textarea>';
}

//Save the Metabox Data
function save_search_ad_custom_fields($post_id, $post) {

  if ( !wp_verify_nonce( $_POST['eventmeta_noncename'], plugin_basename(__FILE__) )) {
  return $post->ID;
  }

  // Is the user allowed to edit the post or page?
  if ( !current_user_can( 'edit_post', $post->ID ))
    return $post->ID;

    // We'll put it into an array to make it easier to loop though.
    $events_meta['_titleeng']           = $_POST['_titleeng'];
    $events_meta['_descriptioneng']     = $_POST['_descriptioneng'];
    $events_meta['_availablefrom']      = $_POST['_availablefrom'];
    $events_meta['_availableto']        = $_POST['_availableto'];
    $events_meta['_streetaddress']      = $_POST['_streetaddress'];
    $events_meta['_postalcode']         = $_POST['_postalcode'];
    $events_meta['_city']               = $_POST['_city'];
    $events_meta['_roominhouse']        = (isset( $_POST[ '_roominhouse' ]) ? 'checked' : 'unchecked');
    $events_meta['_roominapartment']    = (isset( $_POST[ '_roominapartment' ]) ? 'checked' : '');
    $events_meta['_roominstudent']      = (isset( $_POST[ '_roominstudent' ]) ? 'checked' : '');
    $events_meta['_ownhouse']           = (isset( $_POST[ '_ownhouse' ]) ? 'checked' : '');
    $events_meta['_ownapartment']       = (isset( $_POST[ '_ownapartment' ]) ? 'checked' : '');
    $events_meta['_contractsecond']     = (isset( $_POST[ '_contractsecond' ]) ? 'checked' : '');
    $events_meta['_contractfirst']      = (isset( $_POST[ '_contractfirst' ]) ? 'checked' : '');
    $events_meta['_contractchange']     = (isset( $_POST[ '_contractchange' ]) ? 'checked' : '');

    $events_meta['_availableyta']       = $_POST['_availableyta'];
    $events_meta['_availablegemensam']  = $_POST['_availablegemensam'];
    $events_meta['_numberofrooms']      = $_POST['_numberofrooms'];
    $events_meta['_includedel']         = (isset( $_POST[ '_includedel' ]) ? 'checked' : '');
    $events_meta['_includedinternet']   = (isset( $_POST[ '_includedinternet' ]) ? 'checked' : '');
    $events_meta['_includedfurniture']  = (isset( $_POST[ '_includedfurniture' ]) ? 'checked' : '');
    $events_meta['_includedtoilet']     = (isset( $_POST[ '_includedtoilet' ]) ? 'checked' : '');
    $events_meta['_includedbath']       = (isset( $_POST[ '_includedbath' ]) ? 'checked' : '');
    $events_meta['_includedanimal']     = (isset( $_POST[ '_includedanimal' ]) ? 'checked' : '');
    $events_meta['_includedhandicap']   = (isset( $_POST[ '_includedhandicap' ]) ? 'checked' : '');
    $events_meta['_includeddishwasher'] = (isset( $_POST[ '_includeddishwasher' ]) ? 'checked' : '');
    $events_meta['_includedwasher']     = (isset( $_POST[ '_includedwasher' ]) ? 'checked' : '');
    $events_meta['_includedsmoking']    = (isset( $_POST[ '_includedsmoking' ]) ? 'checked' : '');
    $events_meta['_includedpentry']     = (isset( $_POST[ '_includedpentry' ]) ? 'checked' : '');
    $events_meta['_includedentrance']   = (isset( $_POST[ '_includedentrance' ]) ? 'checked' : '');

    $events_meta['_inactivecomment']    = $_POST['_inactivecomment'];
    $events_meta['_inactivereason']     = $_POST['_inactivereason'];
    if($_POST['_inactivereason'] === 'other') {
      $events_meta['_inactivereasoncomment']  = $_POST['_inactivereasoncomment'];
    } else {
      delete_post_meta($post->ID, '_inactivereasoncomment');
    }
    $events_meta['_deletecomment']      = $_POST['_deletecomment'];
    $events_meta['_deletereason']       = $_POST['_deletereason'];
    if($_POST['_deletereason'] === 'other') {
      $events_meta['_deletereasoncomment'] = $_POST['_deletereasoncomment'];
    } else {
      delete_post_meta($post->ID, '_deletereasoncomment');
    }

    // Add values of $events_meta as custom fields
    foreach ($events_meta as $key => $value) { // Cycle through the $events_meta array!
      if( $post->post_type == 'revision' ) return; // Don't store custom data twice
      $value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
      if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
        update_post_meta($post->ID, $key, $value);
      } else { // If the custom field doesn't have a value
        add_post_meta($post->ID, $key, $value);
      }
      if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
    }

    if ($post->post_status === "publish") {
        delete_post_meta($post->ID, '_bop_oldvalues');
    }

    if ($post->post_status === "publish") {
      delete_post_meta($post->ID, '_deletecomment');
      delete_post_meta($post->ID, '_deletereason');
      delete_post_meta($post->ID, '_deletereasoncomment');
      delete_post_meta($post->ID, '_inactivecomment');
      delete_post_meta($post->ID, '_inactivereason');
      delete_post_meta($post->ID, '_inactivereasoncomment');
      delete_post_meta($post->ID, '_bop_oldvalues');
      delete_post_meta($post->ID, '_emailreminder');
    } else if ($post->post_status === "draft") {
      delete_post_meta($post->ID, '_deletecomment');
      delete_post_meta($post->ID, '_deletereason');
      delete_post_meta($post->ID, '_deletereasoncomment');
    } else if ($post->post_status === "raderad") {
      delete_post_meta($post->ID, '_inactivecomment');
      delete_post_meta($post->ID, '_inactivereason');
      delete_post_meta($post->ID, '_inactivereasoncomment');
    }
}

add_action('save_post', 'save_search_ad_custom_fields', 1, 2); // save the custom fields

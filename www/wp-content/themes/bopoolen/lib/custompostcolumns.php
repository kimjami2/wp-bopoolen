<?php

add_filter('manage_rentad_posts_columns', 'rentad_columns');
function rentad_columns($columns) {
  $columns['securitynumber'] = 'Personnummer';
  $columns['name'] = 'Namn';
  $columns['phone'] = 'Telefonnummer';
  $columns['ort'] = 'Ort';
  $columns['hyra'] = 'Hyra';
  $columns['size'] = 'Storlek';
  $columns['included'] = 'Inkluderat';
  $columns['uthyrare'] = 'Uthyrare';
  $columns['approved'] = 'Godkänd';
  return $columns;
}

add_action('manage_rentad_posts_custom_column',  'show_rentad_column');
function show_rentad_column($name) {
    global $post;
    switch ($name) {
        case 'ort':
            echo get_post_meta($post->ID, '_streetaddress', true).' ,'.get_post_meta($post->ID, '_postalcode', true). ' ' .get_post_meta($post->ID, '_city', true);
            break;
        case 'size':
            echo get_post_meta($post->ID, '_availableyta', true);
            break;
        case 'name':
            $authorID = get_the_author_id();
            echo get_user_meta($authorID, 'first_name', true).' '.get_user_meta($authorID, 'last_name', true);
            break;
        case 'phone':
            $authorID = get_the_author_id();
            echo "<a target='_blank' href='http://personer.eniro.se/resultat/".get_user_meta($authorID, '_phone', true)."'>".get_user_meta($authorID, '_phone', true)."</a>";
            break;
        case 'hyra':
            $rent = get_post_meta($post->ID, '_rentprice', true);
            echo $rent;
            break;
        case 'included':
            $str = '';
            $included = [
              #"_includedhandicap",
              #"_includeddishwasher",
              #"_includedwasher",
              #"_includedtoilet",
              #"_includedanimal",
              #"_includedsmoking",
              #"_includedpentry",
              #"_includedentrance",
              "_includedel",
              "_includedinternet",
              "_includedfurniture"
              #"_includedbath"
            ];
            foreach($included as $include) {
              if(get_post_meta($post->ID, $include, true) == 'checked') {
                $str_tmp = explode("_included", $include)[1];
                if($str == '') {
                  $str = $str_tmp;
                } else {
                  $str = $str.", ".__($str_tmp, 'bopoolen');
                }
              }
            }
            echo $str;
            break;
        case 'uthyrare':
            $author = get_the_author();
            $authorID = get_the_author_id();
            echo '<a href="edit.php?post_type=rentad&author='.$authorID.'">'.$author.'</a>';
            $queryOpts = array(
                'orderby' => 'date',
                'order' => 'DESC',
                'showposts' => min(5, 100),
                'post_type' => 'wpcr3_review',
                'post_status' => 'publish',
                'relation' => 'AND'
            );
            $meta_query[] = array(
                'key' => "wpcr3_review_post",
                'value' => $authorID,
                'compare' => '='
            );

            $queryOpts['meta_query'] = $meta_query;
            $reviews = new WP_Query($queryOpts);
            if(get_user_meta($authorID, "_blacklist", true) == 'checked') {
              echo '<span title="'. __('Svartlistad', 'bopoolen').'" class="dashicons dashicons-warning" style="color:darkred"></span>';
            }
            if(get_user_meta($authorID, "_complaint", true) == 'checked') {
              echo '<span title="'. __('Klagomål', 'bopoolen').'" class="dashicons dashicons-warning" style="color:red"></span>';
            }
            if(get_user_meta($authorID, "_usercomment", true) != '') {
              echo '<span title="'. __('Kommentar', 'bopoolen').'" class="dashicons dashicons-warning" style="color:lightsalmon"></span>';
            }
            if(count($reviews->posts)> 0) {
                if(count_avg_rating($authorID) <= 2) {
                    echo '<span title="'. __('Dåliga recensioner', 'bopoolen').'" class="dashicons dashicons-flag" style="color:orange;"></span>';
                }
            }
            if(get_post_meta($post->ID, '_postrejected')[0] == 'true') {
                echo '<span title="'. __('Tidigare avvisad', 'bopoolen').'" class="dashicons dashicons-flag" style="color:red;"></span>';
            }
            break;
        case 'approved':
            echo get_post_meta($post->ID, '_firstTimeApproved', true);
            break;
        case 'securitynumber':
            $post = get_post( $post->ID );
            echo '<a href="//www.upplysning.se/" target="_blank">'.get_the_author_meta( '_securitynumber', $post->Author ).'</a>';
            break;
    }
}



add_filter('manage_searchad_posts_columns', 'searchad_columns_head');
function searchad_columns_head($defaults) {
  $defaults['securitynumber'] = 'Personnummer';
  $defaults['approved'] = 'Godkänd';
  return $defaults;
}

add_action('manage_searchad_posts_custom_column', 'show_searchad_column');
function show_searchad_column($column_name) {
  global $post;
  if ($column_name == 'securitynumber') {
    $post = get_post( $post->ID );
    echo '<a href="//www.upplysning.se/" target="_blank">'.get_the_author_meta( '_securitynumber', $post->Author ).'</a>';
  }
  if ($column_name == 'approved') {
    echo get_post_meta($post->ID, '_firstTimeApproved', true);
  }
}

// add_filter('manage_searchad_posts_columns', 'searchad_columns');
// function searchad_columns($columns) {
//     $columns['ort'] = 'Ort';
//     $columns['hyra'] = 'Hyra';
//     $columns['uthyrare'] = 'Uthyrare';
//     return $columns;
// }

// add_action('manage_searchad_posts_columns',  'show_searchad_column');
// function show_searchad_column($name) {
//     global $post;
//     switch ($name) {
//         case 'ort':
//             $city = get_post_meta($post->ID, '_city', true);
//             echo $city;
//             break;
//         case 'hyra':
//             $rent = get_post_meta($post->ID, '_rentprice', true);
//             echo $rent;
//             break;
//         case 'uthyrare':
//             $author = get_the_author();
//             $authorID = get_the_author_id();
//             echo '<a href="edit.php?post_type=rentad&author='.$authorID.'">'.$author.'</a>';
//             break;
//     }
// }



function run_on_publish_post_only( $new_status, $old_status, $post ) {
  // var_dump($post->post_type);
  // var_dump($new_old);
  if (($post->post_type == 'rentad' || $post->post_type == 'searchad') && $new_status == 'publish' && $old_status !== 'publish' ) {
    update_post_meta($post->ID, '_firstTimeApproved', $post->post_modified);
    // var_dump($post->post_type);
    // var_dump(get_post_meta($post->ID, '_firstTimeApproved', true));
  }
}
add_action( 'transition_post_status', 'run_on_publish_post_only', 10, 3 );

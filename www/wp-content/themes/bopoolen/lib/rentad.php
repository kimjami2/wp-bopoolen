<?php

require_once "ads-common.php";
modifiable_post_types("rentad");

// Registers new custom post type
add_action( 'init', 'custom_rent_ad', 0 );
function custom_rent_ad() {
  register_post_type( 'rentad',
    array(
      'labels' => array(
        'name' => __( 'Uthyrning Ads' ),
        'singular_name' => __( 'uthyrning' ),
        'add_new' => __( 'Add New Ad' ),
        'add_new_item' => __( 'Add New Ad' ),
        'edit_item' => __( 'Edit Ad' ),
        'new_item' => __( 'Add New Ad' ),
        'view_item' => __( 'View Ad' ),
        'search_items' => __( 'Search Ad' ),
        'not_found' => __( 'No ads found' ),
        'not_found_in_trash' => __( 'No ads found in trash' )
      ),
      'menu_icon'   => 'dashicons-format-image',
      'public' => true,
      'supports' => array( 'title', 'editor', 'revisions', 'thumbnail', ),
      'capability_type' => 'post',
      'rewrite' => array("slug" => "rentad"), // Permalinks format
      'register_meta_box_cb' => 'add_rent_ad_custom_fields',
    )
  );
}

// Add the Meta Boxes
add_action( 'add_meta_boxes', 'add_rent_ad_custom_fields' );
function add_rent_ad_custom_fields() {
  add_meta_box('rent_ad_custom_fields_creator', 'Skapare', 'rent_ad_custom_fields_creator', 'rentad', 'normal', 'default');
  add_meta_box('rent_ad_custom_fields_eng', 'Engelskinformation', 'rent_ad_custom_fields_eng', 'rentad', 'normal', 'default');
  add_meta_box('rent_ad_custom_fields', 'Uthyresinformation', 'rent_ad_custom_fields', 'rentad', 'normal', 'default');
  add_meta_box('rent_ad_custom_fields2', 'Info om boendet', 'rent_ad_custom_fields2', 'rentad', 'normal', 'default');
  add_meta_box('rent_ad_custom_inactive_ad', 'Inaktiverad annons', 'rent_ad_custom_inactive_ad', 'rentad', 'normal', 'default');
  add_meta_box('rent_ad_custom_deleting_ad', 'Raderad annons', 'rent_ad_custom_deleting_ad', 'rentad', 'normal', 'default');
  add_meta_box('rent_ad_custom_internal_comment', 'Intern kommentar', 'rent_ad_custom_internal_comment', 'rentad', 'normal', 'default');
  add_meta_box('rent_ad_custom_rejected_comment', 'Avvisningskommentar', 'rent_ad_custom_rejected_comment', 'rentad', 'normal', 'default');

  global $post;
  add_modified_base_info_box_if_required($post, 'rentad');
}

// The Metabox
function rent_ad_custom_fields_creator() {
  global $post;

  // Noncename needed to verify where the data originated
  echo '<style>input[type="checkbox"] { margin-left: 10px; }</style>';
  echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
  wp_create_nonce( plugin_basename(__FILE__) ) . '" />';


  echo '<h4>'.__('Personnummer', 'bopoolen').':</h4>';
  echo '<input type="text" disabled name="" value="' . get_the_author_meta( '_securitynumber', $post->post_author )  . '" placeholder="Personnummer" class="widefat" />';

}

function rent_ad_custom_fields_eng() {
  global $post;

  // Noncename needed to verify where the data originated
  echo '<style>input[type="checkbox"] { margin-left: 10px; }</style>';
  echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
  wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

  $titleEng         = get_post_meta($post->ID, '_titleeng', true);
  $descriptionEng   = get_post_meta($post->ID, '_descriptioneng', true);

  echo '<h4>'.__('Titel på engelska', 'bopoolen').':</h4>';
  echo '<input type="text" name="_titleeng" value="' . $titleEng  . '" placeholder="Titel på engelska" class="widefat" />';

  echo '<h4>'.__('Beskrivning på engelska', 'bopoolen').':</h4>';
  $settings = array( 'media_buttons' => false );
  wp_editor( $descriptionEng, '_descriptioneng', $settings );

  $current_values = array(
      array(
          'key' => '_titleeng',
          'value' => $titleEng,
          'header' => __('Titel på engelska', 'bopoolen'),
      ),
      array(
          'key' => '_descriptioneng',
          'value' => $descriptionEng,
          'header' => __('Beskrivning på engelska', 'bopoolen'),
      ),
  );

  $modified_info = get_changed_meta_values($current_values);
  render_changed_values_list($modified_info);
}


function rent_ad_custom_fields() {
  global $post;

  // Noncename needed to verify where the data originated
  echo '<style>input[type="checkbox"] { margin-left: 10px; }</style>';
  echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
  wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

  // Get the data if its already been entered
  $rentprice        = get_post_meta($post->ID, '_rentprice', true);
  $reasonableRent   = get_post_meta($post->ID, '_reasonableRent', true);
  $availablefrom    = get_post_meta($post->ID, '_availablefrom', true);
  $availableto      = get_post_meta($post->ID, '_availableto', true);
  $streetaddress    = get_post_meta($post->ID, '_streetaddress', true);
  $postalcode       = get_post_meta($post->ID, '_postalcode', true);
  $city             = get_post_meta($post->ID, '_city', true);
  $roominhouse      = get_post_meta($post->ID, '_roominhouse', true);
  $roominapartment  = get_post_meta($post->ID, '_roominapartment', true);
  $roominstudent    = get_post_meta($post->ID, '_roominstudent', true);
  $ownhouse         = get_post_meta($post->ID, '_ownhouse', true);
  $ownapartment     = get_post_meta($post->ID, '_ownapartment', true);
  $contractsecond   = get_post_meta($post->ID, '_contractsecond', true);
  $contractfirst    = get_post_meta($post->ID, '_contractfirst', true);
  $contractchange   = get_post_meta($post->ID, '_contractchange', true);
  $latlng           = get_post_meta($post->ID, '_latlng', true);

  // Echo out the field
  echo '<h4>'.__('Hyra', 'bopoolen').':</h4>';
  echo '<input type="text" name="_rentprice" value="' . $rentprice  . '" placeholder="6000kr" class="widefat" />';
  echo '<input type="checkbox" '.$reasonableRent.' name="_reasonableRent" class="widefat"> Intygar skälig hyra';

  echo '<h4>'.__('Tillgänglit från', 'bopoolen').':</h4>';
  echo '<input type="text" name="_availablefrom" value="' . $availablefrom  . '" placeholder="2015-01-01" class="widefat" />';

  echo '<h4>'.__('Tillgänglit till', 'bopoolen').':</h4>';
  echo '<input type="text" name="_availableto" value="' . $availableto  . '" placeholder="2015-01-01" class="widefat" />';

  echo '<h4>'.__('Gatuadress', 'bopoolen').':</h4>';
  echo '<input type="text" name="_streetaddress" value="' . $streetaddress  . '" placeholder="Stortorget 9" class="widefat" />';

  echo '<h4>'.__('Postnummer', 'bopoolen').':</h4>';
  echo '<input type="text" name="_postalcode" value="' . $postalcode  . '" placeholder="211 22" class="widefat" />';

  echo '<h4>'.__('Ort', 'bopoolen').':</h4>';
  echo '<input type="text" name="_city" value="' . $city  . '" placeholder="Malmö" class="widefat" />';

  echo '<h4>'.__('Latitude & Longitude', 'bopoolen').'</h4>';
  echo '<input type="text" name="_latlng" value="' . $latlng  . '" placeholder="1.2345678, 1,2345678" class="widefat" />';


  echo '<h4>'.__('Typ av boende', 'bopoolen').':</h4>';
  echo '<input type="checkbox" name="_roominhouse" id="_roominhouse" value="yes" class="widefat" '.$roominhouse.'/> Rum i hus';
  echo '<input type="checkbox" name="_roominapartment" id="_roominapartment" value="yes" class="widefat" '.$roominapartment.'/> Rum i lägenhet';
  echo '<input type="checkbox" name="_roominstudent" id="_roominstudent" value="yes" class="widefat" '.$roominstudent.'/> Rum i studentkorridor';
  echo '<input type="checkbox" name="_ownhouse" id="_ownhouse" value="yes" class="widefat" '.$ownhouse.'/> Eget hus';
  echo '<input type="checkbox" name="_ownapartment" id="_ownapartment" value="yes" class="widefat" '.$ownapartment.'/> Egen lägenhet';

  echo '<h4>'.__('Typ av hyresavtal', 'bopoolen').':</h4>';
  echo '<input type="checkbox" name="_contractsecond" id="_contractsecond" value="yes" class="widefat" '.$contractsecond.'/> Andrahand';
  echo '<input type="checkbox" name="_contractfirst" id="_contractfirst" value="yes" class="widefat" '.$contractfirst.'/> Förstahand';
  echo '<input type="checkbox" name="_contractchange" id="_contractchange" value="yes" class="widefat" '.$contractchange.'/> Bytes';

  $current_values = array(
      array('key' => '_rentprice', 'value' => $rentprice, 'header' => __('Hyra', 'bopoolen')),
      array('key' => '_availablefrom', 'value' => $availablefrom, 'header' => __('Tillgänglit från', 'bopoolen')),
      array('key' => '_availableto', 'value' => $availableto, 'header' => __('Tillgänglit till', 'bopoolen')),
      array('key' => '_streetaddress', 'value' => $streetaddress, 'header' => __('Gatuadress', 'bopoolen')),
      array('key' => '_postalcode', 'value' => $postalcode, 'header' => __('Postnummer', 'bopoolen')),
      array('key' => '_city', 'value' => $city, 'header' => __('Ort', 'bopoolen')),
      array('key' => '_latlng', 'value' => $latlng, 'header' => __('Latitude & Longitude', 'bopoolen')),

      array('key' => '_roominhouse', 'value' => $roominhouse, 'header' => __('Rum i hus', 'bopoolen')),
      array('key' => '_roominapartment', 'value' => $roominapartment, 'header' => __('Rum i lägenhet', 'bopoolen')),
      array('key' => '_roominstudent', 'value' => $roominstudent, 'header' => __('Rum i studentkorridor', 'bopoolen')),
      array('key' => '_ownhouse', 'value' => $ownhouse, 'header' => __('Eget hus', 'bopoolen')),
      array('key' => '_ownapartment', 'value' => $ownapartment, 'header' => __('Egen lägenhet', 'bopoolen')),

      array('key' => '_contractfirst', 'value' => $contractfirst, 'header' => __('Förstahand', 'bopoolen')),
      array('key' => '_contractsecond', 'value' => $contractsecond, 'header' => __('Andrahand', 'bopoolen')),
      array('key' => '_contractchange', 'value' => $contractchange, 'header' => __('Bytes', 'bopoolen')),
  );

  $modified_info = get_changed_meta_values($current_values);
  render_changed_values_list($modified_info);
}

// The Metabox
function rent_ad_custom_fields2() {
  global $post;

  // Noncename needed to verify where the data originated
  echo '<style>input[type="checkbox"] { margin-left: 10px; }</style>';
  echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
  wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

  // Get the data if its already been entered
  $availableyta       = get_post_meta($post->ID, '_availableyta', true);
  $availablegemensam  = get_post_meta($post->ID, '_availablegemensam', true);
  $numberofrooms      = get_post_meta($post->ID, '_numberofrooms', true);
  $includedel         = get_post_meta($post->ID, '_includedel', true);
  $includedinternet   = get_post_meta($post->ID, '_includedinternet', true);
  $includedfurniture  = get_post_meta($post->ID, '_includedfurniture', true);
  $includedtoilet     = get_post_meta($post->ID, '_includedtoilet', true);
  $includedbath       = get_post_meta($post->ID, '_includedbath', true);
  $includedanimal     = get_post_meta($post->ID, '_includedanimal', true);
  $includedhandicap   = get_post_meta($post->ID, '_includedhandicap', true);
  $includeddishwasher = get_post_meta($post->ID, '_includeddishwasher', true);
  $includedwasher     = get_post_meta($post->ID, '_includedwasher', true);
  $includedsmoking    = get_post_meta($post->ID, '_includedsmoking', true);
  $includedpentry     = get_post_meta($post->ID, '_includedpentry', true);
  $includedkitchen    = get_post_meta($post->ID, '_includedkitchen', true);
  $includedentrance   = get_post_meta($post->ID, '_includedentrance', true);
  $imgIds             = explode(',', get_post_meta($post->ID, '_imageids', true));

  // Echo out the field
  echo '<h4>'.__('Boendeyta egen yta', 'bopoolen').' (m2):</h4>';
  echo '<input type="text" name="_availableyta" value="' . $availableyta  . '" placeholder="60m2" class="widefat" />';

  echo '<h4>'.__('Boendeyta  Gemensamma tillgängliga ytor', 'bopoolen').' (m2):</h4>';
  echo '<input type="text" name="_availablegemensam" value="' . $availablegemensam  . '" placeholder="120m2" class="widefat" />';

  echo '<h4>'.__('Antal rum', 'bopoolen').':</h4>';
  echo '<input type="text" name="_numberofrooms" value="' . $numberofrooms  . '" placeholder="6st" class="widefat" />';

  echo '<h4>'.__('Ingår i hyran', 'bopoolen').':</h4>';
  echo '<input type="checkbox" name="_includedel" id="_includedel" value="yes" class="widefat" '.$includedel.'/> '.__('El');
  echo '<input type="checkbox" name="_includedinternet" id="_includedinternet" value="yes" class="widefat" '.$includedinternet.'/> '.__('Internet');
  echo '<input type="checkbox" name="_includedfurniture" id="_includedfurniture" value="yes" class="widefat" '.$includedfurniture.'/> '.__('Möblerat');
  echo '<input type="checkbox" name="_includedtoilet" id="_includedtoilet" value="yes" class="widefat" '.$includedtoilet.'/> '.__('Egen toalett');
  echo '<input type="checkbox" name="_includedbath" id="_includedbath" value="yes" class="widefat" '.$includedbath.'/> '.__('Eget badrum');
  echo '<input type="checkbox" name="_includedanimal" id="_includedanimal" value="yes" class="widefat" '.$includedanimal.'/> '.__('Husdjur');
  echo '<br><br>';
  echo '<input type="checkbox" name="_includedhandicap" id="_includedhandicap" value="yes" class="widefat" '.$includedhandicap.'/> '.__('Handikappvänligt');
  echo '<input type="checkbox" name="_includeddishwasher" id="_includeddishwasher" value="yes" class="widefat" '.$includeddishwasher.'/> '.__('Diskmaskin');
  echo '<input type="checkbox" name="_includedwasher" id="_includedwasher" value="yes" class="widefat" '.$includedwasher.'/> '.__('Tvättmaskin');
  echo '<input type="checkbox" name="_includedsmoking" id="_includedsmoking" value="yes" class="widefat" '.$includedsmoking.'/> '.__('Rökning tillåten');
  echo '<input type="checkbox" name="_includedpentry" id="_includedpentry" value="yes" class="widefat" '.$includedpentry.'/> '.__('Pentry');
  echo '<input type="checkbox" name="_includedkitchen" id="_includedkitchen" value="yes" class="widefat" '.$includedkitchen.'/> '.__('Kök');
  echo '<input type="checkbox" name="_includedentrance" id="_includedentrance" value="yes" class="widefat" '.$includedentrance.'/> '.__('Egen ingång');

  echo '<h4>'.__('Bild-idn kopplade till annonsen', 'bopoolen').':</h4>';
  foreach ($imgIds as $value) {
    echo '<img src="'.wp_get_attachment_url( $value ).'" class="" alt="" height="50" width="50">';
    echo ' ';
  }

  $current_values = array(
      array('key' => '_availableyta', 'value' => $availableyta, 'header' => __('Boendeyta egen yta', 'bopoolen')),
      array('key' => '_availablegemensam', 'value' => $availablegemensam, 'header' => __('Boendeyta  Gemensamma tillgängliga ytor', 'bopoolen')),
      array('key' => '_numberofrooms', 'value' => $numberofrooms, 'header' => __('Antal rum', 'bopoolen')),

      array('key' => '_includedel', 'value' => $includedel, 'header' => __('El', 'bopoolen')),
      array('key' => '_includedinternet', 'value' => $includedinternet, 'header' => __('Internet', 'bopoolen')),
      array('key' => '_includedfurniture', 'value' => $includedfurniture, 'header' => __('Möblerat', 'bopoolen')),
      array('key' => '_includedtoilet', 'value' => $includedtoilet, 'header' => __('Egen toalett', 'bopoolen')),
      array('key' => '_includedbath', 'value' => $includedbath, 'header' => __('Eget badrum', 'bopoolen')),
      array('key' => '_includedanimal', 'value' => $includedanimal, 'header' => __('Husdjur', 'bopoolen')),
      array('key' => '_includedhandicap', 'value' => $includedhandicap, 'header' => __('Handikappvänligt', 'bopoolen')),
      array('key' => '_includeddishwasher', 'value' => $includeddishwasher, 'header' => __('Diskmaskin', 'bopoolen')),
      array('key' => '_includedwasher', 'value' => $includedwasher, 'header' => __('Tvättmaskin', 'bopoolen')),
      array('key' => '_includedsmoking', 'value' => $includedsmoking, 'header' => __('Rökning tillåten', 'bopoolen')),
      array('key' => '_includedpentry', 'value' => $includedpentry, 'header' => __('Pentry', 'bopoolen')),
      array('key' => '_includedentrance', 'value' => $includedentrance, 'header' => __('Egen ingång', 'bopoolen')),
  );

  $modified_info = get_changed_meta_values($current_values);
  render_changed_values_list($modified_info);
}

// The field for inactive ad
function rent_ad_custom_inactive_ad() {
  global $post;

  // Noncename needed to verify where the data originated
  echo '<style>input[type="checkbox"] { margin-left: 10px; }</style>';
  echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
  wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

  // Get the data if its already been entered
  $inactivecomment    = get_post_meta($post->ID, '_inactivecomment', true);

  // Get which radiobutton is clicked
  $inactivereason     = get_post_meta($post->ID, '_inactivereason', true);

  // Get other reason comment if inactivereason = other
  if($inactivereason == 'other') {
    $inactivereasoncomment = get_post_meta($post->ID, '_inactivereasoncomment', true);
  }

  // Echo out the field
  echo '<h4>'.__('Kommentar vid inaktivering utav annons', 'bopoolen').':</h4>';
  echo '<textarea rows="5" name="_inactivecomment" class="widefat" style="width:100%;resize:vertical;">' . $inactivecomment  . '</textarea>';
  echo '<h4>'.__('Anledning till inaktivering utav annons', 'bopoolen').':</h4>';

  // Echo out the radio buttons
  echo '<input type="radio" class="radiobutton" name="_inactivereason" value="found_tenant"'.checkValue('found_tenant', $inactivereason).'>'.__('Hittat hyresgäst', 'bopoolen').'<br>';
  echo '<input type="radio" class="radiobutton" name="_inactivereason" value="unavailable"'.checkValue('unavailable', $inactivereason).'>'.__('Vill inte längra hyra ut', 'bopoolen').'<br>';
  echo '<input type="radio" class="radiobutton" name="_inactivereason" value="other"'.checkValue('other', $inactivereason).'>'.__('Annan anledning' ,'bopoolen').'<br>';

  // Echo out the inactive reason other text field
  echo '<h4>'.__('Kommentar vid "Annan anledning" ovan.', 'bopoolen').':</h4>';
  echo '<textarea rows="5" name="_inactivereasoncomment" class="widefat" style="width:100%;resize:vertical;">' . $inactivereasoncomment  . '</textarea>';
}

function checkValue($string, $reason) {
  global $post;

  if($string == $reason) {
    return 'checked="checked"';
  }
}

// The field for deleting ad
function rent_ad_custom_deleting_ad() {
  global $post;

  // Noncename needed to verify where the data originated
  echo '<style>input[type="checkbox"] { margin-left: 10px; }</style>';
  echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
  wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

  // Get the data if its already been entered
  $deletecomment    = get_post_meta($post->ID, '_deletecomment', true);

  // Get which radiobutton is clicked
  $deletereason     = get_post_meta($post->ID, '_deletereason', true);

  // Get other reason comment if inactivereason = other
  if($deletereason == 'other') {
    $deletereasoncomment = get_post_meta($post->ID, '_deletereasoncomment', true);
  }

  // Echo out the field
  echo '<h4>'.__('Kommentar vid raderad annons', 'bopoolen').':</h4>';
  echo '<textarea rows="5" name="_deletecomment" class="widefat" style="width:100%;resize:vertical;">' . $deletecomment  . '</textarea>';
  echo '<h4>'.__('Anledning till radering utav annons', 'bopoolen').':</h4>';

  // Echo out the radio buttons
  echo '<input type="radio" class="radiobutton" name="_deletereason" value="found_tenant"'.checkValue('found_tenant', $deletereason).'>'.__('Hittat hyresgäst', 'bopoolen').'<br>';
  echo '<input type="radio" class="radiobutton" name="_deletereason" value="unavailable"'.checkValue('unavailable', $deletereason).'>'.__('Vill inte längra hyra ut', 'bopoolen').'<br>';
  echo '<input type="radio" class="radiobutton" name="_deletereason" value="other"'.checkValue('other', $deletereason).'>'.__('Annan anledning' ,'bopoolen').'<br>';

  // Echo out the inactive reason other text field
  echo '<h4>'.__('Kommentar vid "Annan anledning" ovan.', 'bopoolen').':</h4>';
  echo '<textarea rows="5" name="_deletereasoncomment" class="widefat" style="width:100%;resize:vertical;">' . $deletereasoncomment  . '</textarea>';
}

// The internal comment field
function rent_ad_custom_internal_comment() {
  global $post;

  // Noncename needed to verify where the data originated
  echo '<style>input[type="checkbox"] { margin-left: 10px; }</style>';
  echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
  wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

  // Get the data if its already been entered
  $internalcomment    = get_post_meta($post->ID, '_internalcomment', true);

  // Echo out the field
  echo '<h4>'.__('Kommentar (endast för bopoolen):', 'bopoolen').'</h4>';
  echo '<textarea rows="5" name="_internalcomment" class="widefat" style="width:100%;resize:vertical;">' . $internalcomment  . '</textarea>';

}


// The rejected comment field SV
function rent_ad_custom_rejected_comment() {
  global $post;

  // Noncename needed to verify where the data originated
  echo '<style>input[type="checkbox"] { margin-left: 10px; }</style>';
  echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
  wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
  $settings = array( 'media_buttons' => false, 'wpautop' => false );

  // Get the data if its already been entered
  $rejectedcommentSV    = get_post_meta($post->ID, '_rejectedcommentsv', true);

  // Echo out the field
  echo '<h4>'.__('Meddelande till annonsör varför hens annons avvisas på svenska:', 'bopoolen').'</h4>';
  wp_editor( $rejectedcommentSV, '_rejectedcommentsv', $settings );

  // Get the data if its already been entered
  $rejectedcommentEN    = get_post_meta($post->ID, '_rejectedcommenten', true);

  // Echo out the field
  echo '<h4>'.__('Meddelande till annonsör varför hens annons avvisas på engelska:', 'bopoolen').'</h4>';
  wp_editor( $rejectedcommentEN, '_rejectedcommenten', $settings );

}



/**
  * Save the Metabox Data
  * @param $post_id -
  * @param $post -
  */
function save_rent_ad_custom_fields($post_id, $post) {

  if ( !wp_verify_nonce( $_POST['eventmeta_noncename'], plugin_basename(__FILE__) )) {
  return $post->ID;
  }

  // Is the user allowed to edit the post or page?
  if ( !current_user_can( 'edit_post', $post->ID ))
    return $post->ID;


    // We'll put it into an array to make it easier to loop though.
    $events_meta['_titleeng']           = $_POST['_titleeng'];
    $events_meta['_descriptioneng']     = $_POST['_descriptioneng'];
    $events_meta['_rentprice']          = $_POST['_rentprice'];
    $events_meta['_reasonableRent']     = $_POST['_reasonableRent'];
    $events_meta['_availablefrom']      = $_POST['_availablefrom'];
    $events_meta['_availableto']        = $_POST['_availableto'];
    $events_meta['_streetaddress']      = $_POST['_streetaddress'];
    $events_meta['_postalcode']         = $_POST['_postalcode'];
    $events_meta['_city']               = $_POST['_city'];
    $events_meta['_roominhouse']        = (isset( $_POST[ '_roominhouse' ]) ? 'checked' : '');
    $events_meta['_roominapartment']    = (isset( $_POST[ '_roominapartment' ]) ? 'checked' : '');
    $events_meta['_roominstudent']      = (isset( $_POST[ '_roominstudent' ]) ? 'checked' : '');
    $events_meta['_ownhouse']           = (isset( $_POST[ '_ownhouse' ]) ? 'checked' : '');
    $events_meta['_ownapartment']       = (isset( $_POST[ '_ownapartment' ]) ? 'checked' : '');
    $events_meta['_contractsecond']     = (isset( $_POST[ '_contractsecond' ]) ? 'checked' : '');
    $events_meta['_contractfirst']      = (isset( $_POST[ '_contractfirst' ]) ? 'checked' : '');
    $events_meta['_contractchange']     = (isset( $_POST[ '_contractchange' ]) ? 'checked' : '');
    $events_meta['_latlng']             = $_POST['_latlng'];

    $events_meta['_availableyta']       = $_POST['_availableyta'];
    $events_meta['_availablegemensam']  = $_POST['_availablegemensam'];
    $events_meta['_numberofrooms']      = $_POST['_numberofrooms'];
    $events_meta['_includedel']         = (isset( $_POST[ '_includedel' ]) ? 'checked' : '');
    $events_meta['_includedinternet']   = (isset( $_POST[ '_includedinternet' ]) ? 'checked' : '');
    $events_meta['_includedfurniture']  = (isset( $_POST[ '_includedfurniture' ]) ? 'checked' : '');
    $events_meta['_includedtoilet']     = (isset( $_POST[ '_includedtoilet' ]) ? 'checked' : '');
    $events_meta['_includedbath']       = (isset( $_POST[ '_includedbath' ]) ? 'checked' : '');
    $events_meta['_includedanimal']     = (isset( $_POST[ '_includedanimal' ]) ? 'checked' : '');
    $events_meta['_includedhandicap']   = (isset( $_POST[ '_includedhandicap' ]) ? 'checked' : '');
    $events_meta['_includeddishwasher'] = (isset( $_POST[ '_includeddishwasher' ]) ? 'checked' : '');
    $events_meta['_includedwasher']     = (isset( $_POST[ '_includedwasher' ]) ? 'checked' : '');
    $events_meta['_includedsmoking']    = (isset( $_POST[ '_includedsmoking' ]) ? 'checked' : '');
    $events_meta['_includedpentry']     = (isset( $_POST[ '_includedpentry' ]) ? 'checked' : '');
    $events_meta['_includedentrance']   = (isset( $_POST[ '_includedentrance' ]) ? 'checked' : '');

    $events_meta['_inactivecomment']    = $_POST['_inactivecomment'];
    $events_meta['_inactivereason']     = $_POST['_inactivereason'];
    if($_POST['_inactivereason'] === 'other') {
      $events_meta['_inactivereasoncomment']  = $_POST['_inactivereasoncomment'];
    } else {
      delete_post_meta($post->ID, '_inactivereasoncomment');
    }
    $events_meta['_deletecomment']      = $_POST['_deletecomment'];
    $events_meta['_deletereason']       = $_POST['_deletereason'];
    if($_POST['_deletereason'] === 'other') {
      $events_meta['_deletereasoncomment'] = $_POST['_deletereasoncomment'];
    } else {
      delete_post_meta($post->ID, '_deletereasoncomment');
    }
    $events_meta['_internalcomment']    = $_POST['_internalcomment'];
    $events_meta['_rejectedcommentsv']    = $_POST['_rejectedcommentsv'];
    $events_meta['_rejectedcommenten']    = $_POST['_rejectedcommenten'];

    // Add values of $events_meta as custom fields
    foreach ($events_meta as $key => $value) { // Cycle through the $events_meta array!
      if( $post->post_type == 'revision' ) return; // Don't store custom data twice
      $value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
      if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
        update_post_meta($post->ID, $key, $value);
      } else { // If the custom field doesn't have a value
        add_post_meta($post->ID, $key, $value);
      }
      if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
    }

    if ($post->post_status === "publish") {
        delete_post_meta($post->ID, '_deletecomment');
        delete_post_meta($post->ID, '_deletereason');
        delete_post_meta($post->ID, '_deletereasoncomment');
        delete_post_meta($post->ID, '_inactivecomment');
        delete_post_meta($post->ID, '_inactivereason');
        delete_post_meta($post->ID, '_inactivereasoncomment');
        delete_post_meta($post->ID, '_bop_oldvalues');
        delete_post_meta($post->ID, '_emailreminder');
    } else if ($post->post_status === "draft") {
        delete_post_meta($post->ID, '_deletecomment');
        delete_post_meta($post->ID, '_deletereason');
        delete_post_meta($post->ID, '_deletereasoncomment');
    } else if ($post->post_status === "raderad") {
        delete_post_meta($post->ID, '_inactivecomment');
        delete_post_meta($post->ID, '_inactivereason');
        delete_post_meta($post->ID, '_inactivereasoncomment');
    }

}

/*add_action('publish_to_trash', 'trash_rent_ad_custom_fields', 1, 2);
add_action('draft_to_trash',   'trash_rent_ad_custom_fields', 1, 2);
add_action('future_to_trash',  'trash_rent_ad_custom_fields', 1, 2);

function my_func() {
  die('trashed post');
}*/

add_action('save_post', 'save_rent_ad_custom_fields', 1, 2); // save the custom fields

//State that wp-post-meta-revisions should save our field
function add_meta_keys_to_revision( $keys ) {
  $keys[] = '_rentprice';
  return $keys;
}
add_filter( 'wp_post_revision_meta_keys', 'add_meta_keys_to_revision' );

//For some reason (havent looked into it) the value is in a array, just pick it out
function extrev_field( $value, $field ) {

  $test = $value[0];
  return $test;
}
add_filter( '_wp_post_revision_field_rev_meta', 'extrev_field', 10, 2 );

add_action( 'transition_post_status', 'rent_ad_post_transition_status', 10, 3 );

function rent_ad_post_transition_status( $new_status, $old_status, $post ){
  // var_dump($new_status);
  // var_dump($old_status);
  if ( 'rejected' != $new_status && ('rejected' == $new_status && 'rejected' == $old_status) )
    return;

  if ( 'rentad' != $post->post_type )
    return; // restrict the filter to a specific post type

  if('rejected' != $new_status) {
    return;
  }

  $rejectedcommentsv    = get_post_meta($post->ID, '_rejectedcommentsv', true);
  $rejectedcommenten    = get_post_meta($post->ID, '_rejectedcommenten', true);

  if(trim($rejectedcommentsv) == '' && trim($rejectedcommenten) == '')
    return;
  $author_info = get_userdata($post->post_author);
  $en_title = get_post_meta($post->id, '_titleeng', true);
  $rentad = $post;

  $replace_values['NAME'] = $author_info->first_name. ' ' . $author_info->last_name;
  $replace_values['SV_TITLE'] = $post->post_title;
  $replace_values['SV_COMMENT'] = $rejectedcommentsv;
  $replace_values['EN_COMMENT'] = $rejectedcommenten;
  $replace_values['EN_TITLE'] = $en_title;

  $args=array(
  'post_type' => 'email_template',
  'post_status' => 'publish',
  'posts_per_page' => -1);

  $query = null;
  $query = new WP_Query($args);

  if( $query->have_posts() ) {
    foreach ($query->posts as $post) {
      if($post->post_title == 'Rejected Ad' && $post->post_status == 'publish'){
        $emailTemplate = $post->post_content;
      }
    }
  }
  wp_reset_query();  // Restore global


  if(isset($emailTemplate)){
    // echo "<pre>" . htmlentities(print_r($emailTemplate, 1)) . "</pre>";
    // echo "<pre>" . htmlentities(print_r($rejectedcommentsv, 1)) . "</pre>";
    // echo "<pre>" . htmlentities(print_r($rejectedcommenten, 1)) . "</pre>";

    $pattern = '!\{\{(\w+)\}\}!';

    /*$content = str_replace("{{NAME}}",$author_info->first_name.' '.$author_info->last_name,$emailTemplate);
    $content = str_replace("{{SV_TITLE}}",$rentad->post_title,$content);
    $content = str_replace("{{SV_COMMENT}}",$rejectedcommentsv,$content);
    $content = str_replace("{{EN_TITLE}}",$en_title,$content);
    $content = str_replace("{{EN_COMMENT}}",$rejectedcommenten,$content);*/
    $content = preg_replace_callback($pattern, function ($matches) use ($replace_values) {
      return $replace_values[$matches[1]];
    }, $emailTemplate);

    // do something awesome

    wp_mail( $author_info->user_email, get_post_meta($post->ID, '_email_subject', true).' '.$rentad->post_title, $content);
  }

}

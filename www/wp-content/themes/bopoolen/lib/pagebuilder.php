<?php

// Add custom attributes to row settings
function add_row_attributes($fields) {

  // Page section checkbox
  $fields['page_section'] = array(
    'name' => __('Page Section', 'siteorigin-panels'),
    'type' => 'checkbox',
    'group' => 'attributes',
    'description' => __('Check this if the row is a page section', 'siteorigin-panels'),
    'priority' => 1,
  );

  // Section style dropdown
  $fields['section_style'] = array(
    'name' => __('Section style', 'siteorigin-panels'),
    'type' => 'select',
    'options' => array(
      '' => __('None', 'siteorigin-panels'),
      'primary' => __('Primary', 'siteorigin-panels'),
      'secondary' => __('Secondary', 'siteorigin-panels'),
      'white' => __('White', 'siteorigin-panels'),
      'dark' => __('Dark', 'siteorigin-panels'),
      'fancy' => __('Fancy', 'siteorigin-panels'),
    ),
    'group' => 'attributes',
    'priority' => 2,
  );

  // Centered content checkbox
  $fields['centered_section'] = array(
    'name' => __('Center content', 'siteorigin-panels'),
    'type' => 'checkbox',
    'group' => 'attributes',
    'description' => __('Check this if the content should be centered', 'siteorigin-panels'),
    'priority' => 3,
  );

  return $fields;
}
add_filter('siteorigin_panels_row_style_fields', 'add_row_attributes');

// Add custom row classes
function add_row_classes($classes, $panels_data) {

  // Page section
  if ($panels_data['style']['page_section']) {
    array_push($classes, 'page-section');
  }

  // Section style
  if ($panels_data['style']['section_style']) {
    array_push($classes, $panels_data['style']['section_style']);
  }

  // Centered content
  if ($panels_data['style']['centered_section']) {
    array_push($classes, 'centered');
  }

  return $classes;
}
add_filter('siteorigin_panels_row_classes', 'add_row_classes', 10, 2);


// Add button style presets
function add_button_styles($presets){
  $presets['default'] = array();
  $presets['primary'] = array();
  $presets['secondary'] = array();
  return $presets;
}
add_filter('origin_widget_presets_button_simple', 'add_button_styles');

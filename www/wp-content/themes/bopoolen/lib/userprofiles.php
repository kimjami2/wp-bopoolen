<?php
add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );

function my_show_extra_profile_fields($user) { ?>

	<br><hr>
	<h3><?php _e('User Information:', 'bopoolen') ?></h3>
	<table class="form-table">
		<tr>
			<th><label for="_username"><?php _e('User name', 'bopoolen') ?>:</label></th>
			<td><input type="text" name="_username" id="username" value="<?php echo esc_attr( get_the_author_meta( '_username', $user->ID ) ); ?>" class="regular-text" /><br /></td>
		</tr>
		<tr>
      <th><label for="_securitynumber"><?php _e('Profile image:', 'bopoolen') ?></label></th>
			<td><input type="text" name="_profilepicture" id="profilepicture" value="<?php echo esc_attr( get_the_author_meta( '_profilepicture', $user->ID ) ); ?>" class="regular-text" /><br /></td>
    </tr>
    <tr>
      <th><label for="_securitynumber"><?php _e('Social security number:', 'bopoolen') ?></label></th>
			<td><input type="text" name="_securitynumber" id="securitynumber" value="<?php echo esc_attr( get_the_author_meta( '_securitynumber', $user->ID ) ); ?>" class="regular-text" /><br /></td>
    </tr>
    <tr>
      <th><label for="_phone"><?php _e('Phone number:', 'bopoolen') ?></label></th>
			<td><input type="text" name="_phone" id="phone" value="<?php echo esc_attr( get_the_author_meta( '_phone', $user->ID ) ); ?>" class="regular-text" /><br /><a href="//personer.eniro.se/resultat/<?php echo esc_attr( get_the_author_meta( '_phone', $user->ID ) ); ?>" target="_blank">Länk till Eniro</a></td>
    </tr>
		<tr>
      <th><label for="_viewname"><?php _e('Display my name in ads:', 'bopoolen') ?></label></th>
			<td><input type="checkbox" name="_viewname" id="viewname" value="checked" class="regular-text" style="width:0;" <?php echo esc_attr( get_the_author_meta( '_viewname', $user->ID ) ); ?>/><br /></td>
    </tr>
		<tr>
      <th><label for="_viewphone"><?php _e('Display my name in ads:', 'bopoolen') ?></label></th>
			<td><input type="checkbox" name="_viewphone" id="viewphone" value="checked" class="regular-text" style="width:0;" <?php echo esc_attr( get_the_author_meta( '_viewphone', $user->ID ) ); ?>/><br /></td>
    </tr>
	</table>
	<br><hr>
	<h3><?php _e('Intern kommentar:') ?></h3>
	<table class="form-table">
		<tr>
      <th><label for="_blacklist"><?php _e('Blacklisted:', 'bopoolen') ?></label></th>
			<td><input type="checkbox" name="_blacklist" id="blacklist" value="checked" class="regular-text" style="width:0;" <?php echo esc_attr( get_the_author_meta( '_blacklist', $user->ID ) ); ?>/><br /></td>
    </tr>
		<tr>
      <th><label for="_complaint"><?php _e('Complaints:', 'bopoolen') ?></label></th>
			<td><input type="checkbox" name="_complaint" id="complaint" value="checked" class="regular-text" style="width:0;" <?php echo esc_attr( get_the_author_meta( '_complaint', $user->ID ) ); ?>/><br /></td>
    </tr>
		<tr>
      <th><label for="_usercomment"><?php _e('Comments:', 'bopoolen') ?></label></th>
			<td><textarea rows="5" cols="10" name="_usercomment" class="regular-text" style="resize:vertical;"><?php echo esc_attr( get_the_author_meta( '_usercomment', $user->ID ) ); ?></textarea></td>
    </tr>
	</table>
<?php }


add_action( 'personal_options_update', 'save_user_custom_fields' );
add_action( 'edit_user_profile_update', 'save_user_custom_fields' );


// save custom user fields
function save_user_custom_fields( $user_id ) {
		// check if checkbox is checked or not
		if (isset($_POST['_viewname'])) {
			update_user_meta( $user_id,'_viewname', sanitize_text_field( $_POST['_viewname'] ) );
		} else {
			update_user_meta( $user_id,'_viewname', sanitize_text_field( '' ) );
		}
		// check if checkbox is checked or not
		if (isset($_POST['_viewphone'])) {
			update_user_meta( $user_id,'_viewphone', sanitize_text_field( $_POST['_viewphone'] ) );
		} else {
			update_user_meta( $user_id,'_viewphone', sanitize_text_field( '' ) );
		}
		// check if checkbox is checked or not
		if (isset($_POST['_blacklist'])) {
			update_user_meta( $user_id,'_blacklist', sanitize_text_field( $_POST['_blacklist'] ) );
		} else {
			update_user_meta( $user_id,'_blacklist', sanitize_text_field( '' ) );
		}
		// check if checkbox is checked or not
		if (isset($_POST['_complaint'])) {
			update_user_meta( $user_id,'_complaint', sanitize_text_field( $_POST['_complaint'] ) );
		} else {
			update_user_meta( $user_id,'_complaint', sanitize_text_field( '' ) );
		}

		// update user fields
    update_user_meta( $user_id,'_securitynumber', sanitize_text_field( $_POST['_securitynumber'] ) );
    update_user_meta( $user_id,'_phone', sanitize_text_field( $_POST['_phone'] ) );
		update_user_meta( $user_id,'_username', sanitize_text_field( $_POST['_username'] ) );
		update_user_meta( $user_id,'_usercomment', sanitize_text_field( $_POST['_usercomment'] ) );
		update_user_meta( $user_id,'_profilepicture', sanitize_text_field( $_POST['_profilepicture'] ) );
}

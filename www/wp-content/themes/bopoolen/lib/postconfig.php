<?php

// remove auto trash delete
function my_remove_schedule_delete() {
  remove_action( 'wp_scheduled_delete', 'wp_scheduled_delete' );
}
add_action( 'init', 'my_remove_schedule_delete' );


// // create new post status for 'avvisade' ads
// register_post_status( 'avvisad', array(
// 	'label'                     => _x( 'Avvisad', 'post' ),
// 	'public'                    => true,
// 	'exclude_from_search'       => false,
// 	'show_in_admin_all_list'    => true,
// 	'show_in_admin_status_list' => true,
// 	'label_count'               => _n_noop( 'Avvisade <span class="count">(%s)</span>', 'Avvisade <span class="count">(%s)</span>' ),
// ) );
//
// add_action( 'post_submitbox_misc_actions', 'my_post_submitbox_misc_actions' );
// function my_post_submitbox_misc_actions(){
//
// global $post;
//
// //only when editing a post
// if( $post->post_type == 'rentad'){
// 		// custom post status: avvisad
// 		$complete = '';
// 		$label = '';
//
// 		if( $post->post_status == 'avvisad' ){
// 		    $complete = ' selected=\"selected\"';
// 		    $label = '<span id=\"post-status-display\"> Avvisad</span>';
// 		}
//
// 		echo '<script>
// 		jQuery(document).ready(function($){
// 		    $("select#post_status").append("<option value=\"avvisad\" '.$complete.'>Avvisad</option>");
// 		    $(".misc-pub-section label").append("'.$label.'");
// 		});
// 		</script>';
// 	}
// }
//
//

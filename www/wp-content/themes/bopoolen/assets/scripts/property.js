"use strict";

var Searchad = function(data) {
  this.searchad = data;
  this.init();
}

Searchad.prototype = {
  init: function() {

  },

  seperateAreas : function(areas) {
    let areasText = '';

    areas = String(areas);
    var areasArray = areas.split(",");

    //console.log('array', areasArray);
    areasArray.forEach(function(area) {
      areasText += area.charAt(0).toUpperCase() + area.slice(1) + ', ';
    });

    return areasText.slice(0,-2);
  },

  seperateContracts: function(contract) {
    let contractText = '';

    contract = String(contract);
    var contractArray = contract.split(",");

    contractArray.forEach(function(contract) {
        if(document.getElementById('langCode').value == 'en') {
          if (contract == 'contractfirst') {
            contract = 'Tenancy agreement';
          } else if (contract == 'contractsecond') {
            contract = 'Sublease agreement';
          } else if (contract == 'contractchange') {
            contract = 'Exchange';
          } else if (contract == 'roominhouse') {
            contract = 'Room in house';
          } else if (contract == 'roominapartment') {
            contract = 'Room in apartment';
          } else if (contract == 'roominstudent') {
            contract = 'Room in student housing';
          } else if (contract == 'ownhouse') {
            contract = 'House';
          } else if (contract == 'ownapartment') {
            contract = 'Apartment';
          }
        } else {
          if (contract == 'contractfirst') {
            contract = 'Förstahand';
          } else if (contract == 'contractsecond') {
            contract = 'Andrahand';
          } else if (contract == 'contractchange') {
            contract = 'Byte';
          } else if (contract == 'roominhouse') {
            contract = 'Rum i hus';
          } else if (contract == 'roominapartment') {
            contract = 'Rum i lägenhet';
          } else if (contract == 'roominstudent') {
            contract = 'Rum i studentboende';
          } else if (contract == 'ownhouse') {
            contract = 'Hus';
          } else if (contract == 'ownapartment') {
            contract = 'Lägenhet';
          }
        }

      contractText += contract + ', ';
    });

    return contractText.slice(0,-2);
  },

  getTableMarkup: function() {

    // markup
    let markup = '',
        type,
        self = this;

    this.searchad.forEach(function(property) {
      var availableto = (property.custom_fields._availableto == 'Tillsvidare' ? 'Tillsvidare' : property.custom_fields._availableto);
      if(document.getElementById('langCode').value == 'en') {
        if (availableto == 'Tillsvidare') {
          availableto = 'Until further notice';
        }
        var title = property.custom_fields._titleeng;

        if (!property.custom_fields._titleeng) {
          title = property.post.post_title;
        }
        markup +=
          '<tr class="listing-property" data-id="' + property.post.ID + '">' +
          '<td width="30%"><a href="' + property.post.guid + '" class="link-primary"><b>' + title + '</b></a></td>' +
          '<td width="17%">' + self.seperateAreas(property.custom_fields._area) + '</td>' +
          '<td width="17%">' + self.seperateContracts(property.custom_fields._accommodationType) + '</td>' +
          '<td width="17%">' + self.seperateContracts(property.custom_fields._contractType) + '</td>' +
          '<td width="12%">' + property.custom_fields._availablefrom + '</td>' +
          '<td width="12%">' + availableto + '</td>' +
          '</tr>';
      } else {
        markup +=
          '<tr class="listing-property" data-id="' + property.post.ID + '">' +
          '<td width="30%"><a href="' + property.post.guid + '" class="link-primary"><b>' + property.post.post_title + '</b></a></td>' +
          '<td width="17%">' + self.seperateAreas(property.custom_fields._area) + '</td>' +
          '<td width="17%">' + self.seperateContracts(property.custom_fields._accommodationType) + '</td>' +
          '<td width="17%">' + self.seperateContracts(property.custom_fields._contractType) + '</td>' +
          '<td width="12%">' + property.custom_fields._availablefrom + '</td>' +
          '<td width="12%">' + availableto + '</td>' +
          '</tr>';
      }
    });
    return markup;
  }

}

var Properties = function(properties) {

  // store our properties object
  this.properties = properties;

  // init
  this.init();
};

Properties.prototype = {

  init: function() {

    // check if property is set
    if(this.properties == null) {
      throw new TypeError('Properties.init(): no property inserted');
      return;
    }
  },

  getPropertyType: function(property) {

    // check if we got our property
    if(property == null || typeof property === 'undefined' || typeof property !== 'object') {
      throw new TypeError('Properties.getPropertyType(): no property inserted');
      return;
    }

    // loop through the object

    for(var type in property.custom_fields) {

      switch(type) {
        case '_roominhouse':
          return {icon: '<i class="fa fa-home"></i>', title: 'Rum i hus'};
        break;
        case '_roominapartment':
          return {icon: '<i class="fa fa-building"></i>', title: 'Rum i lägenhet'};
        break;
        case '_roominstudent':
          return {icon: '<i class="fa fa-building"></i>', title: 'Rum i studentlägenhet'};
        break;
        case '_ownhouse':
          return {icon: '<i class="fa fa-home"></i>', title: 'Rum i hus'};
        break;
        case '_ownapartment':
          return {icon: '<i class="fa fa-building"></i>', title: 'Lägenhet'};
        break;
        case '_contractType':
              if(type == 'contractfirst') {
                return {contract: "Förstahandskontrakt"};
              }
        break;
        default:
          // do nothing
      }

    }

  },

  getContractType: function(property) {
    // check if we got our property
    if(property == null || typeof property === 'undefined' || typeof property !== 'object') {
      throw new TypeError('Properties.getPropertyType(): no property inserted');
      return;
    }
    var contract = property.custom_fields._contractType;
    if(document.getElementById('langCode').value == 'en') {
      if (contract == 'contractfirst') {
        return "Tenancy agreement";
      } else if (contract == 'contractsecond') {
        return "Sublease agreement";
      } else if (contract == 'contractchange') {
        return "Exchange";
      }
    } else {
      if (contract == 'contractfirst') {
        return "Förstahandskontrakt";
      } else if (contract == 'contractsecond') {
        return "Andrahandskontrakt";
      } else if (contract == 'contractchange') {
        return "Utbytes";
      }
    }
  },

  /*
  * Generate HTML markup for table display
  */
  getTableMarkup: function() {

    // markup
    let markup,
        type,
        self = this;

    this.properties.forEach(function(property) {

      // get property type
      property.type = self.getPropertyType(property);
      property.contract = self.getContractType(property);
      var availableto = (property.custom_fields._availableto == 'Tillsvidare' ? 'Tillsvidare' : property.custom_fields._availableto);
      var numberofrooms = typeof property.custom_fields._numberofrooms === 'undefined' ? '' : property.custom_fields._numberofrooms;

      if(document.getElementById('langCode').value == 'en') {
        if(availableto == 'Tillsvidare') {
          availableto = 'Until further notice';
        }
        var title = property.custom_fields._titleeng;

        if(!property.custom_fields._titleeng) {
          title = property.post.post_title;
        }
        markup +=
          '<tr class="listing-property" data-id="'+ property.post.ID +'">' +
          '<td width="30%"><div class="col-xs-1">' + property.type.icon + '</div><div class="col-md-10"><a href="'+property.post.guid+'" class="link-primary"><b>' + title + '</a></b></div></td>' +
          '<td width="8%">' + property.custom_fields._city + '</td>' +
          '<td width="11%">' + property.contract + '</td>' +
          '<td width="10%">' + property.custom_fields._availableyta + ' m<sup>2</sup></td>' +
          '<td width="12%">' + property.custom_fields._availablefrom + '</td>' +
          '<td width="12%">' + availableto + '</td>' +
          '<td width="10%">' + property.custom_fields._rentprice + 'kr</td>' +
          '<td width="10%"><a class="btn btn-button-table btn-sm" href="'+property.post.guid+'" target="_blank">View ad</a></td>' +
          '</tr>';
      } else {
        markup +=
          '<tr class="listing-property" data-id="'+ property.post.ID +'">' +
          '<td width="30%"><div class="col-xs-1">' + property.type.icon + '</div><div class="col-md-10"><a href="'+property.post.guid+'" class="link-primary"><b>' + property.post.post_title + '</a></b></div></td>' +
          '<td width="8%">' + property.custom_fields._city + '</td>' +
          '<td width="11%">' + property.contract + '</td>' +
          '<td width="10%">' + property.custom_fields._availableyta + ' m<sup>2</sup></td>' +
          '<td width="12%">' + property.custom_fields._availablefrom + '</td>' +
          '<td width="12%">' + property.custom_fields._availableto + '</td>' +
          '<td width="10%">' + property.custom_fields._rentprice + 'kr</td>' +
          '<td width="10%"><a class="btn btn-button-table btn-sm" href="'+property.post.guid+'" target="_blank">Visa annons</a></td>' +
          '</tr>';
      }
    });
    return markup;
  }

};

/* ========================================================================
* DOM-based Routing
* Based on http://goo.gl/EUTi53 by Paul Irish
*
* Only fires on body classes that match. If a body class contains a dash,
* replace the dash with an underscore when adding it to the object below.
*
* .noConflict()
* The routing is enclosed within an anonymous function so that you can
* always reference jQuery with $, even when in .noConflict() mode.
* ======================================================================== */
Dropzone.autoDiscover = false;
(function($) {

	// Use this variable to set up the common and page specific functions. If you
	// rename this variable, you will also need to rename the namespace below.
	var Sage = {

		// Get nonce
		getNonce: function(ctrl, method) {
			return $.ajax({
				url: '/api/get_nonce/?controller='+ctrl+'&method='+method,
				type: 'GET'
			});
		},

		checkSecNumberExists: function(value){
			return $.ajax({
				url: '/api/user/check_security_number_exists/?nbr='+value,
				type: 'GET'
			});
		},


		checkValidTitleOrDescription: function(idsString) {
			var scrollToId = '';
			var ids = idsString.split(',');
			var titleIsNotCorrect = false;
			var titleIDs = '';
			$(ids[0]).parent().parent().removeClass('has-error');
			$(ids[1]).parent().parent().removeClass('has-error');
			titleIDs += $(ids[0]).val() !== '' ? ids[0]+' ' : '';
			titleIDs += $(ids[1]).val() !== '' ? ids[1] : '';
			titleIDs = titleIDs.split(' ').filter(function(e){return e;}).length === 0 ? idsString : titleIDs.split(' ').filter(function(e){return e;})[0];
			$(titleIDs).each(function(){
				if(/[a-zåäöA-ZÅÄÖ]/i.test($(this).val())) {
					$(this).parent().parent().removeClass('has-error');
				} else {
					$(this).parent().parent().addClass('has-error');
					titleIsNotCorrect = true;
					scrollToId = scrollToId === '' ? $(this).parent().parent() : scrollToId;
				}
			});

			if(titleIsNotCorrect){
				$("html, body").animate({ scrollTop: ($(scrollToId).offset().top - 150) }, "fast");
				return false;
			}
			else {
				return true;
			}
		},

		checkValidLeasingPeriod: function(idsString){
			var scrollToId = '';
			var selectIsNotCorrect = false;
			var dateFieldsIsNotCorret = false;
			var accommodationTypeDataIsNotCorrect = false;
			//#contractType
			$(idsString).each(function(){
				var parent = $(this).parents('.form-group')[0];
				if(this.value.trim() !== '') {
					$(parent).removeClass('has-error');
					if(this.id === 'timespan'){
						$('#availablefrom, #availableto, #availablefromInfinity').each(function(){
							var parent = $(this).parents('.form-group')[0];
							if($(this).parent().parent().hasClass('visible')){
								if(/^[0-9]{4}[\-]{1}[0-9]{2}[\-]{1}[0-9]{2}$/.test($(this).val()) ) {
									$(parent).removeClass('has-error');
								} else {
									$(parent).addClass('has-error');
									dateFieldsIsNotCorret = true;
									scrollToId = scrollToId === '' ? parent : scrollToId;
								}
							}
						});
					}
					if(this.id === 'accommodationType' && !dateFieldsIsNotCorret){
						// var accommodationTypeIDs = '#availableytaOwn, #availablegemensam, #numberofrooms, #availableyta';
						var accommodationTypeIDs = '#availableytaOwn, #numberofrooms, #availableyta';
						$(accommodationTypeIDs).each(function(){
							var parent = $(this).parents('.form-group')[0];
							var container = $(this).parents('.accomedation-type')[0];
							if($(container).hasClass('show')){
								if($(this).val().replace(/[^0-9]/g,'') > 0) {
									$(parent).removeClass('has-error');
								} else {
									$(parent).addClass('has-error');
									accommodationTypeDataIsNotCorrect = true;
									scrollToId = scrollToId === '' ? parent : scrollToId;
								}
							}
						});
					}
          if(this.id === 'area' && this.value == '') {
            $(this).parent().addClass('has-error');
            $(this).parent().parent().parent().find('label[for="area"]').addClass('has-error');
            accommodationTypeDataIsNotCorrect = true;
          } else {
            $(this).parent().removeClass('has-error');
            $(this).parent().parent().parent().find('label[for="area"]').removeClass('has-error');
          }
          if(this.id === 'contractType' && this.value == '') {
            $(this).parent().addClass('has-error');
            $(this).parent().parent().parent().find('label[for="contractType"]').addClass('has-error');
            accommodationTypeDataIsNotCorrect = true;
          } else {
            $(this).parent().removeClass('has-error');
            $(this).parent().parent().parent().find('label[for="contractType"]').removeClass('has-error');
          }
				} else {
					$(parent).addClass('has-error');
					selectIsNotCorrect = true;
					scrollToId = scrollToId === '' ? parent : scrollToId;
				}
			});

			if(selectIsNotCorrect || accommodationTypeDataIsNotCorrect){
				var yPos = typeof scrollToId === 'string' ? $('#'+scrollToId).offset().top : $(scrollToId).offset().top;
				$("html, body").animate({ scrollTop: (yPos - 150) }, "fast");
				return false;
			}
			else {
				return true;
			}
		},

		checkFormCreateUpdateSearchAd: function(){

      var plusScroll = 150, scrollToId = '';

			if(!Sage.checkValidTitleOrDescription('#title,#titleeng') ){
				return false;
			}
			if(!Sage.checkValidLeasingPeriod('#timespan')){
				return false;
			}
			if(!Sage.checkValidTitleOrDescription('#description,#descriptioneng')){
				return false;
			}

			if($('#accommodationType').val() !== ''){
				$('#accommodationType').parent().parent().parent().removeClass('has-error');
			} else {
				$('#accommodationType').parent().parent().parent().addClass('has-error');
				$("html, body").animate({ scrollTop: ($('#accommodationType').parent().parent().parent().offset().top - plusScroll) }, "fast");
				return false;
			}
		},

		checkFormCreateUpdateRentAd: function(){
			var plusScroll = 150, scrollToId = '';
			if(!Sage.checkValidTitleOrDescription('#title,#title-eng')){
				return false;
			}

			if(!Sage.checkValidLeasingPeriod('#timespan')){
				return false;
			}

			if(/^[a-zåäöA-ZÅÄÖ0-9 \-:/.]{1,}$/.test($('#street').val())) {
				$('#street').parent().parent().parent().parent().removeClass('has-error');
			} else {
				$('#street').parent().parent().parent().parent().addClass('has-error');
				$("html, body").animate({ scrollTop: ($('#street').parent().parent().parent().parent().offset().top - plusScroll) }, "fast");
				return false;
			}

			if(/^[0-9]{3}(\s|[-])?[0-9]{2}$/.test($('#zip').val())) {
				$('#zip').parent().parent().parent().parent().removeClass('has-error');
			} else {
				$('#zip').parent().parent().parent().parent().addClass('has-error');
				$("html, body").animate({ scrollTop: ($('#zip').parent().parent().parent().parent().offset().top - plusScroll) }, "fast");
				return false;
			}

			if(/^[a-zöåäA-ZÅÄÖ]{1}[a-zöåäA-ZÅÄÖ \-]{1,}$/.test($('#city').val())) {
				$('#city').parent().parent().parent().parent().removeClass('has-error');
			} else {
				$('#city').parent().parent().parent().parent().addClass('has-error');
				$("html, body").animate({ scrollTop: ($('#city').parent().parent().parent().parent().offset().top - plusScroll) }, "fast");
				return false;
			}

      if(!Sage.checkValidLeasingPeriod('#area')){
        return false;
      }
			if(!Sage.checkValidLeasingPeriod('#accommodationType')){
				return false;
			}
      if(!Sage.checkValidLeasingPeriod('#contractType')){
        return false;
      }
			if(!Sage.checkValidTitleOrDescription('#description,#description-eng')){
				return false;
			}

			if($('#rent').val().replace(/[^0-9]/g,'') > 0) {
				$('#rent').parent().parent().removeClass('has-error');
				$('#rent').val($('#rent').val().replace(/[^0-9]/g,''));
			} else {
				$('#rent').parent().parent().addClass('has-error');
				$("html, body").animate({ scrollTop: ($('#rent').parent().parent().offset().top - plusScroll) }, "fast");
				return false;
			}

			if($('#descriptionReasonEraseOther').length === 0 && $('#descriptionReasonInaktiv').length === 0){
				if($('#reasonableRent').is(':checked')) {
					$('#reasonableRent').parent().parent().removeClass('has-error');
				} else {
					$('#reasonableRent').parent().parent().addClass('has-error');
					$("html, body").animate({ scrollTop: ($('#reasonableRent').parent().parent().parent().parent().offset().top - plusScroll) }, "fast");
					return false;
				}
			}
			else {
				var description = '';
				var descriptionOther = '';
				var radioName = '';

				if($('#descriptionReasonEraseOther').length > 0 ){
					description = '#descriptionErase';
					descriptionOther = '#descriptionReasonEraseOther';
					radioName = '_deletereason';
				}
				else {
					description = '#descriptionInactive';
					descriptionOther = '#descriptionReasonInaktiv';
					radioName = '_inactivereason';
				}

				if(/[a-zåäöA-ZÅÄÖ]/i.test($(description).val())) {
					$(description).parent().parent().removeClass('has-error');
				} else {
					$(description).parent().parent().addClass('has-error');
					$("html, body").animate({ scrollTop: ($(description).parent().parent().offset().top - plusScroll) }, "fast");
					return false;
				}

				if($('input[name="'+radioName+'"]:checked').length > 0) {
					$('input[name="'+radioName+'"]').parent().parent().removeClass('has-error');

					if($('input[name="'+radioName+'"]:checked')[0].id === 'other'){
						if(/[a-zåäöA-ZÅÄÖ]/i.test($(descriptionOther).val())) {
							$(descriptionOther).parent().parent().removeClass('has-error');
						} else {
							$(descriptionOther).parent().parent().addClass('has-error');
							$("html, body").animate({ scrollTop: ($(descriptionOther).parent().parent().offset().top - plusScroll) }, "fast");
							return false;
						}
					}
				} else {
					$('input[name="'+radioName+'"]').parent().parent().addClass('has-error');
					$("html, body").animate({ scrollTop: ($('input[name="'+radioName+'"]').parent().parent().offset().top - plusScroll) }, "fast");
					return false;
				}
			}
		},

		// All pages
		'common': {
			init: function() {
				$( '.toggle' ).click(function() {
					var toggleClass = $(this).data('toggle');
					if($(toggleClass).hasClass('open')){
						$(toggleClass).removeClass('open');
					}
					else{
						$(toggleClass).addClass('open');
					}
				});
        $('.hide-menu').css('display','none');
        $('.click-menu').prepend('<i class="fa fa-plus-circle" aria-hidden="true"></i> ').on('click', function() {
          if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this)[0].childNodes[0].className = 'fa fa-plus-circle';
            $(this).next().slideUp();
          } else {
            $(this).addClass('active');
            $(this)[0].childNodes[0].className = 'fa fa-minus-circle';
            $(this).next().slideDown();
          }
        });

        if($('.blocksida-panels').length >0) {
          var panels = $('.content').find('.panel-widget-style');

          for(var i = 0; i < panels.length; i++) {
            var panel = $(panels[i]);
            if($(panel).css('background-image') !== '') {
              var image = $(panel).css('background-image');
              $(panel).parent().css('background-image', image);
              $(panel).parent().css('background-size', 'cover');
              $(panel).css('background-image', '');
            }
          }

          if($('.information').length > 0) {
            for(var j = 0; j < $('.information').length; j++) {
              var information = $('.information')[j];
              $(information).parent().css('background','#f9f9f9');
              $(information).parent().css('box-shadow','none');
              $(information).parent().css('height','auto');
              $(information).parent().css('padding','0');
              $(information).parent().css('padding-top','15px');
            }
          }
        }


				if($('.SortingTable').length !== 0) {

					var table = $('.SortingTable').DataTable();
					if($('#langCode').val() === 'sv') {
						var search = $('input[type=search]').parent();
						for(var i = 1; i < search.length; i++) {
							search[i].childNodes[0].textContent = "Sök:";
						}
						var show = $('.dataTables_length');
						for(i = 0; i < show.length; i++) {
							show[i].childNodes[0].childNodes[0].textContent = "Visa ";
							show[i].childNodes[0].childNodes[2].textContent = " poster";
						}
						var info = $('.dataTables_info');
						for(i = 0; i < info.length; i++) {
              var text = info[i].innerText;
							text = text.replace('Showing', 'Visar');
              text = text.replace('to', 'till');
              text = text.replace('of', 'av');
              text = text.replace('entries', 'poster');
              info[i].innerText = text;
						}
            $('.previous').text('Föregående');
            $('.next').text('Nästa');
					}

          table.on('draw', function() {
            if($('#langCode').val() === 'sv') {
              var search = $('input[type=search]').parent();
              for(var i = 1; i < search.length; i++) {
                search[i].childNodes[0].textContent = "Sök:";
              }
              var show = $('.dataTables_length');
              for(i = 0; i < show.length; i++) {
                show[i].childNodes[0].childNodes[0].textContent = "Visa ";
                show[i].childNodes[0].childNodes[2].textContent = " poster";
              }
              var info = $('.dataTables_info');
              for(i = 0; i < info.length; i++) {
                var text = info[i].innerText;
                text = text.replace('Showing', 'Visar');
                text = text.replace('to', 'till');
                text = text.replace('of', 'av');
                text = text.replace('entries', 'poster');
                info[i].innerText = text;
              }
              $('.previous').text('Föregående');
              $('.next').text('Nästa');
            }
          });
				}


        $('.paginate_button').on('click', '.page', function() {
          if($('#langCode').val() === 'sv') {
            var search = $('input[type=search]').parent();
            for(var i = 1; i < search.length; i++) {
              search[i].childNodes[0].textContent = "Sök:";
            }
            var show = $('.dataTables_length');
            for(i = 0; i < show.length; i++) {
              show[i].childNodes[0].childNodes[0].textContent = "Visa ";
              show[i].childNodes[0].childNodes[2].textContent = " poster";
            }
            var info = $('.dataTables_info');
            for(i = 0; i < info.length; i++) {
              var text = info[i].innerText;
              text = text.replace('Showing', 'Visar');
              text = text.replace('to', 'till');
              text = text.replace('of', 'av');
              text = text.replace('entries', 'poster');
              info[i].innerText = text;
            }
            $('.previous').text('Föregående');
            $('.next').text('Nästa');
          }
        });

				$('.search-submit').click(function() {
					if($('.search-field').css('display') !== 'none' &&
						$('.search-field').val() !== '') {
						$('#search-form').submit();
					} else if ($('.search-field').css('display') !== 'none' ||
						$('.search-field').css('display') !== 'inline-block') {
						$('.search-field').toggle('slide');
					}
				});
				$(document).keypress(function(e) {
					if(e.which === 13 && $('.search-field').val() !== '' &&
						$('.search-field').css('display') !== 'none' ) {
						$('#search-form').submit();
					}
				});

				$('[data-toggle="tooltip"]').tooltip();

				$("#listing-table").stupidtable();



				$.fn.datepicker.dates.sv = {
					days: ["Söndag", "Måndag", "Tisdag", "Onsdag", "Torsdag", "Fredag", "Lördag"],
					daysShort: ["Sön", "Mån", "Tis", "Ons", "Tor", "Fre", "Lör"],
					daysMin: ["Sö", "Må", "Ti", "On", "To", "Fr", "Lö"],
					months: ["Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September", "October", "November", "December"],
					monthsShort: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
					today: "Idag",
					clear: "Rensa",
					format: "yyyy-mm-dd",
					titleFormat: "yyyy MM", /* Leverages same syntax as 'format' */
					weekStart: 1
				};
				$('#availabilityDate input').datepicker({
					language: 'sv',
					orientation: 'bottom'
				});
				$('#availabilityDateFrom input').datepicker({
					language: 'sv',
					orientation: 'bottom'
				});
				$('#availabilityDateFromTo input').datepicker({
					language: 'sv',
					orientation: 'bottom'
				});
				$('#search-from').datepicker({
					language: 'sv',
					orientation: 'bottom'
				});
				$('#search-to').datepicker({
					language: 'sv',
					orientation: 'bottom'
				});
				$('#availablefromInfinity').on('changeDate', function(ev){
			    $(this).datepicker('hide');
				});
				$('#availablefrom').on('changeDate', function(ev){
			    $(this).datepicker('hide');
				});
				$('#availableto').on('changeDate', function(ev){
			    $(this).datepicker('hide');
				});

				$('#accommodationType').change(function () {
					$('.accomedation-type').removeClass('show');
					if ($(this).val() && $(this).val().indexOf("own") >= 0){
						$('.accomedation-type.own').addClass('show');
					}
					else {
						$('.accomedation-type.roomin').addClass('show');
					}
				});

				$('#accommodationType.update_rentad').change();

				$('#timespan').change(function () {
					$('.accomedation-date').removeClass('visible');
					if ($(this).val() === '1'){
						$('.accomedation-date.infinity').addClass('visible');
					}
					if ($(this).val() === '2'){
						$('.accomedation-date.time').addClass('visible');
					}
				});

				$('#timespan.update_rentad').change();

				$('form button[type=submit]').on('click', function(e) {
					// e.preventDefault();
				});

				//Subscribe to an ad
				var star = $('.subscribe');
				var starText = $('.subscribe-text');
        var starTextValue;
				star.on('click', function() {
					if(star.hasClass('fa-star-o')) {
						starTextValue = starText.text();
						starText.fadeOut(400, function() {
							// DO AJAX CALL TO CHANGE VALUE OF SUBSCRIPTIONS

							if(starTextValue === 'Save ad') {
								starText.text('Saved!');
							} else {
								starText.text('Sparad!');
							}

							var post = $('#postId').val();

							var query = {
								'_subscription': post,
								'_function': 'add'
							};

							$.ajax({
								url: '/api/user/update_user_meta/',
								type: 'GET',
								data: query
							})
								.done(function(data) {
									starText.fadeIn(400, function() {});
									star.fadeIn(400, function() {});
									//console.log(data);
								})
								.fail(function(data) {
									starText.fadeIn(400, function() {});
									star.fadeIn(400, function() {});
									//console.log(data);
									//alert(data.status);
								});
						});
						star.fadeOut(400, function() {
							star.removeClass('fa-star-o');
							star.addClass('fa-star');
						});
					} else {
						star.fadeOut(400, function() {
							star.removeClass('fa-star');
							star.addClass('fa-star-o');
						});
						starTextValue = starText.text();
						starText.fadeOut(400, function() {
							if(starTextValue === 'Saved!') {
								starText.text('Save ad');
							} else {
								starText.text('Spara annons');
							}
							var post = $('#postId').val();

							var query = {
								'_subscription': post,
								'_function': 'remove'
							};

							$.ajax({
								url: '/api/user/update_user_meta/',
								type: 'GET',
								data: query
							})
								.done(function(data) {
									starText.fadeIn(400, function() {});
									star.fadeIn(400, function() {});
									//console.log(data);
								})
								.fail(function(data) {
									starText.fadeIn(400, function() {});
									star.fadeIn(400, function() {});
									// console.log(data);
									//alert(data.status);
								});
						});
					}
				});

				var subscribe = $('.emailSubscribe').on('click', function() {
					var args = [];
					var value;
					var text;

					if($('.emailSubText').text() === 'Subscribe to above selection!') {
						text = 'Saved!';
					} else {
						text = 'Sparad!';
					}

					$('.multiselect-container input:checkbox').each(function(index) {
						value = $(this).attr('value');
						if($(this).is(':checked')) {
							args.push(value);
						}
						//args[value] = $(this).is(':checked');
					});
					var emailsub = '';
					args.map(function(e) {
						emailsub += e+'/';
					});

					var query = {
						'_emailsub': emailsub
					};

					//add to subscription user meta
					$.ajax({
						url: '/api/user/update_user_meta/',
						type: 'GET',
						data: query
					})
						.done(function(data) {
							//console.log(data);
							$('.emailSubText').text(text);
						})
						.fail(function(data) {
							//console.log(data);
							$('.emailSubText').text(text);
							//alert(data.status);
						});
				});

				//Mark ad as mailed to
				var mail = $('.sendEmail');
				var mailText = $('.sendEmailText');
        var mailTextValue;
				mail.on('click', function() {
					if(mail.hasClass('fa-square-o')) {
						mailTextValue = mailText.text();
						mailText.fadeOut(400, function() {
							// DO AJAX CALL TO CHANGE VALUE OF SUBSCRIPTIONS

							if(mailTextValue === 'Reminder: Already emailed the landlord?') {
								mailText.text('Reminder: Already mailed the landlord!');
							} else {
								mailText.text('Påminnelse: Redan skickat mail!');
							}

							var post = $('#postId').val();

							var query = {
								'_emailed': post,
								'_function': 'add'
							};

							$.ajax({
								url: '/api/user/update_user_meta/',
								type: 'GET',
								data: query
							})
								.done(function(data) {
									mailText.fadeIn(400, function() {});
									mail.fadeIn(400, function() {});
									//console.log(data);
								})
								.fail(function(data) {
									mailText.fadeIn(400, function() {});
									mail.fadeIn(400, function() {});
									//console.log(data);
									//alert(data.status);
								});
						});
						mail.fadeOut(400, function() {
							mail.removeClass('fa-square-o');
							mail.addClass('fa-check-square-o');
						});
					} else {
						mail.fadeOut(400, function() {
							mail.removeClass('fa-check-square-o');
							mail.addClass('fa-square-o');
						});
						mailTextValue = mailText.text();
						mailText.fadeOut(400, function() {
							if(mailTextValue === 'Reminder: Already mailed the landlord!') {
								mailText.text('Reminder: Already emailed the landlord?');
							} else {
								mailText.text('Påminnelse: Har ni redan mailat annonsören?');
							}
							var post = $('#postId').val();

							var query = {
								'_emailed': post,
								'_function': 'remove'
							};

							$.ajax({
								url: '/api/user/update_user_meta/',
								type: 'GET',
								data: query
							})
								.done(function(data) {
									mailText.fadeIn(400, function() {});
									mail.fadeIn(400, function() {});
									//console.log(data);
								})
								.fail(function(data) {
									mailText.fadeIn(400, function() {});
									mail.fadeIn(400, function() {});
									// console.log(data);
									//alert(data.status);
								});
						});
					}
				});

				// multiselect
				$('.multi-select').multiselect({
					includeSelectAllOption: true,
					buttonText: function(options, select) {

						var countOptions = 0,
								selectElm = $(select).context;

						countOptions = $(selectElm).find('option').size();


						/*
						$(select)[0].find('option').each(function() {
							countOptions++;
						});
						*/

						if (options.length === 0) {
							if($('#langCode').val() === 'en') {
								return 'Choose';
							}
							return 'Välj';
						}
						else if(options.length === countOptions) {
							if($('#langCode').val() === 'en') {
                if(options.parent().attr('name') === 'province') {
                  return 'Choose location';
                } else if (options.parent().attr('name') === 'estate-type') {
                  return 'Choose type of housing';
                } else if (options.parent().attr('name') === 'deal-type') {
                  return 'Choose type of contract';
                }
								return 'All';
							}
              if(options.parent().attr('name') === 'province') {
                return 'Välj område';
              } else if (options.parent().attr('name') === 'estate-type') {
                return 'Välj boendeform';
              } else if (options.parent().attr('name') === 'deal-type') {
                return 'Välj kontrakttyp';
              } else {
                return 'Alla';
              }
						}
						else {
							var labels = [];
							options.each(function() {
								if ($(this).attr('label') !== undefined) {
									labels.push($(this).attr('label'));
								}
								else {
									labels.push($(this).html());
								}
							});
							return labels.join(', ') + '';
						}
					},
					selectAllText: ($('#langCode').val() === 'en' ? 'All' : 'Alla'),
					allSelectedText: ($('#langCode').val() === 'en' ? 'All' : 'Alla'),
					buttonWidth: '100%',
				});

				if ($("body").hasClass('redigera-annons-sokes')) {
					$(".multi-select").multiselect();
				} else if($("body").hasClass('profiles')) {
					if($('#loggedIn').val() === 'true') {
						if($('#multiselect').val() === 2) {
							$(".multi-select").multiselect('selectAll', false);
						} else {
							var subs = $('#subscriptions').val();

							subs = subs.split('/');

							subs.map(function(sub) {
								if(sub !== '') {
									$(".multi-select").multiselect('select', sub);
								}
							});
							$(".multi-select").multiselect();
						}
					}
				} else {
					$(".multi-select").multiselect('selectAll', false);
				}
		    	$(".multi-select").multiselect('updateButtonText');
			},
			finalize: function() {
				// JavaScript to be fired on all pages, after page specific JS is fired
			}
		},

    'information': {
      init: function() {
        var panels = $('.content').find('.panel-widget-style');

        for(var i = 0; i < panels.length; i++) {
          var panel = $(panels[i]);
          if($(panel).css('background-image') !== '') {
            var image = $(panel).css('background-image');
            $(panel).parent().css('background-image', image);
            $(panel).parent().css('background-size', 'cover');
            $(panel).css('background-image', '');
          }
        }

        if($('.information').length > 0) {
          for(var j = 1; j < $('.information').length; j++) {
            var information = $('.information')[j];
            $(information).parent().css('background','white');
            $(information).parent().css('box-shadow','none');
            $(information).parent().css('height','auto');
            $(information).parent().css('padding','0');
            $(information).parent().css('padding-top','15px');
          }
        }
      }
    },

		// Home page
		'home': {
			init: function() {
				//console.log('woooh');
				// Get nonce tooken
				Sage.getNonce('posts', 'create_post').done(function(data) {

					// store our nonce
					Sage.uthyresbostader.nonce = data.nonce;

					// show properties
					Bopoolen.showProperties();
				});

        $('#quick-search').on('click', function() {

          var args = {},
            value;

          args['max-rent'] = parseInt(25000);
          args['min-rent'] = parseInt(0);
          args['quick-search'] = true;

          $('.multiselect-container input:checkbox').each(function(index) {
            value = $(this).attr('value');
            args[value] = $(this).is(':checked');
          });
          var str = '?';
          var i = 0;
          for (var key in args) {
            if( i === 0 ) {
              str += key+'='+args[key];
            } else {
              str += '&'+key+'='+args[key];
            }
            i++;
          }
          window.location = '/uthyresbostader/'+str;
          //Bopoolen.showProperties(args);
        });
			},
			finalize: function() {
				// JavaScript to be fired on the home page, after the init JS
			}
		},

    'hitta_anvandare': {
      init: function() {
        $('#search-user-btn').on('click', function(event) {
          var search_value = $('#search-user').val();

          if(search_value.length > 0) {

            $('#search-user-btn').attr('disabled', true).html('Loading..');

            $.ajax({
              url: '/wp-admin/admin-ajax.php',
              type: 'GET',
              dataType: 'json',
              data: {
                'action': 'search_user',
                'search_value': search_value
              }
            }).done(function(data) {
              $('#search-user-btn').attr('disabled', false).html('Search');
              $('#search-user-results').text("");
              $('#search-user-results').append("<hr>");
              if($('#langCode').val() === 'sv') {
                $('#search-user-results').append("<b>Användarnamn | E-postadress</b>");
              } else {
                $('#search-user-results').append("<b>Username | Email</b>");
              }
              $('#search-user-results').append("<hr>");
              data.map(function(user) {
                var link;
                if(user.data.nickname === '') {
                  link = "<a href=/profiles/?user="+user.data.ID+">"+user.data.display_name+"</a>";
                } else {
                  link = "<a href=/profiles/?user="+user.data.ID+">"+user.data.nickname+"</a>";
                }
                var text = "<p>" + link + " | <a href='mailto:" + user.data.user_email + "'>"+  user.data.user_email + "</p><hr>";
                $('#search-user-results').append(text);
              });
            });
          } else {
          }
        });
      }
    },

		'min_profil': {
			init: function() {
				if($('#no-social-number').is(':checked')) {
					$('#user_securitynumber').attr('disabled',true);
				}

				$('#no-social-number').on('click', function() {
					if($(this).is(':checked')) {
						$('#user_securitynumber').attr('disabled', true);
					} else {
						$('#user_securitynumber').attr('disabled', false);
					}
				});

        var text,
          removeText;

				if($('#langCode').val() === 'sv') {
					text = 'Du kan inte ladda upp fler bilder.';
					removeText = 'Ta bort';
				} else {
					text = 'You can not upload any more files.';
					removeText = 'Remove file';
				}

        getProfilePicture();

        function getProfilePicture() {
          var post_id = $('#image-ids').val();
          var data = {
            'action': 'get_profile_picture',
            'post_id': post_id
          };

          $.ajax({
            url: '/wp-admin/admin-ajax.php',
            type: 'POST',
            data: data,
            dataType: 'json'
          })
            .done(function(imagesArray) {
              createImages(imagesArray);
            })
            .fail(function(response) {
              //console.log('getImagesArray() Failed');
              return null;
            });
        }

        function createImages(imagesArray) {
          $("div#dZUpload").dropzone({
            url: "/api/posts/upload_images/",
            autoProcessQueue: false,
            uploadMultiple: true,
            parallelUploads: 1,
            maxFiles: 1,
            acceptedFiles: 'image/*',
            addRemoveLinks: true,
            dictMaxFilesExceeded: text,
            dictRemoveFile: removeText,
            init: function() {
              var dzClosure = this; // Makes sure that 'this' is understood inside the functions below.
              var id;
              var button = false;
              var nbrOfFiles = 0;
              var changed = false;

              for (var i = 0; i < imagesArray.length; i++) {
                this.emit("addedfile", imagesArray[i]);
                this.createThumbnailFromUrl(imagesArray[i], imagesArray[i].url);
                this.emit("complete", imagesArray[i]);
                //existingIds.push(imagesArray[i].serverId);
                //ids.push(imagesArray[i].serverId);
                nbrOfFiles++;
                dzClosure.options.maxFiles--;
              }
              $('.create_user').on('click', function(e) {
                e.preventDefault();
                button = this;
                $('.alert-success.create_success').hide();
                if(validateName($('input[name="user_first_name"]').val())) {
                  $('input[name="user_first_name"]').parent().removeClass('has-error');
                } else {
                  $('input[name="user_first_name"]').parent().addClass('has-error');
                  $("html, body").animate({ scrollTop: 0 }, "fast");
                  return false;
                }

                if(validateName($('input[name="user_last_name"]').val())) {
                  $('input[name="user_last_name"]').parent().removeClass('has-error');
                } else {
                  $('input[name="user_last_name"]').parent().addClass('has-error');
                  $("html, body").animate({ scrollTop: 0 }, "fast");
                  return false;
                }
                if($('#user_securitynumber').val() === '' && !$('#no-social-number').is(':checked')){
                  $('#user_securitynumber').parent().addClass('has-error');
                  $("html, body").animate({ scrollTop: 0 }, "fast");
                  return false;
                }
                else {
                  $('#user_securitynumber').parent().removeClass('has-error');
                  $('#user_securitynumber').parent().removeClass('has-error-security-nbr');
                  $('.alert-danger.securitynumtaken').hide();
                }

                if(!$('#no-social-number').is(':checked')) {
                  if(/^([0-9]{8}[-]{1}[0-9]{4})$/.test($('#user_securitynumber').val())) {
                    if(!luhn_validate($('#user_securitynumber').val())) {
                      $('#user_securitynumber').parent().addClass('has-error-security-nbr');
                      $("html, body").animate({ scrollTop: 0 }, "fast");
                      return false;
                    } else {
                      $('#user_securitynumber').parent().removeClass('has-error-security-nbr');
                    }
                  } else {
                    $('#user_securitynumber').parent().addClass('has-error-security-nbr');
                    $("html, body").animate({ scrollTop: 0 }, "fast");
                    return false;
                  }
                }

                if(!/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{2,})/.test($('input[name="user_phone"]').val())) {
                  $('input[name="user_phone"]').parent().addClass('has-error');
                  $("html, body").animate({ scrollTop: 0 }, "fast");
                  return false;
                }
                else {
                  this.disabled = true;
                  if(!$('#no-social-number').is(':checked')) {
                    Sage.getNonce('user', 'check_security_number_exists').done(function(data) {
                      Sage.checkSecNumberExists($('#user_securitynumber').val())
                        .done(function(exists) {
                          if(!exists){
                            if(nbrOfFiles > 0){
                              if(changed) {
                                dzClosure.processQueue();
                              } else {
                                $('#create_user').submit();
                              }
                            }
                            else {
                              $('#image-ids').val('null');
                              button.form.submit();
                            }
                          }
                          else {
                            button.disabled = false;
                            $('#user_securitynumber').parent().addClass('has-error-security-nbr');
                            $('.alert-danger.securitynumtaken').show();
                            $("html, body").animate({ scrollTop: 0 }, "fast");
                          }
                        });
                    });
                  } else {
                    if(nbrOfFiles > 0){
                      if(changed) {
                        dzClosure.processQueue();
                      } else {
                        $('#create_user').submit();
                      }
                    }
                    else {
                      $('#image-ids').val('null');
                      $('#create_user').submit();
                    }
                  }
                }
              });

              this.on("addedfile", function (file, res) {
                //console.log('added');
                changed = true;
                nbrOfFiles++;
              });

              this.on("removedfile", function (file, res) {
                //console.log('removed');
                nbrOfFiles--;
                changed = true;
                dzClosure.options.maxFiles++;
              });

              this.on("success", function (file, res) {
                if(parseInt(res)){
                  id = res;
                }
              });

              this.on("queuecomplete", function (file) {
                $('#image-ids').val(id);
                if(button) {
                  button.form.submit();
                }
              });
            }
          });
        }
			}
		},

		'sokes_annonser': {
			init: function() {
				// Get nonce tooken
				Sage.getNonce('posts', 'create_post').done(function(data) {

					// store our nonce
					Sage.uthyresbostader.nonce = data.nonce;

					// show properties
					Bopoolen.showSearchAds();
				});

				$('#search-listings').on('click', function() {

					var args = {},
					value;

          if($('#search-text').val() !== '') {
            args['search-text'] = $('#search-text').val();
          }

					$('.multiselect-container input:checkbox').each(function(index) {
						value = $(this).attr('value');
						args[value] = $(this).is(':checked');
					});

          if($('#availablefromInfinity').val() !== '') {
            args.from = $('#availablefromInfinity').val();
          }

					Bopoolen.showSearchAds(args);
				});
			}
		},

		'uthyresbostader': {
			init: function() {

				// Get nonce tooken
				Sage.getNonce('posts', 'create_post').done(function(data) {
          var args = {},
            value;
					// store our nonce
					Sage.uthyresbostader.nonce = data.nonce;

          if($('#quick-search').val() === 'true') {

            args['max-rent'] = parseInt(25000);
            args['min-rent'] = parseInt(0);

            $('.multiselect-container input:checkbox').each(function(index) {
              if($(this)[0].defaultValue === 'multiselect-all' && $('#multiselect-allt').val() === 'true') {
                $(this).prop('checked', true);
              } else if($(this)[0].defaultValue === 'city-lund' && $('#city-lund').val() === 'true') {
                $(this).prop('checked', true);
              } else if($(this)[0].defaultValue === 'city-malmo' && $('#city-malmo').val() === 'true') {
                $(this).prop('checked', true);
              } else if($(this)[0].defaultValue === 'city-helsingborg' && $('#city-helsingborg').val() === 'true') {
                $(this).prop('checked', true);
              } else if($(this)[0].defaultValue === 'outside-lund' && $('#outside-lund').val() === 'true') {
                $(this).prop('checked', true);
              } else if($(this)[0].defaultValue === 'roominhouse' && $('#roominhouse').val() === 'true') {
                $(this).prop('checked', true);
              } else if($(this)[0].defaultValue === 'roominapartment' && $('#roominapartment').val() === 'true') {
                $(this).prop('checked', true);
              } else if($(this)[0].defaultValue === 'roominstudent' && $('#roominstudent').val() === 'true') {
                $(this).prop('checked', true);
              } else if($(this)[0].defaultValue === 'ownhouse' && $('#ownhouse').val() === 'true') {
                $(this).prop('checked', true);
              } else if($(this)[0].defaultValue === 'ownapartment' && $('#ownapartment').val() === 'true') {
                $(this).prop('checked', true);
              } else if($(this)[0].defaultValue === 'contractfirst' && $('#contractfirst').val() === 'true') {
                $(this).prop('checked', true);
              } else if($(this)[0].defaultValue === 'contractsecond' && $('#contractsecond').val() === 'true') {
                $(this).prop('checked', true);
              } else if($(this)[0].defaultValue === 'contractchange' && $('#contractchange').val() === 'true') {
                $(this).prop('checked', true);
              } else {
                $(this).prop('checked', false);
              }
              value = $(this).attr('value');
              args[value] = $(this).is(':checked');
            });
          } else {

            if($('#search-text').val() !== '') {
              args['search-text'] = $('#search-text').val();
            }

            args['max-rent'] = parseInt($('#value-input').html());
            args['min-rent'] = parseInt($('#value-span').html());

            $('.multiselect-container input:checkbox').each(function(index) {
              value = $(this).attr('value');
              args[value] = $(this).is(':checked');
            });
          }
          //args.page = 'uthyresbostader';
					// show properties
					Bopoolen.showProperties(args);
				});

				$('#more-filters').on('click', function() {
					$('.advanced-filters').toggle();
					if($('.advanced-filters').is(':visible')) {
						$('#more-filters').html('Mindre filter');
					} else {
						$('#more-filters').html('Fler filter');
					}
				});


				// Price filter
				var slider = document.getElementById('price-filter');
        if($('#quick-search').val() === 'true') {
          var max = $('#max-rent').val();
          var min = $('#min-rent').val();
          noUiSlider.create(slider, {
            start: [min, max],
            connect: true,
            step: 100,
            margin: 500,
            range: {
              'min': 0,
              'max': 25000
            }
          });
        } else {
          noUiSlider.create(slider, {
            start: [0, 20000],
            connect: true,
            step: 100,
            margin: 500,
            range: {
              'min': 0,
              'max': 25000
            }
          });
        }

				var valueInput = document.getElementById('value-input'),
				valueSpan = document.getElementById('value-span');

				// When the slider value changes, update the input and span
				slider.noUiSlider.on('update', function( values, handle ) {
					if (handle) {
						valueInput.innerHTML = values[handle].substring(0, values[handle].length - 3)+'kr';
					} else {
						valueSpan.innerHTML = values[handle].substring(0, values[handle].length - 3)+'kr';
					}
				});

				// When the input changes, set the slider value
				valueInput.addEventListener('change', function(){
					range.noUiSlider.set([null, this.value]);
				});

				$('.advance-filter-btn button').on('click', function() {

					// change button name
					if($('.advanced-filters').is(':visible')) {
						$(this).html('Mindre filter');
					} else {
						$(this).html('Fler filter');
					}

					$('.advanced-filters').toggle();
				});

				// search with filters

				$('#search-listings').on('click', function() {

					var args = {},
					value;

					if($('#search-text').val() !== '') {
						args['search-text'] = $('#search-text').val();
					}

					args['max-rent'] = parseInt($('#value-input').html());
					args['min-rent'] = parseInt($('#value-span').html());

					$('.multiselect-container input:checkbox').each(function(index) {
						value = $(this).attr('value');
						args[value] = $(this).is(':checked');
					});
          //args.page = 'uthyresbostader';

					Bopoolen.showProperties(args);

				});

			},
		},

		'skapa_annons_sokes': {
			init: function() {
				var noncetoken;

				$('#submit').bind('click', function(event) {
					if($('#submit').hasClass('create_rentad')) {
						event.preventDefault();

						if(Sage.checkFormCreateUpdateSearchAd() === false){
							return false;
						}
            $('#submit').addClass('disabled');
            $('#submit').attr('disapled', true);
						createSearchAd();
					}
				});

        var name = $('#viewname');
        var phone = $('#viewphone');

        if(name.prop('checked')){
          $('#displayName').css('display', 'none');
          $('#displayViewName').css('display', 'block');
        } else {
          $('#displayName').css('display', 'block');
          $('#displayViewName').css('display', 'none');
        }

        if (!phone.prop('checked')) {
          $('#displayPhone').css('display', 'block');
        } else {
          $('#displayPhone').css('display', 'none');
        }

        name.click(function() {
          if (!name.prop('checked')) {
            $('#displayName').css('display', 'block');
            $('#displayViewName').css('display', 'none');
          } else {
            $('#displayName').css('display', 'none');
            $('#displayViewName').css('display', 'block');
          }
        });

        phone.click(function() {
          if (!phone.prop('checked')) {
            $('#displayPhone').css('display', 'block');
          } else {
            $('#displayPhone').css('display', 'none');
          }
        });

				$.ajax({
					url: '/api/get_nonce/?controller=posts&method=create_post',
					type: 'GET',
				})
				.done(function(data) {
					noncetoken = data.nonce;
					//console.log(noncetoken);
				})
				.fail(function(data) {

				});

				// talk to the API about creating a post
				var createSearchAd = function() {

					if(noncetoken) {

						var postdata = {
							type: 'searchad',
							nonce: noncetoken,
							status: 'publish',
              title: ($('#title').val() === '' ? $('#titleeng').val() : $('#title').val()),
              content: ($('#description').val() === '' ? $('#descriptioneng').val() : $('#description').val())
						};
						var titleEng = $('#titleeng').val();
						var descriptionEng = $('#descriptioneng').val();

						var area = $('#area').val(),
						contractType = $('#contractType').val(),
						accommodationType = $('#accommodationType').val();


						var availablefrom = $('#availablefromInfinity')
						.val();
						var availableto = "Tillsvidare";

						if($('.accomedation-date.time.visible').length>0){
							availablefrom = $('#availablefrom')
							.val();
							availableto = $('#availableto')
							.val();
						}

            var viewname = ($('#viewname')
              .is(':checked')) ? 'false' : 'true';
            var viewphone = ($('#viewphone')
              .is(':checked')) ? 'false' : 'true';

            //Args to be sent with the ajax request in the function updateUserProfile
            var args = "_viewname=" + viewname + "&_viewphone=" + viewphone;

						$.ajax({
							url: '/api/posts/create_post/?custom[_area]=' + area + '&custom[_contractType]=' + contractType + '&custom[_accommodationType]=' + accommodationType + '&custom[_descriptioneng]=' + descriptionEng + '&custom[_titleeng]=' + titleEng +'&custom[_availablefrom]=' + availablefrom + '&custom[_availableto]=' + availableto,
							type: 'POST',
							data: postdata,
						})
						.done(function(data) {
              var nonce = $('#nonce').val();
							//Bopoolen.sendMailCreatedAd(data);
              Bopoolen.updateUserProfile(args, nonce, "/tack-ska-du-ha-gast/");
						})
						.fail(function(data) {
							//console.log(data);
							//alert(data.status);
							$('.create_rentad').attr("disabled", false);
              $('#submit').removeClass('disabled');
							$('.error-msg').text("Något gick fel, var god försök igen.");
						});

					} else {
						console.log('noncetoken not set');
					}

				};
			}
		},

		'redigera_annons_sokes': {
			init: function() {

        if($('#goTo').val() !== '') {
          $('.update_rentad').attr('disabled', true);
          var id = '#'+$('#goTo').val();
          var isMobile = false; //initiate as false
          // device detection
          if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) ||
            /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))){
            isMobile = true;
          }
          if(isMobile) {
            $("html, body").animate({ scrollTop: ($(id).offset().top - 500) }, 1000);
          } else {
            $("html, body").animate({ scrollTop: ($(id).offset().top - 300) }, 1000);
          }

          if($('#other').is(':checked')) {
            $('.other').css('display', 'block');
          }

          $('input[type=radio]').on('click', function() {
            if($('#other').is(':checked')) {
              $('.other').css('display', 'block');
            } else {
              $('.other').css('display', 'none');
            }
            $('.update_rentad').attr("disabled", false).removeClass('disabled').css('opacity', '1');
          });

        }

				$(".multi-select").multiselect();
				$(".multi-select").multiselect('updateButtonText');

				var post_id = Bopoolen.getUrlParameter('post_id');
				var noncetoken;

				$('#submit').bind('click', function(event) {
					event.preventDefault();
					event.stopPropagation();

					if(Sage.checkFormCreateUpdateSearchAd() === false){
						return false;
					}
					else {
						$(this).attr('disabled', true).text('Laddar');
					}

					editSearchAd(post_id);
				});

        var name = $('#viewname');
        var phone = $('#viewphone');

        if(name.prop('checked')){
          $('#displayName').css('display', 'none');
          $('#displayViewName').css('display', 'block');
        } else {
          $('#displayName').css('display', 'block');
          $('#displayViewName').css('display', 'none');
        }

        if (!phone.prop('checked')) {
          $('#displayPhone').css('display', 'block');
        } else {
          $('#displayPhone').css('display', 'none');
        }

        name.click(function() {
          if (!name.prop('checked')) {
            $('#displayName').css('display', 'block');
            $('#displayViewName').css('display', 'none');
          } else {
            $('#displayName').css('display', 'none');
            $('#displayViewName').css('display', 'block');
          }
        });

        phone.click(function() {
          if (!phone.prop('checked')) {
            $('#displayPhone').css('display', 'block');
          } else {
            $('#displayPhone').css('display', 'none');
          }
        });

				$.ajax({
					url: '/api/get_nonce/?controller=posts&method=update_post',
					type: 'GET'
				})
				.done(function(data) {
					noncetoken = data.nonce;
					//console.log(noncetoken);
				})
				.fail(function(data) {

				});

				// talk to the API about creating a post
				var editSearchAd = function(post_id) {

					if(noncetoken) {
            if($('#goTo').val() !== '') {
              if($('#goTo').val() === 'inactiveArea') {
                postdata = {
                  id: post_id,
                  nonce: noncetoken,
                  post_type: 'searchad',
                  status: 'draft',
                  title: ($('#title').val() === '' ? $('#title-eng').val() : $('#title').val()),
                  content: ($('#description').val() === '' ? $('#description-eng').val() : $('#description').val())
                };

                reason = $('input[name=_inactivereason]:checked').val();
                reasoncomment = '';
                if(reason === 'other') {
                  reasoncomment = $('#descriptionReasonInaktiv').val();
                }

                $('.update_rentad').attr("disabled", true);
                $('.update_rentad').text('Laddar');

                $.ajax({
                  url: '/api/posts/update_post/?&custom[_inactivereason]=' + reason + '&custom[_inactivereasoncomment]=' + reasoncomment,
                  type: 'POST',
                  data: postdata
                })
                  .done(function(data) {
                    //Bopoolen.updateUserProfile(args, nonce);
                    //Bopoolen.sendMailCreatedAd(data);
                    window.location = "/redigerad-annons/";
                  })
                  .fail(function(data) {
                    //console.log(data);
                    //alert(data.status);
                    $('.update_rentad').attr("disabled", false);
                    $('.error-msg').text("Något gick fel, var god försök igen.");
                  });

              } else if ($('#goTo').val() === 'eraseArea') {

                postdata = {
                  id: post_id,
                  nonce: noncetoken,
                  post_type: 'searchad',
                  status: 'raderad',
                  title: ($('#title').val() === '' ? $('#title-eng').val() : $('#title').val()),
                  content: ($('#description').val() === '' ? $('#description-eng').val() : $('#description').val())
                };

                reason = $('input[name=_deletereason]:checked').val();
                reasoncomment = '';
                if(reason === 'other') {
                  reasoncomment = $('#descriptionReasonEraseOther').val();
                }

                $('.update_rentad').attr("disabled", true);
                $('.update_rentad').text('Laddar');

                $.ajax({
                  url: '/api/posts/update_post/?&custom[_deletereason]=' + reason + '&custom[_deletereasoncomment]=' + reasoncomment,
                  type: 'POST',
                  data: postdata
                })
                  .done(function(data) {
                    //Bopoolen.updateUserProfile(args, nonce);
                    //Bopoolen.sendMailCreatedAd(data);
                    window.location = "/redigerad-annons/";
                  })
                  .fail(function(data) {
                    //console.log(data);
                    //alert(data.status);
                    $('.update_rentad').attr("disabled", false);
                    $('.error-msg').text("Något gick fel, var god försök igen.");
                  });
              }
            } else {

              var postdata = {
                post_type: 'searchad',
                id: post_id,
                nonce: noncetoken,
                status: 'publish',
                title: ($('#title').val() === '' ? $('#titleeng').val() : $('#title').val()),
                content: ($('#description').val() === '' ? $('#descriptioneng').val() : $('#description').val())
              };
              var titleEng = $('#titleeng').val();
              var descriptionEng = $('#descriptioneng').val();

              var area = $('#area').val(),
                contractType = $('#contractType').val(),
                accommodationType = $('#accommodationType').val();

              var viewname = ($('#viewname')
                .is(':checked')) ? 'false' : 'true';
              var viewphone = ($('#viewphone')
                .is(':checked')) ? 'false' : 'true';

              var availablefrom = $('#availablefromInfinity')
                .val();
              var availableto = "Tillsvidare";

              if ($('.accomedation-date.time.visible').length > 0) {
                availablefrom = $('#availablefrom')
                  .val();
                availableto = $('#availableto')
                  .val();
              }

              //Args to be sent with the ajax request in the function updateUserProfile
              var args = "_viewname=" + viewname + "&_viewphone=" + viewphone;

              $.ajax({
                url: '/api/posts/update_post/?custom[_area]=' + area + '&custom[_contractType]=' + contractType + '&custom[_accommodationType]=' + accommodationType + '&custom[_descriptioneng]=' + descriptionEng + '&custom[_titleeng]=' + titleEng + '&custom[_availablefrom]=' + availablefrom + '&custom[_availableto]=' + availableto,
                type: 'POST',
                data: postdata,
              })
                .done(function (data) {
                  //console.log(data);
                  //Bopoolen.sendMailCreatedAd(data);
                  Bopoolen.updateUserProfile(args, $('#nonce').val(), "/redigerad-annons-sokes/");
                  //window.location = "/tack-ska-du-ha-gast/";
                })
                .fail(function (data) {
                  //console.log(data);
                  //alert(data.status);
                  $('.create_rentad').attr("disabled", false);
                  $('.error-msg').text("Något gick fel, var god försök igen.");
                });

            }
          } else {

					}

				};
			}
		},

		// About us page, note the change from about-us to about_us.
		'skapa_annons_uthyres': {
			init: function() {

				var noncetoken;

				$.ajax({
					url: '/api/get_nonce/?controller=posts&method=create_post',
					type: 'GET',
				})
				.done(function(data) {
					noncetoken = data.nonce;
					create(noncetoken);
				})
				.fail(function(data) {
					//console.log(data);
				});


				var name = $('#viewname');
				var phone = $('#viewphone');
				var adress = $('#viewaddress');

				var adressOriginal = $('#displayAdress').text();

				$('#street').change(function() {
					$('#displayAdress').text(adressOriginal + $('#street').val());
				});

				if(name.prop('checked')){
					$('#displayName').css('display', 'none');
					$('#displayViewName').css('display', 'block');
				} else {
					$('#displayName').css('display', 'block');
					$('#displayViewName').css('display', 'none');
				}

				if (!phone.prop('checked')) {
					$('#displayPhone').css('display', 'block');
				} else {
					$('#displayPhone').css('display', 'none');
				}

				if (!adress.prop('checked')) {
					$('#displayAdress').css('display', 'block');
				} else {
					$('#displayAdress').css('display', 'none');
				}

				name.click(function() {
					if (!name.prop('checked')) {
						$('#displayName').css('display', 'block');
						$('#displayViewName').css('display', 'none');
					} else {
						$('#displayName').css('display', 'none');
						$('#displayViewName').css('display', 'block');
					}
				});

				phone.click(function() {
					if (!phone.prop('checked')) {
						$('#displayPhone').css('display', 'block');
					} else {
						$('#displayPhone').css('display', 'none');
					}
				});

				adress.click(function() {
					if (!adress.prop('checked')) {
						$('#displayAdress').css('display', 'block');
					} else {
						$('#displayAdress').css('display', 'none');
					}
				});

				function getLatLang(address) {
					return $.ajax({
						url: 'http://maps.googleapis.com/maps/api/geocode/json?address='+address,
						type: 'GET',
					});
				}

				function create(noncetoken) {
          var text,
            removeText;
					if($('#langCode').val() === 'sv') {
						text = 'Du kan inte ladda upp fler bilder.';
						removeText = 'Ta bort';
					} else {
						text = 'You can not upload any more files.';
						removeText = 'Remove file';
					}

					$("div#dZUpload").dropzone({
						url: "/api/posts/upload_images/",
						autoProcessQueue: false,
						uploadMultiple: true,
						parallelUploads: 5,
						maxFiles: 5,
						// maxFilesize: 1,
						acceptedFiles: 'image/*',
						addRemoveLinks: true,
						dictMaxFilesExceeded: text,
						dictRemoveFile: removeText,
						init: function() {
							dzClosure = this; // Makes sure that 'this' is understood inside the functions below.
							var ids, nbrOfFiles=0;

							$('.create_rentad').on('click', function(e) {
									e.preventDefault();
									if(Sage.checkFormCreateUpdateRentAd() === false){
										return false;
									}

									if(nbrOfFiles > 0){
										dzClosure.processQueue();
										//console.log('processQueue()');
									}
									else {
										//console.log('createAd()');
										create_rentad(noncetoken);
									}
							});

							this.on("addedfile", function (file, res) {
								nbrOfFiles++;
							});

							this.on("removedfile", function (file, res) {
								nbrOfFiles--;
							});

							this.on("success", function (file, res) {
								ids = res;
							});

							this.on("queuecomplete", function (file) {
								$('#image-ids').val(ids);
								create_rentad(noncetoken);
							});
						}
					});
				}

				function create_rentad(noncetoken) {
					// setup postdata
					var postdata = {
						type: 'rentad',
						nonce: noncetoken,
						status: 'pending',
						title: ($('#title').val() === '' ? $('#title-eng').val() : $('#title').val()),
						content: ($('#description').val() === '' ? $('#description-eng').val() : $('#description').val())
					};

					var titleEng = $('#title-eng').val();
					var descriptionEng = $('#description-eng').val();
					var rent = $('#rent')
					.val();
					var reasonableRent = ($('#reasonableRent')
					.is(':checked')) ? 'checked' : '';

					var availablefrom = $('#availablefromInfinity')
					.val();
					var availableto = "Tillsvidare";

					if($('.accomedation-date.time.visible').length>0){
						availablefrom = $('#availablefrom')
						.val();
						availableto = $('#availableto')
						.val();
					}

					var street = $('#street')
					.val();
					var zip = $('#zip')
					.val();
					var city = $('#city')
					.val();
					var area = $('#area')
					.val();

					var roominhouse = ($('#accommodationType').val() === 'roominhouse') ? 'checked' : '';
					var roominapartment = ($('#accommodationType').val() === 'roominapartment') ? 'checked' : '';
					var roominstudent = ($('#accommodationType').val() === 'roominstudent') ? 'checked' : '';
					var ownhouse = ($('#accommodationType').val() === 'ownhouse') ? 'checked' : '';
					var ownapartment = ($('#accommodationType').val() === 'ownapartment') ? 'checked' : '';

          var housingType = $('#accommodationType').val();

					var contractsecond = ($('#contractType').val() === 'contractsecond') ? 'checked' : '';
					var contractfirst = ($('#contractType').val() === 'contractfirst') ? 'checked' : '';
					var contractchange = ($('#contractType').val() === 'contractchange') ? 'checked' : '';

					var contractType = $('#contractType').val();

					var availableyta = $('#availableyta')
					.val();
					if($('.accomedation-type.roomin.show').length>0){
						availableyta = $('#availableytaOwn')
						.val();
					}

					var availablegemensam = $('#availablegemensam')
					.val();

					var numberofrooms = "";
					if($('.accomedation-type.own.show').length>0){
						numberofrooms = $('#numberofrooms')
						.val();
					}

					var includedel = ($('#includedel')
					.is(':checked')) ? 'checked' : '';
					var includedinternet = ($('#includedinternet')
					.is(':checked')) ? 'checked' : '';
					var includedfurniture = ($('#includedfurniture')
					.is(':checked')) ? 'checked' : '';
					var includedtoilet = ($('#includedtoilet')
					.is(':checked')) ? 'checked' : '';
					var includedbath = ($('#includedbath')
					.is(':checked')) ? 'checked' : '';
					var includedanimal = ($('#includedanimal')
					.is(':checked')) ? 'checked' : '';
					var includedhandicap = ($('#includedhandicap')
					.is(':checked')) ? 'checked' : '';
					var includeddishwasher = ($('#includeddishwasher')
					.is(':checked')) ? 'checked' : '';
					var includedwasher = ($('#includedwasher')
					.is(':checked')) ? 'checked' : '';
					var includedsmoking = ($('#includedsmoking')
					.is(':checked')) ? 'checked' : '';
					var includedpentry = ($('#includedpentry')
					.is(':checked')) ? 'checked' : '';
          var includedkitchen = ($('#includedkitchen')
            .is(':checked')) ? 'checked' : '';
					var includedentrance = ($('#includedentrance')
					.is(':checked')) ? 'checked' : '';

					var viewname = ($('#viewname')
					.is(':checked')) ? 'false' : 'true';
					var viewphone = ($('#viewphone')
					.is(':checked')) ? 'false' : 'true';
					var viewaddress = ($('#viewaddress')
					.is(':checked')) ? '' : 'checked';

					//Args to be sent with the ajax request in the function updateUserProfile
					var args = "_viewname=" + viewname + "&_viewphone=" + viewphone;

					var imgIds = $('#image-ids').val();

					var googleAddress = street + ' ' + city + ' ' + zip;

					getLatLang(googleAddress).done(function(data) {

						var latlng;

						if(data.status === 'ZERO_RESULTS')  {
							latlng = 'NULL';
						} else {
							latlng = data.results[0].geometry.location.lat + ',' +data.results[0].geometry.location.lng;
						}

						$('.create_rentad').attr("disabled", true);
						$('.create_rentad').text('Laddar');

						$.ajax({
							url: '/api/posts/create_post/?&custom[_latlng]=' + latlng + '&custom[_rentprice]=' + rent + '&custom[_housingType]=' + housingType + '&custom[_contractType]=' + contractType +  '&custom[_reasonableRent]=' + reasonableRent + '&custom[_availablefrom]=' + availablefrom + '&custom[_area]=' + area + '&custom[_availableto]=' + availableto + '&custom[_streetaddress]=' + street + '&custom[_postalcode]=' + zip + '&custom[_city]=' + city + '&custom[_roominhouse]=' + roominhouse + '&custom[_roominapartment]=' + roominapartment + '&custom[_roominstudent]=' + roominstudent + '&custom[_ownhouse]=' + ownhouse + '&custom[_ownapartment]=' + ownapartment + '&custom[_contractsecond]=' + contractsecond + '&custom[_contractfirst]=' + contractfirst + '&custom[_contractchange]=' + contractchange + '&custom[_availableyta]=' + availableyta + '&custom[_availablegemensam]=' + availablegemensam + '&custom[_numberofrooms]=' + numberofrooms + '&custom[_includedel]=' + includedel + '&custom[_includedinternet]=' + includedinternet + '&custom[_includedfurniture]=' + includedfurniture + '&custom[_includedtoilet]=' + includedtoilet + '&custom[_includedbath]=' + includedbath + '&custom[_includedanimal]=' + includedanimal + '&custom[_includedhandicap]=' + includedhandicap + '&custom[_includeddishwasher]=' + includeddishwasher + '&custom[_includedwasher]=' + includedwasher + '&custom[_includedsmoking]=' + includedsmoking + '&custom[_includedpentry]=' + includedpentry + '&custom[_includedkitchen]=' + includedkitchen + '&custom[_includedentrance]=' + includedentrance + '&custom[_imageids]=' + imgIds + '&custom[_titleeng]=' + titleEng + '&custom[_descriptioneng]=' + descriptionEng + '&custom[_clicker]='+0+ "&custom[_viewaddress]=" + viewaddress,
							type: 'POST',
							data: postdata,
						})
						.done(function(data) {
							var nonce = $('#nonce').val();

              Bopoolen.updateUserProfile(args, nonce, "/tack-ska-du-ha-hyres/");
							//Bopoolen.sendMailCreatedAd(data);
						})
						.fail(function(data) {
							// console.log(data);
							//alert(data.status);
							$('.create_rentad').attr("disabled", false);
							$('.error-msg').text("Något gick fel, var god försök igen.");
						});
					});
				}

			}
		},
		// About us page, note the change from about-us to about_us.
		'redigera_annons_uthyres': {
			init: function() {

        if($('#goTo').val() !== '') {
          var id = '#'+$('#goTo').val();
          var isMobile = false; //initiate as false
          // device detection
          if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) ||
            /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))){
            isMobile = true;
          }
          if(isMobile) {
            $("html, body").animate({ scrollTop: ($(id).offset().top - 500) }, 1000);
          } else {
            $("html, body").animate({ scrollTop: ($(id).offset().top - 300) }, 1000);
          }

          if($('#other').is(':checked')) {
            $('.other').css('display', 'block');
          }

          $('input[type=radio]').on('click', function() {
            if($('#other').is(':checked')) {
              $('.other').css('display', 'block');
            } else {
              $('.other').css('display', 'none');
            }
            $('.update_rentad').attr("disabled", false).removeClass('disabled').css('opacity', '1');
          });

        }

				var post_id = Bopoolen.getUrlParameter("post_id");
				var nonce = $('#nonce').val();

				$.ajax({
					url: '/api/get_nonce/?controller=posts&method=update_post',
					type: 'GET',
				})
				.done(function(data) {
					var noncetoken = data.nonce;
					getImagesArray(noncetoken);
				})
				.fail(function(data) {
					//console.log('Get Nonce Failed: ' + data);
				});

				var name = $('#viewname');
				var phone = $('#viewphone');
				var adress = $('#viewaddress');

				var adressOriginal = $('#displayAdress').text();

				$('#street').change(function() {
					$('#displayAdress').text(adressOriginal + $('#street').val());
				});

				if(name.prop('checked')){
					$('#displayName').css('display', 'block');
					$('#displayViewName').css('display', 'none');
				} else {
					$('#displayName').css('display', 'none');
					$('#displayViewName').css('display', 'block');
				}

				if (phone.prop('checked')) {
					$('#displayPhone').css('display', 'none');
				} else {
					$('#displayPhone').css('display', 'block');
				}

				if (!adress.prop('checked')) {
					$('#displayAdress').css('display', 'block');
				} else {
					$('#displayAdress').css('display', 'none');
				}

				name.click(function() {
					if (!name.prop('checked')) {
						$('#displayName').css('display', 'none');
						$('#displayViewName').css('display', 'block');
					} else {
						$('#displayName').css('display', 'block');
						$('#displayViewName').css('display', 'none');
					}
				});

				phone.click(function() {
					if (phone.prop('checked')) {
						$('#displayPhone').css('display', 'none');
					} else {
						$('#displayPhone').css('display', 'block');
					}
				});

				adress.click(function() {
					if (!adress.prop('checked')) {
						$('#displayAdress').css('display', 'none');
					} else {
						$('#displayAdress').css('display', 'block');
					}
				});

				function getLatLang(address) {
					return $.ajax({
						url: 'http://maps.googleapis.com/maps/api/geocode/json?address='+address,
						type: 'GET',
					});
				}

				function getImagesArray(noncetoken) {
					var data = {
						'action': 'get_image_ids_for_rentad',
						'post_id': post_id,
						'nonce': nonce,
					};

					$.ajax({
						url: '/wp-admin/admin-ajax.php',
						type: 'POST',
						data: data,
						dataType: 'json'
					})
					.done(function(imagesArray) {
						create(noncetoken, imagesArray);
					})
					.fail(function(response) {
						//console.log('getImagesArray() Failed');
						return null;
					});
				}

				function create(noncetoken, imagesArray) {
          var text,
            removeText;
					if($('#langCode').val() === 'sv') {
						text = 'Du kan inte ladda upp fler bilder.';
						removeText = 'Ta bort';
					} else {
						text = 'You can not upload any more files.';
						removeText = 'Remove file';
					}

					$("div#dZUpload").dropzone({
						url: "/api/posts/upload_images/",
						autoProcessQueue: false,
						uploadMultiple: true,
						parallelUploads: 5,
						maxFiles: 5,
						acceptedFiles: 'image/*',
						addRemoveLinks: true,
						dictMaxFilesExceeded: text,
						dictRemoveFile: removeText,
						init: function() {
							dzClosure = this; // Makes sure that 'this' is understood inside the functions below.
							var done, ids=[], existingIds=[], nbrOfFiles=0;

							for (i = 0; i < imagesArray.length; i++) {
								this.emit("addedfile", imagesArray[i]);
								this.createThumbnailFromUrl(imagesArray[i], imagesArray[i].url);
								this.emit("complete", imagesArray[i]);
								existingIds.push(imagesArray[i].serverId);
								ids.push(imagesArray[i].serverId);
								dzClosure.options.maxFiles--;
							}

							$('.update_rentad').on('click', function(e) {
								//console.log('onClick()');
								e.preventDefault();
								e.stopPropagation();

                if($('#goTo').val() !== '') {
									if(Sage.checkFormCreateUpdateRentAd() === false){
										return false;
									}

                  if(dzClosure.getQueuedFiles().length > 0){
                    //console.log('processQueue()');
                    dzClosure.processQueue();
                  }
                  else {
                    $('#image-ids').val(ids);
                    //console.log('removeDeletedImages()');
                    Bopoolen.removeDeletedImages(ids, existingIds, nonce);
                    //console.log('update_rentad()');
                    update_rentad(noncetoken);
                  }
                } else {
									if(Sage.checkFormCreateUpdateRentAd() === false){
										//return false;
									}

                  if(dzClosure.getQueuedFiles().length > 0){
                    //console.log('processQueue()');
                    dzClosure.processQueue();
                  }
                  else {
                    $('#image-ids').val(ids);
                    //console.log('removeDeletedImages()');
                    Bopoolen.removeDeletedImages(ids, existingIds, nonce);
                    //console.log('update_rentad()');
                    update_rentad(noncetoken);
                  }
                }
							});

							this.on("addedfile", function (file, res) {
								nbrOfFiles++;
							});

							this.on("removedfile", function (file, res) {
								if ($.inArray(file.serverId, ids) > -1) {
									ids.splice($.inArray(file.serverId, ids), 1);
									dzClosure.options.maxFiles++;
								} else {
									nbrOfFiles--;
								}
							});

							this.on("success", function (file, res) {
								var newids = res.split(',');
								for (i = 0; i < newids.length; i++) {
									if ($.inArray(newids[i], ids) === -1) {
											ids.push(newids[i]);
									}
								}
								done = true;
							});

							this.on("queuecomplete", function (file) {
								if (done) {
									$('#image-ids').val(ids);
									//console.log('removeDeletedImages()');
									Bopoolen.removeDeletedImages(ids, existingIds, nonce);
									//console.log('update_rentad()');
									update_rentad(noncetoken);
								}
							});
						}
					});
				}

				function update_rentad(noncetoken) {
					// setup postdata
          var comment;
          var reason;
          var reasoncomment;
          var postdata;

          if($('#goTo').val() !== '') {
            if($('#goTo').val() === 'inactiveArea') {
              postdata = {
                id: post_id,
                nonce: noncetoken,
                post_type: 'rentad',
                status: 'draft',
                title: ($('#title').val() === '' ? $('#title-eng').val() : $('#title').val()),
                content: ($('#description').val() === '' ? $('#description-eng').val() : $('#description').val())
              };

              reason = $('input[name=_inactivereason]:checked').val();
              reasoncomment = '';
              if(reason === 'other') {
                reasoncomment = $('#descriptionReasonInaktiv').val();
              }

              $('.update_rentad').attr("disabled", true);
              $('.update_rentad').text('Laddar');

              $.ajax({
                url: '/api/posts/update_post/?&custom[_inactivereason]=' + reason + '&custom[_inactivereasoncomment]=' + reasoncomment,
                type: 'POST',
                data: postdata
              })
                .done(function(data) {
                  //Bopoolen.updateUserProfile(args, nonce);
                  //Bopoolen.sendMailCreatedAd(data);
                  window.location = "/redigerad-annons/";
                })
                .fail(function(data) {
                  //console.log(data);
                  //alert(data.status);
                  $('.update_rentad').attr("disabled", false);
                  $('.error-msg').text("Något gick fel, var god försök igen.");
                });

            } else if ($('#goTo').val() === 'eraseArea') {

              postdata = {
                id: post_id,
                nonce: noncetoken,
                post_type: 'rentad',
                status: 'raderad',
                title: ($('#title').val() === '' ? $('#title-eng').val() : $('#title').val()),
                content: ($('#description').val() === '' ? $('#description-eng').val() : $('#description').val())
              };

              reason = $('input[name=_deletereason]:checked').val();
              reasoncomment = '';
              if(reason === 'other') {
                reasoncomment = $('#descriptionReasonEraseOther').val();
              }

              $('.update_rentad').attr("disabled", true);
              $('.update_rentad').text('Laddar');

              $.ajax({
                url: '/api/posts/update_post/?&custom[_deletereason]=' + reason + '&custom[_deletereasoncomment]=' + reasoncomment,
                type: 'POST',
                data: postdata
              })
                .done(function(data) {
                  //Bopoolen.updateUserProfile(args, nonce);
                  //Bopoolen.sendMailCreatedAd(data);
                  window.location = "/redigerad-annons/";
                })
                .fail(function(data) {
                  //console.log(data);
                  //alert(data.status);
                  $('.update_rentad').attr("disabled", false);
                  $('.error-msg').text("Något gick fel, var god försök igen.");
                });
            }
          } else {

            postdata = {
              id: post_id,
              nonce: noncetoken,
              post_type: 'rentad',
              status: 'modified',
              title: ($('#title').val() === '' ? $('#title-eng').val() : $('#title').val()),
              content: ($('#description').val() === '' ? $('#description-eng').val() : $('#description').val())
            };

            var titleEng = $('#title-eng').val();
            var descriptionEng = $('#description-eng').val();
            var rent = $('#rent').val();
            var reasonableRent = ($('#reasonableRent').is(':checked')) ? 'checked' : '';

            var availablefrom = $('#availablefromInfinity').val();
            var availableto = "Tillsvidare";

            if ($('.accomedation-date.time.visible').length > 0) {
              availablefrom = $('#availablefrom').val();
              availableto = $('#availableto').val();
            }

            var street = $('#street').val();
            var zip = $('#zip').val();
            var city = $('#city').val();
            var area = $('#area').val();

            var roominhouse = ($('#accommodationType').val() === 'roominhouse') ? 'checked' : '';
            var roominapartment = ($('#accommodationType').val() === 'roominapartment') ? 'checked' : '';
            var roominstudent = ($('#accommodationType').val() === 'roominstudent') ? 'checked' : '';
            var ownhouse = ($('#accommodationType').val() === 'ownhouse') ? 'checked' : '';
            var ownapartment = ($('#accommodationType').val() === 'ownapartment') ? 'checked' : '';

            var housingType = $('#accommodationType').val();

            var contractsecond = ($('#contractType').val() === 'contractsecond') ? 'checked' : '';
            var contractfirst = ($('#contractType').val() === 'contractfirst') ? 'checked' : '';
            var contractchange = ($('#contractType').val() === 'contractchange') ? 'checked' : '';

            var availableyta = $('#availableyta').val();
            if ($('.accomedation-type.roomin.show').length > 0) {
              availableyta = $('#availableytaOwn').val();
            }

            var availablegemensam = $('#availablegemensam').val();

            var numberofrooms = "";
            if ($('.accomedation-type.own.show').length > 0) {
              numberofrooms = $('#numberofrooms').val();
            }

            var includedel = ($('#includedel')
              .is(':checked')) ? 'checked' : '';
            var includedinternet = ($('#includedinternet')
              .is(':checked')) ? 'checked' : '';
            var includedfurniture = ($('#includedfurniture')
              .is(':checked')) ? 'checked' : '';
            var includedtoilet = ($('#includedtoilet')
              .is(':checked')) ? 'checked' : '';
            var includedbath = ($('#includedbath')
              .is(':checked')) ? 'checked' : '';
            var includedanimal = ($('#includedanimal')
              .is(':checked')) ? 'checked' : '';
            var includedhandicap = ($('#includedhandicap')
              .is(':checked')) ? 'checked' : '';
            var includeddishwasher = ($('#includeddishwasher')
              .is(':checked')) ? 'checked' : '';
            var includedwasher = ($('#includedwasher')
              .is(':checked')) ? 'checked' : '';
            var includedsmoking = ($('#includedsmoking')
              .is(':checked')) ? 'checked' : '';
            var includedpentry = ($('#includedpentry')
              .is(':checked')) ? 'checked' : '';
            var includedkitchen = ($('#includedkitchen')
              .is(':checked')) ? 'checked' : '';
            var includedentrance = ($('#includedentrance')
              .is(':checked')) ? 'checked' : '';

            var viewname = (!$('#viewname')
              .is(':checked')) ? 'true' : 'false';
            var viewphone = (!$('#viewphone')
              .is(':checked')) ? 'true' : 'false';
            var viewaddress = (!$('#viewaddress')
              .is(':checked')) ? 'true' : 'false';

            //Args to be sent with the ajax request in the function updateUserProfile
            var args = "_viewname=" + viewname + "&_viewphone=" + viewphone;

            var imgIds = $('#image-ids').val();

            var googleAddress = street + ' ' + city + ' ' + zip;

            getLatLang(googleAddress).done(function (data) {

              var latlng;

              if (data.status === 'ZERO_RESULTS') {
                latlng = 'NULL';
              } else {
                latlng = data.results[0].geometry.location.lat + ',' + data.results[0].geometry.location.lng;
              }

              $('.update_rentad').attr("disabled", true);
              $('.update_rentad').text('Laddar');

              $.ajax({
                url: '/api/posts/update_post/?&custom[_latlng]=' + latlng + '&custom[_rentprice]=' + rent + '&custom[_housingType]=' + housingType + '&custom[_reasonableRent]=' + reasonableRent + '&custom[_availablefrom]=' + availablefrom + '&custom[_area]=' + area + '&custom[_availableto]=' + availableto + '&custom[_streetaddress]=' + street + '&custom[_postalcode]=' + zip + '&custom[_city]=' + city + '&custom[_roominhouse]=' + roominhouse + '&custom[_roominapartment]=' + roominapartment + '&custom[_roominstudent]=' + roominstudent + '&custom[_ownhouse]=' + ownhouse + '&custom[_ownapartment]=' + ownapartment + '&custom[_contractsecond]=' + contractsecond + '&custom[_contractfirst]=' + contractfirst + '&custom[_contractchange]=' + contractchange + '&custom[_availableyta]=' + availableyta + '&custom[_availablegemensam]=' + availablegemensam + '&custom[_numberofrooms]=' + numberofrooms + '&custom[_includedel]=' + includedel + '&custom[_includedinternet]=' + includedinternet + '&custom[_includedfurniture]=' + includedfurniture + '&custom[_includedtoilet]=' + includedtoilet + '&custom[_includedbath]=' + includedbath + '&custom[_includedanimal]=' + includedanimal + '&custom[_includedhandicap]=' + includedhandicap + '&custom[_includeddishwasher]=' + includeddishwasher + '&custom[_includedwasher]=' + includedwasher + '&custom[_includedsmoking]=' + includedsmoking + '&custom[_includedpentry]=' + includedpentry + '&custom[_includedkitchen]=' + includedkitchen + '&custom[_includedentrance]=' + includedentrance + '&custom[_imageids]=' + imgIds + '&custom[_titleeng]=' + titleEng + '&custom[_descriptioneng]=' + descriptionEng + "&custom[_viewaddress]=" + viewaddress,
                type: 'POST',
                data: postdata,
              })
                .done(function (data) {
                  Bopoolen.updateUserProfile(args, $('#nonce').val(), "/redigerad-annons-uthyres/");
                  //Bopoolen.sendMailCreatedAd(data);
                })
                .fail(function (data) {
                  //console.log(data);
                  //alert(data.status);
                  $('.update_rentad').attr("disabled", false);
                  $('.error-msg').text("Något gick fel, var god försök igen.");
                });
            });
          }
				}

			}
		},

		'profiles': {
			init: function() {
				var langCode = $('#langCode');
				if(langCode.val() === 'sv') {
          $('.wpcr3_respond_1').prepend('<p>Ditt namn, betyg och recension kommer att visas nedan när recensionen har blivit godkänd.</p>');
					$('.wpcr3_show_btn').text('Skapa din egen recension');
					$('.wpcr3_leave_text').text('Skicka din recension');
					$('label[for="wpcr3_fname"]').text('Namn: ');
					$('label[for="wpcr3_femail"]').text('Mail: ');
					$('label[for="id_wpcr3_frating"]').text('Betyg: ');
					$('label[for="id_wpcr3_ftext"]').text('Recension: ');
					$('.wpcr3_fconfirm2').attr('checked', 'checked');
					$('.wpcr3_check_confirm label').hide();
					$('.wpcr3_submit_btn').text('Skicka');
					$('.wpcr3_cancel_btn').text('Avbryt');
				} else {
          $('.wpcr3_respond_1').prepend('<p>Your name, rating and review will be shown below once it has been approved.</p>');
					$('.wpcr3_fconfirm2').attr('checked', 'checked');
					$('.wpcr3_check_confirm label').hide();
				}
			}
		},

		// Login page/register new customer
		'skapa_konto': {
			init: function() {

				$('#no-social-number').on('click', function() {
					if($(this).is(':checked')) {
						$('#create_socialnumber').attr('disabled', true);
            $('#create_socialnumber').parent().removeClass('has-error');
					} else {
						$('#create_socialnumber').attr('disabled', false);
					}
				});

				$('#create_phone').focusout(function() {
					var value = $('#create_phone').val();
					if(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{2,})/.test(value)) {
						$('#create_phone').parent().removeClass('has-error');
					} else {
						$('#create_phone').parent().addClass('has-error');
					}
				});

        /*$('#create_username').focusout(function() {
          var value = $('#create_username').val();
          if(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{3})/.test(value)) {
            $('#create_username').parent().removeClass('has-error');
          } else {
            $('#create_username').parent().addClass('has-error');
          }
        });

        $('#create_email').focusout(function() {
          var value = $('#create_email').val();
          if(/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(value)) {
            $('#create_email').parent().removeClass('has-error');
          } else {
            $('#create_email').parent().addClass('has-error');
          }
        });

        $('#email_repeat').focusout(function() {
          if($('#create_email').val() !== $('#email_repeat').val()) {
            $('#create_email').parent().addClass('has-error');
            $('#email_repeat').parent().addClass('has-error');
          }
        });

        $('#create_firstname').focusout(function() {
          var value = $('#create_firstname').val();
          if(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{3})/.test(value)) {
            $('#create_firstname').parent().removeClass('has-error');
          } else {
            $('#create_firstname').parent().addClass('has-error');
          }
        });

        $('#create_lastname').focusout(function() {
          var value = $('#create_lastname').val();
          if(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{3})/.test(value)) {
            $('#create_lastname').parent().removeClass('has-error');
          } else {
            $('#create_lastname').parent().addClass('has-error');
          }
        });

        $('#create_password').focusout(function() {
          var value = $('#create_password').val();
          if(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{3})/.test(value)) {
            $('#create_password').parent().removeClass('has-error');
          } else {
            $('#create_password').parent().addClass('has-error');
          }
        });

        $('#InputPassword1').focusout(function() {
          if($('#InputPassword1').val() !== $('#create_password').val()) {
            $('#create_password').parent().addClass('has-error');
            $('#InputPassword1').parent().addClass('has-error');
          }
        });

        if(!$('#no-social-number').is(':checked')) {
          $('#create_socialnumber').focusout(function() {
            var value = $('#create_socialnumber').val();
            if(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{3})/.test(value)) {
              $('#create_socialnumber').parent().removeClass('has-error');
            } else {
              $('#create_socialnumber').parent().addClass('has-error');
            }
          });
        }
        */

				/*

					Alla knapp, inget är förifytl!

				*/

				function get_tinymce_content(id) {
					var content;
					var inputid = id;
					var editor = tinyMCE.get(inputid);
					var textArea = jQuery('textarea#' + inputid);
					if (textArea.length>0 && textArea.is(':visible')) {
						content = textArea.val();
					} else {
						content = editor.getContent();
					}
					return content;
				}
        var text,
          removeText;
				if($('#langCode').val() === 'sv') {
					text = 'Du kan inte ladda upp fler bilder.';
					removeText = 'Ta bort';
				} else {
					text = 'You can not upload any more files.';
					removeText = 'Remove file';
				}

				$("div#dZUpload").dropzone({
					url: "/api/posts/upload_images/",
					autoProcessQueue: false,
					uploadMultiple: true,
					parallelUploads: 1,
					maxFiles: 1,
					acceptedFiles: 'image/*',
					addRemoveLinks: true,
					dictMaxFilesExceeded: text,
					dictRemoveFile: removeText,
					init: function() {
						dzClosure = this; // Makes sure that 'this' is understood inside the functions below.
						var id, nbrOfFiles = 0;

						$('.create_user').on('click', function(e) {
							e.preventDefault();

		          if(validateNameNum($('#create_username').val())) {
		            $('#create_username').parent().removeClass('has-error');
		          } else {
		            $('#create_username').parent().addClass('has-error');
								$("html, body").animate({ scrollTop: 0 }, "fast");
                $('.general-error').show();
								return false;
		          }

		          if(/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test($('#create_email').val())) {
		            $('#create_email').parent().removeClass('has-error');
		          } else {
		            $('#create_email').parent().addClass('has-error');
								$("html, body").animate({ scrollTop: 0 }, "fast");
                $('.general-error').show();
								return false;
		          }

		          if($('#create_email').val() !== $('#email_repeat').val()) {
		            $('#create_email').parent().addClass('has-error');
		            $('#email_repeat').parent().addClass('has-error');
								$("html, body").animate({ scrollTop: 0 }, "fast");
                $('.general-error').show();
								return false;
		          }


							if(validateName($('#create_firstname').val())) {
		            $('#create_firstname').parent().removeClass('has-error');
		          } else {
		            $('#create_firstname').parent().addClass('has-error');
								$("html, body").animate({ scrollTop: 0 }, "fast");
                $('.general-error').show();
								return false;
		          }

		          if(validateName($('#create_lastname').val())) {
		            $('#create_lastname').parent().removeClass('has-error');
		          } else {
		            $('#create_lastname').parent().addClass('has-error');
								$("html, body").animate({ scrollTop: 0 }, "fast");
                $('.general-error').show();
								return false;
		          }

		          if(validateNameNum($('#create_password').val())) {
		            $('#create_password').parent().removeClass('has-error');
		          } else {
		            $('#create_password').parent().addClass('has-error');
								$("html, body").animate({ scrollTop: 0 }, "fast");
                $('.general-error').show();
								return false;
		          }

		          if($('#InputPassword1').val() === '' && $('#create_password').val() === '') {
		            $('#create_password').parent().addClass('has-error');
		            $('#InputPassword1').parent().addClass('has-error');
								$("html, body").animate({ scrollTop: 0 }, "fast");
                $('.general-error').show();
								return false;
		          } else {
                $('#create_password').parent().removeClass('has-error');
                $('#InputPassword1').parent().removeClass('has-error');
              }

              if($('#InputPassword1').val() !== $('#create_password').val()) {
                $('#create_password').parent().addClass('has-error');
                $('#InputPassword1').parent().addClass('has-error');
                $("html, body").animate({ scrollTop: 0 }, "fast");
                $('.general-error').show();
                return false;
              } else {
                $('#create_password').parent().removeClass('has-error');
                $('#InputPassword1').parent().removeClass('has-error');
              }

							if($('#create_socialnumber').val() === '' && !$('#no-social-number').is(':checked')){
								$('#create_socialnumber').parent().addClass('has-error');
								$("html, body").animate({ scrollTop: 0 }, "fast");
                $('.general-error').show();
								return false;
							}
							else {
								$('#create_socialnumber').parent().removeClass('has-error');
								$('#create_socialnumber').parent().removeClass('has-error-security-nbr');
								$('.alert-danger.securitynumtaken').hide();
							}

              if(!$('#no-social-number').is(':checked')) {
                if(/^([0-9]{8}[-]{1}[0-9]{4})$/.test($('#create_socialnumber').val())) {
                  if(!luhn_validate($('#create_socialnumber').val())) {
                    $('#create_socialnumber').parent().addClass('has-error-security-nbr');
                    $("html, body").animate({ scrollTop: 0 }, "fast");
                    $('.general-error').show();
                    return false;
                  } else {
                    $('#create_socialnumber').parent().removeClass('has-error-security-nbr');
                  }
                } else {
                  $('#create_socialnumber').parent().addClass('has-error-security-nbr');
                  $("html, body").animate({ scrollTop: 0 }, "fast");
                  $('.general-error').show();
                  return false;
                }
              }


							if(!$('#terms').is(':checked')) {
								$('.terms').show();
								$("html, body").animate({ scrollTop: 0 }, "fast");
								return false;
							}


							if(!/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{2,})/.test($('#create_phone').val())) {
								$('#create_phone').parent().addClass('has-error');
								$("html, body").animate({ scrollTop: 0 }, "fast");
                $('.general-error').show();
								return false;
							}
							else {
								$('#create_email').parent().removeClass('has-error');
								$('#email_repeat').parent().removeClass('has-error');
								if(nbrOfFiles > 0){
									dzClosure.processQueue();
								}
								else {
                  if($('#langCode').val() === 'sv') {
                    $('.create_user').text('Laddar...');
                  } else {
                    $('.create_user').text('Loading...');
                  }
                  $('.general-error').hide();
                  $('.create_user').css('opacity','0.6');
									create_user('');
								}
							}
						});

						this.on("addedfile", function (file, res) {
							nbrOfFiles++;
						});

						this.on("removedfile", function (file, res) {
							nbrOfFiles--;
						});

						// save informations to params
						var params = {
							_username: $('#create_username')
							.val(),
							user_login: $('#create_email')
							.val(),
							user_pass: $('#create_password')
							.val(),
							user_email: $('#create_email')
							.val(),
							first_name: $('#create_firstname')
							.val(),
							last_name: $('#create_lastname')
							.val(),
							_securitynumber: $('#create_socialnumber')
							.val(),
							_no_social_number: $('#no-social-number')
							.is(':checked'),
							_phone: $('#create_phone')
							.val(),
							_viewname: $('#viewname')
							.is(':checked'),
							_viewphone: $('#viewphone')
							.is(':checked')
						};

						this.on("success", function (file, res) {
							if(parseInt(res)){
								id = res;
							}
						});

						this.on("queuecomplete", function (file) {
							// $('#image-ids').val(ids);
							//console.log(id);
							//console.log(file);
							create_user(id);
						});
					}
				});


				function create_user(imgId){
					var editorContent = get_tinymce_content('bio_tinymec');
					editorContent = editorContent.replace(/(<([^>]+)>)/ig,"");
					if(editorContent === $('#defaultDescription').val()) {
						editorContent = '';
					}
					// save informations to params
					var params = {
						_username: $('#create_username')
						.val(),
						user_login: $('#create_email')
						.val(),
						user_pass: $('#create_password')
						.val(),
						user_email: $('#create_email')
						.val(),
						first_name: $('#create_firstname')
						.val(),
						last_name: $('#create_lastname')
						.val(),
						_securitynumber: $('#create_socialnumber')
						.val(),
						_no_social_number: $('#no-social-number')
						.is(':checked'),
						_phone: $('#create_phone')
						.val(),
						_viewname: $('#viewname')
						.is(':checked'),
						_viewphone: $('#viewphone')
						.is(':checked'),
						description: editorContent,
						_profilepicture: imgId
					};

					//console.log('socialnr', $('#no-social-number')
					//.is(':checked'));

					//console.log('create_user params', params);

					// create user
					$.ajax({
						url: '/api/user/create_user/',
						type: 'GET',
						data: params,
					})
					.done(function(data) {

            if($('#langCode').val() === 'sv') {
              $('.create_user').text('Skapa konto');
            } else {
              $('.create_user').text('Create account');
            }
            $('.create_user').css('opacity','1');
						//console.log(data);
						$('.terms').hide();
						if (data.errors) {
							$('.existing_user_email')
							.show();
							$('.create_success')
							.hide();
							$('.missing')
							.hide();
              $('.securitynumtaken')
                .hide();
							$("html, body").animate({ scrollTop: 0 }, "fast");
						} else if (data === 'Missing') {
							$('.missing')
							.show();
              $('.securitynumtaken')
                .hide();
							$("html, body").animate({ scrollTop: 0 }, "fast");
						} else if (data === 'SecurityTaken') {
              $('.securitynumtaken')
                .show();
              $("html, body").animate({ scrollTop: 0 }, "fast");
            } else {
							$('.create_success')
							.show();
							$('.existing_user_email')
							.hide();
							$('.missing')
							.hide();
              $('.securitynumtaken')
                .hide();
							$("html, body").animate({ scrollTop: 0 }, "fast");
						}
					})
					.fail(function(data) {
            if($('#langCode').val() === 'sv') {
              $('.create_user').text('Skapa konto');
            } else {
              $('.create_user').text('Create account');
            }
            $('.create_user').css('opacity','1');
						$('.error_create')
						.show();
					});
				}
			},

		}
	};

	// The routing fires all common scripts, followed by the page specific scripts.
	// Add additional events for more control over timing e.g. a finalize event
	var UTIL = {
		fire: function(func, funcname, args) {
			var fire;
			var namespace = Sage;
			funcname = (funcname === undefined) ? 'init' : funcname;
			fire = func !== '';
			fire = fire && namespace[func];
			fire = fire && typeof namespace[func][funcname] === 'function';
			if (fire) {
				namespace[func][funcname](args);
			}
		},
		loadEvents: function() {
			// Fire common init JS
			UTIL.fire('common');

			// Fire page-specific init JS, and then finalize JS
			$.each(document.body.className.replace(/-/g, '_')
			.split(/\s+/),
			function(i, classnm) {
				UTIL.fire(classnm);
				UTIL.fire(classnm, 'finalize');
			});

			// Fire common finalize JS
			UTIL.fire('common', 'finalize');
		}
	};

	function sticky_relocate() {
		var window_top = $(window).scrollTop();
		var div_top = $('#sticky-anchor').offset().top;
		if (window_top > div_top) {
			$('#sticky').addClass('stick');
			$('#sticky-anchor').height($('#sticky').outerHeight());
		} else {
			$('#sticky').removeClass('stick');
			$('#sticky-anchor').height(0);
		}
	}

	$(function() {
		$(window).scroll(sticky_relocate);
		sticky_relocate();
	});

	var dir = 1;
	var MIN_TOP = 200;
	var MAX_TOP = 350;

  function validateName(value) {
    return !/[¤¢£¥¦§©ª«¬®¯°±²³µ¶¸¹º»¼½¾¿Þßþƒ†‡•…‰€™☺○♀♂♪♫◙↨☼►◄↕‼▬↑↓→←∟↔▲▼☻♥♦♣♠◘╚╔╩╦╠═╬¡░▒▓│┤╣║╗╝┐└┴┬├─┼▄█┌┘ı▀シ⌂~!@#_\$%\^&\*\(\)=\+\|\[\]\{\}\;\\\:\"\,\<\>\?\/]+/.test(value);
  }

  function validateNameNum(value) {
    return /^[^¤¢£¥¦§©ª«¬®¯°±²³µ¶¸¹º»¼½¾¿Þßþƒ†‡•…‰€™☺○♀♂♪♫◙↨☼►◄↕‼▬↑↓→←∟↔▲▼☻♥♦♣♠◘╚╔╩╦╠═╬¡░▒▓│┤╣║╗╝┐└┴┬├─┼▄█┌┘ı▀シ⌂~!@#_\$%\^&\*\(\)=\+\|\[\]\{\}\;\\\:\"\,\<\>\?\/]+$/.test(value);
  }

	function autoscroll() {
		var window_top = $(window).scrollTop() + dir;
		if (window_top >= MAX_TOP) {
			window_top = MAX_TOP;
			dir = -1;
		} else if (window_top <= MIN_TOP) {
			window_top = MIN_TOP;
			dir = 1;
		}
		$(window).scrollTop(window_top);
		window.setTimeout(autoscroll, 100);
	}

  function luhn_validate(value) {

    //Remove the first 2 digits if it's too long.
    var length = value.length;
    if(value.length === 12 || value.length === 13) {
      for(var i = length-2; i <= value.length; i++ ) {
        value = value.substring(1);
      }
    }

    // accept only digits, dashes or spaces
    if (/[^0-9-\s]+/.test(value)) {return false;}

    // The Luhn Algorithm. It's so pretty.
    var nCheck = 0, nDigit = 0, bEven = false;
    value = value.replace(/\D/g, "");

    for (var n = value.length - 1; n >= 0; n--) {
      var cDigit = value.charAt(n),
        nDigit = parseInt(cDigit, 10);

      if (bEven) {
        if ((nDigit *= 2) > 9) {nDigit -= 9;}
      }

      nCheck += nDigit;
      bEven = !bEven;
    }

    return (nCheck % 10) === 0;
  }

	// Load Events
	$(document)
	.ready(UTIL.loadEvents);


	var Bopoolen = {
		showProperties: function(args) {

			// filter result
			var propertiesMarkup,
			loadTarget = $('#loading');

			Bopoolen.showLoad(loadTarget);
			$('.listing-table-wrapper').hide();

			// load all properties
			Bopoolen.loadProperties(args).done(function(data) {


				//console.log('loadProperties', data);
				// check if data is empty
				if(data.length === 0) {
					$('#no-search-results').show();
					Bopoolen.maps(data);
				} else {
					// Init properties class
					var properties = new Properties(data);

					// append our html to #listing-table
					$('#listing-table tbody').html('');
					$('#listing-table tbody').append(properties.getTableMarkup());

					Bopoolen.maps(data);
					Bopoolen.pagination($('#listing-table'), 20);

					$('#listing-table').on('click', '.listing-property', function() { Bopoolen.clickProperty(this); });

					$('.listing-table-wrapper').show();
					$('#no-search-results').hide();
				}

				Bopoolen.hideLoad(loadTarget);

				$("#listing-table").stupidtable();


			});
		},

		showSearchAds: function(args) {
			// filter result
			var propertiesMarkup,
			loadTarget = $('#loading');

			Bopoolen.showLoad(loadTarget);
			$('.listing-table-wrapper').hide();

			Bopoolen.loadSearchAds(args).done(function(data) {
				if(data.length === 0) {

					$('#no-search-results').show();
					$('.listing-table-wrapper').hide();
					Bopoolen.maps(data);

				} else {
					Bopoolen.hideLoad(loadTarget);

					var searchad = new Searchad(data);

					// append our html to #listing-table
					$('#listing-table tbody').html('');
					$('#listing-table tbody').append(searchad.getTableMarkup());

					$('.listing-table-wrapper').show();
					$('#no-search-results').hide();

					Bopoolen.pagination($('#listing-table'), 20);
				}
				Bopoolen.hideLoad(loadTarget);
			});
		},

		loadSearchAds: function(args) {
			//console.log('args', args)
			return $.ajax({
				url: '/wp-admin/admin-ajax.php',
				type: 'GET',
				dataType: 'json',
				data: {
					'action': 'get_searchad',
					'args': args
				}
			});
		},

		showLoad: function(target) {
			target.show();
		},

		hideLoad: function(target) {
			target.hide();
		},

		pagination: function(target, count) {
			target.each(function() {
				var currentPage = 0;
				var numPerPage = count;
				var $table = $(this);
				$table.bind('repaginate', function() {
					$table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
				});
				$table.trigger('repaginate');
				var numRows = $table.find('tbody tr').length;
				var numPages = Math.ceil(numRows / numPerPage);
				var $pager = $('<ul class="pagination"></ul>');
				for (var page = 0; page < numPages; page++) {
					$('<li class="page-number"></li>').text(page + 1).bind('click', {
						newPage: page
					}, function(event) {
						currentPage = event.data.newPage;
						$table.trigger('repaginate');
						$(this).addClass('active').siblings().removeClass('active');
					}).appendTo($pager).addClass('clickable');
				}
				$('ul.pagination').remove();
				$pager.insertAfter($table).find('li.page-number:first').addClass('active');
			});
		},

		loadProperties: function(args) {
			return $.ajax({
				url: '/wp-admin/admin-ajax.php',
				type: 'GET',
				dataType: 'json',
				data: {
					'action': 'get_rentad',
					'args': args
				}
			});
		},

		sendMailCreatedAd: function(args) {
			return $.ajax({
				url: '/wp-admin/admin-ajax.php',
				type: 'GET',
				dataType: 'json',
				data: {
					'action': 'do_send_mail_when_ad_is_created',
					'args': args
				}
			}).done(function() {
			});
		},

		updateUserProfile: function(args, nonce, page) {
			return $.ajax({
				url: '/wp-admin/admin-ajax.php',
				type: 'POST',
				data: {
					'action': 'update_user_profile_on_create_update_rentad',
					'args': args,
					'nonce': nonce
				}
			})
			.done(function(response){
				//console.log(response);
        window.location = page;
			});
		},

		removeDeletedImages: function(ids, existingIds, nonce) {
			var toBeDeleted = [];
			for (i = 0; i < existingIds.length; i++) {
				if ($.inArray(existingIds[i], ids) === -1) {
					toBeDeleted.push(existingIds[i]);
				}
			}

			if (toBeDeleted.length > 0) {
				return $.ajax({
					url: '/wp-admin/admin-ajax.php',
					type: 'POST',
					data: {
						'action': 'remove_deleted_images_on_update_rentad',
						'args': toBeDeleted,
						'nonce': nonce,
					}
				})
				.done(function(response){
					//return response;
				});
			}
		},

		getUrlParameter: function (sParam) {
		    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
		        sURLVariables = sPageURL.split('&'),
		        sParameterName,
		        i;

		    for (i = 0; i < sURLVariables.length; i++) {
		        sParameterName = sURLVariables[i].split('=');

		        if (sParameterName[0] === sParam) {
		            return sParameterName[1] === undefined ? true : sParameterName[1];
		        }
		    }
		},

		maps: function(data) {

			var mapOptions = {
				zoom: 14,
				scrollwheel: false,
				draggable: true,
				center: new google.maps.LatLng(55.705328, 13.2153141),
			};

			Bopoolen.map = new google.maps.Map(document.getElementById('listing-map'), mapOptions);

			// define our array
			Bopoolen.markers = [];

			data.forEach(function(property) {

				// splite latlng to array
				var latlng = property.custom_fields._latlng[0].split(',');

				Bopoolen.markers[property.post.ID] = [];

				// init google markers
        var housingType;
        if("_housingType" in property.custom_fields) {
          housingType = property.custom_fields._housingType[0];
        } else {
          housingType = "";
        }

        var icon;
				if(housingType === "roominapartment") {
          icon = "/wp-content/themes/bopoolen/assets/images/building-apartment.png";
				} else  {
          //if(housingType === 'roominhouse' || housingType === 'roominstudent' || housingType === 'ownhouse' || housingType === 'ownapartment')
          icon = "/wp-content/themes/bopoolen/assets/images/building-house.png";
        }
				Bopoolen.markers[property.post.ID].marker = new google.maps.Marker({
					position: new google.maps.LatLng(latlng[0], latlng[1]),
					map: Bopoolen.map,
					animation: google.maps.Animation.DROP,
					icon: icon,
				});

				// set additional information
				var contentString = '<div class="infowindow">'+
				'<p class="title"><a href="'+property.post.guid+'" target="_blank">'+property.post.post_title+'</a></p>'+
				'<p class="address">'+property.custom_fields._streetaddress+', '+property.custom_fields._availableyta+' kvd</p>'+
				'<p class="small-title">Tillgänglig från</p>'+
				'<p class="available">'+property.custom_fields._availablefrom+' - '+property.custom_fields._availableto+'</p>'+
				'</div>';

				Bopoolen.markers[property.post.ID].id = property.id;
				Bopoolen.markers[property.post.ID].latlng = latlng;
				Bopoolen.markers[property.post.ID].info = new google.maps.InfoWindow({
					content: contentString
				});

				Bopoolen.markers[property.post.ID].marker.addListener('click', function() {
					Bopoolen.clickMarker(property.post.ID);
				});

			});

		},

		clickMarker: function(id) {
			var markerObj = Bopoolen.markers[id];

			//console.log(markerObj);

			Bopoolen.closeInfoWindows();
			markerObj.info.open(Bopoolen.map, markerObj.marker);

			Bopoolen.map.panTo(new google.maps.LatLng(markerObj.latlng[0], markerObj.latlng[1]));
			$('#listing-table .listing-property').removeClass('active');
			$('#listing-table .listing-property[data-id="'+markerObj.id+'"]').addClass('active');
		},

		clickProperty: function(elm) {
			var markerObj = Bopoolen.markers[$(elm).data('id')];

			$('#listing-table .listing-property').removeClass('active');
			$(elm).addClass('active');

			Bopoolen.closeInfoWindows();
			markerObj.info.open(Bopoolen.map, markerObj.marker);
			Bopoolen.map.panTo(new google.maps.LatLng(markerObj.latlng[0], markerObj.latlng[1]));

		},

		closeInfoWindows: function() {
			for(var key in Bopoolen.markers) {
				var marker = Bopoolen.markers[key];
				marker.info.close();
			}
		}
	};

})(jQuery); // Fully reference jQuery after this point.

<?php
/**
* Template Name: Search ads listing page - Map style
*/
?>

<?php while (have_posts()) : the_post(); ?>
    <?php
  if(!is_user_logged_in()) {
?>
  <div class="content">
    <div class="row">
      <div class="col-sm-12">
        <header>
          <h1 style="text-align:center"><?php echo __('Not signed in', 'bopoolen') ?></h1>
        </header>
        <div class="padding">
          <div class="form-segment-wrapper">
            <p><?php _e('För att komma åt sökesannonserna måste du vara inloggad. Detta för att öka säkerheten och för att bedragare inte ska få tillgång till annonsernas kontaktuppgifter.', 'bopoolen'); ?></p>
            <?php get_template_part('templates/content', 'login'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php
  } else {?>

  <div class="container-fluid listing-page no-padding">
    <!-- <div class="row"> -->
      <!-- <div class="row"> -->
      <div class="col-md-12">
        <?php get_template_part('templates/listing/search', 'map'); ?>
      </div>
        <div class="container filters">
          <div class="col-md-12">
            <h1><?php _e('Find tenant', 'bopoolen'); ?></h1>
            <?php get_template_part('templates/search-ads-listing/search', 'form'); ?>
          </div>
        </div>
      <!-- </div> -->
      <!-- <div class="row listings"> -->
        <div class="container-fluid listings">
          <div class="col-md-12">
            <?php get_template_part('templates/search-ads-listing/search', 'listing'); ?>
          </div>
        </div>
      <!-- </div> -->
    <!-- </div> -->
  </div>

<?php }endwhile; ?>

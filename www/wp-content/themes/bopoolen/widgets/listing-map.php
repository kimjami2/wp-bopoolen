<?PHP
class wpb_listing_map extends WP_Widget {

  function __construct() {
    parent::__construct(
    // widget ID
    'listing_map',

    // widget title
    __('Google Maps with listings', 'wpb_widget_domain'),

    // widget params
    array(
      'description' => __( 'Listing map', 'wpb_widget_domain' ),
      'panels_groups' => array('bopoolen'),
      'panels_icon' => 'ap-logo-icon'
      )
    );

    // add media upload scripts
    add_action('admin_enqueue_scripts', array($this, 'upload_scripts'));
  }

  public function upload_scripts()
  {
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    wp_enqueue_script('upload_media_widget', '/wp-content/themes/galaxystreet/widgets/js/upload-media.js', array('jquery'));

    wp_enqueue_style('thickbox');
  }

  // admin widget
  public function widget( $args, $instance ) {

    $title = apply_filters( 'widget_title', $instance['title'] );

    /*
    * HTML
    */
    $html = '
    <div class="widget-listing-map">
      <div>
        <div id="listing-map"></div>
      </div>
      <div class="listings">
        <table id="listing-table" class="table" width="100%">
          <thead>
            <tr data-id="1">
              <th>'.__('Beskrivning', 'bopoolen').'</th>
              <th>'.__('Stad', 'bopoolen').'</th>
              <th>'.__('Typ', 'bopoolen').'</th>
              <th>'.__('Storlek', 'bopoolen').'</th>
              <th>'.__('Från', 'bopoolen').'</th>
              <th>'.__('Till', 'bopoolen').'</th>
              <th>'.__('Pris', 'bopoolen').'</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>

          </tbody>
        </table>
        <div class="clearfix">
          <a href="/uthyresbostader/" title="" class="show-all btn btn-button-table btn-sm">'.__('Visa alla annonser', 'bopoolen').'</a>
        </div>
      </div>
    </div>
    ';

    echo $html;
  }

  // Widget Backend
  public function form( $instance ) {
    ?>

    <?php
  }

  // Updating widget replacing old instances with new
  public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['backgroundImage'] 		   = ( ! empty( $new_instance['backgroundImage'] ) ) ? strip_tags( $new_instance['backgroundImage'] ) : '';
    return $instance;
  }
}

// Register and load the widget
function wpb_load_listing_map() {
  register_widget('wpb_listing_map');
}
add_action('widgets_init', 'wpb_load_listing_map');
?>

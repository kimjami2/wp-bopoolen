<?php
/**
* Template Name: Choose ad to browse
*/
?>

<?php
$thumb_id = get_post_thumbnail_id();
$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
$thumb_url = (isset($thumb_url_array[0])) ? $thumb_url_array[0] : '';
?>

<div class="container-fluid background-image extra-padding" style="background-image: url('<?php echo $thumb_url_array[0]; ?>');">


  <?php while (have_posts()) : the_post(); ?>
    <div style="padding: 15px;">
      <?php the_content(); ?>
    </div>
  <?php endwhile; ?>

  <div class="container">
    <div class="row">
      <div class="col-md-4 col-md-push-2 col-xs-12">
        <a href="/uthyresbostader" class="card white">
          <p class="icon">
            <i class="fa fa-home" aria-hidden="true" style="font-size: 80px;"></i>
          </p>
          <p class="info">
            <?php _e('Find housing', 'bopoolen'); ?>
          </p>
        </a>
      </div>
      <div class="col-md-4 col-md-push-2 col-xs-12">
        <a href="/sokes-annonser" class="card white">
          <p class="icon">
            <i class="fa fa-male" aria-hidden="true" style="font-size: 80px;"></i>
            <i class="fa fa-female" aria-hidden="true" style="font-size: 80px;"></i>
          </p>
          <p class="info">
            <?php _e('Find tenant', 'bopoolen'); ?>
          </p>
        </a>
      </div>
    </div>
  </div>
</div>

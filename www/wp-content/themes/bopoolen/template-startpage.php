<?php
/**
 * Template Name: Annonser landing
 */
?>

<?php
  $_SESSION['site-section'] = 'annonser';
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'start'); ?>
<?php endwhile; ?>

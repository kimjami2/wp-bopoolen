<?php
/**
* Sage includes
*
* The $sage_includes array determines the code library included in your theme.
* Add or remove files to the array as needed. Supports child theme overrides.
*
* Please note that missing files will produce a fatal error.
*
* @link https://github.com/roots/sage/pull/1042
*/
$sage_includes = [
  'lib/dashboard.php',
  'lib/assets.php',           // Scripts and stylesheets
  'lib/extras.php',           // Custom functions
  'lib/setup.php',            // Theme setup
  'lib/titles.php',           // Page titles
  'lib/wrapper.php',          // Theme wrapper class
  'lib/customizer.php',       // Theme customizer
  'lib/rentad.php',           // Custom posttype for rent ads
  'lib/searchad.php',         // Custom posttype for search ads
  'lib/userroles.php',        // Custom user roles
  'lib/postconfig.php',       // Custom config for posts
  'lib/poststatus.php',       // Custom config for posts statuses
  'lib/userprofiles.php',     // Custom config for user profiles
  'lib/custompostcolumns.php',
  'lib/socialmediashortcode.php',
  'lib/pagebuilder.php',
  'lib/emailtemplates.php',   // Custom posttype for email templates and related functions
];


foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);


/*
* Include custom widgets
*/

foreach ( glob( plugin_dir_path( __FILE__ ) . "widgets/*.php" ) as $file ) {
  include_once $file;
}

/*
* Enque scripts
*/

function thirdpp_scripts() {
  wp_enqueue_script('bootstrap-js', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js');
  wp_enqueue_script('property', get_template_directory_uri() . '/assets/scripts/property.js', array(), '1.0.0');
  wp_enqueue_script('sorter', get_template_directory_uri() . '/assets/scripts/sorter.js', array(), '1.0.0');
  wp_enqueue_script('nouislider-js', get_template_directory_uri() . '/assets/scripts/nouislider.min.js', array(), '1.0.0');
  wp_enqueue_script('google-maps', 'https://maps.googleapis.com/maps/api/js?v=3.exp', array(), '3.0');
}

add_theme_support('post-thumbnails');
update_option('thumbnail_size_w', 150);
update_option('thumbnail_size_h', 150);
update_option('large_size_w', 500);

function count_ads_inactiveErased () {
  global $wpdb;

  $query = 'SELECT COUNT(ID) FROM wp_posts WHERE post_status = "raderad" AND (post_type = "searchad" OR post_type="rentad" )';

  $results = $wpdb->get_results($wpdb->prepare($query, ''), ARRAY_N);
  $tmp = $results[0][0];

  $query = 'SELECT COUNT(ID) FROM wp_posts WHERE post_status = "draft" AND (post_type = "searchad" OR post_type="rentad" )';
  $results = $wpdb->get_results($wpdb->prepare($query, ''), ARRAY_N);

  return intval($results[0][0])+intval($tmp);
}

function count_ads_meta($meta, $meta2, $value) {
  global $wpdb;
  $results = $wpdb->get_results($wpdb->prepare('SELECT COUNT(meta_id) FROM wp_postmeta WHERE meta_value = %s AND(meta_key = "_inactivereason" OR meta_key = "_deletereason")', $value), ARRAY_N);
  if($results[0][0] == null) {
    return 0;
  }
  return $results[0][0];
}

add_filter( 'post_row_actions', 'remove_row_actions', 10, 1 );
function remove_row_actions( $actions )
{
  unset( $actions['trash'] );
  return $actions;
}

function hide_publishing_actions(){
  global $post;
    echo '
      <style type="text/css">
          #delete-action{
              display:none;
          }
      </style>
    ';
}
add_action('admin_head-post.php', 'hide_publishing_actions');
add_action('admin_head-post-new.php', 'hide_publishing_actions');

add_action('reminder_update_rentad_searchad', 'update_rentad_searchad');

function update_rentad_searchad() {

  $args = array(
    'posts_per_page'   => -1,
    'post_type'        => array( 'searchad', 'rentad' ),
    'post_status'      => 'publish'
  );

  $posts_array = get_posts( $args );

  $unpublish_after = 10;

  $current_date = strtotime('now');

  $unpublish_date = $current_date - (3600 * 24 * $unpublish_after);

  foreach ($posts_array as $post ) {
    $post_time = $post -> post_date;
    $post_id = $post -> ID;
    $post_author = $post -> post_author;

    if ($unpublish_date > strtotime($post_time)) {
      $reminder = get_post_meta($post_id, '_emailreminder', true);
      if(!$reminder && $reminder !== 'sent') {
        //Send mail

        $user_email = get_user_email($post_author)->user_email;

        $replace_values['NAME'] = get_user_meta($post_author, 'first_name', true) . ' ' . get_user_meta($post_author, 'last_name', true);
        $replace_values['SV_TITLE'] = $post->post_title;
        if(get_post_meta($post_id, '_titleeng', true) === '') {
          $replace_values['EN_TITLE'] = $post->post_title;
        } else {
          $replace_values['EN_TITLE'] = get_post_meta($post_id, '_titleeng', true);
        }
        $replace_values['LINK'] = 'http://www.test.bopoolen.nu/uppdatera-annons/?post_id='.$post_id;


        $template = get_post_field('post_content', 564);
        $pattern = '!\{\{(\w+)\}\}!';
        $content = preg_replace_callback($pattern, function ($matches) use ($replace_values) {
          return $replace_values[$matches[1]];
        }, $template);

        add_post_meta($post_id, '_emailreminder', 'sent');


        /*$body = sprintf( 'Din annons blir inaktiverad om 4 dagar. Klicka på länken nedan för att hålla den aktiv i två veckor till.

%s

Vänliga hälsningar,
BoPoolen-teamet',
          $link
        );*/
        wp_mail( $user_email, get_post_meta(564, '_email_subject', true), $content );

      }
    }
  }
}

wp_schedule_event( time() + 1, "daily", "reminder_update_rentad_searchad");

function get_user_ids() {
  global $wpdb;

  $results = $wpdb->get_results("SELECT ID from $wpdb->users");
  return $results;
}

function get_user_email($id) {
  global $wpdb;

  $result = $wpdb->get_results($wpdb->prepare("SELECT user_email from $wpdb->users WHERE ID = %s", $id));
  return $result[0];
}

function check_subscription($post) {
  $user_emails = [];
  foreach(get_user_ids() as $user_id) {
    $meta = get_user_meta($user_id->ID, '_emailsub', true);
    if($meta !== '') {
      $nomultiselect = explode('multiselect-all', $meta);

      if($nomultiselect[0] == '') {
        unset($nomultiselect[0]);
      }
      if($nomultiselect[1] == '/') {
        unset($nomultiselect[1]);
      }
      if ($nomultiselect[2] == '/') {
        unset($nomultiselect[2]);
      }
      if($nomultiselect[3] == '/') {
        unset($nomultiselect[3]);
      }
      $user_emailsub = [];
      foreach($nomultiselect as $sub) {
        $tmp = explode('/', $sub);
        foreach($tmp as $s) {
          if($s !== '') {
            array_push($user_emailsub, $s);
          }
        }
      }
      $post_area = get_post_meta($post->ID, '_area');
      $post_contractType = get_post_meta($post->ID, '_contractType');
      $post_housingType = get_post_meta($post->ID, '_housingType');
      if(array_search($post_area[0], $user_emailsub) !== false &&
        array_search($post_contractType[0], $user_emailsub) !== false &&
        array_search($post_housingType[0], $user_emailsub) !== false) {
        $email = get_user_email($user_id->ID);
        array_push($user_emails, $email->user_email);
      }
    }
  }
  return $user_emails;
}
//check_subscription();
add_action('transition_post_status', 'check_rejected', 10, 3 );

function check_rejected($new_status, $old_status, $post) {
  if('rejected' === $old_status) {
    //Flag the post
    add_post_meta($post->ID, '_postrejected', 'true', true);
  } else {
    //Remove the flag
    delete_post_meta($post->ID, '_postrejected');
  }
}


add_action( 'transition_post_status', 'send_mails_on_publish', 10, 3 );

function send_mails_on_publish( $new_status, $old_status, $post )
{

  if ( 'publish' !== $new_status or 'publish' === $old_status
      or 'rentad' !== get_post_type( $post ) )
    return;

  $emails      = check_subscription($post);

  $replace_values['LINK'] = get_permalink( $post );


  $template = get_post_field('post_content', 1010);
  $pattern = '!\{\{(\w+)\}\}!';
  $content = preg_replace_callback($pattern, function ($matches) use ($replace_values) {
    return $replace_values[$matches[1]];
  }, $template);

  /*$body = sprintf( 'En ny annons som stämmer överens med dina önskemål har precis lagts upp!

Se %s

Vänliga hälsningar,
BoPoolen-teamet',
      get_permalink( $post )
  );*/

  foreach($emails as $email) {
    wp_mail( $email, get_post_meta(1010, '_email_subject', true), $content );
  }
}

add_action('init', 'thirdpp_scripts');

add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
  if (!current_user_can('administrator') && !is_admin()) {
    show_admin_bar(false);
  }
}

/*
 * Function to redirect even after headers are sent
 */
function custom_redirect( $url ) {

    if ( headers_sent() ) {

      exit( '<script type="text/javascript">window.location=\'' . $url . '\';</script>' );

    } else {

      wp_redirect( $url );
      exit();

    }
}

function get_user_by_meta_data( $meta_key, $meta_value ) {

  // Query for users based on the meta data
  $user_query = new WP_User_Query(
    array(
      'meta_key'	  =>	$meta_key,
      'meta_value'	=>	$meta_value
    )
  );

  // Get the results from the query, returning the first user
  $users = $user_query->get_results();

  return $users[0];
}


/*
* Get rent ads
*/

add_action("wp_ajax_nopriv_search_user", "search_user");
add_action("wp_ajax_search_user", "search_user");


function search_user($searchQuery = 'peter') {
  global $wpdb;

  $searchQuery = $_GET['search_value'];

  $args = array(
    'meta_query'      => array (
      'relation'        => 'OR',
      array(
        'key'     => 'first_name',
        'value'   => $searchQuery,
        'compare' => 'LIKE'
      ),
      array(
        'key'     => 'last_name',
        'value'   => $searchQuery,
        'compare' => 'LIKE'
      ),
      array(
        'key'     => '_username',
        'value'   => $searchQuery,
        'compare' => 'LIKE'
      )
    )
  );

  $user_query = new WP_User_Query($args);
  $meta_result = $user_query->get_results();

  $searchQuery = '*'.$searchQuery.'*';
  $args = array(
    'search'          => $searchQuery,
    'search_columns'  => array('user_login', 'display_name', 'user_email'),
  );

  $user_query = new WP_User_Query($args);
  $table_result = $user_query->get_results();

  $result_array = [];

  if(count($table_result) > count($meta_result)) {
    foreach($table_result as $table) {
      $found = false;
      foreach($meta_result as $meta) {
        if($table->ID == $meta->ID) {
          $found = true;
        }
      }
      if(!$found) {
        $nickname = get_user_meta($table->ID, '_username', true);
        $table->data->nickname = $nickname;
        array_push($result_array, $table);
      }
    }
    foreach($meta_result as $meta) {
      $nickname = get_user_meta($meta->ID, '_username', true);
      $meta->data->nickname = $nickname;
      array_push($result_array, $meta);
    }

  } else {
    foreach($meta_result as $meta) {
      $found = false;
      foreach($table_result as $table) {
        if($table->ID == $meta->ID) {
          $found = true;
        }
      }
      if(!$found) {
        $nickname = get_user_meta($meta->ID, '_username', true);
        $meta->data->nickname = $nickname;
        array_push($result_array, $meta);
      }
    }

    foreach($table_result as $table) {
      $nickname = get_user_meta($table->ID, '_username', true);
      $table->data->nickname = $nickname;
      array_push($result_array, $table);
    }
  }
  echo json_encode($result_array);
wp_die();
}

/*
 * Gets all the image ids for a certain rentad post
 */

add_action("wp_ajax_get_image_ids_for_rentad", "get_image_ids_for_rentad");

function get_image_ids_for_rentad() {
  $nonce = $_POST['nonce'];
  if ( !wp_verify_nonce( $nonce, 'modified_by_user_' . get_current_user_id() ) )
        wp_die();

  $post_id = $_POST['post_id'];
  $imageIds = get_post_meta($post_id, '_imageids', true);
  $imageArray = array();

  if ($imageIds != "") {

    $imageIds = explode(',', get_post_meta($post_id, '_imageids', true));

    foreach ( $imageIds as $imageId ) {

    $url = wp_get_attachment_url($imageId);
    $filename = basename( $url );
    $imageArray[] = array( 'serverId' => $imageId, 'url' => $url, 'name' => $filename);

    //To be Implemented Later to include the filesize
    //$filesize = filesize(get_attached_file( $imageId ));
    //$imageArray[] = array( 'serverId' => $imageId, 'url' => $url, 'name' => $filename, 'size' => $filesize);

   }

  }

  echo json_encode($imageArray);
  wp_die();
}

add_action("wp_ajax_get_profile_picture", "get_profile_picture");

function get_profile_picture() {

  $post_id = $_POST['post_id'];

  if ($post_id != "") {

      $url = wp_get_attachment_url(intval($post_id));
      $filename = basename( $url );
      $imageArray[] = array( 'serverId' => $post_id, 'url' => $url, 'name' => $filename);
  }

  echo json_encode($imageArray);
  wp_die();
}


/*
 * Delete User Uploaded Images On Updating the rentad
 */

add_action("wp_ajax_remove_deleted_images_on_update_rentad", "remove_deleted_images_on_update_rentad");

function remove_deleted_images_on_update_rentad() {
  $nonce = $_POST['nonce'];
  if ( !wp_verify_nonce( $nonce, 'modified_by_user_' . get_current_user_id() ) )
        wp_die();

  $image_ids = $_POST['args'];

  foreach ($image_ids as $image_id) {
      if (current_user_can( 'edit_post', $image_id ))
        wp_delete_attachment( $image_id, true );
  }

  echo "Done...";
  wp_die();
}

/*
 * Allow users who are authors of the post to edit the post
 * used to allow updating the custom post type using the JSON API
 */
function my_map_meta_cap( $caps, $cap, $user_id, $args ){
    if ( 'edit_post' == $cap ) {
        $post = get_post( $args[0] );
        $post_type = get_post_type_object( $post->post_type );
        $caps = array();
        if ( $user_id == $post->post_author )
            $caps[] = $post_type->cap->edit_posts;
        else
            $caps[] = $post_type->cap->edit_others_posts;
    }
    return $caps;
}
add_filter( 'map_meta_cap', 'my_map_meta_cap', 10, 4 );

/*
 * Update the user metas (_viewname, _viewphone, _viewaddress) on creating a
 * a rentad
 */

add_action("wp_ajax_update_user_profile_on_create_update_rentad", "update_user_profile_on_create_update_rentad");

function update_user_profile_on_create_update_rentad() {
  $nonce = $_POST['nonce'];
  if ( !wp_verify_nonce( $nonce, 'modified_by_user_' . get_current_user_id() ) )
        wp_die();

  parse_str($_POST['args'], $user_meta);
  $user_id = get_current_user_id();

  foreach ($user_meta as $key => $value) {
    update_user_meta( $user_id, $key, $value);
  }

  echo "User Profile Updated!";
  wp_die();
}

/*
* Get search ads
*/

add_action("wp_ajax_nopriv_get_searchad", "get_searchad");
add_action("wp_ajax_get_searchad", "get_searchad");

function get_searchad() {

  $args = isset($_REQUEST['args']) ? $_REQUEST['args'] : false;
  $data = array();

  if($args) {

    $postsArray = array(
      'posts_per_page'	=> -1,
      'post_type'			  => 'searchad',
      'meta_query'      => array(
        'relation'		  => 'AND',
      )
    );

    /*
    * Areas
    */
    $areas = array(
      'cityLund'          => $args['city-lund'] == 'true' ? array('key'	=> '_area', 'value' => 'lund', 'compare' => 'LIKE') : false,
      'cityMalmo'         => $args['city-malmo'] == 'true' ? array('key'	=> '_area', 'value' => 'malmö', 'compare' => 'LIKE') : false,
      'cityHelsingborg'   => $args['city-helsingborg'] == 'true' ? array('key'	=> '_area', 'value' => 'helsingborg', 'compare' => 'LIKE') : false,
      'outsideLund'       => $args['outside-lund'] == 'true' ? array('key'	=> '_area', 'value' => 'outside-lund', 'compare' => 'LIKE') : false,
    );

    $area = [];
    $i = 0;
    foreach($areas as $filter) {
      if($filter) {
        if($i == 0) {
          $area['relation'] = 'OR';
          array_push($area, $filter);
        } else {
          array_push($area, $filter);
        }
        $i++;
      }
    }

    array_push($postsArray['meta_query'], $area);

    $types = array(
      'roominhouse'       => $args['roominhouse'] == 'true' ? array('key'	=> '_accommodationType', 'value' => 'roominhouse', 'compare' => 'LIKE') : false,
      'roominapartment'   => $args['roominapartment'] == 'true' ? array('key'	=> '_accommodationType', 'value' => 'roominapartment', 'compare' => 'LIKE') : false,
      'roominstudent'     => $args['roominstudent'] == 'true' ? array('key'	=> '_accommodationType', 'value' => 'roominstudent', 'compare' => 'LIKE') : false,
      'ownhouse'          => $args['ownhouse'] == 'true' ? array('key'	=> '_accommodationType', 'value' => 'ownhouse', 'compare' => 'LIKE') : false,
      'ownapartment'      => $args['ownapartment'] == 'true' ? array('key'	=> '_accommodationType', 'value' => 'ownapartment', 'compare' => 'LIKE') : false,
    );
    $room = [];
    $i = 0;
    foreach($types as $filter) {
      if($filter) {
        if($i == 0) {
          $room['relation'] = 'OR';
          array_push($room, $filter);
        } else {
          array_push($room, $filter);
        }
        $i++;
      }
    }

    array_push($postsArray['meta_query'], $room);

    $contracts = array(
      'contractfirst'     => $args['contractfirst'] == 'true' ? array('key'	=> '_contractType', 'value' => 'contractfirst', 'compare' => 'LIKE') : false,
      'contractsecond'    => $args['contractsecond'] == 'true' ? array('key'	=> '_contractType', 'value' => 'contractsecond', 'compare' => 'LIKE') : false,
      'contractchange'    => $args['contractchange'] == 'true' ? array('key'	=> '_contractType', 'value' => 'contractchange', 'compare' => 'LIKE') : false,
    );

    $contract = [];
    $i = 0;
    foreach($contracts as $filter) {
      if($filter) {
        if($i == 0) {
          $contract['relation'] = 'OR';
          array_push($contract, $filter);
        } else {
          array_push($contract, $filter);
        }
        $i++;
      }
    }

    array_push($postsArray['meta_query'], $contract);

    if(isset($args['search-text'])) {
      global $wpdb;
      $search_query = 'SELECT ID FROM wp_posts WHERE post_type = "searchad" AND (post_content LIKE %s OR post_title LIKE %s)';

      $like = '%'.$args['search-text'].'%';
      $results = $wpdb->get_results($wpdb->prepare($search_query, $like, $like), ARRAY_N);
      foreach($results as $key => $array){
        $quote_ids[] = $array[0];
      }

      if(!empty($quote_ids)) {
        $postsArray['post__in'] = $quote_ids;
      } else {
        $postsArray['post__in'] = array(0);
      }
    }

    $posts = get_posts($postsArray);

  } else {
    $posts = get_posts(array(
      'posts_per_page'	=> -1,
      'post_type'			  => 'searchad',
    ));

  }

  foreach($posts as $post) {
    array_push($data, array('post' => $post, 'custom_fields' => get_post_custom($post->ID)));
  }
  if($args) {
    if(isset($args['from'])) {
      $data2 = array();
      foreach ($data as $post) {
        if (($args['from'] >= $post['custom_fields']['_availablefrom'][0]) && ($args['from'] < $post['custom_fields']['_availableto'][0])) {
          array_push($data2, $post);
        } else if (($post['custom_fields']['_availableto'][0] === 'Tillsvidare') && ($args['from'] >= $post['custom_fields']['_availablefrom'][0])) {
          array_push($data2, $post);
        }
      }
      echo json_encode($data2);
      wp_die();
    }
  }



  echo json_encode($data);

  wp_die();

}

/*
* Get rent ads
*/

add_action("wp_ajax_nopriv_get_rentad", "get_rentad");
add_action("wp_ajax_get_rentad", "get_rentad");

function get_rentad() {

  $args = isset($_REQUEST['args']) ? $_REQUEST['args'] : false;
  $data = array();

  if($args) {

    $postsArray = array(
      'posts_per_page'	=> -1,
      'post_type'			  => 'rentad',
      'meta_query'      => array(
        'relation'		  => 'AND'
      )
    );

    /**
     * Areas
     */

    $areas = array(
      'cityLund'          => $args['city-lund'] == 'true' ? array('key'	=> '_area', 'value' => 'lund', 'compare' => 'LIKE') : false,
      'cityMalmo'         => $args['city-malmo'] == 'true' ? array('key'	=> '_area', 'value' => 'malmö', 'compare' => 'LIKE') : false,
      'cityHelsingborg'   => $args['city-helsingborg'] == 'true' ? array('key'	=> '_area', 'value' => 'helsingborg', 'compare' => 'LIKE') : false,
      'outsideLund'       => $args['outside-lund'] == 'true' ? array('key'	=> '_area', 'value' => 'other', 'compare' => 'LIKE') : false,
    );

    $area = [];
    $i = 0;
    foreach($areas as $filter) {
      if($filter) {
        if($i == 0) {
          $area['relation'] = 'OR';
          array_push($area, $filter);
        } else {
          array_push($area, $filter);
        }
        $i++;
      }
    }

    array_push($postsArray['meta_query'], $area);

    /*
    * types
    */

    $types = array(
      'roominhouse'       => $args['roominhouse'] == 'true' ? array('key'	=> '_roominhouse', 'value' => 'checked', 'compare' => 'IN') : false,
      'roominapartment'   => $args['roominapartment'] == 'true' ? array('key'	=> '_roominapartment', 'value' => 'checked', 'compare' => 'IN') : false,
      'roominstudent'     => $args['roominstudent'] == 'true' ? array('key'	=> '_roominstudent', 'value' => 'checked', 'compare' => 'IN') : false,
      'ownhouse'          => $args['ownhouse'] == 'true' ? array('key'	=> '_ownhouse', 'value' => 'checked', 'compare' => 'IN') : false,
      'ownapartment'      => $args['ownapartment'] == 'true' ? array('key'	=> '_ownapartment', 'value' => 'checked', 'compare' => 'IN') : false,
    );
    $room = [];
    $i = 0;
    foreach($types as $filter) {
      if($filter) {
        if($i == 0) {
          $room['relation'] = 'OR';
          array_push($room, $filter);
        } else {
          array_push($room, $filter);
        }
        $i++;
      }
    }

    array_push($postsArray['meta_query'], $room);

    /*
    * Contracts
    */

    $contracts = array(
      'contractfirst'     => $args['contractfirst'] == 'true' ? array('key'	=> '_contractfirst', 'value' => 'checked', 'compare' => 'IN') : false,
      'contractsecond'    => $args['contractsecond'] == 'true' ? array('key'	=> '_contractsecond', 'value' => 'checked', 'compare' => 'IN') : false,
      'contractchange'    => $args['contractchange'] == 'true' ? array('key'	=> '_contractchange', 'value' => 'checked', 'compare' => 'IN') : false,
    );

    $contract = [];
    $i = 0;
    foreach($contracts as $filter) {
      if($filter) {
        if($i == 0) {
          $contract['relation'] = 'OR';
          array_push($contract, $filter);
        } else {
          array_push($contract, $filter);
        }
        $i++;
      }
    }

    array_push($postsArray['meta_query'], $contract);

    $priceFilter = array(
      'relation' => 'AND',
      'maxRent' => array('key' => '_rentprice', 'value' => intval($args['max-rent']), 'compare' => '<=', 'type' => 'NUMERIC'),
      'minRent' => array('key' => '_rentprice', 'value' => intval($args['min-rent']), 'compare' => '>=', 'type' => 'NUMERIC'),
    );

    array_push($postsArray['meta_query'], $priceFilter);

    /*
    * Search text
    */

    if(isset($args['search-text'])) {
      global $wpdb;
      $search_query = 'SELECT ID FROM wp_posts WHERE post_type = "rentad" AND (post_content LIKE %s OR post_title LIKE %s)';

      $like = '%'.$args['search-text'].'%';
      $results = $wpdb->get_results($wpdb->prepare($search_query, $like, $like), ARRAY_N);
      foreach($results as $key => $array){
        $quote_ids[] = $array[0];
      }

      if(!empty($quote_ids)) {
        $postsArray['post__in'] = $quote_ids;
      } else {
        $postsArray['post__in'] = array(0);
      }

    }

    $posts = get_posts($postsArray);

  } else {
    $posts = get_posts(array(
      'posts_per_page'	=> -1,
      'post_type'			  => 'rentad',
    ));
  }

  foreach($posts as $post) {
    array_push($data, array('post' => $post, 'custom_fields' => get_post_custom($post->ID)));
  }
  echo json_encode($data);

  wp_die();
}

do_action( 'user_register', $user_id );
add_action('user_register', 'create_user_email');

function create_user_email($user) {

  $email = get_user_email($user)->user_email;
  $username = get_user_meta($user, '_username', true);
  $name = get_user_meta($user, 'first_name', true) . ' ' . get_user_meta($user, 'last_name', true);

  /*$body = sprintf( 'Tack för att du registrerat dig på våran hemsida.

Du kan nu logga in och se annonser från andra som söker boende och hyresgäster på hemsidan.

Vänliga hälsningar,
BoPoolen-teamet'
  );*/

  $template = get_post_field('post_content', 953);
  $pattern = '!\{\{(\w+)\}\}!';
  $replace_values['NAME'] = $name;
  $replace_values['USERNAME'] = $username;
  $content = preg_replace_callback($pattern, function ($matches) use ($replace_values) {
    return $replace_values[$matches[1]];
  }, $template);
  wp_mail( $email, get_post_meta(953, '_email_subject', true), $content );
}

add_filter( 'wp_mail_content_type', function( $content_type ) {
  return 'text/html';
});

if( ! function_exists( 'custom_login_fail' ) ) {
  function custom_login_fail( $username ) {
    $referrer = $_SERVER['HTTP_REFERER']; // where did the post submission come from?
    // if there's a valid referrer, and it's not the default log-in screen
    if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
      if ( !strstr($referrer,'?login=failed') ) { // make sure we don’t append twice
        wp_redirect( $referrer . '?login=failed' ); // append some information (login=failed) to the URL for the theme to use
      } else {
        wp_redirect( $referrer );
      }
      exit;
    }
  }
}
add_action( 'wp_login_failed', 'custom_login_fail' ); // hook failed login
if( ! function_exists( 'custom_login_empty' ) ) {
  function custom_login_empty(){
    $referrer = $_SERVER['HTTP_REFERER'];
    if ( strstr($referrer,get_home_url()) && $user==null ) { // mylogin is the name of the loginpage.
      if ( !strstr($referrer,'?login=empty') ) { // prevent appending twice
        wp_redirect( $referrer . '?login=empty' );
      } else {
        wp_redirect( $referrer );
      }
    }
  }
}
add_action( 'authenticate', 'custom_login_empty');


/*
* Count Rating
*/

function count_avg_rating($user_id = 3) {

  $queryOpts = array(
    'orderby' => 'date',
    'order' => 'DESC',
    'showposts' => min(5, 100),
    'post_type' => 'wpcr3_review',
    'post_status' => 'publish',
    'relation' => 'AND'
  );

  $meta_query[] = array(
    'key' => "wpcr3_review_post",
    'value' => $user_id,
    'compare' => '='
  );

  $queryOpts['meta_query'] = $meta_query;

  $reviews = new WP_Query($queryOpts);

  $totalRating  = 0;
  $totalReviews = count($reviews->posts);

  foreach($reviews->posts as $review) {
    $post_meta = get_post_meta($review->ID);
    $totalRating = $totalRating + $post_meta['wpcr3_review_rating'][0];
  }

  return round($totalRating / $totalReviews);

}

/*---------------------------------------------------
Custom posts Email
----------------------------------------------------*/
function custom_post_testimonials() {
  $labels = array(
    'name'               => _x( 'Emails', 'post type general name' ),
    'singular_name'      => _x( 'Emails', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'bTestimonial' ),
    'add_new_item'       => __( 'Add New Email' ),
    'edit_item'          => __( 'Edit Email' ),
    'new_item'           => __( 'New Email' ),
    'all_items'          => __( 'All Emails' ),
    'view_item'          => __( 'View Email' ),
    'search_items'       => __( 'Search Emails' ),
    'not_found'          => __( 'No testimonials found' ),
    'not_found_in_trash' => __( 'No testimonials found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Emails'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Create emails',
    'public'        => true,
    'menu_position' => 5,
    'menu_icon'     => 'dashicons-format-quote',
    'supports'      => array('title', 'editor', 'thumbnail'),
    'has_archive'   => true,
  );
  register_post_type( 'testimonials', $args );
}
add_action( 'init', 'custom_post_testimonials' );

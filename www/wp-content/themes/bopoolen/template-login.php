<?php
/**
 * Template Name: Login page
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'login'); ?>
<?php endwhile; ?>

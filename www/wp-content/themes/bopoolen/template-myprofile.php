<?php
/**
 * Template Name: My Profile
 */
?>

<?php
  if(!is_user_logged_in ()) {
?>
  <div class="content">
    <div class="row">
      <div class="col-sm-12">
        <header>
          <h1><?php echo __('Not signed in', 'bopoolen') ?></h1>
        </header>
        <div class="form-segment-wrapper">
          <?php get_template_part('templates/content', 'login'); ?>
        </div>
      </div>
    </div>
  </div>
<?php
  } else {
?>
<?php while (have_posts()) : the_post(); ?>

  <?php get_template_part('templates/content', 'profile'); ?>
<?php endwhile; ?>
<?php
  }
?>

<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;
session_start();
#SELECT COUNT(ID) FROM wp_posts where post_status = 'published' AND (post_type = 'searchad' OR post_type = 'rentad');
do_action( 'wpml_switch_language', $_SESSION['lang']);
?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    <?php if (has_post_thumbnail( $post->ID ) && is_page() &&
          !is_page_template('template-choose-ad-to-create.php') &&
          !is_page_template('template-choose-ad-to-browse.php')): ?>
      <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
      <div id="topimages-bg" style="background-image: url('<?php echo $image[0]; ?>')">

      </div>
    <?php endif; ?>

    <?php if (is_page_template('template-startpage.php') ||
              is_page_template('template-listing-page.php') ||
              is_page_template('content-single-rentad.php') ||
              is_page_template('template-landing.php') ||
              is_page_template('template-choose-ad-to-create.php') ||
              is_page_template('template-choose-ad-to-browse.php') ||
              is_page_template('template-links.php') ||
              is_page_template('template-information.php') ||
              is_page_template('template-searchads-listing-page.php'))
          : ?>
        <?php if(is_page_template('template-searchads-listing-page.php') && !is_user_logged_in()) {  ?>
        <div class="wrap" role="document">
            <div class="content clearfix">
                <main class="main">
                    <?php include Wrapper\template_path(); ?>
                </main><!-- /.main -->
            </div><!-- /.content -->
        </div><!-- /.wrap -->

        <?php } else { ?>

    <div class="wrap" role="document">
      <div class="content clearfix">
        <main class="main full-width">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php } ?>

    <?php else: ?>
    <?php if ((is_page_template('template-createpost.php') && !is_user_logged_in()) ||
        (is_page_template('template-searchpost.php') && !is_user_logged_in()) ||
        (is_page_template('template-myprofile.php') && !is_user_logged_in()) ||
        (is_page_template('template-find-user.php') && !is_user_logged_in())) {?>
        <script type="text/javascript">
            window.location.href = '/logga-in';
        </script>
    <?php } else { ?>

    <div class="wrap container" role="document">
      <div class="content row">
        <main class="main">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
        <?php if (Setup\display_sidebar()) : ?>
          <aside class="sidebar">
            <?php include Wrapper\sidebar_path(); ?>
          </aside><!-- /.sidebar -->
        <?php endif; ?>
      </div><!-- /.content -->
    </div><!-- /.wrap -->

    <?php } endif; ?>


    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>

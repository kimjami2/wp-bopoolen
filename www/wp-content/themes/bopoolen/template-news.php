<?php
/**
 * Template Name: News page
 */
?>

<?php
$wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>-1)); ?>
<?php if ( $wpb_all_query->have_posts() ) : ?>
  <?php the_content(); ?>
  <br>
  <ul>
    <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
      <li><h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
      <p><?php echo substr($post->post_content, 0, 100); ?>... <a href="<?php the_permalink() ?>"><?php _e('Read more', 'bopoolen'); ?></a></p></li>
    <?php endwhile; ?>
  </ul>
  <?php wp_reset_postdata(); ?>
<?php else : ?>
  <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

<!-- <?php get_template_part('templates/page', 'header'); ?> -->

<!-- <?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?> -->

<div class="wrap container" role="document">
  <div class="content">
    <main class="main">
      <div class="row">
        <div class="col-sm-3">
          <img src="<?php echo get_the_author_meta('_profilepicture'); ?>" alt="" class="img-responsive" />
          <div class="row">
            <div class="col-xs-12">
              <div class="sum-up">
                <h3><?php echo __('Verified ID', 'bopoolen'); ?></h3>
                <ul>
                  <li>
                    <span><?php echo __('Phonenumber:', 'bopoolen'); ?></span>
                    <span class="value"><?php echo get_the_author_meta('_phone'); ?></span>
                  </li>
                  <li>
                    <span><?php echo __('Rating:', 'bopoolen'); ?></span>
                    <span class="value">4 omdömen</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-9">
          <h2><?php $firstname = get_the_author_meta('first_name'); $lastname = get_the_author_meta('last_name'); echo ''.$firstname.' '.$lastname.''; ?></h2>
          <span class="member_since"><?php echo __('Member since '); $udata = get_userdata( get_current_user_id() ); echo substr($udata->user_registered, 0, -9); ?></span>
          <div class="rating">
            <i class="glyphicon glyphicon-star"></i>
            <i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i>
            <i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i>
            <span>(23 röster)</span>
          </div>
          <p><?php echo get_the_author_meta('description'); ?></p>

          <h2><?php echo __('Rating', 'bopoolen'); ?></h2>

          <div class="row">
            <div class="col-xs-12 col-sm-12">
              <div class="review">
                <div class="row">
                  <div class="col-xs-2 col-sm-2 col-md-1">
                    <img src="<?php echo get_the_author_meta('_profilepicture'); ?>" alt="" class="img-responsive img-circle" />
                    <span class="reviewer">Walter</span>
                  </div>
                  <div class="col-xs-10 col-sm-9 col-md-9 col-sm-offset-1 col-md-offset-1">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                      consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                      </p>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </main><!-- /.main -->
  </div><!-- /.content -->
</div><!-- /.wrap -->

<!-- <?php query_posts( array('post_type' => array( 'rentad' ))); ?>
		<?php while ( have_posts() ) : the_post(); ?>

    <div class="col-md-12">
      <h3><a href="<?php the_permalink(); ?> "><?php the_title(); ?></a></h3>
      <?php
        $rentprice        = get_post_meta($post->ID, '_rentprice', true);
        $availablefrom    = get_post_meta($post->ID, '_availablefrom', true);
        $availableto      = get_post_meta($post->ID, '_availableto', true);
        $streetaddress    = get_post_meta($post->ID, '_streetaddress', true);
        $postalcode       = get_post_meta($post->ID, '_postalcode', true);
        $city             = get_post_meta($post->ID, '_city', true);
        $roominhouse      = get_post_meta($post->ID, '_roominhouse', true);
        $roominapartment  = get_post_meta($post->ID, '_roominapartment', true);
        $roominstudent    = get_post_meta($post->ID, '_roominstudent', true);
        $ownhouse         = get_post_meta($post->ID, '_ownhouse', true);
        $ownapartment     = get_post_meta($post->ID, '_ownapartment', true);
        $contractsecond   = get_post_meta($post->ID, '_contractsecond', true);
        $contractfirst    = get_post_meta($post->ID, '_contractfirst', true);
        $contractchange   = get_post_meta($post->ID, '_contractchange', true);
       ?>
  		<div class="excerpt">
  		<?php echo get_the_excerpt(); ?>
      <br>
      <?php echo $rentprice; ?>
      <br>
      <?php echo $availablefrom; ?>
      <br>
      <?php echo $availableto; ?>
      <br>
      <?php echo $streetaddress; ?>
      <br>
      <?php echo $postalcode; ?>
      <br>
      <?php echo $city; ?>
      <br>
      <?php echo $roominhouse; ?>
      <br>
      <?php echo $roominapartment; ?>
      <br>
      <?php echo $roominstudent; ?>
      <br>
      <?php echo $ownhouse; ?>
      <br>
      <?php echo $ownapartment; ?>
      <br>
      <?php echo $contractsecond; ?>
      <br>
      <?php echo $contractchange; ?>
      <br>
  		</div>
    </div>

		<?php endwhile; ?> -->

<?php the_posts_navigation(); ?>

<?php
/**
 * Template Name: Register page
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'register'); ?>
<?php endwhile; ?>

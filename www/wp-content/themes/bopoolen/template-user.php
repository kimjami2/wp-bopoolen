<?php
/**
* Template Name: User profile
*/
?>

<?php
$user_id = isset($_GET['user']) ? $_GET['user'] : false;
$user = get_userdata($user_id);
$user_meta = get_user_meta($user_id);

if($user_id === false) {
  $url = '/profiles/?user='.get_current_user_id();
  echo '<script type="text/javascript">window.location ="'.$url.'";</script>';
}

if(!is_user_logged_in()) {
  echo '<h1>'.__('Ett fel uppstod', 'bopoolen').'</h1>';
  echo '<p>'.__('Du kan tyvärr inte besöka användarprofiler när du inte är inloggad.', 'bopoolen').'</p>';
} else {

$bio = get_the_author_meta('description');
$postsArray = array(
  'posts_per_page'	=> -1,
  'post_type'			  => array( 'searchad', 'rentad' ),
  'post_status'     => array( 'publish', 'draft', 'raderad', "pending", "modified", 'rejected'),
  'author'          => $user_id
);
$subscription = explode( '/' ,$user_meta['_subscription'][0]);
$subs = array(
  'posts_per_page'  => -1,
  'post_type'       => array('rentad', 'searchad'),
  'post_status'     => array( 'publish'),
  'post__in'         => $subscription
);
$subs = get_posts($subs);
$userProfileId = esc_attr( get_the_author_meta( '_profilepicture', $user->ID ) ) ? esc_attr( get_the_author_meta( '_profilepicture', $user->ID ) ) : '/wp-content/themes/bopoolen/assets/images/dummy_profile.png';

$posts = get_posts($postsArray);

$user_emailsub = $user_meta['_emailsub'][0];
$nomultiselect = explode('multiselect-all', $user_emailsub);
if($nomultiselect[0] == '') {
  unset($nomultiselect[0]);
}
if($nomultiselect[1] == '/') {
  unset($nomultiselect[1]);
}
if ($nomultiselect[2] == '/') {
  unset($nomultiselect[2]);
}
$emailsubstring;
foreach($nomultiselect as $sub) {
  $emailsubstring .= $sub;
}

$userProfileImage = get_post_meta($userProfileId, '_wp_attached_file', true);
?>

<div class="container user-profiles">
  <div class="row">
    <div class="profile col-md-3">

      <div class="profile-image" style="background-image: url('<?php if($userProfileImage !== false) { echo $userProfileImage;} else { echo '/wp-content/themes/bopoolen/assets/images/profile-image.png';} ?>')"></div>

      <div class="contact">
        <p class="verified"><?php _e('Verified ID', 'bopoolen'); ?></p>
        <div class="row">
            <div class="item">
              <p><b><?php _e('Email', 'bopoolen'); ?></b></p>
              <p><?php echo $user->user_email; ?></p>
            </div>
            <?php if($user_meta['_viewphone'][0] == 'true') { ?>
              <div class="item">
                <p><b><?php _e('Phone number', 'bopoolen'); ?></b></p>
                <p><?php echo $user_meta['_phone'][0]; ?></p>
              </div>
              <?php } ?>
              <div class="item">
                <p><b><?php _e('Rating', 'bopoolen'); ?></b></p>
                <?php

                $queryOpts = array(
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'showposts' => min(5, 100),
                    'post_type' => 'wpcr3_review',
                    'post_status' => 'publish',
                    'relation' => 'AND'
                );
                $meta_query[] = array(
                    'key' => "wpcr3_review_post",
                    'value' => $user_id,
                    'compare' => '='
                );

                $queryOpts['meta_query'] = $meta_query;
                $reviews = new WP_Query($queryOpts);
                if(count($reviews->posts)> 0) {
                  ?>
                  <div class="stars">
                    <?php
                    for ($i = 1; $i <= 5; $i++) {
                      $yellow = ($i <= count_avg_rating($user_id)) ? 'yellow' : '';
                      ?>
                      <i class="fa fa-star <?php echo $yellow; ?>" aria-hidden="true"></i>
                      <?php
                    }
                    echo '('.count($reviews->posts).')';
                    ?>
                  </div>
                  <?php
                } else {
                  ?>
                  <div class="stars">
                    <?php
                    for ($i = 1; $i <= 5; $i++) {
                      ?>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <?php
                    }
                    echo '(0)';
                    ?>
                  </div>
                  <?php
                }
                ?>
              </div>
        </div>

          </div>

        </div>

        <div class="info col-md-9">

          <?php
          $showName = esc_attr($user_meta['_viewname'][0]);
          $userName = ($showName) ? $user_meta['first_name'][0].' '.$user_meta['last_name'][0] : $user_meta['_username'][0];
          ?>

          <h1><?php echo $userName ?></h1>
          <p class="member-since"><?php _e('Member since', 'bopoolen'); ?> <?php echo date("M Y", strtotime($user->user_registered)); ?></p>

          <?php //echo the_widget('Author_Rating_Widget'); ?>

          <div class="card-wrapper">
            <p class="bio-title"><b><?php _e('Info om mig', 'bopoolen'); ?></b></p>
            <p class="bio"><?php echo $user_meta['description'][0]; ?></p>
          </div>

          <?php if($user_id == get_current_user_id()): ?>
          <input type="hidden" id="loggedIn" value="true">
          <div class="row" style="margin-top:30px;">
            <div class="col-xs-6 col-sm-6 col-md-3">
              <a href="/min-profil/?profile=edit-profile" class="card edit white edit-profile">
                <p class="icon">
                  <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                </p>
                <p class="info">
                  <?php _e('Edit profile', 'bopoolen'); ?>
                </p>
              </a>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
              <a href="/valj-annons-att-skapa" class="card white">
                <p class="icon">
                  <i class="fa fa-plus-circle" aria-hidden="true"></i>
                </p>
                <p class="info">
                  <?php _e('Create ad', 'bopoolen'); ?>
                </p>
              </a>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
              <a href="/hitta-anvandare" class="card white">
                <p class="icon">
                  <i class="fa fa-user" aria-hidden="true"></i>
                </p>
                <p class="info">
                  <?php _e('Find user', 'bopoolen'); ?>
                </p>
              </a>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
              <a href="<?php echo wp_logout_url(); ?>" class="card white">
                <p class="icon">
                  <i class="fa fa-sign-out" aria-hidden="true"></i>
                </p>
                <p class="info">
                  <?php _e('Sign out', 'bopoolen'); ?>
                </p>
              </a>
            </div>
          </div>
          <div class="row">
            <div class="card-wrapper" style="margin-left: 15px; padding-bottom: 55px;">
              <i class="fa fa-envelope" aria-hidden="true"></i><b data-toggle="tooltip" data-placement="right" title="<?php _e('Här kan du skapa en bevakning och få ett mejl varje gång en annons publiceras på BoPoolen som matchar din bostadssökning.', 'bopoolen'); ?>"><?php _e('My subscriptions', 'bopoolen'); ?></b>
              <div class="clearfix"></div>
              <input type="hidden" id="langCode" value="<?php echo apply_filters( 'wpml_current_language', NULL); ?>">
              <input type="hidden" id="subscriptions" value="<?php echo $emailsubstring; ?>">
              <?php if(count($nomultiselect) == 3 ) { echo '<input type="hidden" id="multiselect" value="3">';} ?>
              <div class="form-group" style="padding-top:25px;">
                <label class="col-md-3" for="area"><?php echo __('Location', 'bopoolen'); ?></label>
                <div class="col-md-9">
                  <select class="form-control dropdown multi-select deselect" id="area" multiple="multiple">
                    <option value="lund"><?php echo __('Lund', 'bopoolen') ?></option>
                    <option value="malmö"><?php echo __('Malmö', 'bopoolen') ?></option>
                    <option value="helsingborg"><?php echo __('Helsingborg', 'bopoolen') ?></option>
                    <option value="outside-lund"><?php echo __('Lund nearby', 'bopoolen') ?></option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3" for="accommodationType"><?php echo __('Type of housing', 'bopoolen') ?></label>
                <div class="col-md-9">
                  <select class="form-control dropdown multi-select deselect" id="accommodationType" multiple="multiple">
                    <option value="roominhouse"><?php echo __('Room in a house', 'bopoolen') ?></option>
                    <option value="roominapartment"><?php echo __('Room in an apartment', 'bopoolen') ?></option>
                    <option value="roominstudent"><?php echo __('Room in student housing', 'bopoolen') ?></option>
                    <option value="ownhouse"><?php echo __('House', 'bopoolen') ?></option>
                    <option value="ownapartment"><?php echo __('Apartment', 'bopoolen') ?></option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3" for="contractType"><?php echo __('Type of contract', 'bopoolen'); ?></label>
                <div class="col-md-9">
                  <select class="form-control dropdown multi-select deselect" id="contractType" multiple="multiple">
                    <option value="contractsecond"><?php echo __('Sublease agreement', 'bopoolen') ?></option>
                    <option value="contractfirst"><?php echo __('Tenancy agreement', 'bopoolen') ?></option>
                    <option value="contractchange"><?php echo __('Exchange', 'bopoolen') ?></option>
                  </select>
                </div>
              </div>
              <h2 class="click-menu" style="margin-top: 25px; margin-right: 15px; float:right; font-size:16px!important; font-weight: 700;"><?php _e('Instruktioner', 'bopoolen'); ?></h2>
              <div class="hide-menu">
                <div class="clearfix"></div>
                <p><?php _e('Skapa en bevakning genom att kryssa för vad det är du söker och klicka på ”prenumerera på valda områden”.', 'bopoolen'); ?></p>
                <p><?php _e('För att avsluta en bevakning kryssa ur alla alternativ och klicka på ”uppdatera bevakning”.', 'bopoolen'); ?></p>
              </div>
              <div class="clearfix"></div>
              <button class="emailSubscribe btn" style="margin-top: 25px; margin-right: 15px; float:right;"><span class="emailSubText"><?php
                  if($emailsubstring) {
                    _e('Update your subscription to above selection!', 'bopoolen');
                  } else {
                    _e('Subscribe to above selection!', 'bopoolen');
                  } ?></span></button>
              <div class="clearfix"></div>

            </div>
          </div>
        <?php endif; ?>
          <!-- <div class="col-md-3">
            <a href="/min-profil/?profile=my-ads" class="card white">
              <p class="icon">
                <i class="fa fa-home" aria-hidden="true"></i>
              </p>
              <p class="info">
              _e('My ads', 'bopoolen'); ?>
              </p>
            </a>
          </div> -->

          <div class="card-wrapper">
            <?php if($user_id == get_current_user_id()): ?>
              <i class="fa fa-home" aria-hidden="true"></i><b data-toggle="tooltip" data-placement="right" title="<?php _e('Här syns de annonser du har skapat.', 'bopoolen'); ?>"><?php _e('My ads'); ?></b>
              <div class="clearfix"></div>
            <?php else:?>
              <p class="my-profile-my-ads"><i class="fa fa-home" aria-hidden="true"></i><b><?php echo $userName.' '; _e('ads'); ?></b></p>

            <?php endif; ?>

            <?php
            if($user_id != get_current_user_id() && count($posts) == 0) {
              ?>
              <p class="no-results"><?php _e('This user haven’t created any ads.'); ?></p>
              <?php
            } else if(count($posts) == 0) {
                ?>
                <p class="no-results"><?php _e('You haven’t created any ads.'); ?></p>
                <?php

              } else {
                foreach ($posts as $post) {

                  if(apply_filters( 'wpml_current_language', NULL) == 'en') {
                    if(get_post_meta($post->ID,'_titleeng', true) === '') {
                      $title = $post->post_title;
                    } else {
                      $title = get_post_meta($post->ID, '_titleeng', true);
                    }
                  } else {
                    $title = $post->post_title;
                  }

                  if(apply_filters( 'wpml_current_language', NULL) == 'en') {
                    if(get_post_meta($post->ID,'_descriptioneng', true) === '') {
                      $content = $post->post_content;
                    } else {
                      $content = get_post_meta($post->ID, '_descriptioneng', true);
                    }
                  } else {
                    $content = $post->post_content;
                  }

                  /*if($post->post_status == 'draft' && $user_id != get_current_user_id()) {
                    continue;
                  } else if ($post->post_status == 'raderad') {
                    continue;
                  } else if ($post->post_status == 'pending' && $user_id != get_current_user_id()) {
                    continue;
                  } else if ($post->post_status == 'modified' && $user_id != get_current_user_id()) {
                    continue;
                  }*/

                  if ($post->post_status == 'raderad') {
                    continue;
                  } else if ($post->post_status != 'publish' && $user_id != get_current_user_id()) {
                    continue;
                  }


                  // grab the custom meta
                  $custom_meta = get_post_custom($post->ID);
                  ?>
                  <div class="col-md-4 col-sm-12">
                    <a class="ad" href="<?php echo $post->guid; ?>">
                      <h4><?php echo $title;
                        if($post->post_type == 'rentad') {
                          _e(' - landlord', 'bopoolen');
                        } else {
                          _e(' - tenant', 'bopoolen');
                        }
                        if($post->post_status == 'draft') {
                          _e(' - inactive', 'bopoolen');
                        } else if ($post->post_status == 'raderad') {
                          _e(' - deleted', 'bopoolen');
                        } else if ($post->post_status == 'pending') {
                          _e(' - pending', 'bopoolen');
                        } else if ($post->post_status == 'modified') {
                          _e(' - modified', 'bopoolen');
                        } else if ($post->post_status == 'rejected') {
                          _e(' - rejected', 'bopoolen');
                        } else if($post->post_status == 'publish') {
                          _e(' - published', 'bopoolen');
                        }
                        ?></h4>

                      <p class="desc"><?php echo substr($content, 0, 100); ?>...</p>

                      <div class="date">
                        <div class="row">
                          <div class="col col-sm-6">
                            &nbsp;&nbsp;<?php echo $custom_meta['_availablefrom'][0]; ?>
                          </div>
                          <div class="col col-sm-6">
                            <?php echo $custom_meta['_availableto'][0]; ?>&nbsp;&nbsp;
                          </div>
                        </div>
                      </div>
                    </a>
                  </div>

                  <?php
                }
            }
            ?>
            <div style="clear:both;"></div>
          </div>
          <?php
          if($user_id == get_current_user_id()) {
            ?>
            <div class="card-wrapper">
              <i class="fa fa-star" aria-hidden="true"></i><b data-toggle="tooltip" data-placement="right" title="<?php _e('Här syns de annonser som du har sparat som favoriter (endast aktiva/publicerade annonser syns)', 'bopoolen'); ?>"><?php _e('My favorites', 'bopoolen'); ?></b>
              <div class="clearfix"></div>
              <?php
              if (count($subs) == 0) {
                ?>
                <p class="no-results"><?php _e('You have no favorites', 'bopoolen'); ?></p>
                <?php

              } else {
                if ($user_id == get_current_user_id()) {
                  foreach ($subs as $sub) {
                    $custom_meta = get_post_custom($sub->ID);
                    if(apply_filters( 'wpml_current_language', NULL) == 'en') {
                      if(get_post_meta($sub->ID,'_titleeng', true) === '') {
                        $title = $sub->post_title;
                      } else {
                        $title = get_post_meta($sub->ID, '_titleeng', true);
                      }
                    } else {
                      $title = $sub->post_title;
                    }

                    if(apply_filters( 'wpml_current_language', NULL) == 'en') {
                      if(get_post_meta($sub->ID,'_descriptioneng', true) === '') {
                        $content = $sub->post_content;
                      } else {
                        $content = get_post_meta($sub->ID, '_descriptioneng', true);
                      }
                    } else {
                      $content = $sub->post_content;
                    }
                    ?>
                    <div class="col-md-4 col-sm-12">
                      <a class="ad" href="<?php echo $sub->guid; ?>">
                        <h4><?php echo $title;
                          if($sub->post_type == 'rentad') {_e(' - landlord', 'bopoolen');}
                          else {
                            _e(' - tenant', 'bopoolen');
                          }?></h4>

                        <p class="desc"><?php echo substr($content, 0, 100); ?>...</p>

                        <div class="date">
                          <div class="row">
                            <div class="col col-sm-6">
                              &nbsp;&nbsp;<?php echo $custom_meta['_availablefrom'][0]; ?>
                            </div>
                            <div class="col col-sm-6">
                              <?php echo $custom_meta['_availableto'][0]; ?>&nbsp;&nbsp;
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>

                    <?php
                  }
                }
              }
              ?>
              <div style="clear:both;"></div>
              <h2 class="click-menu" style="margin-top: 25px; margin-right: 15px; float:right; font-size:16px!important; font-weight: 700;"><?php _e('Instruktioner', 'bopoolen'); ?></h2>
              <div class="hide-menu">
                <div class="clearfix"></div>
                <p><?php _e('Spara en annons som en favorit genom att:', 'bopoolen'); ?></p>
                <ol>
                  <li><?php _e('Se till att vara inloggad','bopoolen'); ?></li>
                  <li><?php _e('Gå in på annonsen som du gillar och vill spara','bopoolen'); ?></li>
                  <li><?php _e('Klicka på Spara (knappen med en stjärna på)','bopoolen'); ?></li>
                </ol>
              </div>
              <div class="clearfix"></div>
            </div>
            <?php
          }
          ?>
          <input type="hidden" id="langCode" value="<?php echo apply_filters( 'wpml_current_language', NULL); ?>">

          <?php if(is_user_logged_in()) {
            if($user_id != get_current_user_id()) {
              echo do_shortcode('[WPCR_SHOW POSTID="' . $user_id . '" NUM="3" PAGINATE="1" PERPAGE="5" SHOWFORM="1" HIDEREVIEWS="0" HIDERESPONSE="0" SNIPPET="" MORE="" HIDECUSTOM="0" ]');
            }
            ?>
            <?php


            foreach ($reviews->posts as $post) {
              $post_meta = get_post_meta($post->ID);

              ?>
              <div class="review-item card-wrapper">
                <div class="stars">
                  <?php
                  for ($i = 1; $i <= 5; $i++) {
                    $yellow = ($i <= $post_meta['wpcr3_review_rating'][0]) ? 'yellow' : '';
                    ?>
                    <i class="fa fa-star <?php echo $yellow; ?>" aria-hidden="true"></i>
                    <?php
                  }
                  ?>
                </div>
                <div class="content">
                  <p><b><?php echo $post->post_title; ?></b></p>

                  <p class="content"><?php echo $post->post_content; ?></p>
                </div>
              </div>
              <?php

            }
          }
          ?>
        </div>
      </div>
    </div>
<?php } ?>

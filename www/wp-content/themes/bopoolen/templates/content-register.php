<div style="padding: 15px;">
  <?php the_content(); ?>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="alert alert-success create_success" role="alert"><?php echo __('Your account has been created. Please log in <a href="/logga-in">here!</a>', 'bopoolen'); ?></div>
      <div class="alert alert-warning existing_user_email" role="alert"><?php echo __('The provided email address already exists in our system. Please try again!', 'bopoolen'); ?></div>
      <div class="alert alert-danger general-error" role="alert"><?php echo __('Please look over the marked fields', 'bopoolen'); ?></div>
      <div class="alert alert-danger error_create" role="alert"><?php echo __('Something went wrong. Please try again!', 'bopoolen'); ?></div>
      <div class="alert alert-danger missing" role="alert"><?php echo __('Please fill in all fields!', 'bopoolen'); ?></div>
      <div class="alert alert-danger securitynumtaken" role="alert"><?php echo __('The selected personal identity number is already in use!', 'bopoolen'); ?></div>
      <div class="alert alert-danger terms" role="alert"><?php echo __('You need to approve the Terms of Use.', 'bopoolen'); ?></div>
      <input type="hidden" id="langCode" value="<?php echo apply_filters( 'wpml_current_language', NULL); ?>">

      <form id="create_user" method="get">
        <div class="form-group">
          <label class="control-label" for="InputEmail1" data-toggle="tooltip" data-placement="right" title="<?php echo _e('Shown in the ad if you choose not to show your real name.', 'bopoolen'); ?>"><?php echo __('User name', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
          <input class="form-control" id="create_username" placeholder="<?php echo __('Enter a username', 'bopoolen') ?>" type="text">
        </div>
        <div class="form-group">
          <label class="control-label" for="InputEmail1" data-toggle="tooltip" data-placement="right" title="<?php echo _e('Used to log in and is always shown in your ad.', 'bopoolen'); ?>"><?php echo __('Email address', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;&nbsp;*</label>
          <input class="form-control" id="create_email" placeholder="<?php echo __('Enter your email', 'bopoolen') ?>" type="email">
        </div>
        <div class="form-group">
          <label class="control-label" for="InputEmail1"><?php echo __('Repeat your email', 'bopoolen') ?></label>
          <input class="form-control" id="email_repeat" placeholder="<?php echo __('Repeat your email', 'bopoolen') ?>" type="email">
        </div>
        <div class="form-group">
          <label class="control-label" for="InputName" data-toggle="tooltip" data-placement="right" title="<?php echo _e('Other users can only see this if you choose to show it in your ad.', 'bopoolen'); ?>"><?php echo __('First name', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
          <input class="form-control" id="create_firstname" placeholder="<?php echo __('Enter your first name', 'bopoolen') ?>" type="text">
        </div>
        <div class="form-group">
          <label class="control-label" for="InputLastName" data-toggle="tooltip" data-placement="right" title="<?php echo _e('Other users can only see this if you choose to show it in your ad.', 'bopoolen'); ?>"><?php echo __('Last name', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
          <input class="form-control" id="create_lastname" placeholder="<?php echo __('Enter your last name', 'bopoolen') ?>" type="text">
        </div>
        <div class="form-group">
          <label class="control-label" data-toggle="tooltip" data-placement="right" title="<?php _e('If you choose not to display your name in the ad, your user name will then be displayed instead', 'bopoolen') ?>"><?php echo __('Do not display name in ads', 'bopoolen') ?>
            &nbsp;&nbsp;&nbsp;<input type="checkbox" name="_viewname" id="viewname" class="regular-text" ><i class="fa fa-info-circle" aria-hidden="true"></i></label>
        </div>
        <div class="form-group">
          <label class="control-label" for="InputPassword"><?php echo __('Password', 'bopoolen') ?></label>
          <input class="form-control" id="create_password" placeholder="<?php echo __('Enter your password', 'bopoolen') ?>" type="password">
        </div>
        <div class="form-group">
          <label class="control-label" for="exampleInputPassword1"><?php echo __('Repeat your password', 'bopoolen') ?></label>
          <input class="form-control" id="InputPassword1" placeholder="<?php echo __('Repeat your password', 'bopoolen') ?>" type="password">
        </div>
        <div class="form-group">
          <label class="control-label" for="InputSecNumber"><?php echo __('Personal identity number (yyyymmdd-nnnn)', 'bopoolen') ?></label>
          <input class="form-control" id="create_socialnumber" placeholder="19810101-0000" type="text">
          <p style="margin-top: 10px; font-family: Lato,sans-serif;">
            <label class="control-label" data-toggle="tooltip" data-placement="right" title="<?php _e('If you do not have a Swedish personal identity number your only qualified to search tenants.', 'bopoolen'); ?>" for="no-social-number"><?php _e('I do not have a Swedish personal identity number.', 'bopoolen'); ?>&nbsp;&nbsp;<input type="checkbox" id="no-social-number" class="regular-text">&nbsp;<i class="fa fa-info-circle" aria-hidden="true"></i></label>
          </p>
        </div>
        <div class="form-group">
          <label class="control-label" for="InputPhoneNr" data-toggle="tooltip" data-placement="right" title="<?php echo _e('Other users can only see the telephone number if you choose to show it in your ad.', 'bopoolen'); ?>"><?php echo __('Telephone number', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
          <input class="form-control" id="create_phone" placeholder="0700-00 00 00" type="text">
        </div>
        <tr>
        <tr>
        <div class="form-group">
          <!-- Tooltip  -->
          <label class="control-label" for="viewphone" data-toggle="tooltip" data-placement="right" title="<?php _e('Select if other users are to be able to contact you by telephone.', 'bopoolen')?>"><?php echo __('Do not show telephone number in the ad', 'bopoolen') ?>
          &nbsp;&nbsp;&nbsp;<input type="checkbox" name="_viewphone" id="viewphone" class="regular-text"><i class="fa fa-info-circle" aria-hidden="true"></i></label>
          <!--&nbsp;&nbsp;&nbsp;<input type="checkbox" name="_viewphone" id="viewphone" value="checked" class="regular-text" checked=""> -->
        </div>
        <tr>
          <div class="form-group" style="font-size:14px; color:red;"><?php _e('*Note: The email address stated above is always shown in your ad.', 'bopoolen'); ?></div>
          <div class="form-group">
            <label class="control-label" for="viewphone"><?php echo __('I approve <a href="/anvandarvillkor" target="_blank">terms of use</a>', 'bopoolen') ?></label>
            &nbsp;&nbsp;&nbsp;<input type="checkbox" id="terms" class="regular-text">
          </div>
          <tr>
          <div class="form-group">
            <input type="hidden" id="defaultDescription" value="<?php _e('Skriv gärna en kort presentation av dig själv. Denna kommer vara synlig för andra användare så de får en inblick av vem du är som person.', 'bopoolen'); ?>">
            <label class="control-label" for="InputBio" data-toggle="tooltip" data-placement="right" title="<?php _e('Skriv gärna en kort presentation av dig själv. Denna kommer vara synlig för andra användare.','bopoolen'); ?>"><?php echo __('Biography', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
            <?php

            $content = __('Skriv gärna en kort presentation av dig själv. Denna kommer vara synlig för andra användare så de får en inblick av vem du är som person.', 'bopoolen');

            $editor_id = 'bio_tinymec';
            $settings =   array(
                'wpautop' => true, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                'textarea_rows' => get_option('default_post_edit_rows', 10), // rows="..."
                'tabindex' => '',
                'editor_css' => '', //  extra styles for both visual and HTML editors buttons,
                'editor_class' => '', // add extra class(es) to the editor textarea
                'teeny' => false, // output the minimal editor config used in Press This
                'dfw' => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)
                'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
            );
            ?>
            <?php wp_editor( $content, $editor_id, $settings = array() ); ?>
          </div>
          <div class="form-segment-wrapper">
            <div id="dZUpload" class="dropzone noheader">
              <p><?php _e('Du kan som max ha 1 bild som profilbild.', 'bopoolen'); ?></p>
              <div class="dz-default dz-message">
                <div class="img-container">
                  <img src="/wp-content/themes/bopoolen/assets/images/upload-image.jpg" alt="" />
                </div>
                <?php echo __('Click here to upload profile image', 'bopoolen') ?></div>
            </div>
          </div>
          <tr>
        <button class="btn btn-primary create_user pull-right" type="submit"><?php echo __('Create account', 'bopoolen') ?></button>
      </form>

    </div>
  </div>
</div>

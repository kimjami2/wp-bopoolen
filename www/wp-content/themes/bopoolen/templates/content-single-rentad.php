<?php while (have_posts()) : the_post(); ?>
  <?php
    /**ADD STUFF **/
    $rentprice        = get_post_meta($post->ID, '_rentprice', true);
    $availablefrom    = get_post_meta($post->ID, '_availablefrom', true);
    $availableto      = get_post_meta($post->ID, '_availableto', true);

    $streetaddress    = get_post_meta($post->ID, '_streetaddress', true);
    $postalcode       = get_post_meta($post->ID, '_postalcode', true);
    $city             = get_post_meta($post->ID, '_city', true);
    $area             = get_post_meta($post->ID, '_area', true);

    $roominhouse      = get_post_meta($post->ID, '_roominhouse', true);
    $roominapartment  = get_post_meta($post->ID, '_roominapartment', true);
    $roominstudent    = get_post_meta($post->ID, '_roominstudent', true);
    $ownhouse         = get_post_meta($post->ID, '_ownhouse', true);
    $ownapartment     = get_post_meta($post->ID, '_ownapartment', true);

    $contractsecond   = get_post_meta($post->ID, '_contractsecond', true);
    $contractfirst    = get_post_meta($post->ID, '_contractfirst', true);
    $contractchange   = get_post_meta($post->ID, '_contractchange', true);

    $availableyta       = get_post_meta($post->ID, '_availableyta', true);
    $availablegemensam  = get_post_meta($post->ID, '_availablegemensam', true);
    $numberofrooms      = get_post_meta($post->ID, '_numberofrooms', true);

    $includedel         = get_post_meta($post->ID, '_includedel', true);
    $includedinternet   = get_post_meta($post->ID, '_includedinternet', true);
    $includedfurniture  = get_post_meta($post->ID, '_includedfurniture', true);
    $includedtoilet     = get_post_meta($post->ID, '_includedtoilet', true);
    $includedbath       = get_post_meta($post->ID, '_includedbath', true);
    $includedanimal     = get_post_meta($post->ID, '_includedanimal', true);
    $includedhandicap   = get_post_meta($post->ID, '_includedhandicap', true);
    $includeddishwasher = get_post_meta($post->ID, '_includeddishwasher', true);
    $includedwasher     = get_post_meta($post->ID, '_includedwasher', true);
    $includedsmoking    = get_post_meta($post->ID, '_includedsmoking', true);
    $includedpentry     = get_post_meta($post->ID, '_includedpentry', true);
    $includedkitchen    = get_post_meta($post->ID, '_includedkitchen', true);
    $includedentrance   = get_post_meta($post->ID, '_includedentrance', true);

    $latlng             = get_post_meta($post->ID, '_latlng', true);
    $clicker            = get_post_meta($post->ID, '_clicker', true);

    $contractType = "";
    if($contractsecond){
     $contractType = "Andrahand";
    }
    if($contractfirst){
     $contractType = "Förstahand";
    }
    if($contractchange){
     $contractType = "Bytes";
    }

    $type = "";
    if($roominhouse){
     $type = "Rum i hus";
    }
    if($roominapartment){
     $type = "Rum i lägenhet";
    }
    if($roominstudent){
     $type = "Rum i studentkorridor";
    }
    if($ownhouse){
     $type = "Hus";
    }
    if($ownapartment){
     $type = "Lägenhet";
    }

    $showMail = esc_attr( get_the_author_meta( '_viewmail', $user->ID ) );
    $showName = esc_attr( get_the_author_meta( '_viewname', $user->ID ) );
    $showPhone = esc_attr( get_the_author_meta( '_viewphone', $user->ID ) );
    $showAddress = get_post_meta($post->ID, '_viewaddress', true);


    $userFullName = esc_attr( get_the_author_meta( 'first_name', $user->ID ) )." ".esc_attr( get_the_author_meta( 'last_name', $user->ID ) );
    $userName = esc_attr( get_the_author_meta( '_username', $user->ID ) );
    if($userName == '' ) {
      $userName = esc_attr( get_the_author_meta( 'nickname', $user->ID ) );
    }
    $userPhone = esc_attr( get_the_author_meta( '_phone', $user->ID ) );
    $userEmail = esc_attr( get_the_author_meta( 'user_email', $user->ID ) );

    //Get the creators profile picture
    $userProfileId = esc_attr( get_the_author_meta( '_profilepicture', $user->ID ) ) ? esc_attr( get_the_author_meta( '_profilepicture', $user->ID ) ) : '/wp-content/themes/bopoolen/assets/images/dummy_profile.png';
    $userProfileImage = get_post_meta($userProfileId, '_wp_attached_file', true);

    //Get the posts images
    $postPictureIds = get_post_meta($post->ID, '_imageids', true);
    $postPictureIds = explode(',', $postPictureIds);
    /*
     * Update the clicker event on load.
     */
    update_post_meta($post->ID, '_clicker', $clicker+1, $clicker);

    // Get if current post is subscribed.
    $meta = get_user_meta(get_current_user_id(), '_subscription', true);
    $meta = explode('/', $meta);
    $subscribed = array_search($post->ID, $meta);

    // Get if email reminder is set or not.
    $meta = get_user_meta(get_current_user_id(), '_mailed', true);
    $meta = explode('/', $meta);
    $mailed = array_search($post->ID, $meta);

  if(apply_filters( 'wpml_current_language', NULL) == 'en') {
    if(get_post_meta($post->ID,'_titleeng', true) === '') {
      $title = $post->post_title;
    } else {
      $title = get_post_meta($post->ID, '_titleeng', true);
    }
  } else {
    //Get first title
    $title = $post->post_title;
  }

  if(apply_filters( 'wpml_current_language', NULL) == 'en') {
    if(get_post_meta($post->ID,'_descriptioneng', true) === '') {
      $content = $post->post_content;
    } else {
      $content = get_post_meta($post->ID, '_descriptioneng', true);
    }
  } else {
    $content = $post->post_content;
  }

  if($post->post_status !== 'publish' && get_current_user_id() != $post->post_author) {
    //Visa felmeddelande.
    echo '<h2>'.__('Ett fel har upstått', 'bopoolen').'</h2>';
    echo '<p>'.__('Annonsen du söker finns tyvärr inte kvar', 'bopoolen').'</p>';

  } else {

  ?>

  <section class="page annonssida">
    <div class="ad-landing-page">
      <div class="header-container">
        <div class="row">
          <div class="col-sm-12">
            <?php if($post->post_author == get_current_user_id()): ?>
              <div class="clearfix live-only">
                <?php if($post->post_status == 'publish') {
                  ?>
                  <a style="padding-left:15px" href="/redigera-annons-uthyres/?post_id=<?php echo $post->ID ?>"
                     class="right"><?php _e('Edit ad', 'bopoolen') ?> <i class="fa fa-pencil-square-o"
                                                                         aria-hidden="true"></i></a>
                  <a style="padding-left:15px"
                     href="/redigera-annons-uthyres/?post_id=<?php echo $post->ID ?>&post_type=inaktiv&goTo=inactiveArea"
                     class="right"><?php _e('Deactivate ad', 'bopoolen') ?> <i class="fa fa-pencil-square-o"
                                                                               aria-hidden="true"></i></a>
                  <a href="/redigera-annons-uthyres/?post_id=<?php echo $post->ID ?>&post_type=raderad&goTo=eraseArea"
                     class="right"><?php _e('Erase ad', 'bopoolen') ?> <i class="fa fa-pencil-square-o"
                                                                          aria-hidden="true"></i></a>
                  <?php
                } else if($post->post_status != 'pending' && $post->post_status != 'modified'){
                  ?>
                  <a style="padding-left:15px" href="/redigera-annons-uthyres/?post_id=<?php echo $post->ID ?>&post_type=activate"
                     class="right"><?php _e('Send for activation', 'bopoolen') ?> <i class="fa fa-pencil-square-o"
                                                                               aria-hidden="true"></i></a>
                  <a href="/redigera-annons-uthyres/?post_id=<?php echo $post->ID ?>&post_type=raderad&goTo=eraseArea"
                     class="right"><?php _e('Erase ad', 'bopoolen') ?> <i class="fa fa-pencil-square-o"
                                                                          aria-hidden="true"></i></a>
                <?php
                } else {
                  ?>
                  <a style="padding-left:15px" href="/redigera-annons-uthyres/?post_id=<?php echo $post->ID ?>"
                     class="right"><?php _e('Edit ad', 'bopoolen') ?> <i class="fa fa-pencil-square-o"
                                                                         aria-hidden="true"></i></a>
                <?php
                }
                ?>
              </div>
            <?php endif; ?>
            <h1 class="entry-title" style="text-align: center;"><?php echo $title; ?></h1>
          </div>
          <div class="clearfix"></div>
          <!-- Feature Box 1 -->
          <div class="col-lg-5 col-md-5">
            <div class="info-box">
              <div class="profile-panel profile-panel-blue">
                <div class="profile-section">
                  <h1 class="header-area"><?php echo __('Month rent', 'bopoolen') ?> <?php echo $rentprice; ?> :- SEK</h1>
                  <table class="table-fill">
                    <tbody>
                      <tr>
                        <td><?php echo $type; ?></td>
                        <td><?php echo $availableyta; ?> m<sup>2</sup></td>
                      </tr>
                      <tr>
                        <td><?php echo $availablefrom; ?></td>
                        <td><?php echo $availableto; ?></td>
                      </tr>
                    </tbody>
                  </table>
                  <?php if($showAddress == 'true') { ?>
                  <h2 class="mid-area"> <?php echo $streetaddress; ?>, <?php echo $postalcode; ?> <?php echo $city;?></h2>
                  <?php } ?>
                  <div class="map-block">
                    <div class="map-responsive">
                      <?php if(isset($latlng) && $latlng != '' && $latlng != 'NULL') :?>
                      <iframe scrolling="no" src="https://maps.google.com/maps?q=<?php echo $latlng; ?>&hl=es;z=14&amp;output=embed" width="100%" height="100" frameborder="0" style="border:0">
                      </iframe>
                    <?php endif; ?>
                    </div>
                    <!--map responsive-->
                  </div>
                </div>
                <a class="btn take-me-to-contact" href="#contact-form"><?php echo __('Contact the lessor', 'bopoolen') ?></a>
              </div>
            </div>
          </div>

          <!-- Feature Box 2 -->
          <div class="col-lg-7 col-md-7">
            <div id="carousel-generic" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators">
                <?php
                $counter = 0;

                foreach($postPictureIds as $id) {
                  if($counter == 0) {
                    echo '<li data-target="#carousel-generic" data-slide-to="0" class="active"></li>';
                  } else {
                    echo '<li data-target="#carousel-generic" data-slide-to="'.$counter.'"></li>';
                  }
                  $counter ++;
                }
                ?>
                <!--<li data-target="#carousel-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-generic" data-slide-to="1" class=""></li>
                <li data-target="#carousel-generic" data-slide-to="2" class=""></li>-->
              </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner">
                <?php
                $count = 0;
                if($postPictureIds[0] == '') {
                  echo '<div class="item active"><img src="/wp-content/uploads/2016/02/advertimg-2.png"></div>';
                } else {
                  foreach($postPictureIds as $id) {
                    $picture = get_post_meta($id, '_wp_attached_file', true);
                    if($count == 0) {
                      echo '<div class="item active"><img src="'.$picture.'" alt="..."></div>';
                      $count++;
                    } else {
                      echo '<div class="item"><img style="height: 450px; width: 100%" src="'.$picture.'" alt="..."></div>';
                    }

                  }
                }
                ?>
              </div>
              <a class="left carousel-control" href="#carousel-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
              </a>
              <a class="right carousel-control" href="#carousel-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
              </a>
            </div>
          </div>
        </div>
      </div>

    </div>

    <div class="info-section">
      <div class="row">
        <h2 class="print-only"><?php _e('Information about the housing', 'bopoolen'); ?></h2>
        <div class="col-md-4 col-sm-6">
            <h3><?php _e('Living', 'bopoolen'); ?></h3>
            <div class="list-container">
              <dl class="clearfix">
                <?php echo ($contractType  ? '<dt>'.__('Rental agreement:', 'bopoolen').' </dt><dd>'.$contractType.'</dd>' : ''); ?>
                <dt><?php _e('Types of housing', 'bopoolen') ?>:</dt><dd><?php echo $type ?></dd>
                <dt><?php _e('City', 'bopoolen') ?>:</dt><dd><?php echo $city ?></dd>
                <dt><?php _e('Address', 'bopoolen') ?>: </dt><dd><?php echo $streetaddress ?></dd>
                <dt><?php _e('Postal code', 'bopoolen') ?>: </dt><dd><?php echo $postalcode ?></dd>
                <dt><?php _e('Area', 'bopoolen') ?>: </dt><dd><?php echo $availableyta .' '.__('sq.m.', 'bopoolen') ?></dd>
                <?php echo ($availablegemensam  ? '<dt>'.__('Commmon area:', 'bopoolen').' </dt><dd>'.$availablegemensam.' '. __('sq.m.', 'bopoolen').'</dd>' : ''); ?>
                <?php echo ($numberofrooms  ? '<dt>'.__('Room:', 'bopoolen').' </dt><dd>'.$numberofrooms.'</dd>' : ''); ?>
              </dl>
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="printright">
            <h3><?php _e('Included in the rent', 'bopoolen'); ?></h3>
            <ul class="fa-ul clearfix">
            <?php
              echo ($includedel ?           '<li><i class="fa fa-plug"></i>'.__('Electricity', 'bopoolen').'</li>' : '');
              echo ($includedinternet ?     '<li><i class="fa fa-globe"></i>'.__('Internet', 'bopoolen').'</li>' : '');
              echo ($includedfurniture  ?   '<li><i class="fa fa-bed"></i>'.__('Furnished', 'bopoolen').'</li>' : '');
            ?>
            </ul>
          </div>
          <div class="printleft">
            <h3><?php _e('Included in the apartment', 'bopoolen'); ?></h3>
            <ul class="fa-ul clearfix">
            <?php
              echo ($includedtoilet  ?      '<li><i class="icon-toilet"></i>'.__('Private toilet', 'bopoolen').'</li>' : '');
              echo ($includedbath   ?       '<li><i class="icon-bathroom"></i>'.__('Private bathroom', 'bopoolen').'</li>' : '');
              echo ($includedanimal   ?     '<li><i class="fa fa-paw"></i>'.__('Pets', 'bopoolen').'</li>' : '');
              echo ($includedhandicap   ?   '<li><i class="fa fa-wheelchair"></i>'.__('Accessible', 'bopoolen').'</li>' : '');
              echo ($includeddishwasher   ? '<li><i class="icon-dishwasher"></i>'.__('Dishwasher', 'bopoolen').'</li>' : '');
              echo ($includedwasher   ?     '<li><i class="icon-washer"></i>'.__('Washing machine', 'bopoolen').'</li>' : '');
              echo ($includedsmoking   ?    '<li><i class="fa fa-2x" style="font-size:1.15em;">&#128685;</i>'.__('Smoking not allowed', 'bopoolen').'</li>' : '');
              echo ($includedpentry  ?      '<li><i class="fa fa-cutlery"></i>'.__('Kitchenette', 'bopoolen').'</li>' : '');
              echo ($includedkitchen  ?     '<li><i class="fa fa-cutlery"></i>'.__('Kitchen', 'bopoolen').'</li>' : '');
              echo ($includedentrance  ?    '<li><i class="fa fa-key"></i>'.__('Private entrance', 'bopoolen').'</li>' : '');
            ?>
            </ul>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <h3><?php _e('Terms', 'bopoolen'); ?></h3>
          <div class="list-container">
            <dl class="clearfix second">
              <dt><?php echo __('Rent:', 'bopoolen') ?> </dt><dd><?php echo $rentprice; ?>:- SEK</dd>
              <dt><?php echo __('Agreement type:', 'bopoolen') ?> </dt><dd><?php echo $contractType ?></dd>
              <dt><?php echo __('From:', 'bopoolen') ?> </dt><dd><?php echo $availablefrom; ?></dd>
              <dt><?php echo __('To:', 'bopoolen') ?> </dt><dd><?php echo $availableto; ?></dd>
            </dl>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid main-text">
      <div class="page" id="infotext">
        <div class="row">
          <div class="col-lg-7 col-md-7">
            <div class="page-txt clearfix">
              <h2 class="print-padding"><?php echo __('Property description', 'bopoolen')?></h2>
              <?php echo $content; ?>
            </div>
          </div>
          <div class="col-lg-5 col-md-5">
            <?php if($post->post_author != get_current_user_id()):
              if(is_user_logged_in()) {?>
              <div class="favorite">
                <input type="hidden" id="postId" value="<?php echo $post->ID; ?>">
                <i class="fa subscribe <?php if($subscribed === false) {echo 'fa-star-o';} else { echo 'fa-star';} ?>" aria-hidden="true"></i><span class="subscribe-text"><?php if($subscribed === false) { _e('Save ad', 'bopoolen');} else { _e('Saved!', 'bopoolen');} ?></span>
              </div>
            <?php }
            endif; ?>
            <div class="profile-panel profile-panel-blue">
              <div class="profile-image">
                <img src="<?php if($userProfileImage !== false) { echo $userProfileImage; } else { echo '/wp-content/themes/bopoolen/assets/images/profile-image.png';} ?>" />
              </div>

              <h2><?php if($showName === 'true') { echo $userFullName; } else { echo $userName;} ?></h2>
              <?php

              $queryOpts = array(
                  'orderby' => 'date',
                  'order' => 'DESC',
                  'showposts' => min(5, 100),
                  'post_type' => 'wpcr3_review',
                  'post_status' => 'publish',
                  'relation' => 'AND'
              );
              $meta_query[] = array(
                  'key' => "wpcr3_review_post",
                  'value' => $post->post_author,
                  'compare' => '='
              );

              $queryOpts['meta_query'] = $meta_query;
              $reviews = new WP_Query($queryOpts);
              if(count($reviews->posts)> 0) {
                ?>
                <div class="stars">
                  <?php
                  for ($i = 1; $i <= 5; $i++) {
                    $yellow = ($i <= count_avg_rating($post->post_author)) ? 'yellow' : '';
                    ?>
                    <i class="fa fa-star <?php echo $yellow; ?>" aria-hidden="true"></i>
                    <?php
                  }
                  echo '('.count($reviews->posts).')';
                  ?>
                </div>
                <?php
              } else {
                ?>
                <div class="stars">
                  <?php
                  for ($i = 1; $i <= 5; $i++) {
                    ?>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <?php
                  }
                  echo '(0)';
                  ?>
                </div>
              <?php
              }
              ?>
              <div class="profile-footer">
                <?php echo '<a class="btn" href="/profiles/?user='.get_the_author_id().'">'.__('See profile', 'bopoolen').'</a>'; ?>
              </div>
            </div>

          </div>
        </div>

        <div class="row">
          <div class="col-md-12 col-xs-12">
            <div id="contact-form" class="contact-form">
              <h2><?php _e('Contact the lessor', 'bopoolen'); ?></h2>
              <p style="margin-top: -20px; font-size: 15px;"><?php _e('If you are interested, contact the housing advertiser using the contact information below.', 'bopoolen'); ?></p>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <a href="mailto:<?php echo $userEmail; ?>?Subject=Bopoolen:%20<?php echo urlencode(the_title()); ?>" class="card live-only">
                    <p class="icon">
                      <i class="fa fa-envelope-o" aria-hidden="true"></i>
                    </p>
                    <p class="info">
                      <?php echo $userEmail; ?>
                    </p>
                  </a>
                  <a class="card print-only">
                    <p class="icon">
                      <i class="fa fa-envelope-o" aria-hidden="true"></i>
                    </p>
                    <p class="info">
                      <?php echo $userEmail; ?>
                    </p>
                  </a>
                </div>
                <?php if($showPhone == 'true') { ?>
                  <div class="col-md-4 col-xs-12">
                    <div class="card">
                      <p class="icon">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                      </p>
                      <p class="info">
                        <?php echo $userPhone; ?>
                      </p>
                    </div>
                  </div>
                <?php } ?>
              </div>
              <?php if($post->post_author != get_current_user_id()):
                if(is_user_logged_in()) {?>
                <div class="clearfix">
                  <div class="mailed">
                    <i class="fa <?php if($mailed === false) {echo 'fa-square-o';} else { echo 'fa-check-square-o';} ?> sendEmail" aria-hidden="true"></i><span style="margin-left: 15px;" class="sendEmailText"><?php if($mailed === false) {_e('Reminder: Already emailed the landlord?', 'bopoolen');} else { _e('Reminder: Already mailed the landlord!', 'bopoolen');} ?></span>
                  </div>
                </div>
              <?php }
              endif; ?>
              <form id="do-contact-form" style="display: none;">
                <input id="contact-target-email" type="hidden" value="<?php echo $userEmail; ?>"  />
                <p><?php echo __('Name', 'bopoolen')?></p>
                <input class="form-control" type="text" id="contact-name" required />
                <p><?php echo __('Your email', 'bopoolen')?></p>
                <input class="form-control" type="email" id="contact-email" required />
                <p><?php echo __('Messages', 'bopoolen')?></p>
                <textarea class="form-control" id="contact-message" rows="5" required></textarea>
                <input type="submit" class="btn btn-brand-primary" id="do-contact" value="<?php _e('Send', 'bopoolen'); ?>">
              </form>
            </div>
          </div>
        </div>

        <script>
        jQuery('#do-contact-form').submit(function(e) {
          e.preventDefault();
          jQuery('#do-contact').html('skickar..');

          jQuery.ajax({
  					url: '/wp-admin/admin-ajax.php',
  					type: 'GET',
  					data: {
  						'action': 'do_contact'
  					}
  				}).done(function(data) {
            alert(data);
          });
        });
        </script>

      </div>

      <div class="carousel-inner print-only">
        <?php
        $count = 0;
        if($postPictureIds[0] == '') {
          echo '<div class="item active"><img src="/wp-content/uploads/2016/02/advertimg-2.png"></div>';
        } else {
          foreach($postPictureIds as $id) {
            $picture = get_post_meta($id, '_wp_attached_file', true);
            if($count == 0) {
              echo '<div class="item active"><img style="height: 450px; width: 100%" src="'.$picture.'" alt="..."></div>';
              $count++;
            } else {
              echo '<div class="item"><img style="height: 450px; width: 100%" src="'.$picture.'" alt="..."></div>';
            }

          }
        }
        ?>
      </div>

      <div class="socialMedia">
        <h2><?php _e('Share this ad on facebook or print it!', 'bopoolen'); ?></h2>
        <div class="socialIcons">
          <?php
            $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            echo do_shortcode('[social_media instagram="http://instagram.com" facebook="http://www.facebook.com/sharer/sharer.php?u='.$actual_link.'" twitter="http://twitter.com/share"]');
          ?>
          <a class="print print hidden-print" href="javascript:window.print();" data-sharetype="print"  title="Print"><i class="fa fa-print" aria-hidden="true"></i></a>
        </div>
      </div>

      <div class="disclaimer">
        <p>
          <?php _e('Annonsören har i enlighet med BoPoolens regler angett en skälig hyra. Är hyran för högt satt kan hyresvärden bli återbetalningsskyldig. Annonsören har även lovat att INTE kräva förskottsbetalning eller deposition innan lägenheten är visad och hyresavtal är skrivet. <br  /><br  />Läs mer om lagar och regler vid andrahandsuthyrning här:', 'bopoolen'); ?>
          <a href="#"><?php _e('Lagar & regler', 'bopoolen'); ?></a>
        </p>
    </div>

    <div class="comments" style="display: none;">
      <div class="comments-container">
        <h1><?php echo _e('Comments', 'bopoolen') ?></h1>
        <?php comments_template('/templates/comments.php'); ?>
        <!-- <ul class="comments-list">
          <li>
            <div class="comment-main-level">
              <div class="comment-avatar"><img src="http://i9.photobucket.com/albums/a88/creaticode/avatar_1_zps8e1c80cd.jpg"></div>
              <div class="comment-box">
                <div class="comment-head">
                  <h6 class="comment-name by-author"><a href="#">Jane Doe</a></h6>
                  <span>20 min sedan</span>
                  <i class="fa fa-reply"></i>
                  <i class="fa fa-heart"></i>
                </div>
                <div class="comment-content">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?
                </div>
              </div>
            </div>
            <ul class="comments-list reply-list">
              <li>
                <div class="comment-avatar"><img src="http://i9.photobucket.com/albums/a88/creaticode/avatar_2_zps7de12f8b.jpg"></div>
                <div class="comment-box">
                  <div class="comment-head">
                    <h6 class="comment-name">John Doe</h6>
                    <span>10 min sedan</span>
                    <i class="fa fa-reply"></i>
                    <i class="fa fa-heart"></i>
                  </div>
                  <div class="comment-content">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?
                  </div>
                </div>
              </li>

              <li>
                <div class="comment-avatar"><img src="http://i9.photobucket.com/albums/a88/creaticode/avatar_1_zps8e1c80cd.jpg" alt=""></div>
                <div class="comment-box">
                  <div class="comment-head">
                    <h6 class="comment-name by-author"><a href="#">Jane Doe</a></h6>
                    <span>5 min sedan</span>
                    <i class="fa fa-reply"></i>
                    <i class="fa fa-heart"></i>
                  </div>
                  <div class="comment-content">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?
                  </div>
                </div>
              </li>
            </ul>
          </li>

          <li>
            <div class="comment-main-level">
              <div class="comment-avatar"><img src="http://i9.photobucket.com/albums/a88/creaticode/avatar_2_zps7de12f8b.jpg" alt=""></div>
              <div class="comment-box">
                <div class="comment-head">
                  <h6 class="comment-name">John Doe</h6>
                  <span>2 min sedan</span>
                  <i class="fa fa-reply"></i>
                  <i class="fa fa-heart"></i>
                </div>
                <div class="comment-content">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?
                </div>
              </div>
            </div>
          </li>
        </ul> -->
      </div>
    </div>
  </section>
<?php } endwhile;  ?>

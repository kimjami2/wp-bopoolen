<div id="loading"><img src="/wp-content/themes/bopoolen/assets/images/ajax-loader.gif"></div>
<div id="no-search-results"><?php _e('No search results', 'bopoolen'); ?></div>
<div class="listing-table-wrapper">
  <table id="listing-table" class="table" width="100%">
    <thead>
      <tr data-id="1">
        <th data-sort="string" width="20%"><p><?php _e('Beskrivning', 'bopoolen') ?>&nbsp;&nbsp;<i class="fa fa-sort" aria-hidden="true"></i></p></th>
        <th width="15%"><p><?php _e('Location', 'bopoolen') ?></p></th>
        <th width="15%"><p><?php _e('Type of housing', 'bopoolen') ?></p></th>
        <th width="15%"><p><?php _e('Type of contract', 'bopoolen') ?></p></th>
        <th data-sort="string" width="12%"><p><?php _e('From', 'bopoolen') ?>&nbsp;&nbsp;<i class="fa fa-sort" aria-hidden="true"></i></p></th>
        <th data-sort="string" width="12%"><p><?php _e('To', 'bopoolen') ?>&nbsp;&nbsp;<i class="fa fa-sort" aria-hidden="true"></i></p></th>
      </tr>
    </thead>
    <tbody>
      <!-- ajax loaded content goes here -->
    </tbody>
  </table>
</div>

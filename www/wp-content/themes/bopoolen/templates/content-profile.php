
<div class="container">
  <div class="row">
    <!--
    <div class="col-md-3">
      <?php //echo wp_nav_menu (array('menu' => 'minprofil')); ?>
    </div>
    -->
    <div class="col-md-12">

<?php
      if(isset($_GET['profile']) && $_GET['profile'] == 'edit-profile') {
        include 'profile/edit-profile.php';
      } else if(isset($_GET['profile']) && $_GET['profile'] == 'my-ads') {
        include 'profile/my-ads.php';
      } else if(isset($_GET['profile']) && $_GET['profile'] == 'find-user') {
        include 'profile/find-user.php';
      } else {
?>
      <div class="row">
        <div class="col-md-4">
          <a href="/valj-annons-att-skapa" class="card white">
            <p class="icon">
              <i class="fa fa-plus-circle" aria-hidden="true"></i>
            </p>
            <p class="info">
              <?php _e('Create ad', 'bopoolen'); ?>
            </p>
          </a>
        </div>
        <div class="col-md-4">
          <a href="/min-profil/?profile=my-ads" class="card white">
            <p class="icon">
              <i class="fa fa-home" aria-hidden="true"></i>
            </p>
            <p class="info">
              <?php _e('My ads', 'bopoolen'); ?>
            </p>
          </a>
        </div>
        <div class="col-md-4">
          <a href="/min-profil/?profile=edit-profile" class="card white">
            <p class="icon">
              <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
            </p>
            <p class="info">
              <?php _e('Edit profile', 'bopoolen'); ?>
            </p>
          </a>
        </div>
      </div>
      <div class="row" style="margin-top:30px;">
        <div class="col-md-4">
          <a href="/min-profil/?profile=find-user" class="card white">
            <p class="icon">
              <i class="fa fa-user" aria-hidden="true"></i>
            </p>
            <p class="info">
              <?php _e('Find user', 'bopoolen'); ?>
            </p>
          </a>
        </div>
        <div class="col-md-4">
          <a href="<?php echo wp_logout_url(); ?>" class="card white">
            <p class="icon">
              <i class="fa fa-sign-out" aria-hidden="true"></i>
            </p>
            <p class="info">
              <?php _e('Sign out', 'bopoolen'); ?>
            </p>
          </a>
        </div>
      </div>
<?php
      }
?>
    </div>
  </div>
</div>

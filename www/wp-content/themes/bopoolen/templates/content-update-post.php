<?php the_content(); ?>

<?php
    $post_id = sanitize_text_field($_GET['post_id']);
    $post = get_post($post_id);
    $post_type = $post->post_type;

    if (empty($post_id)) {
      custom_redirect( home_url() );
    } else if($post_type == 'rentad' || $post_type == 'searchad') {
      //Update the posts post_date to today.
      $today = date("Y-m-d H:i:s");
      $post->post_date = $today;
      wp_update_post($post);

      delete_post_meta($post_id, '_emailreminder');
    }else {
      custom_redirect( home_url() );
    }

?>

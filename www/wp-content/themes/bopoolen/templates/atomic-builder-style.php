<?php
  echo '<style>';
?>
<?php if(get_option('atomic_typography_title_color')){ ?>
  h1,h2,h3,h4,h5,h6 {
    color: <?php echo get_option('atomic_typography_title_color'); ?> !important;
  }
<?php } ?>
<?php if(get_option('atomic_typography_body_color')){ ?>
  .main .textwidget {
    color: <?php echo get_option('atomic_typography_body_color'); ?> !important;
  }
<?php } ?>
<?php if(get_option('atomic_logo_size')){ ?>
  .navbar .navbar-brand {
    font-size: <?php echo get_option('atomic_logo_size'); ?>px !important;
  }
<?php } ?>
<?php if(get_option('atomic_header_top_color')){ ?>
  .top-link  {
    background-color: <?php echo get_option('atomic_header_top_color'); ?> !important;
  }
<?php } ?>
<?php if(get_option('atomic_header_top_color_link')){ ?>
  .top-link a  {
    color: <?php echo get_option('atomic_header_top_color_link'); ?> !important;
  }
<?php } ?>
<?php if(get_option('atomic_header_top_color_link_hover')){ ?>
  .top-link a:hover  {
    color: <?php echo get_option('atomic_header_top_color_link_hover'); ?> !important;
  }
<?php } ?>
<?php if(get_option('atomic_header_menu_color')){ ?>
  .navbar  {
    background: <?php echo get_option('atomic_header_menu_color'); ?> !important;
  }
<?php } ?>
<?php if(get_option('atomic_header_menu_link_color')){ ?>
  .navbar #menu-primary-menu li a  {
    color: <?php echo get_option('atomic_header_menu_link_color'); ?> !important;
  }
<?php } ?>
<?php if(get_option('atomic_header_menu_link_color_hover')){ ?>
  .navbar #menu-primary-menu li a:hover  {
    color: <?php echo get_option('atomic_header_menu_link_color_hover'); ?> !important;
  }
<?php } ?>
<?php if(get_option('atomic_typography_h1')){ ?>
  h1 {
    font-size: <?php echo get_option('atomic_typography_h1'); ?>px !important;
  }
<?php } ?>
<?php if(get_option('atomic_typography_h2')){ ?>
  h2 {
    font-size: <?php echo get_option('atomic_typography_h2'); ?>px !important;
  }
<?php } ?>
<?php if(get_option('atomic_typography_h3')){ ?>
  h3 {
    font-size: <?php echo get_option('atomic_typography_h3'); ?>px !important;
  }
<?php } ?>
<?php if(get_option('atomic_typography_h4')){ ?>
  h4 {
    font-size: <?php echo get_option('atomic_typography_h4'); ?>px !important;
  }
<?php } ?>
<?php if(get_option('atomic_typography_h5')){ ?>
  h5 {
    font-size: <?php echo get_option('atomic_typography_h5'); ?>px !important;
  }
<?php } ?>
<?php if(get_option('atomic_typography_h6')){ ?>
  h6 {
    font-size: <?php echo get_option('atomic_typography_h6'); ?>px !important;
  }
<?php } ?>
<?php if(get_option('atomic_typography_link')){ ?>
  a {
    color: <?php echo get_option('atomic_typography_link'); ?> !important;
  }
<?php } ?>
<?php if(get_option('atomic_typography_link_hover')){ ?>
  a:hover {
    color: <?php echo get_option('atomic_typography_link_hover'); ?> !important;
  }
<?php } ?>
<?php if(get_option('atomic_footer_menu_color')){ ?>
  footer {
    background: <?php echo get_option('atomic_footer_menu_color'); ?> !important;
  }
<?php } ?>
<?php if(get_option('atomic_footer_menu_heading_color')){ ?>
  .footer .widget h3 {
    color: <?php echo get_option('atomic_footer_menu_heading_color'); ?> !important;
  }
<?php } ?>
<?php if(get_option('atomic_footer_menu_link_color')){ ?>
  .footer ul#menu-footer li a {
    color: <?php echo get_option('atomic_footer_menu_link_color'); ?> !important;
  }
<?php } ?>
<?php if(get_option('atomic_footer_menu_link_color_hover')){ ?>
  .footer ul#menu-footer li a:hover {
    color: <?php echo get_option('atomic_footer_menu_link_color_hover'); ?> !important;
  }
<?php } ?>
<?php if(get_option('atomic_footer_menu_body_color')){ ?>
  .footer .textwidget {
    color: <?php echo get_option('atomic_footer_menu_body_color'); ?> !important;
  }
<?php } ?>
<?php if(get_option('atomic_footer_menu_copy_color')){ ?>
  .footer .copyright .textwidget {
    color: <?php echo get_option('atomic_footer_menu_copy_color'); ?> !important;
  }
<?php } ?>
<?php if(get_option('atomic_footer_menu_social_color')){ ?>
  .footer a.social {
    color: <?php echo get_option('atomic_footer_menu_social_color'); ?> !important;
  }
<?php } ?>
<?php if(get_option('atomic_custom_css')){
   echo get_option('atomic_custom_css');
 } ?>
<?php
  echo '</style>';
?>

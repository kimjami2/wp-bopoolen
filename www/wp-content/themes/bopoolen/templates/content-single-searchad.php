<?php while (have_posts()) : the_post(); ?>
  <?php

    $user = get_userdata( $post->post_author );
    $areas              = get_post_meta($post->ID, '_area', true);
    $contractType       = get_post_meta($post->ID, '_contractType', true);
    $accommodationType  = get_post_meta($post->ID, '_accommodationType', true);

    $showMail = esc_attr( get_the_author_meta( '_viewmail', $user->ID ) );
    $showName = esc_attr( get_the_author_meta( '_viewname', $user->ID ) );
    $showPhone = esc_attr( get_the_author_meta( '_viewphone', $user->ID ) );

    $userFullName = esc_attr( get_the_author_meta( 'first_name', $user->ID ) )." ".esc_attr( get_the_author_meta( 'last_name', $user->ID ) );
    $userName = esc_attr( get_the_author_meta( '_username', $user->ID ) );
    if($userName == '' ) {
      $userName = esc_attr( get_the_author_meta( 'nickname', $user->ID ) );
    }
    $userPhone = esc_attr( get_the_author_meta( '_phone', $user->ID ) );
    $userEmail = esc_attr( get_the_author_meta( 'user_email', $user->ID ) );

    $userProfileId = esc_attr( get_the_author_meta( '_profilepicture', $user->ID ) ) ? esc_attr( get_the_author_meta( '_profilepicture', $user->ID ) ) : '/wp-content/themes/bopoolen/assets/images/dummy_profile.png';
    $userProfileImage = get_post_meta($userProfileId, '_wp_attached_file', true);


    // Get if current post is subscribed.
    $meta = get_user_meta(get_current_user_id(), '_subscription', true);
    $meta = explode('/', $meta);
    $subscribed = array_search($post->ID, $meta);

    // Get if email reminder is set or not.
    $meta = get_user_meta(get_current_user_id(), '_mailed', true);
    $meta = explode('/', $meta);
    $mailed = array_search($post->ID, $meta);

    if(apply_filters( 'wpml_current_language', NULL) == 'en') {
      if(get_post_meta($post->ID,'_titleeng', true) === '') {
        $title = $post->post_title;
      } else {
        $title = get_post_meta($post->ID, '_titleeng', true);
      }
    } else {
      $title = $post->post_title;
    }

    if(apply_filters( 'wpml_current_language', NULL) == 'en') {
      if(get_post_meta($post->ID,'_descriptioneng', true) === '') {
        $content = $post->post_content;
      } else {
        $content = get_post_meta($post->ID, '_descriptioneng', true);
      }
    } else {
      $content = $post->post_content;
    }

  ?>

  <section class="page annonssida">

    <div class="container-fluid main-text">
      <div class="page print-start" id="infotext">
        <div class="row">
          <div class="col-sm-12">
            <?php if($post->post_author == get_current_user_id()): ?>
              <div class="clearfix live-only">
                <?php if($post->post_status == 'publish') {
                  ?>
                  <a style="padding-left:15px" href="/redigera-annons-sokes/?post_id=<?php echo $post->ID ?>"
                     class="right"><?php _e('Edit ad', 'bopoolen') ?> <i class="fa fa-pencil-square-o"
                                                                         aria-hidden="true"></i></a>
                  <a style="padding-left:15px"
                     href="/redigera-annons-sokes/?post_id=<?php echo $post->ID ?>&post_type=inaktiv&goTo=inactiveArea"
                     class="right"><?php _e('Deactivate ad', 'bopoolen') ?> <i class="fa fa-pencil-square-o"
                                                                               aria-hidden="true"></i></a>
                  <a href="/redigera-annons-sokes/?post_id=<?php echo $post->ID ?>&post_type=raderad&goTo=eraseArea"
                     class="right"><?php _e('Erase ad', 'bopoolen') ?> <i class="fa fa-pencil-square-o"
                                                                          aria-hidden="true"></i></a>
                  <?php
                } else if($post->post_status == 'draft') {
                  ?>
                  <a style="padding-left:15px" href="/redigera-annons-sokes/?post_id=<?php echo $post->ID ?>&post_type=activate"
                     class="right"><?php _e('Send for activation', 'bopoolen') ?> <i class="fa fa-pencil-square-o"
                                                                                     aria-hidden="true"></i></a>
                  <a href="/redigera-annons-sokes/?post_id=<?php echo $post->ID ?>&post_type=raderad&goTo=eraseArea"
                     class="right"><?php _e('Erase ad', 'bopoolen') ?> <i class="fa fa-pencil-square-o"
                                                                          aria-hidden="true"></i></a>
                  <?php
                } else {
                  ?>
                  <a style="padding-left:15px" href="/redigera-annons-sokes/?post_id=<?php echo $post->ID ?>"
                     class="right"><?php _e('Edit ad', 'bopoolen') ?> <i class="fa fa-pencil-square-o"
                                                                         aria-hidden="true"></i></a>
                  <?php
                }
                ?>
              </div>
            <?php endif; ?>
            <h1 class="entry-title" style="text-align: center;"><?php echo $title; ?></h1>
          </div>
          <div class="col-lg-7 col-md-7">
            <div class="page-txt clearfix">
              <h2><?php echo __('Property description', 'bopoolen')?></h2>
              <?php echo $content; ?>
              <h3><?php echo __('Location', 'bopoolen')?></h3>
              <?php
                foreach(explode(',',$areas) as $area) {
                  echo '<p class="tag">'.ucfirst(__($area, 'bopoolen')).'</p>';
                }
              ?>
              <h3><?php echo __('Type of housing', 'bopoolen')?></h3>
              <?php
                foreach(explode(',',$accommodationType) as $accommodation) {
                  echo '<p class="tag">'.ucfirst(__($accommodation,'bopoolen')).'</p>';
                }
              ?>
              <h3><?php echo __('Type of contract', 'bopoolen')?></h3>
              <?php
              foreach(explode(',',$contractType) as $contract) {
                echo '<p class="tag">'.ucfirst(__($contract, 'bopoolen')).'</p>';
              }
              ?>
            </div>
          </div>
          <div class="col-lg-5 col-md-5">
            <?php if($post->post_author != get_current_user_id()):
              if(is_user_logged_in()) {?>
                <div class="favorite">
                  <input type="hidden" id="postId" value="<?php echo $post->ID; ?>">
                  <i class="fa subscribe <?php if($subscribed === false) {echo 'fa-star-o';} else { echo 'fa-star';} ?>" aria-hidden="true"></i><span class="subscribe-text"><?php if($subscribed === false) { _e('Save ad', 'bopoolen');} else { _e('Saved!', 'bopoolen');} ?></span>
                </div>
              <?php }
            endif; ?>
            <div class="profile-panel profile-panel-blue">
              <div class="profile-image">
                <img src="<?php if($userProfileImage !== false) { echo $userProfileImage; } else { echo '/wp-content/themes/bopoolen/assets/images/profile-image.png';} ?>" />
              </div>
              <h2><?php if($showName === 'true') { echo $userFullName; } else { echo $userName;} ?></h2>
              <?php

              $queryOpts = array(
                'orderby' => 'date',
                'order' => 'DESC',
                'showposts' => min(5, 100),
                'post_type' => 'wpcr3_review',
                'post_status' => 'publish',
                'relation' => 'AND'
              );
              $meta_query[] = array(
                'key' => "wpcr3_review_post",
                'value' => $post->post_author,
                'compare' => '='
              );

              $queryOpts['meta_query'] = $meta_query;
              $reviews = new WP_Query($queryOpts);
              if(count($reviews->posts)> 0) {
                ?>
                <div class="stars">
                  <?php
                  for ($i = 1; $i <= 5; $i++) {
                    $yellow = ($i <= count_avg_rating($post->post_author)) ? 'yellow' : '';
                    ?>
                    <i class="fa fa-star <?php echo $yellow; ?>" aria-hidden="true"></i>
                    <?php
                  }
                  echo '('.count($reviews->posts).')';
                  ?>
                </div>
                <?php
              } else {
                ?>
                <div class="stars">
                  <?php
                  for ($i = 1; $i <= 5; $i++) {
                    ?>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <?php
                  }
                  echo '(0)';
                  ?>
                </div>
                <?php
              }
              ?>
              <div class="profile-footer">
                <?php echo '<a class="btn" href="/profiles/?user='.$post->post_author.'">'.__('See profile', 'bopoolen').'</a>'; ?>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12 col-xs-12">
            <div id="contact-form" class="contact-form">
              <h2><?php _e('Contact tenant', 'bopoolen'); ?></h2>
              <p style="margin-top: -20px; font-size: 15px;"><?php _e('If you are interested, contact the tenant using the contact information below.', 'bopoolen'); ?></p>
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <a href="mailto:<?php echo $userEmail; ?>?Subject=Bopoolen:%20<?php echo urlencode($title); ?>" class="card">
                    <p class="icon">
                      <i class="fa fa-envelope-o" aria-hidden="true"></i>
                    </p>
                    <p class="info">
                      <?php echo $userEmail; ?>
                    </p>
                  </a>
                </div>
                <?php if($showPhone == 'true') { ?>
                  <div class="col-md-4 col-xs-12">
                    <div class="card">
                      <p class="icon">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                      </p>
                      <p class="info">
                        <?php echo $userPhone; ?>
                      </p>
                    </div>
                  </div>
                <?php } ?>
              </div>
              <?php if($post->post_author != get_current_user_id()):
                if(is_user_logged_in()) {?>
                  <div class="clearfix">
                    <div class="mailed">
                      <i class="fa <?php if($mailed === false) {echo 'fa-square-o';} else { echo 'fa-check-square-o';} ?> sendEmail" aria-hidden="true"></i><span style="margin-left: 15px;" class="sendEmailText"><?php if($mailed === false) {_e('Reminder: Already emailed the landlord?', 'bopoolen');} else { _e('Reminder: Already mailed the landlord!', 'bopoolen');} ?></span>
                    </div>
                  </div>
                <?php }
              endif; ?>
              <form id="do-contact-form" style="display: none;">
                <input id="contact-target-email" type="hidden" value="<?php echo $userEmail; ?>"  />
                <p><?php _e('Name', 'bopoolen') ?></p>
                <input class="form-control" type="text" id="contact-name" required />
                <p><?php _e('Your email', 'bopoolen') ?></p>
                <input class="form-control" type="email" id="contact-email" required />
                <p><?php _e('Message', 'bopoolen') ?></p>
                <textarea class="form-control" id="contact-message" rows="5" required></textarea>
                <input type="submit" class="btn btn-brand-primary" id="do-contact" value="<?php _e('Send', 'bopoolen'); ?>">
              </form>
            </div>
          </div>
        </div>

        <script>
        jQuery('#do-contact-form').submit(function(e) {
          e.preventDefault();
          jQuery('#do-contact').html('skickar..');

          jQuery.ajax({
  					url: '/wp-admin/admin-ajax.php',
  					type: 'GET',
  					data: {
  						'action': 'do_contact'
  					}
  				}).done(function(data) {
            alert(data);
          });
        });
        </script>

      </div>

    <div class="comments" style="display: none;">
      <div class="comments-container">
        <h1><?php echo __('Kommentarer', 'bopoolen') ?></h1>
        <?php comments_template('/templates/comments.php'); ?>
        <!-- <ul class="comments-list">
          <li>
            <div class="comment-main-level">
              <div class="comment-avatar"><img src="http://i9.photobucket.com/albums/a88/creaticode/avatar_1_zps8e1c80cd.jpg"></div>
              <div class="comment-box">
                <div class="comment-head">
                  <h6 class="comment-name by-author"><a href="#">Jane Doe</a></h6>
                  <span>20 min sedan</span>
                  <i class="fa fa-reply"></i>
                  <i class="fa fa-heart"></i>
                </div>
                <div class="comment-content">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?
                </div>
              </div>
            </div>
            <ul class="comments-list reply-list">
              <li>
                <div class="comment-avatar"><img src="http://i9.photobucket.com/albums/a88/creaticode/avatar_2_zps7de12f8b.jpg"></div>
                <div class="comment-box">
                  <div class="comment-head">
                    <h6 class="comment-name">John Doe</h6>
                    <span>10 min sedan</span>
                    <i class="fa fa-reply"></i>
                    <i class="fa fa-heart"></i>
                  </div>
                  <div class="comment-content">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?
                  </div>
                </div>
              </li>

              <li>
                <div class="comment-avatar"><img src="http://i9.photobucket.com/albums/a88/creaticode/avatar_1_zps8e1c80cd.jpg" alt=""></div>
                <div class="comment-box">
                  <div class="comment-head">
                    <h6 class="comment-name by-author"><a href="#">Jane Doe</a></h6>
                    <span>5 min sedan</span>
                    <i class="fa fa-reply"></i>
                    <i class="fa fa-heart"></i>
                  </div>
                  <div class="comment-content">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?
                  </div>
                </div>
              </li>
            </ul>
          </li>

          <li>
            <div class="comment-main-level">
              <div class="comment-avatar"><img src="http://i9.photobucket.com/albums/a88/creaticode/avatar_2_zps7de12f8b.jpg" alt=""></div>
              <div class="comment-box">
                <div class="comment-head">
                  <h6 class="comment-name">John Doe</h6>
                  <span>2 min sedan</span>
                  <i class="fa fa-reply"></i>
                  <i class="fa fa-heart"></i>
                </div>
                <div class="comment-content">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?
                </div>
              </div>
            </div>
          </li>
        </ul> -->
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="socialMedia">
          <h2><?php _e('Share this ad on facebook or print it!', 'bopoolen'); ?></h2>
          <div class="socialIcons">
            <?php
              $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
              echo do_shortcode('[social_media instagram="http://instagram.com" facebook="http://www.facebook.com/sharer/sharer.php?u='.$actual_link.'" twitter="http://twitter.com/share"]');
            ?>
            <a class="print print hidden-print" href="javascript:window.print();" data-sharetype="print"  title="Print"><i class="fa fa-print" aria-hidden="true"></i></a>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endwhile; ?>

<?php the_content(); ?>
<h1>test</h1>
<div class="ad-landing-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="image-bg-fluid" style="background-image: url('img/abstract.jpg')">
                    <div class="site-heading">
                        <h1>Heading 1</h1>
                        <button type="button" class="btn btn-default">Hitta bostad</button>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="image-bg-fluid" style="background-image: url('img/abstract.jpg')">
                    <div class="site-heading">
                        <h1>Heading 2</h1>
                        <button type="button" class="btn btn-default">Hyra bostad</button>
                    </div>
                </div>
            </div>
            <!-- col end -->
        </div>
        <!-- row end -->
    </div>
    <!-- container end -->
</div>
<!-- image bg end-->
<div class="main">
<div class="page" id="main-text">
    <div class="content container">
        <div class="row">
            <p class="col-md-5 col-md-offset-1"><h1 class="text-center">Heading 1</h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ut consequat diam, nec volutpat libero. Integer maximus ultrices viverra. Aenean condimentum tincidunt lobortis. Vestibulum elementum velit non nulla ultricies lacinia vehicula non urna. Nam fermentum eros sed ultricies euismod.&nbsp;cures.</p>
            <p class="col-md-5 col-md-offset-1"><h1 class="text-center">Heading 2</h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ut consequat diam, nec volutpat libero. Integer maximus ultrices viverra. Aenean condimentum tincidunt lobortis. Vestibulum elementum velit non nulla ultricies lacinia vehicula non urna. Nam fermentum eros sed ultricies euismod.&nbsp;cures.</p>
            <!-- col end -->
        </div>
        <!-- row end -->
    </div>
    <!-- content container -->
</div>
<!-- Info section -->
<div class="page" id="info">
    <div class="content container">
        <div class="row">
            <article class="service col-sm-4 col-md-4 col-xs-12">
                <img class="icon" src="" alt="Icon">
                <h3>Service 1</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ut consequat diam, nec volutpat libero.</p>
                <button type="button" class="btn btn-default">Mer</button>
            </article>
            <article class="service col-sm-4 col-md-4 col-xs-12">
                <img class="icon" src="" alt="Icon">
                <h3>Service 2</h3>
                <p>Proin vel lorem neque. Donec venenatis massa nisi, nec viverra sapien bibendum sodales. Proin a massa et lectus a.</p>
                <button type="button" class="btn btn-default">Mer</button>
            </article>
            <article class="service col-sm-4 col-md-4 col-xs-12">
                <img class="icon" src="" alt="Icon">
                <h3>Service 3</h3>
                <p>Donec venenatis massa nisi, nec viverra sapien bibendum sodales.Donec venenatis massa nisi, nec viverra sapien bi.</p>
                <button type="button" class="btn btn-default">Mer</button>
            </article>
        </div>
        <!-- row -->
    </div>
    <!-- content container -->
</div>
<!-- info section -->
<div class="page" id="partners">
    <div class="container-fluid">
        <h1 class="text-center">Samarbetspartner</h1>
    </div>
    <!-- container -->
</div>
<!-- partners page -->
</div>

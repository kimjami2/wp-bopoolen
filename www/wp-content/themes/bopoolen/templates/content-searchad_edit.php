<?php the_content(); ?>
<div class="container-fluid adverthandling">

<?php
  if(!is_user_logged_in()) {
?>
  <div class="content">
    <div class="row">
      <div class="col-sm-12">
        <header>
          <h1><?php echo __('Not signed in', 'bopoolen') ?></h1>
        </header>
        <div class="form-segment-wrapper">
          <?php get_template_part('templates/content', 'login'); ?>
        </div>
      </div>
    </div>
  </div>

<?php
  } else {

    $post_id = sanitize_text_field($_GET['post_id']);
    $post_type = get_post_field( 'post_type', $post_id );

    if (empty($post_id)) {
      custom_redirect( home_url() );
    } elseif ($post_type != 'searchad' || !current_user_can( 'edit_post', $post_id )) { ?>

    <div class="content">
      <div class="row">
        <div class="col-sm-12">
          <header>
            <h1><?php echo __('Sorry...', 'bopoolen') ?></h1>
          </header>
          <div class="form-segment-wrapper">
            <p>
              <?php _e('You cannot edit this post. Please contact info@bopoolen.nu if you have any questions.', 'bopoolen'); ?>
            </p>
          </div>
        </div>
      </div>
    </div>

    <?php } else {
      $user_id = get_current_user_id();

      $nonce             = wp_create_nonce( 'modified_by_user_' . $user_id );

      $title             = get_post_field( 'post_title', $post_id );
      $description       = get_post_field( 'post_content', $post_id );
      $titleeng          = get_post_meta($post_id, '_titleeng', true);
      $descriptioneng    = get_post_meta($post_id, '_descriptioneng', true);
      $area              = explode(",", get_post_meta($post_id, '_area', true));
      $contracttype      = explode(",", get_post_meta($post_id, '_contractType', true));
      $accommodationtype = explode(",", get_post_meta($post_id, '_accommodationType', true));
      $availablefrom     = get_post_meta($post_id, '_availablefrom', true);
      $availableto       = get_post_meta($post_id, '_availableto', true);

      $descriptionDelete  = get_post_meta($post_id, '_deletecomment', true);
      $deleteReason       = get_post_meta($post_id, '_deletereason', true);
      $deleteReasonComment= get_post_meta($post_id, '_deletereasoncomment', true);

      $descriptionInaktiv   = get_post_meta($post_id, '_inactivecomment', true);
      $inactivereason       = get_post_meta($post_id, '_inactivereason', true);
      $inactivereasoncomment= get_post_meta($post_id, '_inactivereasoncomment', true);

      $user = get_userdata($user_id);
      $user_meta = get_user_meta($user_id);

      $user_name = $user_meta['first_name'][0] . ' ' . $user_meta['last_name'][0];
      $user_viewName = $user->display_name;
      $user_phone = $user_meta['_phone'][0];
      $user_email = $user->data->user_email;
      $goTo = isset($_GET['goTo']) && $_GET['goTo'] != '' ? $_GET['goTo'] : '';
      $toomany = false;

      if(isset($_GET['post_type']) && $_GET['post_type'] == 'activate') {
        $postsArray = array(
          'posts_per_page'	=> -1,
          'post_type'			  => array( 'searchad' ),
          'post_status'     => array( 'publish'),
          'author'          => get_current_user_id()
        );

        $posts = get_posts($postsArray);
        if(count($posts) >= 1) {
          $toomany = true;
          ?>
          <div class="alert alert-danger" role="alert" style="display:block;"><?php echo __('You can only publish one ad per user. Please be aware that you can change and edit your published ad.', 'bopoolen'); ?></div>
          <?php
        }
      }

      $view_name = isset($user_meta['_viewname'][0]) && $user_meta['_viewname'][0] == 'true' ? 'checked' : '';
      $view_phone = isset($user_meta['_viewphone'][0]) && $user_meta['_viewphone'][0] == 'true' ? 'checked' : '';

      if(!$toomany) {
        ?>
        <input type="hidden" id="goTo" value="<?php echo $goTo; ?>">
        <div class="content">
          <form name="createpost" id="createpost" method="post" class="form-horizontal clearfix"
                data-toggle="validator">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-segment-wrapper">
                  <div class="form-group">
                    <label class="control-label col-md-3" for="title" data-toggle="tooltip" data-placement="right"
                           title="<?php _e('Shown on the Swedish verison of the website.', 'bopoolen'); ?>"><?php echo __('Swedish title ', 'bopoolen') ?>
                      <i class="fa fa-info-circle" aria-hidden="true"></i></label>

                    <div class="col-md-9">
                      <input class="form-control" name="title" id="title" value="<?php echo $title; ?>"
                             placeholder="<?php echo __('Enter your title', 'bopoolen') ?>" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="titleeng" data-toggle="tooltip" data-placement="right"
                           title="<?php _e('Shown on the English version of the website. Many of BoPoolen’s users are international students.', 'bopoolen'); ?>"><?php echo __('English title', 'bopoolen') ?>
                      <i class="fa fa-info-circle" aria-hidden="true"></i></label>

                    <div class="col-md-9">
                      <input class="form-control" name="titleeng" id="titleeng" value="<?php echo $titleeng; ?>"
                             placeholder="<?php echo __('Enter your title in english', 'bopoolen') ?>" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3"><?php echo __('Looking to rent from', 'bopoolen') ?></label>

                    <div class="col-md-9">
                      <div class="row multi-inputs">
                        <div class="col-sm-12">
                          <label for="timespan"><?php echo __('Leasing period', 'bopoolen') ?></label>
                          <select class="form-control update_rentad" id="timespan" required>
                            <option value=""
                                    disabled="disabled"><?php echo __('- Choose leasing period', 'bopoolen') ?></option>
                            <option
                              value="1" <?php echo($availableto == "Tillsvidare" ? "selected" : ""); ?> ><?php echo __('Until further notice', 'bopoolen') ?></option>
                            <option
                              value="2" <?php echo($availableto != "Tillsvidare" ? "selected" : ""); ?> ><?php echo __('Fixed term', 'bopoolen') ?></option>
                          </select>
                        </div>
                        <div class="col-sm-12 accomedation-date infinity">
                          <div class="input-group input-date" id="availabilityDateFrom">
                            <span class="input-group-addon"><?php echo __('From', 'bopoolen') ?></span>
                            <input type="text" class="form-control" name="availablefromInfinity"
                                   placeholder="<?php echo __('yyyy-mm-dd', 'bopoolen') ?>"
                                   value="<?php echo $availablefrom; ?>" id="availablefromInfinity" required>
                          </div>
                        </div>
                        <div class="col-sm-12 accomedation-date time">
                          <div class="input-group input-date input-daterange" id="availabilityDateFromTo">
                            <span class="input-group-addon"><?php echo __('From', 'bopoolen') ?></span>
                            <input type="text" class="form-control"
                                   placeholder="<?php echo __('yyyy-mm-dd', 'bopoolen') ?>"
                                   value="<?php echo $availablefrom; ?>" name="availablefrom" id="availablefrom"
                                   required>
                            <span class="input-group-addon"><?php echo __('To', 'bopoolen') ?></span>
                            <input type="text" class="form-control"
                                   placeholder="<?php echo __('yyyy-mm-dd', 'bopoolen') ?>"
                                   value="<?php echo($availableto != "Tillsvidare" ? $availableto : ""); ?>"
                                   name="availableto" id="availableto" required>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3" for="area"><?php echo __('Location', 'bopoolen'); ?></label>

                    <div class="col-md-9">
                      <select class="form-control dropdown multi-select" id="area" multiple="multiple" name="province"
                              required>
                        <option
                          value="lund" <?php echo(in_array("lund", $area) ? 'selected="selected"' : ""); ?>><?php echo __('Lund', 'bopoolen') ?></option>
                        <option
                          value="malmö" <?php echo(in_array("malmö", $area) ? 'selected="selected"' : ""); ?>><?php echo __('Malmö', 'bopoolen') ?></option>
                        <option
                          value="helsingborg" <?php echo(in_array("helsingborg", $area) ? 'selected="selected"' : ""); ?>><?php echo __('Helsingborg', 'bopoolen') ?></option>
                        <option
                          value="outside-lund" <?php echo(in_array("outside-lund", $area) ? 'selected="selected"' : ""); ?>><?php echo __('Lund nearby', 'bopoolen') ?></option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="accommodationType" data-toggle="tooltip"
                           data-placement="right"
                           title="<?php _e('What kind of housing are you looking for?', 'bopoolen'); ?>"><?php echo __('Type of housing', 'bopoolen') ?>
                      <i class="fa fa-info-circle" aria-hidden="true"></i></label required>

                    <div class="col-md-9">
                      <select class="form-control dropdown multi-select" id="accommodationType" multiple="multiple"
                              name="estate-type" required>
                        <option
                          value="roominhouse" <?php echo(in_array("roominhouse", $accommodationtype) ? 'selected="selected"' : ""); ?>><?php echo __('Room in house', 'bopoolen') ?></option>
                        <option
                          value="roominapartment" <?php echo(in_array("roominapartment", $accommodationtype) ? 'selected="selected"' : ""); ?>><?php echo __('Room in apartment', 'bopoolen') ?></option>
                        <option
                          value="roominstudent" <?php echo(in_array("roominstudent", $accommodationtype) ? 'selected="selected"' : ""); ?>><?php echo __('Room in student housing', 'bopoolen') ?></option>
                        <option
                          value="ownhouse" <?php echo(in_array("ownhouse", $accommodationtype) ? 'selected="selected"' : ""); ?>><?php echo __('House', 'bopoolen') ?></option>
                        <option
                          value="ownapartment" <?php echo(in_array("ownapartment", $accommodationtype) ? 'selected="selected"' : ""); ?>><?php echo __('Apartment', 'bopoolen') ?></option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3" for="contractType" data-toggle="tooltip" data-placement="right"
                           title="<?php _e('What kind of lease are you looking for?', 'bopoolen'); ?>"><?php echo __('Contract', 'bopoolen'); ?>
                      <i class="fa fa-info-circle" aria-hidden="true"></i></label>

                    <div class="col-md-9">
                      <select class="form-control dropdown multi-select" id="contractType" multiple="multiple"
                              name="deal-type" required>
                        <option
                          value="contractsecond" <?php echo(in_array("contractsecond", $contracttype) ? 'selected="selected"' : ""); ?>><?php echo __('Sublease Agreements', 'bopoolen') ?></option>
                        <option
                          value="contractfirst" <?php echo(in_array("contractfirst", $contracttype) ? 'selected="selected"' : ""); ?>><?php echo __('Lease contract', 'bopoolen') ?></option>
                        <option
                          value="contractchange" <?php echo(in_array("contractchange", $contracttype) ? 'selected="selected"' : ""); ?>><?php echo __('Exchange', 'bopoolen') ?></option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="description" data-toggle="tooltip" data-placement="right"
                           title="<?php _e('Shown on the Swedish verison of the website. ', 'bopoolen'); ?>"><?php echo __('Personal presentation in Swedish', 'bopoolen') ?>
                      <i class="fa fa-info-circle" aria-hidden="true"></i></label>

                    <div class="col-md-9">
                      <textarea id="description" class="form-control" rows="5"><?php echo $description; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="descriptioneng" data-toggle="tooltip"
                           data-placement="right"
                           title="<?php _e('Shown on the English version of the website.', 'bopoolen'); ?>"><?php echo __('Personal presentation in English', 'bopoolen') ?>
                      <i class="fa fa-info-circle" aria-hidden="true"></i></label>

                    <div class="col-md-9">
                      <textarea id="descriptioneng" class="form-control"
                                rows="5"><?php echo $descriptioneng; ?></textarea>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <header>
              <h1><?php echo __('Contact details', 'bopoolen') ?></h1>
            </header>
            <div class="form-segment-wrapper">
              <div>
                <label class="topmargin" data-toggle="tooltip" data-placement="right"
                       title="<?php _e('Dessa kontaktuppgifter kommer att synas i din publicerade annons', 'bopoolen'); ?>"><?php _e('This information can be later changed in your profile section', 'bopoolen'); ?>
                  <i class="fa fa-info-circle" aria-hidden="true"></i></label>
              </div>
              <p id="displayName">
                <label class="info"><?php echo _e('Name', 'bopoolen') ?></label>
                <span><?php echo ': ' . $user_name; ?></span>
              </p>

              <p id="displayViewName">
                <label class="info"><?php echo _e('View name', 'bopoolen') ?></label>
                <span><?php echo ': ' . $user_viewName; ?></span>
              </p>

              <p id="displayPhone">
                <label class="info"><?php echo _e('Phone number', 'bopoolen') ?></label>
                <span><?php echo ': ' . $user_phone; ?></span>
              </p>

              <p id="displayEmail">
                <label class="info"><?php echo _e('Email', 'bopoolen') ?></label>
                <span><?php echo ': ' . $user_email; ?></span>
              </p>

              <div class="checkbox-wrapper">
                <div class="form-group nomargin cbo">
                  <label class="control-label" data-toggle="tooltip" data-placement="right"
                         title="<?php _e('If you choose not to display your name in the ad, your user name will then be displayed instead', 'bopoolen') ?>"><?php echo __('Do not display name in ads', 'bopoolen') ?>
                    &nbsp;&nbsp;&nbsp;<input type="checkbox" name="_viewname" id="viewname"
                                             class="regular-text" <?php if ($user_meta['_viewname'][0] == 'false') {
                      echo 'checked';
                    } ?>><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                </div>
                <div class="form-group nomargin cbo">
                  <!-- Tooltip  -->
                  <label class="control-label" for="viewphone" data-toggle="tooltip" data-placement="right"
                         title="<?php _e('Select if other users are to be able to contact you by telephone.', 'bopoolen') ?>"><?php echo __('Do not show telephone number in the ad', 'bopoolen') ?>
                    &nbsp;&nbsp;&nbsp;<input type="checkbox" name="_viewphone" id="viewphone"
                                             class="regular-text" <?php if ($user_meta['_viewphone'][0] == 'false') {
                      echo 'checked';
                    } ?>><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                  <!--&nbsp;&nbsp;&nbsp;<input type="checkbox" name="_viewphone" id="viewphone" value="checked" class="regular-text" checked=""> -->
                </div>
              </div>
            </div>
            <input type="hidden" name="nonce" id="nonce" value="<?php echo $nonce; ?>">

            <?php
            if (isset($_GET['post_type']) && $_GET['post_type'] != '') {
              if ($_GET['post_type'] == 'inaktiv') {
                ?>
                <header><h1 id="inactiveArea"><?php echo __('Deactivade ad', 'bopoolen') ?></h1></header>
                <div class="form-segment-wrapper">
                  <div class="form-group">
                    <label
                      class="col-md-3 control-label"><?php _e('Reason for deactivating your ad', 'bopoolen'); ?></label>

                    <div class="col-md-9">
                      <?php
                      echo '<input type="radio" class="radiobutton" name="_inactivereason" value="found_tenant"' . checkValue('found_tenant', $inactivereason) . '>' . __('Jag har hittat boende', 'bopoolen') . '<br>';
                      echo '<input type="radio" class="radiobutton" name="_inactivereason" id="other" value="other"' . checkValue('other', $inactivereason) . '>' . __('Annan anledning', 'bopoolen') . '<br>';
                      ?>
                    </div>
                  </div>
                  <hr class="other" style="display:none;">
                  <div class="form-group other" style="display:none;">
                    <label class="col-md-3 control-label"
                           for="descriptionReasonInaktiv"><?php echo __('Comment of "Other reason" above', 'bopoolen') ?></label>

                    <div class="col-md-9">
                      <textarea id="descriptionReasonInaktiv" class="form-control"
                                rows="5"><?php echo $inactivereasoncomment; ?></textarea>
                    </div>
                  </div>
                </div>
                <p class="error-msg"></p>
                <button id="submit" type="submit" formnovalidate
                        class="btn btn-primary right update_rentad"><?php echo __('Deactivate ad', 'bopoolen') ?></button>
                <?php
              } else if ($_GET['post_type'] == 'raderad') {
                ?>
                <header>
                  <h1 id="eraseArea"><?php echo __('Delete ad', 'bopoolen') ?></h1>
                </header>
                <div class="form-segment-wrapper">
                  <div class="form-group">
                    <label
                      class="col-md-3 control-label"><?php _e('Reason for deleting your ad', 'bopoolen'); ?></label>

                    <div class="col-md-9">
                      <?php
                      echo '<input type="radio" class="radiobutton" name="_deletereason" value="found_tenant"' . checkValue('found_tenant', $deleteReason) . '>' . __('Jag har hittat boende', 'bopoolen') . '<br>';
                      echo '<input type="radio" class="radiobutton" name="_deletereason" id="other" value="other"' . checkValue('other', $deleteReason) . '>' . __('Annan anledning', 'bopoolen') . '<br>';
                      ?>
                    </div>
                  </div>
                  <hr class="other" style="display:none;">
                  <div class="form-group other" style="display:none;">
                    <label class="col-md-3 control-label"
                           for="descriptionReasonErase"><?php echo __('Comment of "Other reason" above', 'bopoolen') ?></label>

                    <div class="col-md-9">
                      <textarea id="descriptionReasonEraseOther" class="form-control"
                                rows="5"><?php echo $deleteReasonComment; ?></textarea>
                    </div>
                  </div>
                </div>
                <p class="error-msg"></p>
                <button id="submit" formnovalidate type="submit"
                        class="btn btn-primary right update_rentad"><?php echo __('Delete ad', 'bopoolen') ?></button>
                <?php
              } else {
                ?>
                <p class="error-msg"></p>
                <button id="submit" type="submit"
                        class="btn btn-primary right update_rentad"><?php echo __('Update ad', 'bopoolen') ?></button>
                <?php
              }
            } else {
              ?>
              <p class="error-msg"></p>
              <button id="submit" type="submit"
                      class="btn btn-primary right update_rentad"><?php echo __('Update ad', 'bopoolen') ?></button>
              <?php
            }

            ?>
          </form>
        </div>
        <?php
      }
    }
  }
  ?>
</div>

<?php
  $user_id = get_current_user_id();
  $security_number = get_user_meta($user_id, '_securitynumber')[0];
?>
<?php the_content(); ?>
<div class="container-fluid adverthandling">

<?php
  if(!is_user_logged_in()) {
?>
  <div class="content">
    <div class="row">
      <div class="col-sm-12">
        <header>
          <h1><?php echo __('Not signed in', 'bopoolen') ?></h1>
        </header>
        <div class="form-segment-wrapper">
          <?php get_template_part('templates/content', 'login'); ?>
        </div>
      </div>
    </div>
  </div>
<?php
  } else if(empty($security_number)) {
?>
  <div class="content">
    <div class="row">
      <div class="col-sm-12">
        <header>
          <h1><?php echo __('No social number', 'bopoolen') ?></h1>
        </header>
        <div class="form-segment-wrapper">
          <p>
            <?php _e('To post or edit a rent ad you need a swedish social number, please contact info@bopoolen.nu for futher questions.', 'bopoolen'); ?>
          </p>
        </div>
      </div>
    </div>
  </div>
<?php
  } else {

    $post_id = sanitize_text_field($_GET['post_id']);
    $post_type = get_post_field( 'post_type', $post_id );

    if (empty($post_id)) {
      custom_redirect( home_url() );
    } elseif ($post_type != 'rentad' || !current_user_can( 'edit_post', $post_id )) { ?>

    <div class="content">
      <div class="row">
        <div class="col-sm-12">
          <header>
            <h1><?php echo __('Sorry...', 'bopoolen') ?></h1>
          </header>
          <div class="form-segment-wrapper">
            <p>
              <?php _e('You cannot edit this post. Please contact info@bopoolen.nu if you have any questions.', 'bopoolen'); ?>
            </p>
          </div>
        </div>
      </div>
    </div>

    <?php } else {

      $nonce            = wp_create_nonce( 'modified_by_user_' . $user_id );

      $title            = get_post_field( 'post_title', $post_id );
      $description      = get_post_field( 'post_content', $post_id );
      $titleeng         = get_post_meta($post_id, '_titleeng', true);
      $descriptioneng   = get_post_meta($post_id, '_descriptioneng', true);

      $rentprice        = get_post_meta($post_id, '_rentprice', true);
      $availablefrom    = get_post_meta($post_id, '_availablefrom', true);
      $availableto      = get_post_meta($post_id, '_availableto', true);

      $streetaddress    = get_post_meta($post_id, '_streetaddress', true);
      $postalcode       = get_post_meta($post_id, '_postalcode', true);
      $city             = get_post_meta($post_id, '_city', true);
      $area             = get_post_meta($post_id, '_area', true);

      $roominhouse      = get_post_meta($post_id, '_roominhouse', true);
      $roominapartment  = get_post_meta($post_id, '_roominapartment', true);
      $roominstudent    = get_post_meta($post_id, '_roominstudent', true);
      $ownhouse         = get_post_meta($post_id, '_ownhouse', true);
      $ownapartment     = get_post_meta($post_id, '_ownapartment', true);

      $contractsecond   = get_post_meta($post_id, '_contractsecond', true);
      $contractfirst    = get_post_meta($post_id, '_contractfirst', true);
      $contractchange   = get_post_meta($post_id, '_contractchange', true);

      $availableyta       = get_post_meta($post_id, '_availableyta', true);
      $availablegemensam  = get_post_meta($post_id, '_availablegemensam', true);
      $numberofrooms      = get_post_meta($post_id, '_numberofrooms', true);

      $availablefrom      = get_post_meta($post_id, '_availablefrom', true);
      $availableto        = get_post_meta($post_id, '_availableto', true);

      $includedel         = get_post_meta($post_id, '_includedel', true);
      $includedinternet   = get_post_meta($post_id, '_includedinternet', true);
      $includedfurniture  = get_post_meta($post_id, '_includedfurniture', true);
      $includedtoilet     = get_post_meta($post_id, '_includedtoilet', true);
      $includedbath       = get_post_meta($post_id, '_includedbath', true);
      $includedanimal     = get_post_meta($post_id, '_includedanimal', true);
      $includedhandicap   = get_post_meta($post_id, '_includedhandicap', true);
      $includeddishwasher = get_post_meta($post_id, '_includeddishwasher', true);
      $includedwasher     = get_post_meta($post_id, '_includedwasher', true);
      $includedsmoking    = get_post_meta($post_id, '_includedsmoking', true);
      $includedpentry     = get_post_meta($post_id, '_includedpentry', true);
      $includedkitchen    = get_post_meta($post_id, '_includedkitchen', true);
      $includedentrance   = get_post_meta($post_id, '_includedentrance', true);



      $descriptionDelete  = get_post_meta($post_id, '_deletecomment', true);
      $deleteReason       = get_post_meta($post_id, '_deletereason', true);
      $deleteReasonComment= get_post_meta($post_id, '_deletereasoncomment', true);

      $descriptionInaktiv   = get_post_meta($post_id, '_inactivecomment', true);
      $inactivereason       = get_post_meta($post_id, '_inactivereason', true);
      $inactivereasoncomment= get_post_meta($post_id, '_inactivereasoncomment', true);

      $latlng             = get_post_meta($post_id, '_latlng', true);

      $contractType = "";
      if($contractsecond){
       $contractType = "contractsecond";
      }
      if($contractfirst){
       $contractType = "contractfirst";
      }
      if($contractchange){
       $contractType = "contractchange";
      }

      $type = "";
      if($roominhouse){
       $type = "roominhouse";
      }
      if($roominapartment){
       $type = "roominapartment";
      }
      if($roominstudent){
       $type = "roominstudent";
      }
      if($ownhouse){
       $type = "ownhouse";
      }
      if($ownapartment){
       $type = "ownapartment";
      }

      $user_meta = get_user_meta($user_id);
      $user = get_userdata($user_id);

      $user_name = $user_meta['first_name'][0] . ' ' . $user_meta['last_name'][0];
      $user_viewName = $user->display_name;
      $user_phone = $user_meta['_phone'][0];
      $user_email = $user->data->user_email;
      $goTo = isset($_GET['goTo']) && $_GET['goTo'] != '' ? $_GET['goTo'] : '';

      $view_name = isset($user_meta['_viewname'][0]) && $user_meta['_viewname'][0] == 'checked' ? 'checked' : '';
      $view_phone = isset($user_meta['_viewphone'][0]) && $user_meta['_viewphone'][0] == 'checked' ? 'checked' : '';
      $view_address = isset($user_meta['_viewaddress'][0]) && $user_meta['_viewaddress'][0] == 'checked' ? 'checked' : '';
  ?>
      <input type="hidden" id="langCode" value="<?php echo apply_filters( 'wpml_current_language', NULL); ?>">
      <input type="hidden" id="goTo" value="<?php echo $goTo; ?>">
    <div class="content">
        <form name="updatepost" id="updatepost" method="post" enctype="multipart/form-data" class="form-horizontal clearfix"  data-toggle="validator">
          <div class="row">
            <div class="col-sm-12">
              <header>
                <h1><?php echo __('Living', 'bopoolen') ?></h1>
              </header>
              <div class="form-segment-wrapper">
                <div class="form-group">
                  <label class="control-label col-md-3" for="title" data-toggle="tooltip" data-placement="right" title="<?php _e('Shown on the Swedish version of the website.', 'bopoolen'); ?>"><?php echo __('Swedish title ', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                  <div class="col-md-9">
                    <input class="form-control" name="title" id="title" value="<?php echo $title; ?>" placeholder="<?php echo __('Enter your title', 'bopoolen') ?>" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3" for="title" data-toggle="tooltip" data-placement="right" title="<?php _e('Shown on the English version of the website. Many of BoPoolen’s users are international students.', 'bopoolen'); ?>"><?php echo __('English title', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                  <div class="col-md-9">
                    <input class="form-control" name="title-eng" id="title-eng" value="<?php echo $titleeng; ?>" placeholder="<?php echo __('Enter your title in english', 'bopoolen') ?>" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3"><?php echo __('Available from (YYYY-MM-DD)', 'bopoolen') ?></label>
                  <div class="col-md-9">
                    <div class="row multi-inputs">
                      <div class="col-sm-12">
                        <label for="timespan"><?php echo __('Leasing period', 'bopoolen') ?></label>
                        <select class="form-control update_rentad" id="timespan" required>
                          <option value="" disabled="disabled"><?php echo __('- Choose leasing period', 'bopoolen') ?></option>
                          <option value="1" <?php echo ($availableto == "Tillsvidare" ? "selected": "" ); ?> ><?php echo __('Until further notice', 'bopoolen') ?></option>
                          <option value="2" <?php echo ($availableto != "Tillsvidare" ? "selected": "" ); ?> ><?php echo __('Fixed term', 'bopoolen') ?></option>
                        </select>
                      </div>
                      <div class="col-sm-12 accomedation-date infinity">
                        <div class="input-group input-date" id="availabilityDateFrom">
                          <span class="input-group-addon"><?php echo __('From', 'bopoolen') ?></span>
                          <input type="text" class="form-control" name="availablefromInfinity" placeholder="<?php echo __('yyyy-mm-dd', 'bopoolen') ?>" value="<?php echo $availablefrom; ?>" id="availablefromInfinity" required>
                        </div>
                      </div>
                      <div class="col-sm-12 accomedation-date time">
                        <div class="input-group input-date input-daterange" id="availabilityDateFromTo">
                          <span class="input-group-addon"><?php echo __('From', 'bopoolen') ?></span>
                          <input type="text" class="form-control" placeholder="<?php echo __('yyyy-mm-dd', 'bopoolen') ?>" value="<?php echo $availablefrom; ?>" name="availablefrom" id="availablefrom" required>
                          <span class="input-group-addon"><?php echo __('To', 'bopoolen') ?></span>
                          <input type="text" class="form-control" placeholder="<?php echo __('yyyy-mm-dd', 'bopoolen') ?>" value="<?php echo ($availableto != "Tillsvidare" ? $availableto : "" ); ?>" name="availableto" id="availableto" required>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label" for="title" data-toggle="tooltip" data-placement="right" title="<?php _e('If you are not registered at the address, we need to see a documented connection to the address before we can approve your ad and we may request that you send a copy of your purchase agreement or rental agreement to bopoolen@lus.lu.se', 'bopoolen'); ?>"><?php echo __('Address', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                  <div class="col-md-9">
                    <div class="row multi-inputs">
                      <div class="col-md-12">
                        <label class="control-label" for="street"><?php echo __('Street address:', 'bopoolen') ?></label>
                        <input class="form-control" name="street" id="street" value="<?php echo $streetaddress; ?>" placeholder="<?php echo __('Street address', 'bopoolen') ?>" type="text" required>
                      </div>
                      <div class="col-md-4">
                        <label class="control-label" for="zip"><?php echo __('Post code:', 'bopoolen') ?></label>
                        <input class="form-control" name="zip" id="zip" value="<?php echo $postalcode; ?>" placeholder="<?php echo __('Post code', 'bopoolen') ?>" type="text" required>
                      </div>
                      <div class="col-md-8">
                        <label class="control-label" for="city"><?php echo __('Town / city:', 'bopoolen') ?></label>
                        <input class="form-control" name="city" id="city" value="<?php echo $city; ?>" placeholder="<?php echo __('Town / city', 'bopoolen') ?>" type="text" required>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="checkbox">
                    <label class="col-md-3 control-label addressfield" for="viewaddress" style="margin-right:35px;" data-toggle="tooltip" data-placement="right" title="<?php _e('Mark with a cross if you do not want to show the address in the ad. ', 'bopoolen'); ?>"><?php echo __('Do not show the address in the ad', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                    <input type="checkbox" id="viewaddress" style="top:15px;" <?php echo $view_address; ?>>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3" for="area"><?php echo __('Location', 'bopoolen'); ?></label>
                  <div class="col-md-9">
                    <select class="form-control dropdown" id="area" required>
                      <option value="lund" <?php echo ( $area == "lund" ? "selected" : "" ); ?> ><?php echo __('Lund', 'bopoolen') ?></option>
                      <option value="malmö" <?php echo ( $area == "malmö" ? "selected" : "" ); ?> ><?php echo __('Malmö', 'bopoolen') ?></option>
                      <option value="helsingborg" <?php echo ( $area == "helsingborg" ? "selected" : "" ); ?> ><?php echo __('Helsingborg', 'bopoolen') ?></option>
                      <option value="outside-lund" <?php echo ( $area == "outside-lund" ? "selected" : "" ); ?> ><?php echo __('Lund nearby', 'bopoolen') ?></option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3" for="accommodationType" data-toggle="tooltip" data-placement="right" title="<?php _e('What kind of housing are you offering?', 'bopoolen'); ?>"><?php echo __('Type of housing', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label required>
                  <div class="col-md-9">
                    <select class="form-control update_rentad" id="accommodationType">
                      <option value="" <?php echo ( $type == "" ? "selected" : "" ); ?> disabled="disabled"><?php echo __('- Choose types of housing', 'bopoolen') ?></option>
                      <option value="roominhouse" <?php echo ( $type == "roominhouse" ? "selected" : "" ); ?> ><?php echo __('Room in house', 'bopoolen') ?></option>
                      <option value="roominapartment" <?php echo ( $type == "roominapartment" ? "selected" : "" ); ?> ><?php echo __('Room in apartment', 'bopoolen') ?></option>
                      <option value="roominstudent" <?php echo ( $type == "roominstudent" ? "selected" : "" ); ?> ><?php echo __('Room in student housing', 'bopoolen') ?></option>
                      <option value="ownhouse" <?php echo ( $type == "ownhouse" ? "selected" : "" ); ?> ><?php echo __('House', 'bopoolen') ?></option>
                      <option value="ownapartment" <?php echo ( $type == "ownapartment" ? "selected" : "" ); ?> ><?php echo __('Appartment', 'bopoolen') ?></option>
                    </select>
                  </div>
                </div>
                <div class="accomedation-type own">
                  <div class="form-group">
                    <label class="control-label col-md-3" for="numberofrooms"><?php echo __('Room', 'bopoolen') ?></label>
                    <div class="col-md-9">
                      <select class="form-control" id="numberofrooms" required>
                        <option value="" disabled="disabled"><?php echo __('- Choose number of rooms', 'bopoolen') ?></option>
                        <option value="1" <?php echo ( $numberofrooms == "1" ? "selected" : "" ); ?> >1</option>
                        <option value="2" <?php echo ( $numberofrooms == "2" ? "selected" : "" ); ?> >2</option>
                        <option value="3" <?php echo ( $numberofrooms == "3" ? "selected" : "" ); ?> >3</option>
                        <option value="4" <?php echo ( $numberofrooms == "4" ? "selected" : "" ); ?> >4</option>
                        <option value="5" <?php echo ( $numberofrooms == "5" ? "selected" : "" ); ?> >5</option>
                        <option value="6" <?php echo ( $numberofrooms == "6" ? "selected" : "" ); ?> >6</option>
                        <option value="7" <?php echo ( $numberofrooms == "7" ? "selected" : "" ); ?> >7</option>
                        <option value="8" <?php echo ( $numberofrooms == "8" ? "selected" : "" ); ?> >8</option>
                        <option value="9" <?php echo ( $numberofrooms == "9" ? "selected" : "" ); ?> >9</option>
                        <option value="9+" <?php echo ( $numberofrooms == "9+" ? "selected" : "" ); ?> >9+</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="availableyta" data-toggle="tooltip" data-placement="right" title="<?php _e('State total number of m2', 'bopoolen'); ?>"><?php echo __('Number of m<sup>2</sup>', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                    <div class="col-md-9">
                      <div class="input-group">
                        <input type="text" class="form-control" name="availableyta" id="availableyta" placeholder="<?php echo __('Area in m2', 'bopoolen') ?>" value="<?php echo $availableyta; ?>" required pattern="^[0-9]*$" data-error="<?php echo __('Numeric value only', 'bopoolen') ?>">
                        <span class="input-group-addon"><?php echo __('sq.m.', 'bopoolen') ?></span>
                      </div>
                      <span class="help-block with-errors"></span>
                    </div>
                  </div>
                </div>
                <div class="accomedation-type roomin">
                  <div class="form-group">
                    <label class="control-label col-md-3" for="availableyta" data-toggle="tooltip" data-placement="right" title="<?php _e('The number of m2 that comprise the tenant’s private space.', 'bopoolen'); ?>"><?php echo __('Own space', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                    <div class="col-md-9">
                      <div class="input-group">
                        <input type="text" class="form-control" name="availableytaOwn" id="availableytaOwn" placeholder="<?php echo __('Area in m2', 'bopoolen') ?>" value="<?php echo $availableyta; ?>" required pattern="^[0-9]*$" data-error="<?php echo __('Numeric value only', 'bopoolen') ?>">
                        <span class="input-group-addon"><?php echo __('sq.m.', 'bopoolen') ?></span>
                      </div>
                      <span class="help-block with-errors"></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3" for="availablegemensam" data-toggle="tooltip" data-placement="right" title="<?php _e('The total space the tenant has access to.', 'bopoolen'); ?>"><?php echo __('Total space', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                    <div class="col-md-9">
                      <div class="input-group">
                        <input type="text" class="form-control" name="availablegemensam" id="availablegemensam" placeholder="<?php echo __('Area in m2', 'bopoolen') ?>" value="" pattern="^[0-9]*$" data-error="<?php echo __('Numeric value only', 'bopoolen') ?>">
                        <span class="input-group-addon"><?php echo __('sq.m.', 'bopoolen') ?></span>
                      </div>
                      <span class="help-block with-errors"></span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3" for="contractType" data-toggle="tooltip" data-placement="right" title="<?php _e('What kind of lease are you offering?', 'bopoolen'); ?>"><?php echo __('Contract', 'bopoolen'); ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                  <div class="col-md-9">
                    <select class="form-control" id="contractType">
                      <option value="contractsecond" <?php echo ( $contractType == "contractsecond" ? "selected" : "" ); ?> ><?php echo __('Sublease agreements', 'bopoolen') ?></option>
                      <option value="contractfirst" <?php echo ( $contractType == "contractfirst" ? "selected" : "" ); ?> ><?php echo __('Tenancy agreement', 'bopoolen') ?></option>
                      <option value="contractchange" <?php echo ( $contractType == "contractchange" ? "selected" : "" ); ?> ><?php echo __('Exchange', 'bopoolen') ?></option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3" for="description" data-toggle="tooltip" data-placement="right" title="<?php _e('A short description of the housing. This text is shown on the Swedish site.', 'bopoolen'); ?>"><?php echo __('Swedish description of the housing', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                  <div class="col-md-9">
                    <textarea id="description" class="form-control" rows="5"><?php echo $description; ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3" for="description" data-toggle="tooltip" data-placement="right" title="<?php _e('A short description of the housing. This text is shown on the English site. Many of BoPoolen’s users are international students.' ,'bopoolen'); ?>"><?php echo __('English description of the housing', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                  <div class="col-md-9">
                    <textarea id="description-eng" class="form-control" rows="5"><?php echo $descriptioneng; ?></textarea>
                  </div>
                </div>
                <!-- <div class="form-group">
                  <label class="control-label col-md-3" for="description"><?php echo __('Upload images', 'bopoolen') ?></label>
                  <div class="col-md-9">
                    <div class="input-group">
                      <input id="fileupload" type="file" name="fileToUpload" multiple="multiple" accept="image/jpg, image/jpeg">
                      <span class="help-block with-errors"></span>
                    </div>
                  </div>
                </div> -->
              </div>
            </div>
          </div>

          <header>
            <h1><?php echo __('Terms', 'bopoolen') ?></h1>
          </header>
          <div class="form-segment-wrapper">
            <div class="form-group">
              <label class="control-label col-md-3" for="rent" data-toggle="tooltip" data-placement="right" title="<?php _e('Monthly rent in SEK', 'bopoolen'); ?>"><?php echo __('Rent', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
              <div class="col-md-9">
                <div class="input-group">
                  <input type="text" class="form-control" name="rent" id="rent" value="<?php echo $rentprice; ?>" placeholder="<?php echo __('Month rent in Kr', 'bopoolen') ?>" value="" pattern="^[0-9]*$" data-error="<?php echo __('Numeric value only', 'bopoolen') ?>" required>
                  <span class="input-group-addon">Kr</span>
                </div>
                <span class="help-block with-errors"></span>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-9 col-md-offset-3">
                <div class="info-text">
                  <p>
                    <?php echo __('Reminder: Please note that your rent must be reasonable. According to the law, the rent for a sublet may not be higher than the rent you pay or the costs you have for the housing. If you rent out part of the housing, the rent shall be proportional. If you charge an excessive rent there is a risk that you will be liable to make repayments. Read more about setting rents under Laws & Regulations <a href="/info-om-uthyrning/for-dig-som-hyr-ut/" target="_blank">here</a>.', 'bopoolen') ?>
                  </p>
                  <p>
                    <?php echo __('Student finance is currently SEK 9,901. The National Board of Housing, Building and Planning’s reasonable rent recommendation for a student is SEK 2,476.', 'bopoolen') ?>
                  </p>
                  <div class="checkbox-wrapper">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" id="reasonableRent" data-error="<?php echo __('Please attest that according to you this is a reasonable rent', 'bopoolen') ?>" required>
                        <?php echo __('I hereby certify that the set rent is reasonable', 'bopoolen') ?>
                        <span><?php _e('You can read more about the rent you are entitled to charge <a href="http://www.hyresnamnden.se/Hyra-i-andra-hand/Hyran/" target="_blank">here</a>.', 'bopoolen'); ?></span>
                      </label>
                      <div class="help-block with-errors"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-9">
                <div class="checkbox-wrapper">
                  <h1><?php _e('Included in the rent', 'bopoolen');?></h1>
                  <div class="checkbox">
                    <label for="includedel">
                      <input type="checkbox" id="includedel" value="checked" <?php echo $includedel.'=""'; ?>"><?php echo __('Electricity', 'bopoolen') ?>
                    </label>
                  </div>
                  <div class="checkbox">
                    <label for="includedinternet">
                      <input type="checkbox" id="includedinternet" value="checked" <?php echo $includedinternet.'=""'; ?>"><?php echo __('Internet', 'bopoolen') ?>
                    </label>
                  </div>
                  <div class="checkbox">
                    <label for="includedfurniture">
                      <input type="checkbox" id="includedfurniture" value="checked" <?php echo $includedfurniture.'=""'; ?>"><?php echo __('Fully furnished', 'bopoolen') ?>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <header>
            <h1><?php _e('About the housing', 'bopoolen'); ?></h1>
          </header>
          <div class="form-segment-wrapper">
            <div class="checkbox-wrapper">

              <div class="checkbox">
                <label for="includedtoilet">

                  <input type="checkbox" id="includedtoilet" value="checked" <?php echo $includedtoilet.'=""'; ?>"><?php echo __('Own toilet', 'bopoolen') ?>
                </label>
              </div>
              <div class="checkbox">
                <label for="includedbath">
                  <input type="checkbox" id="includedbath" value="checked" <?php echo $includedbath.'=""'; ?>"><?php echo __('Own bathroom', 'bopoolen') ?>
                </label>
              </div>
              <div class="checkbox">
                <label for="includedpentry">
                  <input type="checkbox" id="includedpentry" value="checked" <?php echo $includedpentry.'=""'; ?>"><?php echo __('Own kitchenette', 'bopoolen') ?>
                </label>
              </div>
              <div class="checkbox">
                <label for="includedkitchen">
                  <input type="checkbox" id="includedkitchen" value="checked" <?php echo $includedkitchen.'=""'; ?>"><?php echo __('Own kitchenette', 'bopoolen') ?>
                </label>
              </div>
              <div class="checkbox">
                <label for="includedentrance">

                  <input type="checkbox" id="includedentrance" value="checked" <?php echo $includedentrance.'=""'; ?>"><?php echo __('Own entrance', 'bopoolen') ?>
                </label>
              </div>
              <div class="checkbox">
                <label for="includedhandicap">
                  <input type="checkbox" id="includedhandicap" value="checked" <?php echo $includedhandicap.'=""'; ?>"><?php echo __('Disabled access', 'bopoolen') ?>
                </label>
              </div>
              <div class="checkbox">
                <label for="includeddishwasher">
                  <input type="checkbox" id="includeddishwasher" value="checked" <?php echo $includeddishwasher.'=""'; ?>"><?php echo __('Dishwasher', 'bopoolen') ?>
                </label>
              </div>
              <div class="checkbox">
                <label for="includedwasher">
                  <input type="checkbox" id="includedwasher" value="checked" <?php echo $includedwasher.'=""'; ?>"><?php echo __('Washing machine', 'bopoolen') ?>
                </label>
              </div>
              <div class="checkbox">
                <label for="includedanimal">
                  <input type="checkbox" id="includedanimal" value="checked" <?php echo $includedanimal.'=""'; ?>"><?php echo __('Pets allowed', 'bopoolen') ?>
                </label>
              </div>
              <div class="checkbox">
                <label for="includedsmoking">
                  <input type="checkbox" id="includedsmoking" value="checked" <?php echo $includedsmoking.'=""'; ?>"><?php echo __('Smoking not allowed', 'bopoolen') ?>
                </label>
              </div>
            </div>
          </div>

          <header>
            <h1><?php echo __('Contact details', 'bopoolen') ?></h1>
          </header>
          <div class="form-segment-wrapper">
            <div>
              <label class="topmargin" data-toggle="tooltip" data-placement="right" title="<?php _e('Dessa kontaktuppgifter kommer att synas i din publicerade annons', 'bopoolen'); ?>"><?php _e('This information can be later changed in your profile section', 'bopoolen'); ?> <i class="fa fa-info-circle" aria-hidden="true"></i></label>
            </div>
            <p id="displayName">
            <label class="info"><?php echo _e('Name', 'bopoolen') ?></label>
            <span ><?php echo ': '.$user_name; ?></span>
            </p>
            <p id="displayViewName">
            <label class="info"><?php echo _e('View name', 'bopoolen') ?></label>
            <span ><?php echo ': '.$user_viewName; ?></span>
            </p>
            <p id="displayPhone">
            <label class="info"><?php echo _e('Phone number', 'bopoolen') ?></label>
            <span ><?php echo ': '.$user_phone; ?></span>
            </p>
            <p id="displayEmail">
            <label class="info"><?php echo _e('Email', 'bopoolen') ?></label>
            <span ><?php echo ': '.$user_email; ?></span>
            </p>
            <div class="checkbox-wrapper">
              <div class="form-group nomargin cbo">
                <label class="control-label" data-toggle="tooltip" data-placement="right" title="<?php _e('If you choose not to display your name in the ad, your user name will then be displayed instead', 'bopoolen') ?>"><?php echo __('Do not display name in ads', 'bopoolen') ?>
                  &nbsp;&nbsp;&nbsp;<input type="checkbox" name="_viewname" id="viewname" class="regular-text" <?php if($user_meta['_viewname'][0] == 'false') {echo 'checked';} ?>><i class="fa fa-info-circle" aria-hidden="true"></i></label>
              </div>
              <div class="form-group nomargin cbo">
                <!-- Tooltip  -->
                <label class="control-label" for="viewphone" data-toggle="tooltip" data-placement="right" title="<?php _e('Select if other users are to be able to contact you by telephone.', 'bopoolen')?>"><?php echo __('Do not show telephone number in the ad', 'bopoolen') ?>
                  &nbsp;&nbsp;&nbsp;<input type="checkbox" name="_viewphone" id="viewphone" class="regular-text" <?php if($user_meta['_viewphone'][0] == 'false') {echo 'checked';} ?>><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                <!--&nbsp;&nbsp;&nbsp;<input type="checkbox" name="_viewphone" id="viewphone" value="checked" class="regular-text" checked=""> -->
              </div>
            </div>
          </div>

          <header>
            <h1><?php echo __('Upload images', 'bopoolen') ?></h1>
          </header>
          <div class="form-segment-wrapper">
            <p><?php _e('Du kan som max ladda upp 5 bilder per annons.', 'bopoolen'); ?></p>
            <div id="dZUpload" class="dropzone multiple">

              <div class="dz-default dz-message">
                <div class="img-container">
                  <img src="/wp-content/themes/bopoolen/assets/images/upload-image.jpg" alt="" />
                </div>
                <?php echo __('Click here to upload images', 'bopoolen') ?> </div>
            </div>
          </div>

          <input type="hidden" name="image-ids" value="" id="image-ids">
          <input type="hidden" name="nonce" value="<?php echo $nonce; ?>" id="nonce">
          <?php
            if(isset($_GET['post_type']) && $_GET['post_type'] != '') {
              if($_GET['post_type'] == 'inaktiv') {
                ?>
                <header><h1 id="inactiveArea"><?php echo __('Deactivade ad', 'bopoolen') ?></h1></header>
                <div class="form-segment-wrapper">
                  <div class="form-group">
                    <label class="col-md-3 control-label"><?php _e('Reason for deactivating your ad', 'bopoolen'); ?></label>
                    <div class="col-md-9">
                      <?php
                      echo '<input type="radio" class="radiobutton" name="_inactivereason" value="found_tenant"'.checkValue('found_tenant', $inactivereason).'>'.__('Hittat hyresgäst', 'bopoolen').'<br>';
                      echo '<input type="radio" class="radiobutton" name="_inactivereason" value="unavailable"'.checkValue('unavailable', $inactivereason).'>'.__('Vill inte längra hyra ut', 'bopoolen').'<br>';
                      echo '<input type="radio" class="radiobutton" name="_inactivereason" id="other" value="other"'.checkValue('other', $inactivereason).'>'.__('Annan anledning' ,'bopoolen').'<br>';
                      ?>
                    </div>
                  </div>
                  <hr class="other" style="display:none;">
                  <div class="form-group other" style="display:none;">
                    <label class="col-md-3 control-label" for="descriptionReasonInaktiv"><?php echo __('Comment of "Other reason" above', 'bopoolen') ?></label>
                    <div class="col-md-9">
                      <textarea id="descriptionReasonInaktiv" class="form-control" rows="5"><?php echo $inactivereasoncomment; ?></textarea>
                    </div>
                  </div>
                </div>
                <p class="error-msg"></p>
                <button id="submit" type="submit" formnovalidate class="btn btn-primary right update_rentad"><?php echo __('Deactivate ad', 'bopoolen') ?></button>
                <?php
              } else if ($_GET['post_type'] == 'raderad') {
                ?>
                <header>
                  <h1 id="eraseArea"><?php echo __('Delete ad', 'bopoolen') ?></h1>
                </header>
                <div class="form-segment-wrapper">
                  <div class="form-group">
                    <label class="col-md-3 control-label"><?php _e('Reason for deleting your ad', 'bopoolen'); ?></label>
                    <div class="col-md-9">
                      <?php
                        echo '<input type="radio" class="radiobutton" name="_deletereason" value="found_tenant"'.checkValue('found_tenant', $deleteReason).'>'.__('Hittat hyresgäst', 'bopoolen').'<br>';
                        echo '<input type="radio" class="radiobutton" name="_deletereason" value="unavailable"'.checkValue('unavailable', $deleteReason).'>'.__('Vill inte längra hyra ut', 'bopoolen').'<br>';
                        echo '<input type="radio" class="radiobutton" name="_deletereason" id="other" value="other"'.checkValue('other', $deleteReason).'>'.__('Annan anledning' ,'bopoolen').'<br>';
                      ?>
                    </div>
                  </div>
                  <hr class="other" style="display:none;">
                  <div class="form-group other" style="display:none;">
                    <label class="col-md-3 control-label" for="descriptionReasonErase"><?php echo __('Comment of "Other reason" above', 'bopoolen') ?></label>
                    <div class="col-md-9">
                      <textarea id="descriptionReasonEraseOther" class="form-control" rows="5"><?php echo $deleteReasonComment; ?></textarea>
                    </div>
                  </div>
                </div>
                <p class="error-msg"></p>
                <button id="submit" formnovalidate type="submit" class="btn btn-primary right update_rentad"><?php echo __('Delete ad', 'bopoolen') ?></button>
                <?php
              } else {
                ?>
                <p class="error-msg"></p>
                <button id="submit" type="submit"
                        class="btn btn-primary right update_rentad"><?php echo __('Update ad', 'bopoolen') ?></button>
                <?php
              }
            } else {
              ?>
              <p class="error-msg"></p>
              <button id="submit" type="submit" class="btn btn-primary right update_rentad"><?php echo __('Update ad', 'bopoolen') ?></button>
              <?php
            }

          ?>


        </form>

    </div>
    <?php
    }
  }
  ?>
</div>

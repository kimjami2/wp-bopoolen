<div class="container-fluid">
  <div class="row">
    <div class="col-md-16">
      <div class="alert alert-warning" role="alert"><?php echo __('The username or password is incorrect. Please try again!', 'bopoolen'); ?></div>
      <?php
      if ($_GET['mode'] == 'blocked') {
        $errormsg = __('Your account has been blocked. Please contact BoPoolen for more information.', 'bopoolen');
        echo '<div class="alert alert-danger blocked" role="alert">'.$errormsg.'</div>';
      }
      if($_GET['login'] === 'failed') {
        $loginError = __('The provided information does not match anything in our system. Please try again.', 'bopoolen');
        echo '<div class="alert alert-danger blocked" role="alert">'.$loginError.'</div>';
      }

      ?>
      <?php the_content(); do_shortcode('[profilepress-registration id="1"]');?>
      <h2><?php echo __('Login', 'bopoolen') ?></h2>
      <form name="loginform" id="loginform" action="/wp-login.php" method="post">
        <div class="form-group">
          <label class="control-label" for="InputEmail1"><?php echo __('Email', 'bopoolen') ?></label>
          <input class="form-control" name="log" id="user_login" placeholder="<?php echo __('Enter your email', 'bopoolen') ?>" type="email">
        </div>
        <div class="form-group">
          <label class="control-label" for="InputPassword"><?php echo __('Password', 'bopoolen') ?></label>
          <input class="form-control" name="pwd" id="user_pass" placeholder="<?php echo __('Enter your password', 'bopoolen') ?>" type="password">
        </div>

        <div class="row">
          <div class="col-md-6 col-sm-12">
            <a href="<?php echo wp_lostpassword_url( get_permalink() ); ?>" class="pull-left"><?php echo __('Forgotten your password?', 'bopoolen') ?></a><br />
            <?php _e('Don’t have an account? Create one', 'bopoolen'); ?> <a href="/skapa-konto/"><?php _e('here', 'bopoolen'); ?></a>
          </div>
          <div class="col-md-6 col-sm-12">
            <button class="btn btn-primary login pull-right" id="submit" type="submit"><?php echo __('Login', 'bopoolen') ?></button>
          </div>
        </div>

      </form>
    </div>

  </div>
</div>

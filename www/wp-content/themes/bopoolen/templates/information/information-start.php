<?php
if(apply_filters( 'wpml_current_language', NULL) == 'en') {
  //eng images
  $images = array(
    '1' => 'slider-en/CHECKLIST1.jpg',
    '2' => 'slider-en/LU ACC.jpg',
    '3' => 'slider-en/STUDENTLUND.jpg',
    '4' => 'slider-en/AF BOSTÄDER.jpg',
    '5' => 'slider-en/NATIONS.jpg',
    '6' => 'slider-en/BOPOOLEN.jpg',
    '7' => 'slider-en/ADS IN OTHER.jpg',
    '8' => 'slider-en/OTHER PROVIDERS.jpg',
    '9' => 'slider-en/TEMPORARY.jpg',
    '10' => 'slider-en/TIPS.jpg'
  );

  $text = array(
    '1' => '<h2>Checklist for housing</h2>
            <p>Do you feel perplexed when confronting the task of getting housing in Lund? This checklist can help you
            to get a clearer understanding of how to move forward.</p>
            <p><b>Go through the slides to see the full checklist.</b></p>',
    '2' => '<h2>Apply at LU Accommodation</h2>
            <p>If you are an international exchange or degree-seeking student, you can apply for housing through
            LU Accommodation, who manage a limited number of rooms and flats in Lund, Malmö and Helsingborg.</p>
            <p>For more information visit the website <a href="http://www.luaccommodation.lu.se/" target="_blank">here</a>.</p>',
    '3' =>  '<h2>Join Studentlund</h2>
            <p><a href="http://www.studentlund.se/" target="_blank">The membership</a> also allows you to join the AF Bostäder housing queue.</p>
            <p><a href="http://www.studentlund.se/" target="_blank">Click here to sign up</a></p>
            <p><b>Studentlund – all of Lund’s student life in one membership.</b></p>',
    '4' => '<h2>Register on AF Bostäder</h2>
            <p><b>Put yourself in the AF Bostäder housing queue.</b></p>
            <p>AF Bostäder is Lund’s largest student housing company with more than 6000 housing units.</p>
            <p>You can read more about the terms for joining the queue and how you get housing with AF Bostäder
            <a href="https://www.afbostader.se/en/stay-with-us/need-a-student-housing/searching-for-a-student-housing/" target="_blank">here</a>.</p>',
    '5' => '<h2>Contact the nations</h2>
            <p>The nations in Lund are student organisations that also have some student housing.</p>
            <p>The procedure to get housing differs between the different nations, so the best thing to do is to contact them and ask.</p>
            <p>You will find the contact information
            <a href="http://www.studentlund.se/nation/nation/" target="_blank">here</a>.</p>',
    '6' => '<h2>Use BoPoolen</h2>
            <p>Use BoPoolen to upload and respond to ads.</p>
            <p>Here on BoPoolen’s website you can <a href="/skapa-annons-sokes/" target="_blank">upload an ad</a> where you describe yourself and what type of housing you are looking for.</p>
            <p>You can also
            <a href="/uthyresbostader/" target="_blank">respond to ads</a> uploaded by landlords.</p>',
    '7' => '<h2>Make yourself heard in other channels.</h2>
            <p>Use <a href="https://www.blocket.se/" target="_blank">Blocket</a> and <a href="https://www.facebook.com/" target="_blank">Facebook</a> to reach a broader audience with your housing search. </p>
            <p>Write about yourself and what you are looking for. It can also be a good idea to put up ads on bulletin boards around town.</p>',
    '8' => '<h2>Other housing providers.</h2>
            <p><b>Get in touch with municipal housing companies and other housing companies.</b></p>
            <p>There are also some foundations in Lund that each have a small number of housing units.</p>
            <p>Not all of these options assign housing through a queue system. It is important to show that you are really interested and make sure to make a good impression.</p>
            <p>You can explore these possibilities <a href="/lankar/" target="_blank">here</a>.</p>',
    '9' => '<h2>Arrange for a temporary accommodation.</h2>
            <p>It can unfortunately sometimes be hard to find long-term accommodation in or near Lund, at least at a start. Don’t lose hope!</p>
            <p>Look for accommodation to cover the first weeks or months of your stay, and then look for more long-term accommodation on
            site. It can help if you are able to meet the landlords. For short-term accommodation, check
            <a href="https://www.airbnb.se/" target="_blank">AirBnb</a>, hostels or <a href="https://www.couchsurfing.com/" target="_blank">Couchsurfing</a>.</p>',
    '10' => '<h2>Do you want tips about what to think of when you look for housing?</h2>
            <p><a href="#" target="_blank">Click here!</a></p>
            <p>Do you want to print the cheklist? <a href="#" target="_blank">Click here</a></p>
            <p>Please don’t hestitate to <a href="/kontakta-oss/" target="_blank">contact us</a> for further questions.</p>'
  );

} else {
  //sv images
  $images = array(
    '1' => 'slider-sv/CHECKLISTA.jpg',
    '2' => 'slider-sv/STUDENTLUND.jpg',
    '3' => 'slider-sv/AF BOSTÄDER.jpg',
    '4' => 'slider-sv/NATIONER.jpg',
    '5' => 'slider-sv/BOPOOLEN.jpg',
    '6' => 'slider-sv/STIFTELSER.jpg',
    '7' => 'slider-sv/ANDRA KANALER.jpg',
    '8' => 'slider-sv/ÖVRIGA.jpg',
    '9' => 'slider-sv/TILLFÄLLIGT.jpg',
    '10' => 'slider-sv/TIPS.jpg'
  );

  $text = array(
    '1' => '<h2>CHECKLISTA FÖR BOSTADSSÖKANDE</h2>
            <p>Känner du dig handfallen inför uppgiften att skaffa bostad i Lund?
            Denna checklista kan hjälpa dig få en tydligare bild av hur du ska gå till väga!
            </p>
            <p><b>Klicka dig igenom bildspelet för att se hela listan.</b></p>',
    '2' => '<h2>Gå med i Studentlund</h2>
            <p>Medlemskapet ger dig tillträde till AF Bostäders bostadskö. <a href="http://www.studentlund.se/" target="_blank">Klicka här för att komma till Studentlunds hemsida och bli medlem.</a></p>
            <p>Studentlund är hela Lunds studentliv samlat i ett medlemskap och utgörs av nationerna, Akademiska Föreningen och studentkårerna.</p>',
    '3' =>  '<h2>Ställ dig i kö hos AF Bostäder</h2>
            <p>AF Bostäder är Lunds största studentbostadsföretag med över 6000 studentbostäder.
            <a href="https://www.afbostader.se/att-bo-hos-oss/soka-studentbostad/soker-du-studentbostad/" target="_blank">Klicka här</a> för att läsa mer om vad som krävs för att få ställa dig i kö, och hur det går till att skaffa en bostad.</p>',
    '4' => '<h2>Sök bostad hos nationerna</h2>
            <p>Nationerna i Lund är studentorganisationer som även har studentbostadshus knutna till sig.</p>
            <p>Tillvägagångssättet för att skaffa bostad på nation varierar mellan nationerna, så det enklaste är att höra av sig och fråga.</p>
            <p>Du hittar kontaktuppgifterna
            <a href="http://www.studentlund.se/nation/nation/" target="_blank">här</a>.</p>',
    '5' => '<h2>Använd BoPoolen</h2>
            <p>Här på BoPoolens hemsida kan du <a href="/skapa-annons-sokes/" target="_blank">lägga upp en annons</a> där du beskriver dig själv och vad för bostad du söker.</p>
            <p>Du kan även
            <a href="/uthyresbostader/" target="_blank">besvara hyresvärdars annonser</a>.</p>
            <p>BoPoolen är en andrahandsförmedling för studenter som drivs av Lunds universitets studentkårer (LUS).</p>',
    '6' => '<h2>Kontakta stiftelser</h2>
            <p>I Lund finns ett antal stiftelser som inrättats för att på olika sätt hjälpa och främja studenter.</p>
            <p>Stiftelserna har ofta ett mindre antal bostäder och egna regler för tilldelning av bostäder.</p>
            <p>Därför är det bäst att kontakta dem och fråga. <a href="/lankar/" target="_blank">Här</a> hittar du en lista över stiftelser i Lund.</p>',
    '7' => '<h2>Annonsera i andra kanaler</h2>
            <p>Använd dig av <a href="https://www.blocket.se/" target="_blank">Blocket</a> och <a href="https://www.facebook.com/" target="_blank">Facebook</a> för att nå ut bredare med ditt bostadssökande.</p>
            <p>Skriv om dig själv och vad du letar efter. Om du vill kan du även sätta upp personliga annonser på anslagstavlor på relevanta ställen.</p>',
    '8' => '<h2>Sök hos övriga aktörer</h2>
            <p>Det finns många bostadsaktörer i Lund som riktar sig till alla invånare.</p>
            <p><a href="http://www.lkf.se/" target="_blank">LKF</a> har generellt mycket lång väntetid till sina bostäder, men det skadar inte att höra med dem.
            Övriga bostadsaktörer har ibland lediga bostäder. Du hittar listan över dessa aktörer <a href="/lankar/" target="_blank">här</a>.</p>',
    '9' => '<h2>Skaffa ett tillfälligt boende.</h2>
            <p>Tyvärr kan det ibland till en början vara svårt att hitta en långsiktig bostad i eller nära Lund. Ge inte upp hoppet!</p>
            <p>Se om du kan hitta boende för de första veckorna eller månaderna av din vistelse, och sök sedan bostad på plats.
            Det kan underlätta ditt bostadssökande att ha möjlighet att träffa hyresvärdarna, eftersom de då kan få en bild av vem du är.</p>',
    '10' => '<h2>Vill du ha tips på saker att tänka på som bostadssökande?</h2>
            <p>Klicka <a href="#" target="_blank">här!</a></p>
            <p><b>Vill du ha checklistan i utskriftsformat?</b> <a href="#" target="_blank">Klicka här!</a></p>
            <p>Tveka inte att <a href="/kontakta-oss/" target="_blank">höra av dig till oss</a> om du har frågor angående ditt bostadssökande!</p>'
  );
}
?>
<div class="image-slider-wrapper container-fluid">
  <h1 class="title"><?php _e('How does it work?', 'bopoolen'); ?></h1>
  <div class="col-lg-12 information-list content">
    <?php the_content(); ?>
  </div>
  <div class="container-slider">
    <div class="image-slider container">
      <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
        <!-- Indicators | de små knapparna och hur många sidor det finns -->
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active">1</li>
          <li data-target="#myCarousel" data-slide-to="1">2</li>
          <li data-target="#myCarousel" data-slide-to="2">3</li>
          <li data-target="#myCarousel" data-slide-to="3">4</li>
          <li data-target="#myCarousel" data-slide-to="4">5</li>
          <li data-target="#myCarousel" data-slide-to="5">6</li>
          <li data-target="#myCarousel" data-slide-to="6">7</li>
          <li data-target="#myCarousel" data-slide-to="7">8</li>
          <li data-target="#myCarousel" data-slide-to="8">9</li>
          <li data-target="#myCarousel" data-slide-to="9">10</li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <div class="row">
              <div class="col-md-7">
                <img class="img-responsive" src="/wp-content/themes/bopoolen/assets/images/<?php echo $images['1']; ?>" alt="Chania">
              </div>
              <div class="col-md-5 text">
                <p>
                  <?php echo $text['1'];?>
                </p>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="row">
              <div class="col-md-7">
                <img class="img-responsive" src="/wp-content/themes/bopoolen/assets/images/<?php echo $images['2']; ?>" alt="Chania">
              </div>
              <div class="col-md-5 text">
                <p>
                  <?php echo $text['2'];?>
                </p>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="row">
              <div class="col-md-7">
                <img class="img-responsive" src="/wp-content/themes/bopoolen/assets/images/<?php echo $images['3']; ?>" alt="Chania">
              </div>
              <div class="col-md-5 text">
                <p>
                  <?php echo $text['3'];?>
                </p>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="row">
              <div class="col-md-7">
                <img class="img-responsive" src="/wp-content/themes/bopoolen/assets/images/<?php echo $images['4']; ?>" alt="Chania">
              </div>
              <div class="col-md-5 text">
                <p>
                  <?php echo $text['4'];?>
                </p>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="row">
              <div class="col-md-7">
                <img class="img-responsive" src="/wp-content/themes/bopoolen/assets/images/<?php echo $images['5']; ?>" alt="Chania">
              </div>
              <div class="col-md-5 text">
                <p>
                  <?php echo $text['5'];?>
                </p>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="row">
              <div class="col-md-7">
                <img class="img-responsive" src="/wp-content/themes/bopoolen/assets/images/<?php echo $images['6']; ?>" alt="Chania">
              </div>
              <div class="col-md-5 text">
                <p>
                  <?php echo $text['6'];?>
                </p>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="row">
              <div class="col-md-7">
                <img class="img-responsive" src="/wp-content/themes/bopoolen/assets/images/<?php echo $images['7']; ?>" alt="Chania">
              </div>
              <div class="col-md-5 text">
                <p>
                  <?php echo $text['7'];?>
                </p>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="row">
              <div class="col-md-7">
                <img class="img-responsive" src="/wp-content/themes/bopoolen/assets/images/<?php echo $images['8']; ?>" alt="Chania">
              </div>
              <div class="col-md-5 text">
                <p>
                  <?php echo $text['8'];?>
                </p>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="row">
              <div class="col-md-7">
                <img class="img-responsive" src="/wp-content/themes/bopoolen/assets/images/<?php echo $images['9']; ?>" alt="Chania">
              </div>
              <div class="col-md-5 text">
                <p>
                  <?php echo $text['9'];?>
                </p>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="row">
              <div class="col-md-7">
                <img class="img-responsive" src="/wp-content/themes/bopoolen/assets/images/<?php echo $images['10']; ?>" alt="Chania">
              </div>
              <div class="col-md-5 text">
                <p>
                  <?php echo $text['10'];?>
                </p>
              </div>
            </div>
          </div>
        </div>

          <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <div style="background-image: url('http://bopoolen.dev/wp-content/uploads/2016/04/bild_hem-1.jpg');"></div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid partners" style="background-image: url('/wp-content/uploads/2016/02/University-building-e1461003600659.jpg'); height: auto;">
  <div class="wrapper" style="display: table; width:100%;">
    <div id="mobilePad" style="display:none;">&nbsp;</div>
    <div class="col-md-3 col-md-push-3">
      <h1><?php _e('Operated by Lund University Student Unions', 'bopoolen'); ?></h1>
    </div>
    <div id="mobilePad-min" class="col-md-6"style="display:none;">&nbsp;</div>
    <div class="col-md-3 col-md-push-4" id="partnerText">
      <h1><?php _e('Partners', 'bopoolen'); ?></h1>
    </div>
    <div class="clear"></div>
    <div class="row">
      <div id="mobilePad" style="display:none;">&nbsp;</div>
      <div class="col-md-3 col-md-push-3 img">
        <img src="/wp-content/uploads/2015/12/lus_logo.png" />
      </div>
      <div class="col-md-3 col-md-push-3 img" id="image2nd">
        <img src="/wp-content/uploads/2015/12/LundLogo.png" />
      </div>
      <div class="col-md-3 col-md-push-3 img" id="image3rd">
        <img src="/wp-content/uploads/2015/12/LundUniversity-new.png" />
      </div>
    </div>
  </div>
</div>

<!--

<div class="panel-grid page-section fancy centered" id="pg-49-3"><div class="panel-row-style-partners-wrapper partners-wrapper panel-row-style"><div class="panel-grid-cell" id="pgc-49-3-0"><div class="so-panel widget widget_sow-editor panel-first-child" id="panel-49-3-0-0"><div style="color: #ffffff;" class="panel-widget-style"><div class="so-widget-sow-editor so-widget-sow-editor-base">
<div class="siteorigin-widget-tinymce textwidget">
	<h1><span style="color: #ffffff;">Samarbetspartners</span></h1>
</div>
</div></div></div><div class="so-panel widget widget_siteorigin-panels-builder panel-last-child" id="panel-49-3-0-1"><div id="pl-w56cc83af7ed17"><div class="panel-grid" id="pg-w56cc83af7ed17-0"><div class="panel-grid-cell" id="pgc-w56cc83af7ed17-0-0">&nbsp;</div><div class="panel-grid-cell" id="pgc-w56cc83af7ed17-0-1"><div class="so-panel widget widget_sow-image panel-first-child panel-last-child" id="panel-w56cc83af7ed17-0-1-0"><div class="so-widget-sow-image so-widget-sow-image-sow-image-1c389ca87c1a">

<div class="sow-image-container">
	<img src="http://new.bopoolen.nu/wp-content/uploads/2015/12/LundUniversity-new.png" width="704" height="887" srcset="http://new.bopoolen.nu/wp-content/uploads/2015/12/LundUniversity-new-238x300.png 238w, http://new.bopoolen.nu/wp-content/uploads/2015/12/LundUniversity-new.png 704w" class="so-widget-image">
</div>

</div></div></div><div class="panel-grid-cell" id="pgc-w56cc83af7ed17-0-2"><div class="so-panel widget widget_sow-image panel-first-child panel-last-child" id="panel-w56cc83af7ed17-0-2-0"><div class="so-widget-sow-image so-widget-sow-image-sow-image-1c389ca87c1a">

<div class="sow-image-container">
	<img src="http://new.bopoolen.nu/wp-content/uploads/2015/12/LundLogo.png" width="252" height="516" srcset="" class="so-widget-image">
</div>

</div></div></div><div class="panel-grid-cell" id="pgc-w56cc83af7ed17-0-3">&nbsp;</div></div></div></div></div></div></div>
-->


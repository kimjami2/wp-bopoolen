<?php
  $security_number = get_user_meta(get_current_user_id(), '_securitynumber')[0];
?>
<?php the_content(); ?>
<div class="container-fluid adverthandling">

<?php
  if(!is_user_logged_in()) {
?>
  <div class="content">
    <div class="row">
      <div class="col-sm-12">
        <header>
          <h1><?php echo __('Not signed in', 'bopoolen') ?></h1>
        </header>
        <div class="form-segment-wrapper">
          <?php get_template_part('templates/content', 'login'); ?>
        </div>
      </div>
    </div>
  </div>
<?php
  } else if(empty($security_number)) {
?>
  <div class="content">
    <div class="row">
      <div class="col-sm-12">
        <header>
          <h1><?php echo __('No social number', 'bopoolen') ?></h1>
        </header>
        <div class="form-segment-wrapper">
          <p>
            <?php _e('As you have not stated a Swedish personal identity number when you created your user account, you
cannot create a HOUSING ad. Before we can publish your ad, we need to verify that you have a connection to the address, so please contact us at BoPoolen for assistance.', 'bopoolen'); ?>
          </p>
        </div>
      </div>
    </div>
  </div>
<?php
  } else {

    $user_id = get_current_user_id();
    $user = get_userdata($user_id);
    $user_meta = get_user_meta($user_id);

    $user_name = $user_meta['first_name'][0] . ' ' . $user_meta['last_name'][0];
    $user_viewName = $user->display_name;
    $user_phone = $user_meta['_phone'][0];
    $user_email = $user->data->user_email;

    $nonce = wp_create_nonce( 'modified_by_user_' . $user_id );

    $view_name = isset($user_meta['_viewname'][0]) && $user_meta['_viewname'][0] == 'true' ? 'checked' : '';
    $view_phone = isset($user_meta['_viewphone'][0]) && $user_meta['_viewphone'][0] == 'true' ? 'checked' : '';
    $view_address = isset($user_meta['_viewaddress'][0]) && $user_meta['_viewaddress'][0] == 'checked' ? 'checked' : '';

?>
    <input type="hidden" id="langCode" value="<?php echo apply_filters( 'wpml_current_language', NULL); ?>">

  <div class="content">
      <form name="createpost" id="createpost" method="post" enctype="multipart/form-data" class="form-horizontal clearfix"  data-toggle="validator">
        <div class="row">
          <div class="col-sm-12">
            <header>
              <h1><?php echo __('Living', 'bopoolen') ?></h1>
            </header>
            <div class="form-segment-wrapper">
              <div class="form-group">
                <label class="control-label col-md-3" for="title" data-toggle="tooltip" data-placement="right" title="<?php _e('Shown on the Swedish version of the website.', 'bopoolen'); ?>"><?php echo __('Swedish title ', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                <div class="col-md-9">
                  <input class="form-control" name="title" id="title" placeholder="<?php echo __('Write your Swedish title', 'bopoolen') ?>" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3" for="title" data-toggle="tooltip" data-placement="right" title="<?php _e('Shown on the English version of the website. Many of BoPoolen’s users are international students.', 'bopoolen'); ?>"><?php echo __('English title', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                <div class="col-md-9">
                  <input class="form-control" name="title-eng" id="title-eng" placeholder="<?php echo __('Write your English title', 'bopoolen') ?>" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 less-line-height-ipad"><?php echo __('Available from (YYYY-MM-DD)', 'bopoolen') ?></label>
                <div class="col-md-9">
                  <div class="row multi-inputs">
                    <div class="col-sm-12">
                      <label for="timespan"><?php echo __('Leasing period', 'bopoolen') ?></label>
                      <select class="form-control" id="timespan" required>
                        <option value="" selected disabled="disabled"><?php echo __('- Choose leasing period', 'bopoolen') ?></option>
                        <option value="1"><?php echo __('Until further notice', 'bopoolen') ?></option>
                        <option value="2"><?php echo __('Fixed term', 'bopoolen') ?></option>
                      </select>
                    </div>
                    <div class="col-sm-12 accomedation-date infinity">
                      <div class="input-group input-date" id="availabilityDateFrom">
                        <span class="input-group-addon"><?php echo __('From', 'bopoolen') ?></span>
                        <input type="text" class="form-control" name="availablefromInfinity" placeholder="<?php echo __('yyyy-mm-dd', 'bopoolen') ?>" value="" id="availablefromInfinity" required>
                      </div>
                    </div>
                    <div class="col-sm-12 accomedation-date time">
                      <div class="input-group input-date input-daterange" id="availabilityDateFromTo">
                        <span class="input-group-addon"><?php echo __('From', 'bopoolen') ?></span>
                        <input type="text" class="form-control" placeholder="<?php echo __('yyyy-mm-dd', 'bopoolen') ?>" value="" name="availablefrom" id="availablefrom" required>
                        <span class="input-group-addon"><?php echo __('To', 'bopoolen') ?></span>
                        <input type="text" class="form-control" placeholder="<?php echo __('yyyy-mm-dd', 'bopoolen') ?>" value="" name="availableto" id="availableto" required>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label" for="title" data-toggle="tooltip" data-placement="right" title="<?php _e('If you are not registered at the address, we need to see a documented connection to the address before we can approve your ad and we may request that you send a copy of your purchase agreement or rental agreement to bopoolen@lus.lu.se', 'bopoolen'); ?>"><?php echo __('Address', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                <div class="col-md-9">
                  <div class="row multi-inputs">
                    <div class="col-md-12">
                      <label class="control-label" for="street"><?php echo __('Street address:', 'bopoolen') ?></label>
                      <input class="form-control" name="street" id="street" placeholder="<?php echo __('Street address', 'bopoolen') ?>" type="text" required>
                    </div>
                    <div class="col-md-4">
                      <label class="control-label" for="zip"><?php echo __('Post code:', 'bopoolen') ?></label>
                      <input class="form-control" name="zip" id="zip" placeholder="<?php echo __('Post code', 'bopoolen') ?>" type="text" required>
                    </div>
                    <div class="col-md-8">
                      <label class="control-label" for="city"><?php echo __('Town / city:', 'bopoolen') ?></label>
                      <input class="form-control" name="city" id="city" placeholder="<?php echo __('Town / city', 'bopoolen') ?>" type="text" required>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="checkbox">
                  <label class="col-md-3 control-label less-line-height addressfield" for="viewaddress" style="margin-right:35px;" data-toggle="tooltip" data-placement="right" title="<?php _e('Mark with a cross if you do not want to show the address in the ad. ', 'bopoolen'); ?>"><?php echo __('Do not show the address in the ad', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                    <input type="checkbox" id="viewaddress" style="top:10px;" <?php echo $view_address; ?>>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label" for="area"><?php echo __('Location', 'bopoolen'); ?></label>
                <div class="col-md-9">
                  <select class="form-control dropdown" id="area" required>
                    <option value="" selected disabled="disabled"><?php echo __('- Select location.', 'bopoolen') ?></option>
                    <option value="lund"><?php echo __('Lund', 'bopoolen') ?></option>
                    <option value="malmö"><?php echo __('Malmö', 'bopoolen') ?></option>
                    <option value="helsingborg"><?php echo __('Helsingborg', 'bopoolen') ?></option>
                    <option value="outside-lund"><?php echo __('Lund nearby', 'bopoolen') ?></option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3" for="accommodationType" data-toggle="tooltip" data-placement="right" title="<?php _e('What kind of housing are you offering?', 'bopoolen'); ?>"><?php echo __('Type of housing', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label required>
                <div class="col-md-9">
                  <select class="form-control" id="accommodationType">
                    <option value="" selected disabled="disabled"><?php echo __('- Select type of housing.', 'bopoolen') ?></option>
                    <option value="roominhouse"><?php echo __('Room in a house', 'bopoolen') ?></option>
                    <option value="roominapartment"><?php echo __('Room in an apartment', 'bopoolen') ?></option>
                    <option value="roominstudent"><?php echo __('Room in student housing', 'bopoolen') ?></option>
                    <option value="ownhouse"><?php echo __('House', 'bopoolen') ?></option>
                    <option value="ownapartment"><?php echo __('Apartment', 'bopoolen') ?></option>
                  </select>
                </div>
              </div>
              <div class="accomedation-type own">
                <div class="form-group">
                  <label class="control-label col-md-3" for="numberofrooms" data-toggle="tooltip" data-placement="right" title="<?php _e('State total number of rooms', 'bopoolen'); ?>"><?php echo __('Number of rooms', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                  <div class="col-md-9">
                    <select class="form-control" id="numberofrooms" required>
                      <option value="" disabled="disabled"><?php echo __('- Choose number of rooms', 'bopoolen') ?></option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="9+">9+</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3" for="availableyta" data-toggle="tooltip" data-placement="right" title="<?php _e('State total number of m2', 'bopoolen'); ?>"><?php echo __('Number of m<sup>2</sup>', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                  <div class="col-md-9">
                    <div class="input-group">
                      <input type="text" class="form-control" name="availableyta" id="availableyta" placeholder="<?php echo __('Area in m2', 'bopoolen') ?>" value="" required pattern="^[0-9]*$" data-error="<?php echo __('Numeric value only', 'bopoolen') ?>">
                      <span class="input-group-addon"><?php echo __('sq.m.', 'bopoolen') ?></span>
                    </div>
                    <span class="help-block with-errors"></span>
                  </div>
                </div>
              </div>
              <div class="accomedation-type roomin">
                <div class="form-group">
                  <label class="control-label col-md-3" for="availableyta" data-toggle="tooltip" data-placement="right" title="<?php _e('The number of m2 that comprise the tenant’s private space.', 'bopoolen'); ?>"><?php echo __('Own space', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                  <div class="col-md-9">
                    <div class="input-group">
                      <input type="text" class="form-control" name="availableytaOwn" id="availableytaOwn" placeholder="<?php echo __('Area in m2', 'bopoolen') ?>" value="" required pattern="^[0-9]*$" data-error="<?php echo __('Numeric value only', 'bopoolen') ?>">
                      <span class="input-group-addon"><?php echo __('sq.m.', 'bopoolen') ?></span>
                    </div>
                    <span class="help-block with-errors"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3" for="availablegemensam" data-toggle="tooltip" data-placement="right" title="<?php _e('The total space the tenant has access to.', 'bopoolen'); ?>"><?php echo __('Total space', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                  <div class="col-md-9">
                    <div class="input-group">
                      <input type="text" class="form-control" name="availablegemensam" id="availablegemensam" placeholder="<?php echo __('Area in m2', 'bopoolen') ?>" value="" pattern="^[0-9]*$" data-error="<?php echo __('Numeric value only', 'bopoolen') ?>">
                      <span class="input-group-addon"><?php echo __('sq.m.', 'bopoolen') ?></span>
                    </div>
                    <span class="help-block with-errors"></span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label" for="contractType" data-toggle="tooltip" data-placement="right" title="<?php _e('What kind of lease are you offering?', 'bopoolen'); ?>"><?php echo __('Contract', 'bopoolen'); ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                <div class="col-md-9">
                  <select class="form-control" id="contractType">
                    <option value="" selected disabled="disabled"><?php echo __('- Select type of contract', 'bopoolen') ?></option>
                    <option value="contractsecond"><?php echo __('Sublease agreement ', 'bopoolen') ?></option>
                    <option value="contractfirst"><?php echo __('Tenancy agreement', 'bopoolen') ?></option>
                    <option value="contractchange"><?php echo __('Exchange', 'bopoolen') ?></option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 less-line-height" for="description" data-toggle="tooltip" data-placement="right" title="<?php _e('A short description of the housing. This text is shown on the Swedish site.', 'bopoolen'); ?>"><?php echo __('Swedish description of the housing', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                <div class="col-md-9">
                  <textarea id="description" class="form-control" rows="5"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 less-line-height" for="description" data-toggle="tooltip" data-placement="right" title="<?php _e('A short description of the housing. This text is shown on the English site. Many of BoPoolen’s users are international students.' ,'bopoolen'); ?>"><?php echo __('English description of the housing', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
                <div class="col-md-9">
                  <textarea id="description-eng" class="form-control" rows="5"></textarea>
                </div>
              </div>
              <!-- <div class="form-group">
                <label class="control-label col-md-3" for="description"><?php echo __('Upload images', 'bopoolen') ?></label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input id="fileupload" type="file" name="fileToUpload" multiple="multiple" accept="image/jpg, image/jpeg">
                    <span class="help-block with-errors"></span>
                  </div>
                </div>
              </div> -->
            </div>
          </div>
        </div>

        <header>
          <h1><?php echo __('Terms', 'bopoolen') ?></h1>
        </header>
        <div class="form-segment-wrapper">
          <div class="form-group">
            <label class="control-label col-md-3" for="rent" data-toggle="tooltip" data-placement="right" title="<?php _e('Monthly rent in SEK', 'bopoolen'); ?>"><?php echo __('Rent', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
            <div class="col-md-9">
              <div class="input-group">
                <input type="text" class="form-control" name="rent" id="rent" placeholder="<?php echo __('Month rent in Kr', 'bopoolen') ?>" value="" pattern="^[0-9]*$" data-error="<?php echo __('Numeric value only', 'bopoolen') ?>" required>
                <span class="input-group-addon">Kr</span>
              </div>
              <span class="help-block with-errors"></span>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-9 col-md-offset-3">
              <div class="info-text">
                <p>
                  <?php echo __('Reminder: Please note that your rent must be reasonable. According to the law, the rent for a sublet may not be higher than the rent you pay or the costs you have for the housing. If you rent out part of the housing, the rent shall be proportional. If you charge an excessive rent there is a risk that you will be liable to make repayments. Read more about setting rents under Laws & Regulations <a href="/info-om-uthyrning/for-dig-som-hyr-ut/" target="_blank">here</a>.', 'bopoolen') ?>
                </p>
                <p>
                  <?php echo __('Student finance is currently SEK 9,901. The National Board of Housing, Building and Planning’s reasonable rent recommendation for a student is SEK 2,476.', 'bopoolen') ?>
                </p>
                <div class="checkbox-wrapper">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" id="reasonableRent" data-error="<?php echo __('Please attest that according to you this is a reasonable rent', 'bopoolen') ?>" required>
                      <?php echo __('I hereby certify that the set rent is reasonable', 'bopoolen') ?><br>
                      <span><?php _e('You can read more about the rent you are entitled to charge <a href="http://www.hyresnamnden.se/Hyra-i-andra-hand/Hyran/" target="_blank">here</a>.', 'bopoolen'); ?></span>
                    </label>
                    <div class="help-block with-errors"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <header style="padding: 20px 30px 20px 0;">
            <h1><?php _e('Included in the rent', 'bopoolen');?></h1>
          </header>
          <div class="form-group">
            <div class="col-md-9">
              <div class="checkbox-wrapper">
                <div class="col-md-offset-0">
                  <div class="checkbox">
                    <label for="includedel">
                      <input type="checkbox" id="includedel" value="checked"><?php echo __('Electricity', 'bopoolen') ?>
                    </label>
                  </div>
                  <div class="checkbox">
                    <label for="includedinternet">
                      <input type="checkbox" id="includedinternet" value="checked"><?php echo __('Internet', 'bopoolen') ?>
                    </label>
                  </div>
                  <div class="checkbox">
                    <label for="includedfurniture">
                      <input type="checkbox" id="includedfurniture" value="checked"><?php echo __('Fully furnished', 'bopoolen') ?>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <header>
          <h1><?php _e('About the housing', 'bopoolen'); ?></h1>
        </header>
        <div class="form-segment-wrapper">
          <div class="checkbox-wrapper">
            <div class="checkbox">
              <label for="includedtoilet">
                <input type="checkbox" id="includedtoilet" value="checked"><?php echo __('Own toilet', 'bopoolen') ?>
              </label>
            </div>
            <div class="checkbox">
              <label for="includedbath">
                <input type="checkbox" id="includedbath" value="checked"><?php echo __('Own bathroom', 'bopoolen') ?>
              </label>
            </div>
            <div class="checkbox">
              <label for="includedpentry">
                <input type="checkbox" id="includedpentry" value="checked"><?php echo __('Own kitchenette', 'bopoolen') ?>
              </label>
            </div>
            <div class="checkbox">
              <label for="includedkitchen">
                <input type="checkbox" id="includedkitchen" value="checked"><?php echo __('Own kitchen', 'bopoolen') ?>
              </label>
            </div>
            <div class="checkbox">
              <label for="includedentrance">
                <input type="checkbox" id="includedentrance" value="checked"><?php echo __('Own entrance', 'bopoolen') ?>
              </label>
            </div>
            <div class="checkbox">
              <label for="includedhandicap">
                <input type="checkbox" id="includedhandicap" value="checked"><?php echo __('Disabled access', 'bopoolen') ?>
              </label>
            </div>
            <div class="checkbox">
              <label for="includeddishwasher">
                <input type="checkbox" id="includeddishwasher" value="checked"><?php echo __('Dishwasher', 'bopoolen') ?>
              </label>
            </div>
            <div class="checkbox">
              <label for="includedwasher">
                <input type="checkbox" id="includedwasher" value="checked"><?php echo __('Washing machine', 'bopoolen') ?>
              </label>
            </div>
            <div class="checkbox">
              <label for="includedanimal">
                <input type="checkbox" id="includedanimal" value="checked"><?php echo __('Pets allowed', 'bopoolen') ?>
              </label>
            </div>
            <div class="checkbox">
              <label for="includedsmoking">
                <input type="checkbox" id="includedsmoking" value="checked"><?php echo __('Smoking not allowed', 'bopoolen') ?>
              </label>
            </div>
          </div>
        </div>

        <header>
          <h1><?php echo __('Contact details', 'bopoolen') ?></h1>
        </header>
        <div class="form-segment-wrapper">
          <div>
            <label class="topmargin" data-toggle="tooltip" data-placement="right" title="<?php _e('Dessa kontaktuppgifter kommer att synas i din publicerade annons', 'bopoolen'); ?>"><?php _e('This information can be later changed in your profile section', 'bopoolen'); ?> <i class="fa fa-info-circle" aria-hidden="true"></i></label>
          </div>
          <p id="displayName">
          <label class="info"><?php echo _e('Name', 'bopoolen') ?></label>
          <span ><?php echo ': '.$user_name; ?></span>
          </p>
          <p id="displayViewName">
          <label class="info"><?php echo _e('View name', 'bopoolen') ?></label>
          <span ><?php echo ': '.$user_viewName; ?></span>
          </p>
          <p id="displayPhone">
          <label class="info"><?php echo _e('Phone number', 'bopoolen') ?></label>
          <span ><?php echo ': '.$user_phone; ?></span>
          </p>
          <p id="displayEmail">
          <label class="info"><?php echo _e('Email', 'bopoolen') ?></label>
          <span ><?php echo ': '.$user_email; ?></span>
          </p>
          <div class="checkbox-wrapper">
            <div class="form-group nomargin cbo">
              <label class="control-label" data-toggle="tooltip" data-placement="right" title="<?php _e('If you choose not to display your name in the ad, your user name will then be displayed instead', 'bopoolen') ?>"><?php echo __('Do not display name in ads', 'bopoolen') ?>
                &nbsp;&nbsp;&nbsp;<input type="checkbox" name="_viewname" id="viewname" class="regular-text" <?php if($user_meta['_viewname'][0] == 'false') {echo 'checked';} ?>><i class="fa fa-info-circle" aria-hidden="true"></i></label>
            </div>
            <div class="form-group nomargin cbo">
              <!-- Tooltip  -->
              <label class="control-label" for="viewphone" data-toggle="tooltip" data-placement="right" title="<?php _e('Select if other users are to be able to contact you by telephone.', 'bopoolen')?>"><?php echo __('Do not show telephone number in the ad', 'bopoolen') ?>
                &nbsp;&nbsp;&nbsp;<input type="checkbox" name="_viewphone" id="viewphone" class="regular-text" <?php if($user_meta['_viewphone'][0] == 'false') {echo 'checked';} ?>><i class="fa fa-info-circle" aria-hidden="true"></i></label>
              <!--&nbsp;&nbsp;&nbsp;<input type="checkbox" name="_viewphone" id="viewphone" value="checked" class="regular-text" checked=""> -->
            </div>
          </div>
        </div>

        <header>
          <h1><?php echo __('Upload images', 'bopoolen') ?></h1>
        </header>
        <div class="form-segment-wrapper">
          <p><?php _e('Du kan som max ladda upp 5 bilder per annons.', 'bopoolen'); ?></p>
          <div id="dZUpload" class="dropzone multiple">

            <div class="dz-default dz-message">
              <div class="img-container">
                <img src="/wp-content/themes/bopoolen/assets/images/upload-image.jpg" alt="" />
              </div>
              <?php echo __('Click here to upload images', 'bopoolen') ?> </div>
          </div>
        </div>

        <input type="hidden" name="image-ids" value="" id="image-ids">
        <input type="hidden" name="nonce" value="<?php echo $nonce; ?>" id="nonce">

        <p class="error-msg"></p>
        <button  id="submit" type="submit" class="btn btn-primary right create_rentad"><?php echo __('Create ad', 'bopoolen') ?></button>
      </form>

  </div>
  <?php
  }
  ?>
</div>

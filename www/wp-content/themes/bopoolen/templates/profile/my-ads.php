<?php
  /*
  * Get user ads
  */

  $user_id = get_current_user_id();

  /* Accepted posts */
  $acceptedPostsArray = array(
    'posts_per_page'	=> -1,
    'post_type'			  => array('rentad', 'searchad'),
    'author'          => $user_id,
    'post_status'     => 'publish'
  );

  $acceptedPosts = get_posts($acceptedPostsArray);

  /* Waiting posts */
  $waitingPostsArray = array(
    'posts_per_page'	=> -1,
    'post_type'			  => array('rentad', 'searchad'),
    'author'          => $user_id,
    'post_status'     => array( 'pending', 'modified' )
  );

  $waitingPosts = get_posts($waitingPostsArray);

  /* Rejected posts */
  $rejectedPostsArray = array(
    'posts_per_page'	=> -1,
    'post_type'			  => array('rentad', 'searchad'),
    'author'          => $user_id,
    'post_status'     => 'rejected'
  );

  $rejectedPosts = get_posts($rejectedPostsArray);

  /* Auto Unpublished posts */
  $unpublishedPostsArray = array(
    'posts_per_page'  => -1,
    'post_type'       => array('rentad', 'searchad'),
    'author'          => $user_id,
    'post_status'     => 'auto_unpublished'
  );

  $unpublishedPosts = get_posts($unpublishedPostsArray);
?>

<div class="my-ads card white no-hover">
  <div class="row accepted">
    <h3><?php _e('Accepted Ads', 'bopoolen') ?></h2>
    <div class="clear"></div>
<?php
    if(count($posts) == 0) {
?>
    <p class="no-results"><?php _e('You haven’t created any ads.', 'bopoolen') ?></p>
<?php

    } else {
      foreach($acceptedPosts as $post) {

      $postType = $post->post_type == 'searchad' ? __('Search ad', 'bopoolen') : __('Rent ad', 'bopoolen');

      // grab the custom meta
      $custom_meta = get_post_custom($post->ID);
  ?>
      <div class="col-md-4 col-sm-12">
        <a class="ad" href="<?php echo $post->guid; ?>">
          <h4><?php echo $post->post_title; ?> - <?php echo $postType; ?></h4>
          <p class="desc"><?php echo substr($post->post_content,0, 100); ?>...</p>

          <div class="date">
            <div class="row">
              <div class="col col-sm-6">
                &nbsp;&nbsp;<?php echo $custom_meta['_availablefrom'][0]; ?>
              </div>
              <div class="col col-sm-6">
                <?php echo $custom_meta['_availableto'][0]; ?>&nbsp;&nbsp;
              </div>
            </div>
          </div>
        </a>
      </div>

  <?php
      }
    }
?>

  </div>
  <div class="row waiting">
    <h3><?php _e('Waiting approval Ads', 'bopoolen') ?></h2>
      <div class="clear"></div>
  <?php
      if(count($posts) == 0) {
  ?>
      <p class="no-results"><?php _e('You have no ads waiting for approval.', 'bopoolen') ?></p>
  <?php

      } else {
        foreach($waitingPosts as $post) {

        $postType = $post->post_type == 'searchad' ? __('Search ad', 'bopoolen') : __('Rent ad', 'bopoolen');

        // grab the custom meta
        $custom_meta = get_post_custom($post->ID);
    ?>
        <div class="col-md-4 col-sm-12">
          <a class="ad" href="<?php echo $post->guid; ?>">
            <h4><?php echo $post->post_title; ?>  - <?php echo $postType; ?></h4>
            <p class="desc"><?php echo substr($post->post_content,0, 100); ?>...</p>

            <div class="date">
              <div class="row">
                <div class="col col-sm-6">
                  &nbsp;&nbsp;<?php echo $custom_meta['_availablefrom'][0]; ?>
                </div>
                <div class="col col-sm-6">
                  <?php echo $custom_meta['_availableto'][0]; ?>&nbsp;&nbsp;
                </div>
              </div>
            </div>
          </a>
        </div>

    <?php
        }
      }
  ?>
  </div>
  <div class="row rejected">
    <h3><?php _e('Rejected Ads', 'bopoolen') ?></h2>
      <div class="clear"></div>
  <?php
      if(count($posts) == 0) {
  ?>
      <p class="no-results"><?php _e('You have no rejected ads.', 'bopoolen') ?></p>
  <?php

      } else {
        foreach($rejectedPosts as $post) {

        $postType = $post->post_type == 'searchad' ? __('Search ad', 'bopoolen') : __('Rent ad', 'bopoolen');

        // grab the custom meta
        $custom_meta = get_post_custom($post->ID);
    ?>
        <div class="col-md-4 col-sm-12">
          <a class="ad" href="<?php echo $post->guid; ?>">
            <h4><?php echo $post->post_title; ?> - <?php echo $postType; ?></h4>
            <p class="desc"><?php echo substr($post->post_content,0, 100); ?>...</p>

            <div class="date">
              <div class="row">
                <div class="col col-sm-6">
                  &nbsp;&nbsp;<?php echo $custom_meta['_availablefrom'][0]; ?>
                </div>
                <div class="col col-sm-6">
                  <?php echo $custom_meta['_availableto'][0]; ?>&nbsp;&nbsp;
                </div>
              </div>
            </div>
          </a>
        </div>

    <?php
        }
      }
  ?>
  </div>

    <div class="row unpublished">
    <h3><?php _e('Unpublished Ads', 'bopoolen') ?></h2>
      <div class="clear"></div>
  <?php
      if(count($posts) == 0) {
  ?>
      <p class="no-results"><?php _e('You have no unpublished ads.', 'bopoolen') ?></p>
  <?php

      } else {
        foreach($unpublishedPosts as $post) {

        $postType = $post->post_type == 'searchad' ? __('Search ad', 'bopoolen') : __('Rent ad', 'bopoolen');

        // grab the custom meta
        $custom_meta = get_post_custom($post->ID);
    ?>
        <div class="col-md-4 col-sm-12">
          <a class="ad" href="<?php echo $post->guid; ?>">
            <h4><?php echo $post->post_title; ?> - <?php echo $postType; ?></h4>
            <p class="desc"><?php echo substr($post->post_content,0, 100); ?>...</p>

            <div class="date">
              <div class="row">
                <div class="col col-sm-6">
                  &nbsp;&nbsp;<?php echo $custom_meta['_availablefrom'][0]; ?>
                </div>
                <div class="col col-sm-6">
                  <?php echo $custom_meta['_availableto'][0]; ?>&nbsp;&nbsp;
                </div>
              </div>
            </div>
          </a>
        </div>

    <?php
        }
      }
  ?>
  </div>

  <div class="clear"></div>
</div>

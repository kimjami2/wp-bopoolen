<h2><?php _e('Edit profile'); ?></h2>

<?php

  $user_id = get_current_user_id();
  // update user meta
  if(isset($_POST['edit-profile'])) {
    // check if checkbox is checked or not
    if (isset($_POST['_viewname'])) {
      update_user_meta( $user_id,'_viewname', 'false' );
    } else {
      update_user_meta( $user_id,'_viewname', 'true' );
    }
    // check if checkbox is checked or not
    if (isset($_POST['_viewphone'])) {
      update_user_meta( $user_id,'_viewphone', 'false' );
    } else {
      update_user_meta( $user_id,'_viewphone', 'true' );
    }
    // check if checkbox is checked or not
    if (isset($_POST['_viewmail'])) {
      update_user_meta( $user_id,'_viewmail', sanitize_text_field( $_POST['_viewmail'] ) );
    } else {
      update_user_meta( $user_id,'_viewmail', sanitize_text_field( '' ) );
    }
    if($_POST['bio_tinymec'] !== __('Skriv gärna en kort presentation av dig själv. Denna kommer vara synlig för andra användare så de får en inblick av vem du är som person.', 'bopoolen')) {
      update_user_meta( $user_id,'description', $_POST['bio_tinymec'] );
    }
    update_user_meta( $user_id,'first_name', $_POST['user_first_name'] );
    update_user_meta( $user_id,'last_name', $_POST['user_last_name'] );
    update_user_meta( $user_id,'_securitynumber', $_POST['user_securitynumber'] );
    update_user_meta( $user_id,'_phone', $_POST['user_phone'] );
    
    if(is_numeric($_POST['imageid'])){
      $currentImageId = get_user_meta($user_id, '_profilepicture', true);
      //http://zblogged.com/wp-content/uploads/2015/11/5.png
      if( is_numeric($currentImageId) && $currentImageId != $_POST['imageid']){
        wp_delete_attachment( $currentImageId , true);
      }
      update_user_meta( $user_id, '_profilepicture', $_POST['imageid']);
    }
    if(isset($_POST['imageid']) && $_POST['imageid'] == 'null') {
      $currentImageId = get_user_meta($user_id, '_profilepicture', true);
      wp_delete_attachment( $currentImageId , true);
    }
  }

  $user = get_userdata($user_id);
  $user_meta = get_user_meta($user_id);
  $bio = get_the_author_meta('description');
  $user_tmp = get_user_by_meta_data("_securitynumber", '19950826-4175');

?>

<?php if(isset($user_meta['_profileimage'][0]) && is_numeric(intval($user_meta['_profilepicture'][0])) ){
  //echo '<div class="form-group"><img src="'.wp_get_attachment_url( intval($user_meta['_profilepicture'][0]) ).'" class="" alt="" width="20%" width="50"></div>';
}?>
<?php if(isset($_POST['edit-profile'])) {
  echo '<div class="alert alert-success create_success" style="display:block;" role="alert">'.__('Your account has been successfully updated.', 'bopoolen').'</div>';
}?>

<form id="create_user" method="post" action="">
  <input type="hidden" name="imageid" id="image-ids" value="<?php if(isset($user_meta['_profilepicture'][0]) && intval($user_meta['_profilepicture'][0]) !== '') {echo intval($user_meta['_profilepicture'][0]); } else {} ?> ">
  <input type="hidden" id="langCode" value="<?php echo apply_filters( 'wpml_current_language', NULL); ?>">
  <div class="form-group">
    <label class="control-label" for="InputEmail1"><?php echo __('Email', 'bopoolen') ?></label>
    <input class="form-control" name="user_email" placeholder="<?php echo __('Enter your email') ?>" type="email" value="<?php echo esc_attr($user->data->user_email); ?>" disabled>
  </div>
  <div class="form-group">
    <label class="control-label" for="InputName" data-toggle="tooltip" data-placement="right" title="<?php _e('Other users can only see this if you choose to show it in your ad.', 'bopoolen'); ?>"><?php echo __('First name', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
    <input class="form-control" name="user_first_name" placeholder="<?php echo __('Enter your first name') ?>" type="text" value="<?php echo esc_attr($user_meta['first_name'][0]); ?>">
  </div>
  <div class="form-group">
    <label class="control-label" for="InputLastName" data-toggle="tooltip" data-placement="right" title="<?php _e('Other users can only see this if you choose to show it in your ad.', 'bopoolen'); ?>"><?php echo __('Last name', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
    <input class="form-control" name="user_last_name" placeholder="<?php echo __('Enter your last name') ?>" type="text" value="<?php echo esc_attr($user_meta['last_name'][0]); ?>">
  </div>
  <div class="alert alert-danger securitynumtaken" role="alert"><?php echo __('The selected personal identity number is already in use!', 'bopoolen'); ?></div>
  <div class="form-group">
    <label class="control-label" for="InputSecNumber"><?php echo __('Personal identity number (yyyymmdd-nnnn)', 'bopoolen') ?></label>
    <input class="form-control" name="user_securitynumber" id="user_securitynumber" placeholder="19810101-0000" type="text" value="<?php echo esc_attr($user_meta['_securitynumber'][0]); ?>">
    <p style="margin-top: 10px; font-family: Lato,sans-serif;">
      <label class="control-label" data-toggle="tooltip" data-placement="right" title="<?php _e('If you do not have a Swedish personal identity number your only qualified to search tenants.', 'bopoolen'); ?>" for="no-social-number"><?php _e('I do not have a Swedish personal identity number.', 'bopoolen'); ?>&nbsp;&nbsp;<input type="checkbox" id="no-social-number" class="regular-text" <?php if(esc_attr($user_meta['_securitynumber'][0]) == '') { echo 'checked';} ?>>&nbsp;<i class="fa fa-info-circle" aria-hidden="true"></i></label>
    </p>
  </div>
  <div class="form-group">
    <label class="control-label" for="InputPhoneNr" data-toggle="tooltip" data-placement="right" title="<?php _e('Other users can only see the telephone number if you choose to show it in your ad.', 'bopoolen'); ?>"><?php echo __('Telephone number', 'bopoolen') ?><i class="fa fa-info-circle" aria-hidden="true"></i></label>
    <input class="form-control" name="user_phone" placeholder="0700-00 00 00" type="text" value="<?php echo esc_attr($user_meta['_phone'][0]); ?>">
  </div>
  <div class="form-group">
    <label class="control-label" data-toggle="tooltip" data-placement="right" title="<?php _e('If you choose not to display your name in the ad, your user name will then be displayed instead', 'bopoolen') ?>"><?php echo __('Do not display name in ads', 'bopoolen') ?>
    &nbsp;&nbsp;&nbsp;<input type="checkbox" name="_viewname" id="viewname" value="checked" class="regular-text" <?php if($user_meta['_viewname'][0] == 'false') {echo 'checked';} ?>>
  </div>
  <div class="form-group">
    <label class="control-label" for="viewphone" data-toggle="tooltip" data-placement="right" title="<?php _e('Select if other users are to be able to contact you by telephone.', 'bopoolen')?>"><?php echo __('Do not show telephone number in the ad', 'bopoolen') ?>
    &nbsp;&nbsp;&nbsp;<input type="checkbox" name="_viewphone" id="viewphone" value="checked" class="regular-text" <?php if($user_meta['_viewphone'][0] == 'false') {echo 'checked';} ?>>
  </div>
  <div class="form-group">
    <label class="control-label" for="InputBio" data-toggle="tooltip" data-placement="right" title="<?php _e('Skriv gärna en kort presentation av dig själv. Denna kommer vara synlig för andra användare.','bopoolen'); ?>"><?php echo __('Biography', 'bopoolen') ?></label>
    <?php
    $content = $user_meta['description'][0];
    if($content == '') {
      $content = __('Skriv gärna en kort presentation av dig själv. Denna kommer vara synlig för andra användare så de får en inblick av vem du är som person.', 'bopoolen');
    }
    $editor_id = 'bio_tinymec';
    $settings =   array(
      'wpautop' => true, // use wpautop?
      'media_buttons' => false, // show insert/upload button(s)
      'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
      'textarea_rows' => get_option('default_post_edit_rows', 10), // rows="..."
      'tabindex' => '',
      'editor_css' => '', //  extra styles for both visual and HTML editors buttons,
      'editor_class' => '', // add extra class(es) to the editor textarea
      'teeny' => false, // output the minimal editor config used in Press This
      'dfw' => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)
      'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
      'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
    );
    ?>
    <?php wp_editor( $content, $editor_id, $settings = array() ); ?>
  </div>


  <div class="form-segment-wrapper">
    <div id="dZUpload" class="dropzone noheader">
      <p><?php _e('Du kan som max ha 1 bild som profilbild.', 'bopoolen'); ?></p>
      <div class="dz-default dz-message">
        <div class="img-container">
          <img src="/wp-content/themes/bopoolen/assets/images/upload-image.jpg" alt="" />
        </div>
        <?php echo __('Click here to upload profile image', 'bopoolen') ?></div>
    </div>
  </div>



  <input type="hidden" name="edit-profile" value="">
  <button class="btn btn-primary create_user pull-right" type="submit" name="edit-profile" value="go"><?php echo __('Save', 'bopoolen') ?></button>
</form>

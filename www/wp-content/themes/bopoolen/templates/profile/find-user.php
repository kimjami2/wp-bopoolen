<?php the_content();?>

<p></p>
<div class="clearfix">
  <input type="text" id="search-user" />
  <button id="search-user-btn" class="btn btn-primary"><?php _e('Search', 'bopoolen'); ?></button>

  <div id="search-user-results">

  </div>
</div>

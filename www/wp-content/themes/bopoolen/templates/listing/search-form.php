<div class="form-horizontal clearfix listing-form">
  <?php echo the_content();

  if(isset($_GET) && count($_GET) != 0) {
    foreach(array_keys($_GET) as $key) {
      echo '<input type="hidden" id="'.$key.'" value="'.$_GET[$key].'">';
    }
  }
  ?>
  <input type="hidden" id="langCode" value="<?php echo apply_filters( 'wpml_current_language', NULL); ?>">
  <div class="advanced-filters">
    <div class="form-group">

      <div class="col-md-4">
        <p class="name"><?php _e('Location', 'bopoolen') ?></p>
        <select class="form-control dropdown multi-select" name="province" multiple="multiple">
          <option value="city-lund"><?php _e('Lund', 'bopoolen') ?></option>
          <option value="city-malmo"><?php _e('Malmö', 'bopoolen') ?></option>
          <option value="city-helsingborg"><?php _e('Helsingborg', 'bopoolen') ?></option>
          <option value="outside-lund"><?php echo __('Lund nearby', 'bopoolen') ?></option>
        </select>
      </div>

      <div class="col-md-4">
        <p class="name"><?php _e('Type of housing', 'bopoolen') ?></p>
        <select class="form-control dropdown multi-select" name="estate-type" multiple="multiple">
          <option value="roominhouse"><?php _e('Room in house', 'bopoolen') ?></option>
          <option value="roominapartment"><?php _e('Room in apartment', 'bopoolen') ?></option>
          <option value="roominstudent"><?php _e('Room in student housing', 'bopoolen') ?></option>
          <option value="ownhouse"><?php _e('House', 'bopoolen') ?></option>
          <option value="ownapartment"><?php _e('Apartment', 'bopoolen') ?></option>
        </select>
      </div>

      <div class="col-md-4">
        <p class="name"><?php _e('Type of contract', 'bopoolen') ?></p>
        <select class="form-control dropdown multi-select" name="deal-type" multiple="multiple">
          <option value="contractfirst"><?php _e('Lease contract', 'bopoolen') ?></option>
          <option value="contractsecond"><?php _e('Sublease agreements', 'bopoolen') ?></option>
          <option value="contractchange"><?php _e('Exchange', 'bopoolen') ?></option>
        </select>
      </div>

    </div>
  </div>

  <div class="default-filters">
    <div class="form-group">
      <div class="row">
        <div class="col-md-6 col-sm-10">
          <p class="name"><?php _e('Rent', 'bopoolen') ?></p>
          <div id="price-filter">
          </div>
          <div class="row values">
            <div class="col-md-6">
              <div id="value-span"></div>
            </div>
            <div class="col-md-6">
              <div id="value-input"></div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-8">
          <p class="name"><?php _e('Freetext', 'bopoolen') ?></p>
          <input class="form-control" id="search-text" name="search" placeholder="<?php _e('Freetext', 'bopoolen') ?> .." required="" type="text">
        </div>
        <div class="col-md-2">
          <div class="cta">
            <button id="search-listings" class="btn btn-primary">&nbsp;&nbsp;<?php _e('Search', 'bopoolen') ?>&nbsp;&nbsp;</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="loading"><img src="/wp-content/themes/bopoolen/assets/images/ajax-loader.gif"></div>
<div id="no-search-results"><?php _e('No search results', 'bopoolen'); ?></div>
<div class="listing-table-wrapper">
  <table id="listing-table" class="table" width="100%">
    <thead>
      <tr data-id="1">
        <th data-sort="string" width="30%"><p><?php _e('Beskrivning', 'bopoolen') ?>&nbsp;&nbsp;<i class="fa fa-sort" aria-hidden="true"></i></p></th>
        <th data-sort="string" width="8%"><p><?php _e('City', 'bopoolen') ?>&nbsp;&nbsp;<i class="fa fa-sort" aria-hidden="true"></i></p></th>
        <th data-sort="int" width="11%"><p><?php _e('Type of contract', 'bopoolen') ?>&nbsp;&nbsp;<i class="fa fa-sort" aria-hidden="true"></i></p></th>
        <th data-sort="int" width="12%"><p><?php _e('Size', 'bopoolen') ?>&nbsp;&nbsp;<i class="fa fa-sort" aria-hidden="true"></i></p></th>
        <th data-sort="string" width="12%"><p><?php _e('From', 'bopoolen') ?>&nbsp;&nbsp;<i class="fa fa-sort" aria-hidden="true"></i></p></th>
        <th data-sort="string" width="12%"><p><?php _e('To', 'bopoolen') ?>&nbsp;&nbsp;<i class="fa fa-sort" aria-hidden="true"></i></p></th>
        <th data-sort="int" width="10%"><p><?php _e('Price', 'bopoolen') ?>&nbsp;&nbsp;<i class="fa fa-sort" aria-hidden="true"></i></p></th>
        <th width="10%"><p>&nbsp;&nbsp;</p></th>
      </tr>
    </thead>
    <tbody>
      <!-- ajax loaded content goes here -->
    </tbody>
  </table>
</div>

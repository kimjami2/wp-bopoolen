<div class="landing">
  <div class="image-bg-fluid" style="background-image: url('<?php echo get_option('atomic_homepage_img'); ?>');">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-md-12">
          <div class="row clearfix">
            <div class="seperation ads col-md-6">
              <div class="row">
                <div class="text col-lg-6 col-lg-push-3">
                  <h1>Annonser</h2>
                  <p>Aenean lacinia bibendum nulla sed consectetur. Cras mattis consectetur purus sit amet fermentum.</p>
                  <a style="width:250px;" href="/annonser" class="btn btn-button-img btn-lg" >Annonser&nbsp;&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
            <div class="seperation information col-md-6">
              <div class="row">
                <div class="text col-lg-6 col-lg-push-3">
                  <h1>Information</h2>
                  <p>Aenean lacinia bibendum nulla sed consectetur. Cras mattis consectetur purus sit amet fermentum.</p>
                  <a style="width:250px;" href="/information" class="btn btn-button-img btn-lg" >Information&nbsp;&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php the_content(); ?>

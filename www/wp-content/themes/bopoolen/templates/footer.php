<?php
/**
 * FOOTER
 */

$footerSidebarClass = 'col-sm-12';
$footerSidebars = wp_get_sidebars_widgets();
$numFooterSidebar = 0;

// Footer 1
if ($footerSidebars['sidebar-footer-1']) :
  $numFooterSidebars++;
endif;

// Footer 2
if ($footerSidebars['sidebar-footer-2']) :
  $numFooterSidebars++;
endif;

// Footer 3
if ($footerSidebars['sidebar-footer-3']) :
  $numFooterSidebars++;
endif;

if ($numFooterSidebars === 1) {
  $footerSidebarClass = 'col-sm-12';
} else if ($numFooterSidebars === 2) {
  $footerSidebarClass = 'col-sm-6';
} else if ($numFooterSidebars === 3) {
  $footerSidebarClass = 'col-sm-4';
}

  $footer_style = "";

?>
<footer class="footer" role="contentinfo" <?php echo $footer_style; ?>>
  <div class="container">
    <div class="row">

      <?php if ($footerSidebars['sidebar-footer-1']) : ?>
      <div class="<?php echo $footerSidebarClass; ?>">
        <?php dynamic_sidebar('sidebar-footer-1'); ?>
        <div class="spacing visible-xs"></div>
      </div>
      <?php endif; ?>

      <?php if ($footerSidebars['sidebar-footer-2']) : ?>
      <div class="<?php echo $footerSidebarClass; ?>">
        <?php dynamic_sidebar('sidebar-footer-2'); ?>
        <div class="spacing visible-xs"></div>
      </div>
      <?php endif; ?>

      <?php if ($footerSidebars['sidebar-footer-3']) : ?>
      <div class="<?php echo $footerSidebarClass; ?>">
        <?php dynamic_sidebar('sidebar-footer-3'); ?>
      </div>
      <?php endif; ?>
    </div>
  </div>

  <?php if ($footerSidebars['sidebar-copyright']) : ?>
    <div class="container copyright">
      <div class="row">
        <div class="col-md-12">
          <?php dynamic_sidebar('sidebar-copyright'); ?>
        </div>
      </div>
    </div>
  <?php endif; ?>
</footer>

  <?php if(!is_front_page() && get_option('atomic_homepage_img_button1_link') && get_option('atomic_homepage_img_button2_link')){ ?>
  <div class="btn-toolbar-static <?php if(apply_filters( 'wpml_current_language', NULL) == 'en') {echo 'toolbar-static-en';} ?>">
    <div class="wrapper toggle" data-toggle=".btn-toolbar-static">
      <p>
        <?PHP if(apply_filters( 'wpml_current_language', NULL) == 'sv') {?>
          A<br />N<br />N<br />O<br />N<br />S<br />E<br />R<br /><i class="fa fa-chevron-left fa-2" aria-hidden="true"></i>
        <?PHP } else {?>
          A<br />D<br />S<br /><i class="fa fa-chevron-left fa-2" aria-hidden="true"></i>
        <?PHP } ?>

      </p>
    </div>
    <div class="wrapper">
      <?PHP if(apply_filters( 'wpml_current_language', NULL) == 'sv') {?>
        <a href="<?php echo get_option('atomic_homepage_img_button1_link'); ?>" class="btn btn-button-img btn-lg"><?php echo get_option('atomic_homepage_img_button1_text'); ?></a>
        <a href="<?php echo get_option('atomic_homepage_img_button2_link'); ?>" class="btn btn-button-img btn-lg"><?php echo get_option('atomic_homepage_img_button2_text'); ?></a>
      <?PHP } else {?>
        <a href="<?php echo get_option('atomic_homepage_img_button1_link'); ?>" class="btn btn-button-img btn-lg"><?php echo get_option('atomic_homepage_img_button1_text_en'); ?></a>
        <a href="<?php echo get_option('atomic_homepage_img_button2_link'); ?>" class="btn btn-button-img btn-lg"><?php echo get_option('atomic_homepage_img_button2_text_en'); ?></a>
      <?PHP } ?>
    </div>
  </div>
<?php } ?>

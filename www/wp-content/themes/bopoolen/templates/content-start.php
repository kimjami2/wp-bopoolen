<?php
  $campaign_logo = get_option('atomic_homepage_promo_image_link');
?>
<?php if(get_option('atomic_homepage_img')){ ?>
  <div class="image-bg-fluid" style="background-image: url('<?php echo get_option('atomic_homepage_img'); ?>');">
    <div class="container-fluid">
      <div class="row">
        <input type="hidden" id="langCode" value="<?php echo apply_filters( 'wpml_current_language', NULL); ?>">
        <div class="col-md-6 col-sm-12 left">
          <div class="quick-search">
            <div class="wrapper">
              <h1><?php _e('FIND STUDENT HOUSING', 'bopoolen') ?></h1>
              <div class="row">
                <div class="col-md-12">
                  <p class="name"><?php _e('Location', 'bopoolen') ?></p>
                  <select class="form-control dropdown multi-select" name="province" multiple="multiple">
                    <option value="city-lund" checked><?php _e('Lund', 'bopoolen') ?></option>
                    <option value="city-malmo" checked><?php _e('Malmö', 'bopoolen') ?></option>
                    <option value="city-helsingborg" checked><?php _e('Helsingborg', 'bopoolen') ?></option>
                    <option value="outside-lund"><?php echo __('Lund nearby', 'bopoolen') ?></option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <p class="name"><?php _e('Type of housing', 'bopoolen') ?></p>
                  <select class="form-control dropdown multi-select" name="estate-type" multiple="multiple">
                    <option value="roominhouse"><?php _e('Room in house', 'bopoolen') ?></option>
                    <option value="roominapartment"><?php _e('Room in apartment', 'bopoolen') ?></option>
                    <option value="roominstudent"><?php _e('Room in student housing', 'bopoolen') ?></option>
                    <option value="ownhouse"><?php echo __('House', 'bopoolen') ?></option>
                    <option value="ownapartment"><?php echo __('Apartment', 'bopoolen') ?></option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <p class="name"><?php _e('Contract', 'bopoolen') ?></p>
                  <select class="form-control dropdown multi-select" name="deal-type" multiple="multiple">
                    <option value="contractfirst"><?php _e('Lease contract', 'bopoolen') ?></option>
                    <option value="contractsecond"><?php _e('Sublease agreements', 'bopoolen') ?></option>
                    <option value="contractchange"><?php _e('Exchange', 'bopoolen') ?></option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 cta-wrapper">
                  <btn class="btn btn-primary" id="quick-search"><?php _e('Search', 'bopoolen') ?></btn>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-12 right">
          <div class="wrapper">
            <div>
              <?php if(get_option('atomic_homepage_promo') == 'true') { ?>
              <div class="promo">
                <a href="<?php echo get_option('atomic_homepage_promo_link'); ?>" class="bubble" <?php
                //Find image url and set as background
                if(get_option('atomic_homepage_promo_image') == 'true') {
                  echo 'style="background-image:url('."'".$campaign_logo."'".'); background-size: 260px"';
                }
                ?>>
                  <h1>
                    <?php if(get_option('atomic_homepage_promo_image') == 'false') {
                      echo __(get_option('atomic_homepage_promo_title'), 'bopoolen');
                    } ?>
                  </h1>
                  <p>
                    <?php if(get_option('atomic_homepage_promo_image') == 'false') {
                      echo __(get_option('atomic_homepage_promo_text'), 'bopoolen');
                    } ?>
                  </p>
                  <span><?php if(get_option('atomic_homepage_promo_image') == 'false') {
                      echo __('Läs mer', 'bopoolen').'&nbsp;&nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i></span>';
                    }
                    ?>
                </a>
              </div>
              <?php } ?>
              <div class="uthyrare">
                <h1><?php _e('Looking for a tenant?', 'bopoolen') ?></h1>
                <p class="total">
                  <b><?php
                  $count_posts = wp_count_posts('searchad');
                  $notenantAds = $count_posts->publish;
                  echo $notenantAds; ?></b> <?php _e('ads made by students that seek housing', 'bopoolen') ?>
                </p>
                <a href="/sokes-annonser/"><btn class="btn btn-primary" id="quick-search"><?php _e('Find tenant', 'bopoolen') ?></btn></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php } ?>

<?php the_content(); ?>

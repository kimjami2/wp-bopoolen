<?php
    $bopoolen_logo = "/wp-content/uploads/2016/02/purple-logo.png";
    //session_start();
    if(isset($_GET['action'])) {
      $_SESSION['lang'] = $_GET['action'];
    }

    if($_SESSION['lang']) {
      if($_SESSION['lang'] === 'sv') {
          do_action( 'wpml_switch_language', 'en');
      }else if($_SESSION['lang'] === 'en') {
          do_action( 'wpml_switch_language', 'sv');
      }
    }
 ?>
<div id="sticky-anchor"></div>
<div id="sticky">
 <?php if(get_option('atomic_header_top_info') == 'true'){ ?>
   <header class="banner top-link hidden-xs" style="display: <?php echo $show_top_bar ?>">
     <input type="hidden" id="langCode" value="<?php echo apply_filters( 'wpml_current_language', NULL); ?>">
     <div class="container">
       <div class="row">
         <div class="col-sm-5 col-md-6">
           <p><?php _e('BoPoolen.nu - Safe and secure student housing in Lund', 'bopoolen');?></p>
         </div>

         <div class="col-sm-7 col-md-6">

          
          <div class="links">
            <?php
            if ( is_user_logged_in() ) {
              echo '<a href="'.wp_logout_url().'" class="pull-right logout">'.__('Sign out', 'bopoolen').'</a>';
              } else {
              }
            ?>
             <a id="changeLang" class="pull-right" href="?action=<?php echo apply_filters( 'wpml_current_language', NULL); ?>"><?php if(apply_filters( 'wpml_current_language' ,  NULL) === 'sv') {
                 echo 'English';
               } else {
                 echo __('Svenska', 'bopoolen');
               } ?></a>
             <?php
             if ( is_user_logged_in() ) {
               echo '<a href="/profiles/?user='.get_current_user_id().'" class="pull-right login">'.__('My profile', 'bopoolen').'</a>';

             } else {
               echo '<a href="logga-in" class="pull-right login">'.__('Login', 'bopoolen').'</a>';
             }
              ?>
          </div>
          <div class="search-wrapper">
            <div class="pull-right">
              <?php get_search_form(true); ?>
            </div>
          </div>
         </div>
       </div>
     </div>
   </header>
<?php } ?>
<header class="banner">
  <!-- Navigation -->
  <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only"><?php echo __('Toggle navigation', 'bopoolen') ?></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <?php if(get_option('atomic_logo_use_image') == 'true'){ ?>
          <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>"><img src="<?php echo $bopoolen_logo ?>" alt="bopoolenlogo"  class="img-responsive"/></a>
        <?php } ?>
        <?php if(get_option('atomic_logo_use_image') == 'false'){ ?>
          <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>"><?php echo get_option('atomic_logo_text') ?></a>
        <?php } ?>
      </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav']);
        endif;
        ?>
      </div>
    </div>
  </nav>
</header>
</div>

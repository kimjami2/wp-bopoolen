<?php
/**
 * Template Name: Edit post Uthyres
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'rentad_edit'); ?>
<?php endwhile; ?>

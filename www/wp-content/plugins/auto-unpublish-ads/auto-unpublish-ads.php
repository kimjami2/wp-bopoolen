<?php
/*
    Plugin Name: Auto Unpublish Ads
    Author: Siam Zamir - KNOWIT AB
    Version: 0.1
    Description: Runs every 24th hour, and changes the status of the expired ads to auto_unpublished, also sends a notification to the add owner via mail.
    Usage: Go to Settings -> General to set the number of days elapsed
    before the ads are set to auto_unpublished. Also choose an email template to be used for sending the email notification.
*/

register_activation_hook( __FILE__, "start_auto_unpublish_ads_job");

function start_auto_unpublish_ads_job() {
    wp_schedule_event( time() + 1, "daily", "run_auto_unpublish_ads_job_hook");
}

register_deactivation_hook( __FILE__, "stop_auto_unpublish_ads_job" );

function stop_auto_unpublish_ads_job() {
    wp_clear_scheduled_hook( "run_auto_unpublish_ads_job_hook" );
}

add_action("run_auto_unpublish_ads_job_hook", "run_auto_unpublish_ads_job");

function auto_unpublish_ads_add_settings_link( $links ) {
    $settings_link = '<a href="options-general.php">' . __( 'Settings' ) . '</a>';
    array_push( $links, $settings_link );
    return $links;
}

$plugin = plugin_basename( __FILE__ );
add_filter( "plugin_action_links_$plugin", 'auto_unpublish_ads_add_settings_link' );

// Main function
function run_auto_unpublish_ads_job() {
    
    $email_template_id = get_option( 'auto_unpublish_ads_template' );
    $unpublish_after = intval(get_option( 'auto_unpublish_ads_after' ));
    if (!is_int($unpublish_after) || !$unpublish_after < 0) {
        $unpublish_after = 14;
    }

    $args = array(
        'posts_per_page'   => -1,
        'post_type'        => array( 'searchad', 'rentad' ),
        'post_status'      => 'publish'
    );

    $posts_array = wp_list_pluck(get_posts( $args ), 'post_date', 'ID');

    $current_date = strtotime('now');
    $unpublish_date = $current_date - (3600 * 24 * $unpublish_after);

    foreach ($posts_array as $post_id => $post_time) {
        if ($unpublish_date > strtotime($post_time)) {
            wp_update_post(array('ID' => $post_id, 'post_status' => 'auto_unpublished'));
            if (!empty($email_template_id)) {
                send_auto_unpublish_ads_email( $email_template_id, $post_id );   
            }
        }
    }
    
}

//Sends the emails
function send_auto_unpublish_ads_email( $template_post_id, $current_post_id ) {

    $author_id = get_post_field('post_author', $current_post_id);
    $receiver = get_the_author_meta('user_email', $author_id);
    $subject = get_post_meta($template_post_id, '_email_subject', true);

    $content = get_post_field('post_content', $template_post_id);

    $mail_send = wp_mail( $receiver, $subject, $content);

    if ( $mail_send ) {
        //The email is sent!
    }
    else {
        error_log( "Error Sending Email (Auto Unpublish Ads)\n", 3, "debug.log");
    }

}

//Registers the settings
function auto_unpublish_ads_settings_init() {

    //Settings Section
    add_settings_section(
        'auto_unpublish_ads_section',
        'Auto Unpublish Ads Settings',
        'auto_unpublish_ads_setting_section_callback_function',
        'general'
    );

    //Settings Entry for Email Template
    add_settings_field(
        'auto_unpublish_ads_after',
        'Unpublish after (days)',
        'auto_unpublish_ads_after_callback_function',
        'general',
        'auto_unpublish_ads_section'
    );

    //Settings Entry for After
    add_settings_field(
        'auto_unpublish_ads_template',
        'Email Template',
        'auto_unpublish_ads_template_callback_function',
        'general',
        'auto_unpublish_ads_section'
    );

    register_setting( 'general', 'auto_unpublish_ads_after' );
    register_setting( 'general', 'auto_unpublish_ads_template' );
    
}

add_action( 'admin_init', 'auto_unpublish_ads_settings_init' );

//Settings Section Callback
function auto_unpublish_ads_setting_section_callback_function() {
    echo '<p>' . __('Here you can choose which email template should be used for notification while automatically inactivating ads.', 'bopoolen') . '</p>';
 }

//Settings Entry "Template" Callback
function auto_unpublish_ads_template_callback_function() {
    $email_template_id = get_option( 'auto_unpublish_ads_template' );

    $post_ids = get_posts(array(
        'post_type'   => 'email_template',
        'post_status' => 'publish',
        'fields'      => 'ids',
    ));

    if (!empty ($post_ids)) {
        echo '<select name="auto_unpublish_ads_template" id="auto_unpublish_ads_template">';
        echo '<option value="">' . __('Select an email template', 'bopoolen') . '</option>';

        foreach($post_ids as $post_id) {
            $title = get_the_title($post_id);
            if ($email_template_id == $post_id) {
                echo "<option value='$post_id' selected>" . $title . "</option>";
            } else {
                echo "<option value='$post_id' >" . $title . "</option>";
            }
        }

        echo "</select>";
    } else {
        echo '<input name="auto_unpublish_ads_template" id="auto_unpublish_ads_template" type="text" value="" class="regular-text" disabled="disabled" placeholder="' . __('No published email templates found!', 'bopoolen') . '"></input>';
    }
}

//Settings Entry "After" Callback
function auto_unpublish_ads_after_callback_function() {
    $unpublish_after = get_option( 'auto_unpublish_ads_after' );
    echo '<input type="text" name="auto_unpublish_ads_after" id="auto_unpublish_ads_after" value="' . $unpublish_after . '" />';
}

//Registers new post status "Auto Unpublished"
function auto_unpublished_post_status(){
    register_post_status( 'auto_unpublished', array(
        'label'                     => _x( 'Auto Unpublished', 'post' ),
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Auto Unpublished <span class="count">(%s)</span>', 'Auto Unpublished <span class="count">(%s)</span>' ),
    ) );
}

add_action( 'init', 'auto_unpublished_post_status' );

function auto_unpublished_append_post_status_list(){
     global $post;
     $complete = '';
     $label = '';
     if($post->post_type == 'rentad' || $post->post_type == 'searchad'){
          if($post->post_status == 'auto_unpublished'){
               $complete = ' selected=\"selected\"';
               $label = '<span id=\"post-status-display\"> Auto Unpublished</span>';
          }
          echo '
          <script>
          jQuery(document).ready(function($){
               $("select#post_status").append("<option value=\"auto_unpublished\" '.$complete.'>Auto Unpublished</option>");
               $(".misc-pub-section label").append("'.$label.'");
          });
          </script>
          ';
     }
}

add_action('admin_footer-post.php', 'auto_unpublished_append_post_status_list');
































?>
<?php
/*
Plugin Name: Atomic Builder - Galaxy Street
Description: Atomic Builder lets you customize your experience
Author: Atomic Pixel
Version: 0.1
*/

/*---------------------------------------------------
register settings
----------------------------------------------------*/
function atomic_builder_init(){
register_setting( 'theme_settings', 'theme_settings' );
}

/*---------------------------------------------------
add settings page to menu
----------------------------------------------------*/
function add_settings_page() {
add_menu_page( __( 'Atomic Builder' ), __( 'Atomic Builder' ), 'manage_options', 'atomic-builder', 'theme_settings_page');
}

/*---------------------------------------------------
add actions
----------------------------------------------------*/
add_action( 'admin_init', 'atomic_builder_init' );
add_action( 'admin_menu', 'add_settings_page' );

/*---------------------------------------------------
Includes
----------------------------------------------------*/
add_action('admin_head', 'atomic_builder_head_files');

function atomic_builder_head_files() {
  if(isset($_GET['page']) && $_GET['page'] == 'atomic-builder') {
    echo '<link rel="stylesheet" href="'.plugins_url().'/atomic-builder/css/materialdesignicons.min.css" type="text/css" media="all" />';
    echo '<link rel="stylesheet" href="'.plugins_url().'/atomic-builder/css/bootstrap.min.css" type="text/css" media="all" />';
    echo '<link rel="stylesheet" href="'.plugins_url().'/atomic-builder/css/colorpicker.css" type="text/css" media="all" />';
    echo '<link rel="stylesheet" href="'.plugins_url().'/atomic-builder/css/atomic-builder.css" type="text/css" media="all" />';

    echo '<script type="text/javascript" src="'.plugins_url().'/atomic-builder/js/bootstrap.min.js"></script>';
    echo '<script type="text/javascript" src="'.plugins_url().'/atomic-builder/js/jquery-2.1.4.min.js"></script>';
    echo '<script type="text/javascript" src="'.plugins_url().'/atomic-builder/js/colorpicker.js"></script>';
    echo '<script type="text/javascript" src="'.plugins_url().'/atomic-builder/js/atomic-builder.js"></script>';
  }
}

/*---------------------------------------------------
Media upload
----------------------------------------------------*/
wp_enqueue_style('thickbox'); // call to media files in wp
wp_enqueue_script('thickbox');
wp_enqueue_script('media-upload');

/*---------------------------------------------------
Theme Panel Output
----------------------------------------------------*/
function theme_settings_page() {

  // include settings
  include 'atomic-settings.php';
  ?>

  <script type="text/javascript">
  var atomicSettings = <?php echo json_encode($settings); ?>;
  </script>

	<div id="atomic-builder">
    <div id="navbar clearfix">

      <ul class="nav nav-tabs">
        <li class="ns"><div class="logo" style="background-image: url('<?PHP echo plugins_url(); ?>/atomic-builder/images/atomic_pixel_logo.png')"></div></li>
        <li class="ns active"><a href="#general" data-toggle="tab"><i class="mdi mdi-home"></i>&nbsp;&nbsp;General</a></li>
        <li class="ns"><a href="#header" data-toggle="tab"><i class="mdi mdi-border-top"></i>&nbsp;&nbsp;Header</span></a></li>
        <li class="ns"><a href="#footer" data-toggle="tab"><i class="mdi mdi-border-bottom"></i>&nbsp;&nbsp;Footer</span></a></li>
        <li class="ns"><a href="#typography" data-toggle="tab"><i class="mdi mdi-alphabetical"></i>&nbsp;&nbsp;Typography</a></li>
      </ul>

      <div id="" class="tab-content clearfix">
        <div class="tab-pane active" id="general">
          <?php include 'pages/general.php'; ?>
        </div>
        <div class="tab-pane" id="header">
          <?php include 'pages/header.php'; ?>
        </div>
        <div class="tab-pane" id="footer">
          <?php include 'pages/footer.php'; ?>
        </div>
        <div class="tab-pane" id="typography">
          <?php include 'pages/typography.php'; ?>
        </div>
      </div>

    </div>
	</div>
	<?php
}



/*---------------------------------------------------
Save atomic builder
----------------------------------------------------*/
add_action( 'wp_ajax_save_atomic_builder', 'save_atomic_builder' );

function save_atomic_builder() {

  // include settings
  include 'atomic-settings.php';

  // grab our fresh data
  $atomicSettings = isset($_POST['atomic-settings']) ? $_POST['atomic-settings'] : false ;

  // iteration threw all our settings
  foreach($atomicSettings as $key => $setting) {
      update_option($setting['id'], $setting['value'], true);
  }

}

/*---------------------------------------------------
Misc.
----------------------------------------------------*/
function atomic_checkbox($option) {
  $value = get_option($option);

  if($value == 'true') {
    echo 'checked';
  }
}
?>

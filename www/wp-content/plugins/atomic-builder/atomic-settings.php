<?PHP

// the theme name
$themeName = 'Theme Atomic Nova';

/*---------------------------------------------------
Theme colors
----------------------------------------------------*/

/*
$settings['menu'] = array(
array('id' => 'atomic_menu_background_color', 'placeholder' => '#83bdd4', 'value' => get_option('atomic_menu_background_color'), 'title' => 'Menu background', 'desc' => 'Color will be at opacity 80%'),
array('id' => 'atomic_menu_text_color', 'placeholder' => '#c45555', 'value' => get_option('atomic_menu_text_color'), 'title' => 'Menu text color', 'desc' => 'Color will be at opacity 80%')
);

$settings['header'] = array(
array('id' => 'atomic_social_facebook', 'group' => 'socialmedia', 'placeholder' => '', 'value' => get_option('atomic_social_facebook'), 'title' => 'Facebook URL', 'desc' => 'An URL to your Facebook site'),
array('id' => 'atomic_social_twitter', 'group' => 'socialmedia', 'placeholder' => '', 'value' => get_option('atomic_social_twitter'), 'title' => 'Twitter URL', 'desc' => 'An URL to your Twitter site'),
array('id' => 'atomic_social_tumblr', 'group' => 'socialmedia', 'placeholder' => '', 'value' => get_option('atomic_social_tumblr'), 'title' => 'Tumblr URL', 'desc' => 'An URL to your Tumblr site'),
array('id' => 'atomic_social_googleplus', 'group' => 'socialmedia', 'placeholder' => '', 'value' => get_option('atomic_social_googleplus'), 'title' => 'Google Plus URL', 'desc' => 'An URL to your Google Plus site'),
array('id' => 'atomic_social_vimeo', 'group' => 'socialmedia', 'placeholder' => '', 'value' => get_option('atomic_social_vimeo'), 'title' => 'Vimeo URL', 'desc' => 'An URL to your Vimeo site'),
array('id' => 'atomic_social_youtube', 'group' => 'socialmedia', 'placeholder' => '', 'value' => get_option('atomic_social_youtube'), 'title' => 'Youtube URL', 'desc' => 'An URL to your Youtube site'),
array('id' => 'atomic_social_pinterest', 'group' => 'socialmedia', 'placeholder' => '', 'value' => get_option('atomic_social_pinterest'), 'title' => 'Pinterest URL', 'desc' => 'An URL to your Pinterest site')
);
*/

/* google fonts */
$googleFonts = array(
   'Alegreya Sans',
   'Arimo',
   'Arvo',
   'Bitter',
   'Cabin',
   'Dosis',
   'Droid Sans',
   'Droid Serif',
   'EB Garamond',
   'Hind',
   'Indie Flower',
   'Lato',
   'Merriweather',
   'Montserrat',
   'Noto Sans',
   'Nunito',
   'Open sans',
   'Oswald',
   'Oxygen',
   'Pacifico',
   'Poiret One',
   'PT Sans',
   'Raleway',
   'Roboto',
   'Titillium Web',
   'Ubuntu'
);

$settings = array(
   /* general */
   'atomic_page_title' => array('id' => 'atomic_page_title', 'placeholder' => 'title/name', 'value' => get_option('atomic_page_title', ''), 'title' => 'The website title/name', 'desc' => 'This will mostly be used as the website name and logo'),

   /* header */
   'atomic_logo_size' => array('id' => 'atomic_logo_size', 'placeholder' => '28', 'value' => get_option('atomic_logo_size', ''), 'title' => 'The logo size', 'desc' => 'The logo size is based on pixels, eg 18px'),
   'atomic_logo_text' => array('id' => 'atomic_logo_text', 'placeholder' => 'Galaxy Street', 'value' => get_option('atomic_logo_text', ''), 'title' => 'Logo text', 'desc' => 'The logo as text'),
   'atomic_logo_use_image' => array('id' => 'atomic_logo_use_image', 'placeholder' => '', 'value' => get_option('atomic_logo_use_image', ''), 'title' => 'Use an image as logo', 'desc' => 'Use an image as logo instead of text'),
   'atomic_logo_image_logo' => array('id' => 'atomic_logo_image_logo', 'placeholder' => '', 'value' => get_option('atomic_logo_image_logo', ''), 'title' => 'Image logo', 'desc' => ''),
   'atomic_header_top_color' => array('id' => 'atomic_header_top_color', 'placeholder' => '#99a5b3', 'value' => get_option('atomic_header_top_color', ''), 'title' => 'Top bar color', 'desc' => 'Top color in hex'),
   'atomic_header_top_color_link' => array('id' => 'atomic_header_top_color_link', 'placeholder' => '#fff', 'value' => get_option('atomic_header_top_color_link', ''), 'title' => 'Top bar link color', 'desc' => 'Link color in hex'),
   'atomic_header_top_color_link_hover' => array('id' => 'atomic_header_top_color_link_hover', 'placeholder' => '#fff', 'value' => get_option('atomic_header_top_color_link_hover', ''), 'title' => 'Top bar link hover color', 'desc' => 'Link hover color in hex'),

   'atomic_homepage_img' => array('id' => 'atomic_homepage_img', 'placeholder' => 'http://bopoolen.dev/wp-content/uploads/2016/01/slide1-1.png', 'value' => get_option('atomic_homepage_img', ''), 'title' => 'Startpage image', 'desc' => 'background images in header (if no image, header will not be displayed)'),
   'atomic_homepage_img_title' => array('id' => 'atomic_homepage_img_title', 'placeholder' => 'Bopoolen', 'value' => get_option('atomic_homepage_img_title', ''), 'title' => 'Startpage image title', 'desc' => 'display title on image'),
   'atomic_homepage_img_subtitle' => array('id' => 'atomic_homepage_img_subtitle', 'placeholder' => 'Trygga boende för lunds studenter', 'value' => get_option('atomic_homepage_img_subtitle', ''), 'title' => 'Startpage image subtitle', 'desc' => 'display subtitle on image'),
   'atomic_homepage_img_button1_link' => array('id' => 'atomic_homepage_img_button1_link', 'placeholder' => '#', 'value' => get_option('atomic_homepage_img_button1_link', ''), 'title' => 'Startpage image button 1', 'desc' => 'button link'),
   'atomic_homepage_img_button2_link' => array('id' => 'atomic_homepage_img_button2_link', 'placeholder' => '#', 'value' => get_option('atomic_homepage_img_button2_link', ''), 'title' => 'Startpage image button 2', 'desc' => 'button link'),
   'atomic_homepage_img_button1_text' => array('id' => 'atomic_homepage_img_button1_text', 'placeholder' => 'Hitta bostad', 'value' => get_option('atomic_homepage_img_button1_text', ''), 'title' => 'Startpage image button 1', 'desc' => 'button text'),
   'atomic_homepage_img_button2_text' => array('id' => 'atomic_homepage_img_button2_text', 'placeholder' => 'Hyra bostad', 'value' => get_option('atomic_homepage_img_button2_text', ''), 'title' => 'Startpage image button 2', 'desc' => 'button text'),
    'atomic_homepage_img_button1_text_en' => array('id' => 'atomic_homepage_img_button1_text_en', 'placeholder' => 'Find landlord', 'value' => get_option('atomic_homepage_img_button1_text_en', ''), 'title' => 'Startpage image button 1 - en', 'desc' => 'button text'),
    'atomic_homepage_img_button2_text_en' => array('id' => 'atomic_homepage_img_button2_text_en', 'placeholder' => 'Find tenant', 'value' => get_option('atomic_homepage_img_button2_text_en', ''), 'title' => 'Startpage image button 2 - en', 'desc' => 'button text'),

   'atomic_homepage_promo' => array('id' => 'atomic_homepage_promo', 'placeholder' => '', 'value' => get_option('atomic_homepage_promo', ''), 'title' => 'Visa kampanjbubbla', 'desc' => 'Kampanjbubbla i headern'),
   'atomic_homepage_promo_image' => array('id' => 'atomic_homepage_promo_image', 'placeholder' => '', 'value' => get_option('atomic_homepage_promo_image', ''), 'title' => 'Använd bild i kampanjbubbla', 'desc' => ''),
   'atomic_homepage_promo_image_link' => array('id' => 'atomic_homepage_promo_image_link', 'placeholder' => '', 'value' => get_option('atomic_homepage_promo_image_link', ''), 'title' => 'Kampanjbild länk', 'desc' => ''),
   'atomic_homepage_promo_title' => array('id' => 'atomic_homepage_promo_title', 'placeholder' => 'Title ..', 'value' => get_option('atomic_homepage_promo_title', ''), 'title' => 'Titel i kampanjbubbla', 'desc' => ''),
   'atomic_homepage_promo_text' => array('id' => 'atomic_homepage_promo_text', 'placeholder' => 'Text ..', 'value' => get_option('atomic_homepage_promo_text', ''), 'title' => 'Text i kampanjbubbla', 'desc' => ''),
   'atomic_homepage_promo_link' => array('id' => 'atomic_homepage_promo_link', 'placeholder' => 'Länk ..', 'value' => get_option('atomic_homepage_promo_link', ''), 'title' => 'Kampnjebubbla länk', 'desc' => ''),


   'atomic_header_menu_color' => array('id' => 'atomic_header_menu_color', 'placeholder' => '#f8f8f8', 'value' => get_option('atomic_header_menu_color', ''), 'title' => 'Menu background color', 'desc' => 'Background color in hex'),
   'atomic_header_menu_link_color' => array('id' => 'atomic_header_menu_link_color', 'placeholder' => '#555', 'value' => get_option('atomic_header_menu_link_color', ''), 'title' => 'Menu links', 'desc' => 'Link color in hex'),
   'atomic_header_menu_link_color_hover' => array('id' => 'atomic_header_menu_link_color_hover', 'placeholder' => '#777', 'value' => get_option('atomic_header_menu_link_color_hover', ''), 'title' => 'Menu link hover', 'desc' => 'Link hover color in hex'),

   'atomic_footer_menu_color' => array('id' => 'atomic_footer_menu_color', 'placeholder' => '#bfaeb8', 'value' => get_option('atomic_footer_menu_color', ''), 'title' => 'Footer background color', 'desc' => 'Background color in hex'),
   'atomic_footer_menu_heading_color' => array('id' => 'atomic_footer_menu_heading_color', 'placeholder' => '#fff', 'value' => get_option('atomic_footer_menu_heading_color', ''), 'title' => 'Footer heading', 'desc' => 'Footer heading color in hex'),
   'atomic_footer_menu_body_color' => array('id' => 'atomic_footer_menu_body_color', 'placeholder' => '#fff', 'value' => get_option('atomic_footer_menu_body_color', ''), 'title' => 'Footer body', 'desc' => 'Footer body color in hex'),
   'atomic_footer_menu_copy_color' => array('id' => 'atomic_footer_menu_copy_color', 'placeholder' => '#fff', 'value' => get_option('atomic_footer_menu_copy_color', ''), 'title' => 'Footer copyright', 'desc' => 'Footer copyright color in hex'),
   'atomic_footer_menu_social_color' => array('id' => 'atomic_footer_menu_social_color', 'placeholder' => '#fff', 'value' => get_option('atomic_footer_menu_social_color', ''), 'title' => 'Footer social icons', 'desc' => 'Social icons color in hex'),

   'atomic_footer_menu_link_color' => array('id' => 'atomic_footer_menu_link_color', 'placeholder' => '#555', 'value' => get_option('atomic_footer_menu_link_color', ''), 'title' => 'Footer links', 'desc' => 'Link color in hex'),
   'atomic_footer_menu_link_color_hover' => array('id' => 'atomic_footer_menu_link_color_hover', 'placeholder' => '#777', 'value' => get_option('atomic_footer_menu_link_color_hover', ''), 'title' => 'Footer link hover', 'desc' => 'Link hover color in hex'),

   'atomic_header_top_info' => array('id' => 'atomic_header_top_info', 'placeholder' => '', 'value' => get_option('atomic_header_top_info', ''), 'title' => 'Display the topmenu header', 'desc' => 'The topmenu header is displayed at the top at the page'),

   'atomic_contact_adress' => array('id' => 'atomic_contact_adress', 'placeholder' => '', 'value' => get_option('atomic_contact_adress', ''), 'title' => 'Streetadress', 'desc' => 'Streetadress to show googlemap on contact page'),
   'atomic_contact_postal' => array('id' => 'atomic_contact_postal', 'placeholder' => '', 'value' => get_option('atomic_contact_postal', ''), 'title' => 'Zipcode', 'desc' => 'Zipcode to show googlemap on contact page'),
   'atomic_contact_city' => array('id' => 'atomic_contact_city', 'placeholder' => '', 'value' => get_option('atomic_contact_city', ''), 'title' => 'City', 'desc' => 'City to show googlemap on contact page'),

   'atomic_social_facebook' => array('id' => 'atomic_social_facebook', 'placeholder' => 'facebook.com', 'value' => get_option('atomic_social_facebook', ''), 'title' => 'Facebook URL', 'desc' => 'An URL to your Facebook site'),
   'atomic_social_twitter' => array('id' => 'atomic_social_twitter', 'placeholder' => 'twitter.com', 'value' => get_option('atomic_social_twitter', ''), 'title' => 'Twitter URL', 'desc' => 'An URL to your Twitter site'),
   'atomic_social_tumblr' => array('id' => 'atomic_social_tumblr', 'placeholder' => 'tumblr.com', 'value' => get_option('atomic_social_tumblr', ''), 'title' => 'Tumblr URL', 'desc' => 'An URL to your Tumblr site'),
   'atomic_social_googleplus' => array('id' => 'atomic_social_googleplus', 'placeholder' => 'googleplus.com', 'value' => get_option('atomic_social_googleplus', ''), 'title' => 'Google Plus URL', 'desc' => 'An URL to your Google Plus site'),
   'atomic_social_vimeo' => array('id' => 'atomic_social_vimeo', 'placeholder' => 'vimeo.com', 'value' => get_option('atomic_social_vimeo', ''), 'title' => 'Vimeo URL', 'desc' => 'An URL to your Vimeo site'),
   'atomic_social_youtube' => array('id' => 'atomic_social_youtube', 'placeholder' => 'youtube.com', 'value' => get_option('atomic_social_youtube', ''), 'title' => 'Youtube URL', 'desc' => 'An URL to your Youtube site'),
   'atomic_social_pinterest' => array('id' => 'atomic_social_pinterest', 'placeholder' => 'pinterest.com', 'value' => get_option('atomic_social_pinterest', ''), 'title' => 'Pinterest URL', 'desc' => 'An URL to your Pinterest site'),

   'atomic_custom_css' => array('id' => 'atomic_custom_css', 'placeholder' => 'body { background: #fff !important; }', 'value' => get_option('atomic_custom_css', ''), 'title' => 'CSS', 'desc' => 'Write your custom css'),


   /* gallery */
   'atomic_gallery_corners' => array('id' => 'atomic_gallery_corners', 'placeholder' => 'border radius', 'value' => get_option('atomic_gallery_corners', ''), 'title' => 'Rounded corner', 'desc' => 'Rounded corners on images in pixels, eg 4px (0 = no rounded corners)'),
   'atomic_gallery_header' => array('id' => 'atomic_gallery_header', 'placeholder' => '', 'value' => get_option('atomic_gallery_header', ''), 'title' => 'Show the header at the gallery templates', 'desc' => 'Show the header containing the title and content with a background'),

   /* typography */
   'atomic_typography_title_font' => array('id' => 'atomic_typography_title_font', 'placeholder' => 'atomic_typography', 'value' => get_option('atomic_typography_title_font', ''), 'title' => 'Title typography', 'desc' => 'Fonts for all title headings', 'options' => $googleFonts),
   'atomic_typography_content_font' => array('id' => 'atomic_typography_content_font', 'placeholder' => 'atomic_typography', 'value' => get_option('atomic_typography_content_font', ''), 'title' => 'Content typography', 'desc' => 'Fonts for all content text', 'options' => $googleFonts),
   'atomic_typography_title_color' => array('id' => 'atomic_typography_title_color', 'placeholder' => '#9e9e9e', 'value' => get_option('atomic_typography_title_color', ''), 'title' => 'Headings color', 'desc' => 'Headings color in hex'),
   'atomic_typography_body_color' => array('id' => 'atomic_typography_body_color', 'placeholder' => '#9e9e9e', 'value' => get_option('atomic_typography_body_color', ''), 'title' => 'Body color', 'desc' => 'Body color in hex'),
   'atomic_typography_h1' => array('id' => 'atomic_typography_h1', 'placeholder' => '36', 'value' => get_option('atomic_typography_h1', ''), 'title' => 'H1 font size', 'desc' => 'Header 1 font size in pixels'),
   'atomic_typography_h2' => array('id' => 'atomic_typography_h2', 'placeholder' => '30', 'value' => get_option('atomic_typography_h2', ''), 'title' => 'H2 font size', 'desc' => 'Header 2 font size in pixels'),
   'atomic_typography_h3' => array('id' => 'atomic_typography_h3', 'placeholder' => '28', 'value' => get_option('atomic_typography_h3', ''), 'title' => 'H3 font size', 'desc' => 'Header 3 font size in pixels'),
   'atomic_typography_h4' => array('id' => 'atomic_typography_h4', 'placeholder' => '18', 'value' => get_option('atomic_typography_h4', ''), 'title' => 'H4 font size', 'desc' => 'Header 4 font size in pixels'),
   'atomic_typography_h5' => array('id' => 'atomic_typography_h5', 'placeholder' => '14', 'value' => get_option('atomic_typography_h5', ''), 'title' => 'H5 font size', 'desc' => 'Header 5 font size in pixels'),
   'atomic_typography_h6' => array('id' => 'atomic_typography_h6', 'placeholder' => '12', 'value' => get_option('atomic_typography_h6', ''), 'title' => 'H6 font size', 'desc' => 'Header 6 font size in pixels'),
   'atomic_typography_link' => array('id' => 'atomic_typography_link', 'placeholder' => '#fff', 'value' => get_option('atomic_typography_link', ''), 'title' => 'Link color', 'desc' => 'Link color in hex'),
   'atomic_typography_link_hover' => array('id' => 'atomic_typography_link_hover', 'placeholder' => '#000', 'value' => get_option('atomic_typography_link_hover', ''), 'title' => 'Link color hover', 'desc' => 'Link color hover in hex'),

);

?>

'use strict';

/*---------------------------------------------------
Main
----------------------------------------------------*/
$(document).ready(function() {
   $('.color-input').minicolors();
});

/*---------------------------------------------------
Save
----------------------------------------------------*/
$(document).ready(function() {
   $('.atomic-builder-save').click(function() {
      showLoadingText(this, 'Saving..', 'Save', true);
      atomicBuilderSave(this);
   });

   function showLoadingText(elm, loadText, defaultText, isLoading) {
      var text = isLoading ? loadText : defaultText ;
      $(elm).html(text);
   }

   function atomicBuilderSave(elm) {

      // grab our latest values
      $.each(atomicSettings, function(i, setting) {
         if($('input[name='+setting['id']+']').length > 0) {
            // check if its an checkbox
            if($('input[name='+setting['id']+']').is(':checkbox')) {
               setting['value'] = $('input[name='+setting['id']+']').prop('checked');
            } else {
               setting['value'] = $('input[name='+setting['id']+']').val();
            }

         } else if($('select[name='+setting['id']+']').length > 0) {
            setting['value'] = $('select[name='+setting['id']+']').val();
         } else if($('textarea[name='+setting['id']+']').length > 0) {
           setting['value'] = $('textarea[name='+setting['id']+']').val();
         }
      });

      // save our new data to the ajax fetcher
      $.ajax({
         url: 'admin-ajax.php',
         type: 'POST',
         data: { 'action': 'save_atomic_builder', 'atomic-settings': atomicSettings }
      }).success(function(data) {
         showLoadingText(elm, 'Saving..', 'Save', false);
         console.log(data);
      }).fail(function(data) {
         console.log('Atomic builder failed to save', data);
      });
   }

   //preview fonts
   $('.select-typography').on('change', function() {
      var selectedFont = $(this).find(":selected").text().replace(' ', '-').toLowerCase(),
          previewContainer = $(this).next('.selected-typography');

      previewContainer.css('background-image', 'url(../wp-content/plugins/atomic-builder/images/fonts/'+selectedFont+'.jpg)');

   });
});

/*---------------------------------------------------
Media upload
----------------------------------------------------*/
$(function(){

   var wpssTargetInput = false;


   $('.wpss_select_image').click(function(event) {
      wpssTargetInput = $(this).prev('input');
      console.log(wpssTargetInput);
      formfield = $('#wpss_upload_image').attr('name');
      tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
      return false;
   });

   window.send_to_editor = function(html) {
      var imgurl = $('img',html).attr('src');
      $(wpssTargetInput).val(imgurl);
      tb_remove();

      //$('#wpss_upload_image_thumb').html("<img height='65' src='"+imgurl+"'/>");
   }
});

/*---------------------------------------------------
Navbar
----------------------------------------------------*/
var tabsFn = (function() {

   function init() {
      setHeight();

      $(window).on('resize', function() {
         setHeight();
      });
   }

   function setHeight() {
      const atomicBuilder = $('#atomic-builder');
      const atomicBuilderNav = $('#atomic-builder .nav-tabs');
      const atomicBuilderContent = $('#atomic-builder .tab-content');
      let wpHeight = $(window).height() - 120;

      atomicBuilder.height(wpHeight);
      atomicBuilderNav.height(wpHeight);
      atomicBuilderContent.height(wpHeight);
   }

   $(init);
})();

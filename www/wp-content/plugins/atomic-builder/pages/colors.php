<?php include(plugin_dir_path( __FILE__ ) . 'atomic-settings.php'); ?>

<div class="head clearfix">
  <div class="title">
    <h2>Colors</h2>
  </div>
  <div class="cta">
    <btn class="btn btn-default">Restore settings</btn>
    <btn class="atomic-builder-save btn btn-primary">Save</btn>
  </div>
</div>

<p class="section-title">Theme colors</p>

<?php
foreach($settings['menu'] as $setting) {
?>
<div class="row">
  <div class="col-md-5 info">
    <p class="title"><?php echo $setting['title']; ?></p>
    <p class="desc"><?php echo $setting['desc']; ?></p>
  </div>
  <div class="col-md-7">
    <input type="text" name="<?php echo $setting['id']; ?>" class="form-control color-input" placeholder="<?php echo $setting['value']; ?>" value="<?php echo $setting['value']; ?>" />
  </div>
</div>
<?php
}
?>

<p class="section-title">Header image</p>
<div class="row">
  <div class="col-md-5 info">
    <p class="title">Header image</p>
    <p class="desc">Upload your own image as header></p>
  </div>
  <div class="col-md-7">
    <div id="wpss_upload_image_thumb" class="wpss-file"></div>
    <input id="wpss_upload_image" type="text" name="wpss_upload_image" value="" class="wpss_text wpss-file form-control media-upload" />
    <btn id="wpss_upload_image_button"class="wpss-filebtn btn btn-primary">Upload image</button>
  </div>
</div>

<div class="footer clearfix">
  <div class="cta">
    <btn class="btn btn-default">Restore settings</btn>
    <btn class="atomic-builder-save btn btn-primary">Save</btn>
  </div>
</div>

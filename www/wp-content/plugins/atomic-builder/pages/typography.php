<?php include(plugin_dir_path( __FILE__ ) . 'atomic-settings.php'); ?>

<!-- HEAD -->
<div class="head clearfix">
   <div class="title">
      <h2>Typography</h2>
   </div>
   <div class="cta">
      <btn class="atomic-builder-save btn btn-primary">Save</btn>
   </div>
</div>
<!-- END HEAD -->

<!-- <p class="section-title">Fonts</p>
<div class="row">
   <div class="col-md-6 info">
      <p class="title"><?php echo $settings['atomic_typography_title_font']['title']; ?></p>
      <p class="desc"><?php echo $settings['atomic_typography_title_font']['desc']; ?></p>
   </div>
   <div class="col-md-6">
      <select name="atomic_typography_title_font" class="select-typography">
         <?php
            foreach($settings['atomic_typography_title_font']['options'] as $key => $option)
            {
               $checked = $settings['atomic_typography_title_font']['value'] == $key ? 'selected': '';
               echo '<option value="'.$key.'" '.$checked.'>'.$option.'</option>';
            }
         ?>
      </select>
      <?php $currentPreviewFont = str_replace(' ', '-', $settings['atomic_typography_title_font']['options'][$settings['atomic_typography_title_font']['value']]); ?>
      <div class="selected-typography" style="background-image: url(../wp-content/plugins/atomic-builder/images/fonts/<?php echo $currentPreviewFont; ?>.jpg)">
      </div>
   </div>
</div>
<div class="row">
   <div class="col-md-6 info">
      <p class="title"><?php echo $settings['atomic_typography_content_font']['title']; ?></p>
      <p class="desc"><?php echo $settings['atomic_typography_content_font']['desc']; ?></p>
   </div>
   <div class="col-md-6">
      <select name="atomic_typography_content_font" class="select-typography">
         <?php
            foreach($settings['atomic_typography_content_font']['options'] as $key => $option)
            {
               $checked = $settings['atomic_typography_content_font']['value'] == $key ? 'selected': '';
               echo '<option value="'.$key.'" '.$checked.'>'.$option.'</option>';
            }
         ?>
      </select>
      <?php $currentPreviewFont = str_replace(' ', '-', $settings['atomic_typography_content_font']['options'][$settings['atomic_typography_content_font']['value']]); ?>
      <div class="selected-typography" style="background-image: url(../wp-content/plugins/atomic-builder/images/fonts/<?php echo $currentPreviewFont; ?>.jpg)">
      </div>
   </div>
</div> -->

<p class="section-title">Text color</p>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_typography_title_color']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_typography_title_color']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_typography_title_color" class="form-control number-input" placeholder="<?php echo $settings['atomic_typography_title_color']['placeholder']; ?>" value="<?php echo $settings['atomic_typography_title_color']['value']; ?>" />
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_typography_body_color']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_typography_body_color']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_typography_body_color" class="form-control number-input" placeholder="<?php echo $settings['atomic_typography_body_color']['placeholder']; ?>" value="<?php echo $settings['atomic_typography_body_color']['value']; ?>" />
  </div>
</div>

<p class="section-title">Text sizes</p>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_typography_h1']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_typography_h1']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="number" name="atomic_typography_h1" class="form-control number-input" placeholder="<?php echo $settings['atomic_typography_h1']['placeholder']; ?>" value="<?php echo $settings['atomic_typography_h1']['value']; ?>" /> px
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_typography_h2']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_typography_h2']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="number" name="atomic_typography_h2" class="form-control number-input" placeholder="<?php echo $settings['atomic_typography_h2']['placeholder']; ?>" value="<?php echo $settings['atomic_typography_h2']['value']; ?>" /> px
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_typography_h3']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_typography_h3']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="number" name="atomic_typography_h3" class="form-control number-input" placeholder="<?php echo $settings['atomic_typography_h3']['placeholder']; ?>" value="<?php echo $settings['atomic_typography_h3']['value']; ?>" /> px
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_typography_h4']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_typography_h4']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="number" name="atomic_typography_h4" class="form-control number-input" placeholder="<?php echo $settings['atomic_typography_h4']['placeholder']; ?>" value="<?php echo $settings['atomic_typography_h4']['value']; ?>" /> px
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_typography_h5']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_typography_h5']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="number" name="atomic_typography_h5" class="form-control number-input" placeholder="<?php echo $settings['atomic_typography_h5']['placeholder']; ?>" value="<?php echo $settings['atomic_typography_h5']['value']; ?>" /> px
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_typography_h6']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_typography_h6']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="number" name="atomic_typography_h6" class="form-control number-input" placeholder="<?php echo $settings['atomic_typography_h6']['placeholder']; ?>" value="<?php echo $settings['atomic_typography_h6']['value']; ?>" /> px
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_typography_link']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_typography_link']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_typography_link" class="form-control number-input" placeholder="<?php echo $settings['atomic_typography_link']['placeholder']; ?>" value="<?php echo $settings['atomic_typography_link']['value']; ?>" />
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_typography_link_hover']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_typography_link_hover']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_typography_link_hover" class="form-control number-input" placeholder="<?php echo $settings['atomic_typography_link_hover']['placeholder']; ?>" value="<?php echo $settings['atomic_typography_link_hover']['value']; ?>" />
  </div>
</div>

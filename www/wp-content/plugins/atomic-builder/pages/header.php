<?php include(plugin_dir_path( __FILE__ ) . 'atomic-settings.php'); ?>

<!-- HEAD -->
<div class="head clearfix">
  <div class="title">
    <h2>Header</h2>
  </div>
  <div class="cta">
    <btn class="atomic-builder-save btn btn-primary">Save</btn>
  </div>
</div>
<!-- END HEAD -->

<p class="section-title">The logo</p>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_logo_size']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_logo_size']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="number" name="atomic_logo_size" class="form-control number-input" placeholder="<?php echo $settings['atomic_logo_size']['placeholder']; ?>" value="<?php echo $settings['atomic_logo_size']['value']; ?>" /> px
  </div>
</div>
<div class="row clearfix">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_logo_text']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_logo_text']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_logo_text" class="form-control" placeholder="<?php echo $settings['atomic_logo_text']['placeholder']; ?>" value="<?php echo $settings['atomic_logo_text']['value']; ?>" />
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_logo_use_image']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_logo_use_image']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <div class="checkbox input-checkbox">
      <label>
        <input name="atomic_logo_use_image" type="checkbox" <?php atomic_checkbox('atomic_logo_use_image'); ?>> Yes
      </label>
    </div>
  </div>
</div>

<!-- atomic_logo_image_logo -->
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_logo_image_logo']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_logo_image_logo']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <div id="wpss_upload_image_thumb" class="wpss-file"></div>
    <input name="atomic_logo_image_logo" type="text" class="wpss_text wpss-file form-control media-upload" value="<?php echo $settings['atomic_logo_image_logo']['value']; ?>"/>
  </div>
</div>

<p class="section-title">Menu</p>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_header_menu_color']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_header_menu_color']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <div id="wpss_upload_image_thumb" class="wpss-file"></div>
    <input name="atomic_header_menu_color" type="text" class="wpss_text wpss-file form-control" value="<?php echo $settings['atomic_header_menu_color']['value']; ?>"  placeholder="<?php echo $settings['atomic_header_menu_color']['placeholder']; ?>"/>
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_header_menu_link_color']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_header_menu_link_color']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <div id="wpss_upload_image_thumb" class="wpss-file"></div>
    <input name="atomic_header_menu_link_color" type="text" class="wpss_text wpss-file form-control" value="<?php echo $settings['atomic_header_menu_link_color']['value']; ?>"  placeholder="<?php echo $settings['atomic_header_menu_link_color']['placeholder']; ?>"/>
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_header_menu_link_color_hover']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_header_menu_link_color_hover']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <div id="wpss_upload_image_thumb" class="wpss-file"></div>
    <input name="atomic_header_menu_link_color_hover" type="text" class="wpss_text wpss-file form-control" value="<?php echo $settings['atomic_header_menu_link_color_hover']['value']; ?>"  placeholder="<?php echo $settings['atomic_header_menu_link_color_hover']['placeholder']; ?>"/>
  </div>
</div>


<!-- atomic_header_top_info -->
<p class="section-title">Top menu</p>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_header_top_info']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_header_top_info']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <div class="checkbox input-checkbox">
      <label>
        <input name="atomic_header_top_info" type="checkbox" <?php atomic_checkbox('atomic_header_top_info'); ?>> Show
      </label>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_header_top_color']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_header_top_color']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <div id="wpss_upload_image_thumb" class="wpss-file"></div>
    <input name="atomic_header_top_color" type="text" class="wpss_text wpss-file form-control" value="<?php echo $settings['atomic_header_top_color']['value']; ?>"  placeholder="<?php echo $settings['atomic_header_top_color']['placeholder']; ?>"/>
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_header_top_color_link']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_header_top_color_link']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <div id="wpss_upload_image_thumb" class="wpss-file"></div>
    <input name="atomic_header_top_color_link" type="text" class="wpss_text wpss-file form-control" value="<?php echo $settings['atomic_header_top_color_link']['value']; ?>"  placeholder="<?php echo $settings['atomic_header_top_color_link']['placeholder']; ?>"/>
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_header_top_color_link_hover']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_header_top_color_link_hover']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <div id="wpss_upload_image_thumb" class="wpss-file"></div>
    <input name="atomic_header_top_color_link_hover" type="text" class="wpss_text wpss-file form-control" value="<?php echo $settings['atomic_header_top_color_link_hover']['value']; ?>"  placeholder="<?php echo $settings['atomic_header_top_color_link_hover']['placeholder']; ?>"/>
  </div>
</div>

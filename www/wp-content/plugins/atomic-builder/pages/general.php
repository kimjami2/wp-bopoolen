<?php include(plugin_dir_path( __FILE__ ) . '../atomic-settings.php'); ?>

<!-- HEAD -->
<div class="head clearfix">
  <div class="title">
    <h2>General</h2>
  </div>
  <div class="cta">
    <btn class="atomic-builder-save btn btn-primary">Save</btn>
  </div>
</div>
<!-- END HEAD -->

<p class="section-title">Title</p>
<div class="row clearfix">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_page_title']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_page_title']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_page_title" class="form-control" placeholder="<?php echo $settings['atomic_page_title']['placeholder']; ?>" value="<?php echo $settings['atomic_page_title']['value']; ?>" />
  </div>
</div>

<p class="section-title">Kampanjbubbla</p>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_homepage_promo']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_homepage_promo']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <div class="checkbox input-checkbox">
      <label>
        <input name="atomic_homepage_promo" type="checkbox" <?php atomic_checkbox('atomic_homepage_promo'); ?>> Ja
      </label>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_homepage_promo_image']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_homepage_promo_image']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <div class="checkbox input-checkbox">
      <label>
        <input name="atomic_homepage_promo_image" type="checkbox" <?php atomic_checkbox('atomic_homepage_promo_image'); ?>> Ja
      </label>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_homepage_promo_image_link']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_homepage_promo_image_link']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_homepage_promo_image_link" class="form-control" placeholder="<?php echo $settings['atomic_homepage_promo_image_link']['placeholder']; ?>" value="<?php echo $settings['atomic_homepage_promo_image_link']['value']; ?>" />
  </div>
</div>
<div class="row clearfix">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_homepage_promo_title']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_homepage_promo_title']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_homepage_promo_title" class="form-control" placeholder="<?php echo $settings['atomic_homepage_promo_title']['placeholder']; ?>" value="<?php echo $settings['atomic_homepage_promo_title']['value']; ?>" />
  </div>
</div>
<div class="row clearfix">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_homepage_promo_text']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_homepage_promo_text']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_homepage_promo_text" class="form-control" placeholder="<?php echo $settings['atomic_homepage_promo_text']['placeholder']; ?>" value="<?php echo $settings['atomic_homepage_promo_text']['value']; ?>" />
  </div>
</div>
<div class="row clearfix">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_homepage_promo_link']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_homepage_promo_link']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_homepage_promo_link" class="form-control" placeholder="<?php echo $settings['atomic_homepage_promo_link']['placeholder']; ?>" value="<?php echo $settings['atomic_homepage_promo_link']['value']; ?>" />
  </div>
</div>

<p class="section-title">Startpage header</p>
<div class="row clearfix">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_homepage_img']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_homepage_img']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_homepage_img" class="form-control" placeholder="<?php echo $settings['atomic_homepage_img']['placeholder']; ?>" value="<?php echo $settings['atomic_homepage_img']['value']; ?>" />
  </div>
</div>
<div class="row clearfix">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_homepage_img_title']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_homepage_img_title']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_homepage_img_title" class="form-control" placeholder="<?php echo $settings['atomic_homepage_img_title']['placeholder']; ?>" value="<?php echo $settings['atomic_homepage_img_title']['value']; ?>" />
  </div>
</div>
<div class="row clearfix">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_homepage_img_subtitle']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_homepage_img_subtitle']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_homepage_img_subtitle" class="form-control" placeholder="<?php echo $settings['atomic_homepage_img_subtitle']['placeholder']; ?>" value="<?php echo $settings['atomic_homepage_img_subtitle']['value']; ?>" />
  </div>
</div>
<div class="row clearfix">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_homepage_img_button1_link']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_homepage_img_button1_link']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_homepage_img_button1_link" class="form-control" placeholder="<?php echo $settings['atomic_homepage_img_button1_link']['placeholder']; ?>" value="<?php echo $settings['atomic_homepage_img_button1_link']['value']; ?>" />
  </div>
</div>
<div class="row clearfix">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_homepage_img_button1_text']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_homepage_img_button1_text']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_homepage_img_button1_text" class="form-control" placeholder="<?php echo $settings['atomic_homepage_img_button1_text']['placeholder']; ?>" value="<?php echo $settings['atomic_homepage_img_button1_text']['value']; ?>" />
  </div>
</div>
<div class="row clearfix">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_homepage_img_button1_text_en']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_homepage_img_button1_text_en']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_homepage_img_button1_text_en" class="form-control" placeholder="<?php echo $settings['atomic_homepage_img_button1_text_en']['placeholder']; ?>" value="<?php echo $settings['atomic_homepage_img_button1_text_en']['value']; ?>" />
  </div>
</div>
<div class="row clearfix">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_homepage_img_button2_link']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_homepage_img_button2_link']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_homepage_img_button2_link" class="form-control" placeholder="<?php echo $settings['atomic_homepage_img_button2_link']['placeholder']; ?>" value="<?php echo $settings['atomic_homepage_img_button2_link']['value']; ?>" />
  </div>
</div>
<div class="row clearfix">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_homepage_img_button2_text']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_homepage_img_button2_text']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_homepage_img_button2_text" class="form-control" placeholder="<?php echo $settings['atomic_homepage_img_button2_text']['placeholder']; ?>" value="<?php echo $settings['atomic_homepage_img_button2_text']['value']; ?>" />
  </div>
</div>
<div class="row clearfix">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_homepage_img_button2_text_en']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_homepage_img_button2_text_en']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_homepage_img_button2_text_en" class="form-control" placeholder="<?php echo $settings['atomic_homepage_img_button2_text_en']['placeholder']; ?>" value="<?php echo $settings['atomic_homepage_img_button2_text_en']['value']; ?>" />
  </div>
</div>

<p class="section-title">Contact Page Information</p>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_contact_adress']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_contact_adress']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_contact_adress" class="form-control" placeholder="<?php echo $settings['atomic_contact_adress']['placeholder']; ?>" value="<?php echo $settings['atomic_contact_adress']['value']; ?>" />
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_contact_postal']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_contact_postal']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_contact_postal" class="form-control" placeholder="<?php echo $settings['atomic_contact_postal']['placeholder']; ?>" value="<?php echo $settings['atomic_contact_postal']['value']; ?>" />
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_contact_city']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_contact_city']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_contact_city" class="form-control" placeholder="<?php echo $settings['atomic_contact_city']['placeholder']; ?>" value="<?php echo $settings['atomic_contact_city']['value']; ?>" />
  </div>
</div>

<p class="section-title">Socialmedia links</p>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_social_facebook']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_social_facebook']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_social_facebook" class="form-control" placeholder="<?php echo $settings['atomic_social_facebook']['placeholder']; ?>" value="<?php echo $settings['atomic_social_facebook']['value']; ?>" />
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_social_twitter']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_social_twitter']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_social_twitter" class="form-control" placeholder="<?php echo $settings['atomic_social_twitter']['placeholder']; ?>" value="<?php echo $settings['atomic_social_twitter']['value']; ?>" />
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_social_tumblr']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_social_tumblr']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_social_tumblr" class="form-control" placeholder="<?php echo $settings['atomic_social_tumblr']['placeholder']; ?>" value="<?php echo $settings['atomic_social_tumblr']['value']; ?>" />
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_social_googleplus']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_social_googleplus']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_social_googleplus" class="form-control" placeholder="<?php echo $settings['atomic_social_googleplus']['placeholder']; ?>" value="<?php echo $settings['atomic_social_googleplus']['value']; ?>" />
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_social_vimeo']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_social_vimeo']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_social_vimeo" class="form-control" placeholder="<?php echo $settings['atomic_social_vimeo']['placeholder']; ?>" value="<?php echo $settings['atomic_social_vimeo']['value']; ?>" />
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_social_youtube']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_social_youtube']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_social_youtube" class="form-control" placeholder="<?php echo $settings['atomic_social_youtube']['placeholder']; ?>" value="<?php echo $settings['atomic_social_youtube']['value']; ?>" />
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_social_pinterest']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_social_pinterest']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <input type="text" name="atomic_social_pinterest" class="form-control" placeholder="<?php echo $settings['atomic_social_pinterest']['placeholder']; ?>" value="<?php echo $settings['atomic_social_pinterest']['value']; ?>" />
  </div>
</div>

<p class="section-title">Custom css</p>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_custom_css']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_custom_css']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <textarea class='form-control' name="atomic_custom_css" placeholder='<?php echo $settings['atomic_custom_css']['placeholder']; ?>' rows="10"><?php echo $settings['atomic_custom_css']['value']; ?></textarea>
  </div>
</div>

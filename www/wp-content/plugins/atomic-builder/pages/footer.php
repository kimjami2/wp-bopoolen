<?php include(plugin_dir_path( __FILE__ ) . 'atomic-settings.php'); ?>

<!-- HEAD -->
<div class="head clearfix">
  <div class="title">
    <h2>Footer</h2>
  </div>
  <div class="cta">
    <btn class="atomic-builder-save btn btn-primary">Save</btn>
  </div>
</div>
<!-- END HEAD -->

<p class="section-title">Color</p>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_footer_menu_color']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_footer_menu_color']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <div id="wpss_upload_image_thumb" class="wpss-file"></div>
    <input name="atomic_footer_menu_color" type="text" class="wpss_text wpss-file form-control" value="<?php echo $settings['atomic_footer_menu_color']['value']; ?>"  placeholder="<?php echo $settings['atomic_footer_menu_color']['placeholder']; ?>"/>
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_footer_menu_heading_color']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_footer_menu_heading_color']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <div id="wpss_upload_image_thumb" class="wpss-file"></div>
    <input name="atomic_footer_menu_heading_color" type="text" class="wpss_text wpss-file form-control" value="<?php echo $settings['atomic_footer_menu_heading_color']['value']; ?>"  placeholder="<?php echo $settings['atomic_footer_menu_heading_color']['placeholder']; ?>"/>
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_footer_menu_body_color']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_footer_menu_body_color']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <div id="wpss_upload_image_thumb" class="wpss-file"></div>
    <input name="atomic_footer_menu_body_color" type="text" class="wpss_text wpss-file form-control" value="<?php echo $settings['atomic_footer_menu_body_color']['value']; ?>"  placeholder="<?php echo $settings['atomic_footer_menu_body_color']['placeholder']; ?>"/>
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_footer_menu_copy_color']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_footer_menu_copy_color']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <div id="wpss_upload_image_thumb" class="wpss-file"></div>
    <input name="atomic_footer_menu_copy_color" type="text" class="wpss_text wpss-file form-control" value="<?php echo $settings['atomic_footer_menu_copy_color']['value']; ?>"  placeholder="<?php echo $settings['atomic_footer_menu_copy_color']['placeholder']; ?>"/>
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_footer_menu_social_color']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_footer_menu_social_color']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <div id="wpss_upload_image_thumb" class="wpss-file"></div>
    <input name="atomic_footer_menu_social_color" type="text" class="wpss_text wpss-file form-control" value="<?php echo $settings['atomic_footer_menu_social_color']['value']; ?>"  placeholder="<?php echo $settings['atomic_footer_menu_social_color']['placeholder']; ?>"/>
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_footer_menu_link_color']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_footer_menu_link_color']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <div id="wpss_upload_image_thumb" class="wpss-file"></div>
    <input name="atomic_footer_menu_link_color" type="text" class="wpss_text wpss-file form-control" value="<?php echo $settings['atomic_footer_menu_link_color']['value']; ?>"  placeholder="<?php echo $settings['atomic_footer_menu_link_color']['placeholder']; ?>"/>
  </div>
</div>
<div class="row">
  <div class="col-md-6 info">
    <p class="title"><?php echo $settings['atomic_footer_menu_link_color_hover']['title']; ?></p>
    <p class="desc"><?php echo $settings['atomic_footer_menu_link_color_hover']['desc']; ?></p>
  </div>
  <div class="col-md-6">
    <div id="wpss_upload_image_thumb" class="wpss-file"></div>
    <input name="atomic_footer_menu_link_color_hover" type="text" class="wpss_text wpss-file form-control" value="<?php echo $settings['atomic_footer_menu_link_color_hover']['value']; ?>"  placeholder="<?php echo $settings['atomic_footer_menu_link_color_hover']['placeholder']; ?>"/>
  </div>
</div>

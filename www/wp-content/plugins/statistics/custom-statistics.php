<?php
/**
* Plugin Name: Statistik
* Plugin URI:
* Description: Statistik om dina posts
* Version: 1.0
* Author: Mathias Andersson
**/

function enqueue_excelscript() {
  wp_enqueue_script( 'datatables',  '/wp-content/themes/bopoolen/assets/scripts/jquery.dataTables.min.js');
  wp_enqueue_style( 'datatables-css',   "https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" );
  wp_enqueue_style( 'datatables-button-css',   "https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" );
  // wp_enqueue_style( 'datatables-FA-css',   "https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css");
}
add_action( 'admin_enqueue_scripts', 'enqueue_excelscript' );


add_action('admin_menu', 'admin_export_to_excel_menu');
function admin_export_to_excel_menu() {
	add_menu_page('Statistik', 'Statistik', 'administrator', 'statistik', 'admin_export_to_excel');
}

function admin_export_to_excel() {
  ?>
  <script type="text/javascript" src="/wp-content/themes/bopoolen/dist/scripts/jquery.js"></script>
  <script type="text/javascript" src="/wp-content/themes/bopoolen/assets/scripts/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $('#dvData table').DataTable({
        dom: 'Bfrtip',
        buttons: [
          'excel',
        ],
        // columnDefs: [
        //   {
        //     render: function (data, type, row) {
        //       return (data === 'X') ? '<i class="fa fa-check" aria-hidden="true"></i>' : '<span></span>';
        //     },
        //     targets: 7
        //   }
        // ]
      });
    });
  </script>



  <div class="wrap">

    <h1>Statistik</h1>


    <form class="" action="admin.php" method="GET">
      <h3>Filtrering:</h3>
      <input type="hidden" name="page" value="statistik">
      <h4>Boendeform</h4>
      <label>
        <input type="checkbox" name="living[]" value="ownapartment" <?php echo (isset($_GET['living']) && in_array('ownapartment', $_GET['living']) ? 'checked' : '')?>>
        Lägenhet
      </label>
      <label>
        <input type="checkbox" name="living[]" value="ownhouse" <?php echo (isset($_GET['living']) && in_array('ownhouse', $_GET['living']) ? 'checked' : '')?>>
        Hus
      </label>
      <label>
        <input type="checkbox" name="living[]" value="roominstudent" <?php echo (isset($_GET['living']) && in_array('roominstudent', $_GET['living']) ? 'checked' : '')?>>
        Rum i studentboende
      </label>
      <label>
        <input type="checkbox" name="living[]" value="roominhouse" <?php echo (isset($_GET['living']) && in_array('roominhouse', $_GET['living']) ? 'checked' : '')?>>
        Rum i lägenhet
      </label>
      <label>
        <input type="checkbox" name="living[]" value="roominapartment" <?php echo (isset($_GET['living']) && in_array('roominapartment', $_GET['living']) ? 'checked' : '')?>>
        Rum i lägenhet
      </label>



      <h4>Kontrakt</h4>
      <label>
        <input type="checkbox" name="contract[]" value="contractfirst" <?php echo (isset($_GET['contract']) && in_array('contractfirst', $_GET['contract'])? 'checked' : '')?>>
        Förstahand
      </label>
      <label>
        <input type="checkbox" name="contract[]" value="contractsecond" <?php echo (isset($_GET['contract']) && in_array('contractsecond', $_GET['contract'])? 'checked' : '')?>>
        Andrahand
      </label>
      <label>
        <input type="checkbox" name="contract[]" value="contractchange" <?php echo (isset($_GET['contract']) && in_array('contractchange', $_GET['contract'])? 'checked' : '')?>>
        Byte
      </label>

      <h4>Område</h4>
      <select name="area">
        <option value="" disabled selected>Välj område</option>
        <option value="" >Lund</option>
        <option value="" >Malmö</option>
        <option value="" >Helsingborg</option>
        <option value="" >Lund med omnejd</option>
      </select>

      <br>
      <br>
      <input type="submit" value="Filtrera" />
    </form>

    <br>
    <hr>
    <br>
    <br>
  <?php
// echo "<pre>" . htmlentities(print_r(get_post_meta($post->ID), 1)) . "</pre>";
    if (isset($_GET['contract']) || isset($_GET['living']) ||  isset($_GET['city'])) {
      $args = array(
        'post_type'  => array('searchad', 'rentad'),
        'meta_query'      => array (
        'relation'        => 'AND'
        )
      );

      if(isset($_GET['contract'])){
        $args['meta_query']['relation'] = 'OR';
        if(in_array('contractfirst', $_GET['contract'])){
          $args['meta_query'][] =  array(
            'key'     => '_contractfirst',
            'value'   => 'checked',
            'compare' => '=',
            'post_type'    => 'rentad'
          );
        }

        if(in_array('contractsecond', $_GET['contract'])){
          $args['meta_query'][] =  array(
            'key'     => '_contractsecond',
            'value'   => 'checked',
            'compare' => '=',
            'post_type'    => 'rentad'
          );
        }

        if(in_array('contractchange', $_GET['contract'])){
          $args['meta_query'][] =  array(
            'key'     => '_contractchange',
            'value'   => 'checked',
            'compare' => '=',
            'post_type'    => 'rentad'
          );
        }

        foreach($_GET['contract'] as $value){
          $args['meta_query'][] =  array(
            'key'     => '_contractType',
            'value'   => $value,
            'compare' => 'LIKE',
            'post_type'    => 'searchad'
          );
        }
      }


      if(isset($_GET['living'])){
        $args['meta_query']['relation'] = 'OR';
        if(in_array('roominhouse', $_GET['living'])){
          $args['meta_query'][] =  array(
          'key'     => '_roominhouse',
          'value'   => 'checked',
          'compare' => '=',
          'post_type'    => 'rentad'
          );
        }

        if(in_array('roominapartment', $_GET['living'])){
          $args['meta_query'][] =  array(
          'key'     => '_roominapartment',
          'value'   => 'checked',
          'compare' => '=',
          'post_type'    => 'rentad'
          );
        }

        if(in_array('roominstudent', $_GET['living'])){
          $args['meta_query'][] =  array(
          'key'     => '_roominstudent',
          'value'   => 'checked',
          'compare' => '=',
          'post_type'    => 'rentad'
          );
        }

        if(in_array('ownhouse', $_GET['living'])){
          $args['meta_query'][] =  array(
          'key'     => '_ownhouse',
          'value'   => 'checked',
          'compare' => '=',
          'post_type'    => 'rentad'
          );
        }

        if(in_array('ownapartment', $_GET['living'])){
          $args['meta_query'][] =  array(
          'key'     => '_ownapartment',
          'value'   => 'checked',
          'compare' => '=',
          'post_type'    => 'rentad'
          );
        }

        foreach($_GET['living'] as $value){
          $args['meta_query'][] =  array(
          'key'     => '_accommodationType',
          'value'   => $value,
          'compare' => 'LIKE',
          'post_type'    => 'searchad'
          );
        }
      }


      if(isset($_GET['area'])){
        $args['meta_query'][] =  array(
        'key'     => '_area',
        'value'   => strip_tags($_GET['area']),
        'compare' => '=',
        );
      }

      $ads = new WP_Query($args);
      $ads = $ads->posts;

      $noads = 0;
      $notenantAds = 0;
      foreach ($ads as $post) {
        $noads = $post->status == 'publish' ? $noads + 1 : $noads;
        $notenantAds = $post->post_type == 'searchad' && $post->status == 'publish' ? $notenantAds + 1 : $notenantAds;
      }

    }
    else {
      $postsArray = array(
        'posts_per_page'	=> -1,
        'post_type'			  => array( 'searchad', 'rentad' ),
        'post_status'     => array( 'publish', 'draft', 'raderad', "pending", "modified", 'rejected')
      );
      $ads = get_posts($postsArray);
      $count_posts = wp_count_posts('rentad');
      $published_posts = $count_posts->publish;

      $count_posts = wp_count_posts('searchad');
      $notenantAds = $count_posts->publish;

      $noads = intval($notenantAds)+intval($published_posts);

    }

    // echo "<pre>" . htmlentities(print_r($ads, 1)) . "</pre>";
    echo "<h2>Totalt antal publicerade annonser: ".$noads." </h2>";

      // if(isset($_GET['contract']) || isset($_GET['living'])){
      //   $filterdAds = [];
      //   $noads = 0;
      //   foreach ($ads as $post) {
      //     $check = true;
      //
      //     if($post->post_type == 'rentad'){
      //       $contractSecond = get_post_meta($post->ID, '_contractsecond', true) == 'checked' ? 'contractsecond' : '';
      //       $contractFirst = get_post_meta($post->ID, '_contractfirst', true) == 'checked' ? 'contractfirst' : '';
      //       $contractExchange = get_post_meta($post->ID, '_contractchange', true) == 'checked' ? 'contractchange' : '';
      //
      //       $roominhouse = get_post_meta($post->ID, '_roominhouse', true) == 'checked' ? 'roominhouse' : '';
      //       $roominapartment = get_post_meta($post->ID, '_roominapartment', true) == 'checked' ? 'roominapartment' : '';
      //       $roominstudent = get_post_meta($post->ID, '_roominstudent', true) == 'checked' ? 'roominstudent' : '';
      //       $ownhouse = get_post_meta($post->ID, '_ownhouse', true) == 'checked' ? 'ownhouse' : '';
      //       $ownapartment = get_post_meta($post->ID, '_ownapartment', true) == 'checked' ? 'ownapartment' : '';
      //     }
      //
      //     if(isset($_GET['contract'])){
      //       if($post->post_type == 'rentad' &&
      //         in_array($contractSecond, $_GET['contract']) ||
      //         in_array($contractFirst, $_GET['contract']) ||
      //         in_array($contractExchange, $_GET['contract'])){
      //         $filterdAds[] = $post;
      //         $check = false;
      //       }
      //       if($post->post_type == 'searchad'){
      //         $data = get_post_meta($post->ID, '_contractType', true);
      //         foreach (explode(',',($data == '' ? [] : $data)) as $val) {
      //           if(in_array($val, $_GET['contract'])){
      //             $filterdAds[] = $post;
      //             $check = false;
      //           }
      //         }
      //       }
      //     }
      //
      //     if(isset($_GET['living']) && $check){
      //       if($post->post_type == 'rentad' &&
      //         in_array($roominhouse, $_GET['living']) ||
      //         in_array($roominapartment, $_GET['living']) ||
      //         in_array($roominstudent, $_GET['living']) ||
      //         in_array($ownhouse, $_GET['living']) ||
      //         in_array($ownapartment, $_GET['living'])){
      //         $filterdAds[] = $post;
      //       }
      //       if($post->post_type == 'searchad'){
      //         $data = get_post_meta($post->ID, '_accommodationType', true);
      //         foreach (explode(',',($data == '' ? [] : $data)) as $val) {
      //           if(in_array($val, $_GET['living'])){
      //             $filterdAds[] = $post;
      //             $check = false;
      //           }
      //         }
      //       }
      //     }
      //
      //     if($post->post_status == 'publish')
      //       $noads++;
      //   }
      //   $ads = $filterdAds;
      // }




    ?>

    <?php
    $noads = $noads + count_ads_inactiveErased();
    if($noads == 0) {
      echo '<div><em>Inga annonser kunde hittas!</em></div>';
    } else { ?>
    <div id="dvData">
      <table>
        <thead>
        <tr>
          <th>Typ</th>
          <th>Titel</th>
          <th>Status</th>
          <th>Upplagd</th>
          <th>Senast ändrad</th>
          <th>Område</th>
          <th>Boendeform</th>
          <th>Kontrakttyp</th>
          <th>Antal&nbsp;klick</th>
          <th>Inaktiverad/raderad&nbsp;anledning&nbsp;(kommentar)</th>
          <th>Hittat&nbsp;en&nbsp;hyresg&#228;st/Hittat&nbsp;boende</th>
          <th>Vill&nbsp;inte&nbsp;l&#228;ngre&nbsp;hyra&nbsp;ut</th>
          <th>Annan&nbsp;anledning</th>
          <th>Annan&nbsp;anledning&nbsp;(kommentar)</th>
        </tr>
      </thead>
      <tfoot>
      <tr>
        <th>Typ</th>
        <th>Titel</th>
        <th>Status</th>
        <th>Upplagd</th>
        <th>Senast ändrad</th>
        <th>Område</th>
        <th>Boendeform</th>
        <th>Kontrakttyp</th>
        <th>Antal&nbsp;klick</th>
        <th>Inaktiverad/raderad&nbsp;anledning&nbsp;(kommentar)</th>
        <th>Hittat&nbsp;en&nbsp;hyresg&#228;st/Hittat&nbsp;boende</th>
        <th>Vill&nbsp;inte&nbsp;l&#228;ngre&nbsp;hyra&nbsp;ut</th>
        <th>Annan&nbsp;anledning</th>
        <th>Annan&nbsp;anledning&nbsp;(kommentar)</th>
      </tr>
    </tfoot>

        <?php
        $nopostinactive = count_ads_inactiveErased();
        $nopostfoundtenant = count_ads_meta('_deletereason', '_inactivereason', 'found_tenant');
        $nopostunavailable = count_ads_meta('_deletereason', '_inactivereason', 'unavailable');
        $nopostother = count_ads_meta('_deletereason', '_inactivereason', 'other');

        foreach ($ads as $ad) {
          $post = get_post($ad);
          $postClicks = get_post_meta($post->ID, '_clicker', true);
          if ($postClicks == '') {
            $postClicks = 0;
          }
          if ($post->post_status == 'draft') {
            $postcomment = get_post_meta($post->ID, '_inactivecomment', true);
            $postreason = get_post_meta($post->ID, '_inactivereason', true);
            $postreasoncomment = get_post_meta($post->ID, '_inactivereasoncomment', true);
          } else if ($post->post_status == 'raderad') {
            $postcomment = get_post_meta($post->ID, '_deletecomment', true);
            $postreason = get_post_meta($post->ID, '_deletereason', true);
            $postreasoncomment = get_post_meta($post->ID, '_deletereasoncomment', true);
          } else {
            $postcomment = '';
            $postreason = '';
            $postreasoncomment = '';
          }
          $postStatus = $post->post_status;
          if ($postStatus === 'draft') {
            $postStatus = 'inaktiv';
          }
          echo '<tr><td>' . ($post->post_type == 'searchad' ? 'Sökes' : 'Uthyres' ). '</td>';
          echo '<td>' . $post->post_title . '</td>';
          echo '<td>' . $postStatus . '</td>';
          echo '<td>' . $post->post_date . '</td>';
          echo '<td>' . $post->post_modified . '</td>';
          echo '<td>' . get_post_meta($post->ID, '_area', true) . '</td>';
          $contractType = [];
          $accommodationType = [];
          if($post->post_type == 'searchad'){
            $contractTypeArr = explode(',',get_post_meta($post->ID, '_contractType', true));
            foreach ($contractTypeArr as $val) {
              switch ($val) {
                case 'contractsecond':
                  $contractType[] = 'Andrahands';
                  break;
                case 'contractfirst':
                  $contractType[] = 'Förstahands';
                  break;
                case 'contractchange':
                  $contractType[] = 'Bytes';
                  break;
              }
            }
            $accommodationTypeArr = explode(',',get_post_meta($post->ID, '_accommodationType', true));
            foreach ($accommodationTypeArr as $val) {
              switch ($val) {
                case 'roominhouse':
                  $accommodationType[] = 'Rum i hus';
                  break;
                case 'roominapartment':
                  $accommodationType[] = 'Rum i lägenhet';
                  break;
                case 'roominstudent':
                  $accommodationType[] = 'Rum i studentlägenhet';
                  break;
                case 'ownhouse':
                  $accommodationType[] = 'Hus';
                  break;
                case 'ownapartment':
                  $accommodationType[] = 'Lägenhet';
                  break;
              }
            }
          }
          else {
            $contractType[] = get_post_meta($post->ID, '_contractsecond', true) == 'checked' ? 'Andrahands' : '';
            $contractType[] = get_post_meta($post->ID, '_contractfirst', true) == 'checked' ? 'Förstahands' : '';
            $contractType[] = get_post_meta($post->ID, '_contractchange', true) == 'checked' ? 'Bytes' : '';
            $accommodationType[] = get_post_meta($post->ID, '_roominhouse', true) == 'checked' ? 'Rum i hus' : '';
            $accommodationType[] = get_post_meta($post->ID, '_roominapartment', true) == 'checked' ? 'Rum i lägenhet' : '';
            $accommodationType[] = get_post_meta($post->ID, '_roominstudent', true) == 'checked' ? 'Rum i studentlägenhet' : '';
            $accommodationType[] = get_post_meta($post->ID, '_ownhouse', true) == 'checked' ? 'Hus' : '';
            $accommodationType[] = get_post_meta($post->ID, '_ownapartment', true) == 'checked' ? 'Lägenhet' : '';
          }

          echo '<td>' . implode(', ',array_filter($accommodationType)) . '</th>';
          echo '<td>' . implode(', ',array_filter($contractType)) . '</th>';
          echo '<td>' . $postClicks . '</td>';
          echo '<td>' . $postcomment . '</td>';
          if ($postreason == 'found_tenant') {
            echo '<td>X</td>';
          } else {
            echo '<td></td>';
          }
          if ($postreason == 'unavailable') {
            echo '<td>X</td>';
          } else {
            echo '<td></td>';
          }
          if ($postreason == 'other') {
            echo '<td>X</td>';
          } else {
            echo '<td></td>';
          }
          echo '<td>' . $postreasoncomment . '</td>';
          echo '</tr>';
        }
        ?>
        </table>
        <br>
        <br>
        <br>
        <br>
        <table>
        <tr>
          <th>Antal&nbsp;publicerade&nbsp;sökes&nbsp;annonser</th>
          <th>Antal&nbsp;publicerade&nbsp;hyres&nbsp;annonser</th>
          <th>Antal&nbsp;inaktiverade/raderade</th>
          <th>Antal&nbsp;hittat&nbsp;hyresg&#228;st</th>
          <th>Antal&nbsp;vill&nbsp;inte&nbsp;l&#228;ngre&nbsp;hyra&nbsp;ut</th>
          <th>Antal&nbsp;annan&nbsp;anledning</th>
        </tr>
        <tr>
          <?php
          $nolandlordAds = $noads - $notenantAds - count_ads_inactiveErased();
          echo '<td>' . $notenantAds . '</td>';
          echo '<td>' . $nolandlordAds . '</td>';
          echo '<td>' . $nopostinactive . '</td>';
          echo '<td>' . $nopostfoundtenant . '</td>';
          echo '<td>' . $nopostunavailable . '</td>';
          echo '<td>' . $nopostother . '</td>';
          ?>
        </tr>
      </table>
    </div>

  </div>

  <?php
  }
}

<?php
/*
Controller name: Posts
Controller description: Data manipulation methods for posts
*/

class JSON_API_Posts_Controller {

  public function create_post() {
    global $json_api;
    if (!current_user_can('edit_posts')) {
      $json_api->error("You need to login with a user that has 'edit_posts' capacity.");
    }
    if (!$json_api->query->nonce) {
      $json_api->error("You must include a 'nonce' value to create posts. Use the `get_nonce` Core API method.");
    }
    $nonce_id = $json_api->get_nonce_id('posts', 'create_post');
    if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
      $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
    }
    nocache_headers();
    $post = new JSON_API_Post();
    $id = $post->create($_REQUEST);
    if (empty($id)) {
      $json_api->error("Could not create post.");
    }
    return array(
      'post' => $post
    );
  }

  public function update_post() {
    global $json_api;
    $post = $json_api->introspector->get_current_post();
    if (empty($post)) {
      $json_api->error("Post not found.");
    }
    if (!current_user_can('edit_post', $post->ID)) {
      $json_api->error("You need to login with a user that has the 'edit_post' capacity for that post.");
    }
    if (!$json_api->query->nonce) {
      $json_api->error("You must include a 'nonce' value to update posts. Use the `get_nonce` Core API method.");
    }
    $nonce_id = $json_api->get_nonce_id('posts', 'update_post');
    if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
      $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
    }
    nocache_headers();
    $post = new JSON_API_Post($post);
    $post->update($_REQUEST);
    return array(
      'post' => $post
    );
  }

  public function delete_post() {
    global $json_api;
    $post = $json_api->introspector->get_current_post();
    if (empty($post)) {
      $json_api->error("Post not found.");
    }
    if (!current_user_can('edit_post', $post->ID)) {
      $json_api->error("You need to login with a user that has the 'edit_post' capacity for that post.");
    }
    if (!current_user_can('delete_posts')) {
      $json_api->error("You need to login with a user that has the 'delete_posts' capacity.");
    }
    if ($post->post_author != get_current_user_id() && !current_user_can('delete_other_posts')) {
      $json_api->error("You need to login with a user that has the 'delete_other_posts' capacity.");
    }
    if (!$json_api->query->nonce) {
      $json_api->error("You must include a 'nonce' value to update posts. Use the `get_nonce` Core API method.");
    }
    $nonce_id = $json_api->get_nonce_id('posts', 'delete_post');
    if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
      $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
    }
    nocache_headers();
    wp_delete_post($post->ID);
    return array();
  }



  public function upload_images() {
    global $json_api;
    $url = parse_url($_SERVER['REQUEST_URI']);
    $defaults = array();
    $query = wp_parse_args($url['query']);

    $files = [];
    for ($i=0; $i < count($_FILES['file']['name']); $i++) {
      $files[] = [];
      foreach ($_FILES['file'] as $key => $value) {
        $files[$i][$key] = $_FILES['file'][$key][$i];
      }
    }
    // var_dump($_FILES);
    require_once( ABSPATH . 'wp-admin/includes/admin.php' );
    $ids = [];
    foreach( $files as $file ) {
      // var_dump($file);
      $file_return = wp_handle_upload( $file, array('test_form' => false ) );
      // var_dump($file_return);
      if( isset( $file_return['error'] ) || isset( $file_return['upload_error_handler'] ) ) {
          return false;
      } else {
          // Get the path to the upload directory.
          $wp_upload_dir = wp_upload_dir();
          // var_dump($file_return);
          $filename = $file_return['file'];

          $attachment = array(
              'post_mime_type' => $file_return['type'],
              'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
              
              //Added this to distinguish between the user uploaded images via Dropzone.js and thos via the admin dashboard
              'post_content' => 'user_uploaded',
              
              'post_status' => 'inherit',
              'guid' => $wp_upload_dir['url'] . '/' . basename( $filename ),
          );
          $attachment_id = wp_insert_attachment( $attachment, $wp_upload_dir['url'] . '/' . basename( $filename ) );
          $image = imagecreatefromstring(file_get_contents($file_return['url']));
          $exif = exif_read_data($file_return['file']);
          if(!empty($exif['Orientation'])) {
            switch($exif['Orientation']) {
              case 8:
                $image = imagerotate($image,90,0);
                break;
              case 3:
                $image = imagerotate($image,180,0);
                break;
              case 6:
                $image = imagerotate($image,-90,0);
                break;
            }

            imagejpeg($image, $file_return['file']);
          }

        // $attachment_data = wp_generate_attachment_metadata( $attachment_id, $filename );
          // wp_update_attachment_metadata( $attachment_id, $attachment_data );
          // if( 0 < intval( $attachment_id ) ) {
          // 	return $attachment_id;
          // }

        $ids[] = $attachment_id;
      }
    }

    return implode(',',$ids);

  }
}

?>

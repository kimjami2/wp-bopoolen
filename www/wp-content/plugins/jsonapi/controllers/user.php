<?php
/*
Controller name: User
Controller description: Create/Update/Delete user data
*/

class JSON_API_User_Controller {

  function login_user() {
    global $json_api;
    require_once(ABSPATH.'wp-admin/includes/user.php' );

    $url = parse_url($_SERVER['REQUEST_URI']);
    $defaults = array();
    $query = wp_parse_args($url['query']);

    $username       = $query['username'];
    $password       = $query['password'];

    $login_user = wp_login( $username, $password, '');
    // login user
    if ( ! is_wp_error( $login_user ) ) {
      return $login_user;
    } else {
      return $login_user;
    }
  }

  function create_user() {
    global $json_api;
    $url = parse_url($_SERVER['REQUEST_URI']);
    $defaults = array();
    $query = wp_parse_args($url['query']);

    $user_login       = $query['user_login'];
    $user_pass        = $query['user_pass'];
    $user_email       = $query['user_email'];
    $first_name       = $query['first_name'];
    $last_name        = $query['last_name'];
    $_securitynumber  = $query['_securitynumber'];
    $_phone           = $query['_phone'];
    $_username        = $query['_username'];
    $_viewname        = $query['_viewname'];
    $_viewphone        = $query['_viewphone'];
    $_profileimage     = $query['_profilepicture'];
    $_no_social_number = $query['_no_social_number'];
    $description       = $query['description'];

    $userdata = array(
      'user_login'	 =>	$user_login,
      'user_pass'	   =>	$user_pass,
      'user_email'	 =>	$user_email,
      'first_name'	 =>	$first_name,
      'last_name'	   =>	$last_name,
    );

    if(count(get_user_by_meta_data("_securitynumber", $_securitynumber)) >= 1) {
      if($_no_social_number !== 'true') {
        return 'SecurityTaken';
      }
    }

    if (isset($user_login) && !empty($user_login) && isset($user_pass) && !empty($user_pass) && isset($user_email) && !empty($user_email) && isset($first_name) && !empty($first_name) && isset($last_name) && !empty($last_name) && !empty($_username) && isset($_phone) && !empty($_phone)) {
      // create new user
      if(isset($_no_social_number) && $_no_social_number == 'true' || isset($_securitynumber) && !empty($_securitynumber)) {
        $create_user = wp_insert_user( $userdata );
        if ( ! is_wp_error( $create_user ) ) {
          update_user_meta( $create_user,'_securitynumber', sanitize_text_field( $_securitynumber ) );
          update_user_meta( $create_user,'_phone', sanitize_text_field( $_phone ) );
          update_user_meta( $create_user,'_username', sanitize_text_field( $_username ) );
          update_user_meta( $create_user,'_viewname', sanitize_text_field( $_viewname ) );
          update_user_meta( $create_user,'_viewphone', sanitize_text_field( $_viewphone ) );
          update_user_meta( $create_user,'_profilepicture', sanitize_text_field( $_profileimage) );
          update_user_meta( $create_user, 'description', sanitize_text_field( $description) );
          return true;
        } else {
          return $create_user;
        }
      } else {
        return 'Missing';
      }
    } else {
      return 'Missing';
    }
  }

  function update_user() {
    global $json_api;
    $url = parse_url($_SERVER['REQUEST_URI']);
    $defaults = array();
    $query = wp_parse_args($url['query']);

    $user_id          = get_current_user_id();
    $user_pass        = $query['user_pass'];
    $user_email       = $query['user_email'];
    $first_name       = $query['first_name'];
    $last_name        = $query['last_name'];
    $_securitynumber  = $query['_securitynumber'];
    $_phone           = $query['_phone'];
    $_username        = $query['_username'];
    $_viewname        = $query['_viewname'];
    $_viewphone       = $query['_viewphone'];

    $userdata = array(
      'ID' => $user_id,
      'user_pass'	=>	$user_pass,
      'user_email'	=>	$user_email,
      'first_name'	=>	$first_name,
      'last_name'	=>	$last_name,
    );

    // update user
    $update_user = wp_update_user( $userdata ) ;
    if ( ! is_wp_error( $update_user ) ) {
      update_user_meta($user_id, '_securitynumber', $_securitynumber);
      update_user_meta($user_id, '_phone', $_phone);
      update_user_meta($user_id, '_viewname', $_viewname);
      update_user_meta($user_id, '_viewphone', $_viewphone);
      update_user_meta($user_id, '_username', $_username);
      return true;
    } else {
      return $update_user;
    }
  }

  function update_user_meta() {
    global $json_api;
    $url = parse_url($_SERVER['REQUEST_URI']);
    $defaults = array();
    $query = wp_parse_args($url['query']);

    $user_id          = get_current_user_id();
    $_subscription    = $query['_subscription'];
    $_function        = $query['_function'];
    $_mailed          = $query['_emailed'];
    $_emailsub        = $query['_emailsub'];

    // update user
    $metaSub = get_user_meta($user_id, '_subscription', true);
    $metaMail = get_user_meta($user_id, '_mailed', true);
    $metaMailSub = get_user_meta($user_id, '_mailsub', true);

    if($_emailsub !== null) {

      //multiselect-all/lund/malm�/helsingborg/outside-lund/multiselect-all/contractsecond/contractfirst/contractchange/

      if($_emailsub == '') {
        delete_user_meta($user_id, '_emailsub');
        return true;
      }
      update_user_meta($user_id, '_emailsub', $_emailsub);
      return true;
    }
    if($_function == 'add') {
      if($_subscription !== null) {
        $_subscription = $_subscription.'/';
        $meta = $metaSub;

        $meta = explode('/', $meta);

        $meta = array_search($query['_subscription'], $meta);
        if($meta === false) {
          $meta = get_user_meta($user_id, '_subscription', true);
          $_subscription = $meta.$_subscription;

          update_user_meta($user_id, '_subscription', $_subscription);
          return true;
        }
        return true;
      } else {
        $_mailed = $_mailed.'/';
        $meta = $metaMail;

        $meta = explode('/', $meta);

        $meta = array_search($query['_emailed'], $meta);
        if($meta === false) {
          $meta = get_user_meta($user_id, '_mailed', true);
          $_subscription = $meta.$_mailed;

          update_user_meta($user_id, '_mailed', $_subscription);
          return true;
        }
        return true;
      }

    } else if($_function == 'remove') {
      if($_subscription !== null) {
        $_subscription = $_subscription.'/';
        $meta = $metaSub;

        $meta = explode($_subscription, $meta);
        $meta = $meta[0].$meta[1];

        update_user_meta($user_id, '_subscription', $meta);
        return true;
      } else {
        $_mailed = $_mailed.'/';
        $meta = $metaMail;

        $meta = explode($_mailed, $meta);
        $meta = $meta[0].$meta[1];

        update_user_meta($user_id, '_mailed', $meta);
        return true;
      }

    }
    return $meta;
  }

  function delete_user() {
    global $json_api;
    require_once(ABSPATH.'wp-admin/includes/user.php' );
    $current_user = wp_get_current_user();

    // delete user
    $delete_user = wp_delete_user( $current_user->ID );
    if ( ! is_wp_error( $delete_user ) ) {
      return $delete_user;
    } else {
      return $delete_user;
    }
  }


  function check_security_number_exists(){
    global $json_api;
    $url = parse_url($_SERVER['REQUEST_URI']);
    $defaults = array();
    $query = wp_parse_args($url['query']);

    $user = get_user_by_meta_data("_securitynumber", $query['nbr']);
    if($user && intval($user->data->ID) != get_current_user_id()) {
      return true;
    }
    else {
      return false;
    }

  }



}

?>
